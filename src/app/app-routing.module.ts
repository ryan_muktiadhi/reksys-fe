import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuardGuard } from './guard/guard.guard';
import { BackmenulayoutComponent } from './layout/backmenulayout/backmenulayout.component';
import { FullmenulayoutComponent } from './layout/fullmenulayout/fullmenulayout.component';
import { MainmenulayoutComponent } from './layout/mainmenulayout/mainmenulayout.component';
import { NomenulayoutComponent } from './layout/nomenulayout/nomenulayout.component';
import { ErrorpageComponent } from './pages/errorpage/errorpage.component';
import { HomeComponent } from './pages/home/home.component';
import { HomeadminComponent } from './pages/homeadmin/homeadmin.component';
import { KomihomeComponent } from './pages/komi/komihome/komihome.component';
import { KomiinterceptComponent } from './pages/komi/komihome/komiintercept.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotpasswordComponent } from './pages/forgotpassword/forgotpassword.component';
import { ApplicationdetailComponent } from './pages/komi/applicationgroup/applicationdetail/applicationdetail.component';
import { ApplicationgroupComponent } from './pages/komi/applicationgroup/applicationgroup.component';
import { ApplicationsComponent } from './pages/root/applications/applications.component';
import { EventlogsComponent } from './pages/root/eventlogs/eventlogs.component';
import { OauthsettingsComponent } from './pages/root/oauthsettings/oauthsettings.component';
import { ResourceusageComponent } from './pages/root/resourceusage/resourceusage.component';
import { SmtpaccountsComponent } from './pages/root/smtpaccounts/smtpaccounts.component';
import { UserdetailComponent } from './pages/komi/users/userdetail/userdetail.component';
import { UsersComponent } from './pages/komi/users/users.component';
import { BicadministrationComponent } from './pages/komi/bicadministration/bicadministration.component';
import { ProxymaintenanceComponent } from './pages/komi/proxymaintenance/proxymaintenance.component';
import { TransactionmonitorComponent } from './pages/komi/transactionmonitor/transactionmonitor.component';
import { TransactionreportComponent } from './pages/komi/transactionreport/transactionreport.component';
import { BicadmindetailComponent } from './pages/komi/bicadministration/bicadmindetail/bicadmindetail.component';
import { ProxymaintenancedetailComponent } from './pages/komi/proxymaintenance/proxymaintenancedetail/proxymaintenancedetail.component';
import { SystemparamComponent } from './pages/komi/systemparam/systemparam.component';
import { SystemparamdetailComponent } from './pages/komi/systemparam/systemparamdetail/systemparamdetail.component';
import { AliasproxyComponent } from './pages/komi/aliasproxy/aliasproxy.component';
import { AliasproxydetailComponent } from './pages/komi/aliasproxy/aliasproxydetail/aliasproxydetail.component';
import { NetworkmanageComponent } from './pages/komi/networkmanage/networkmanage.component';
import { ChanneltypeComponent } from './pages/komi/channeltype/channeltype.component';
import { ChanneltypedetailComponent } from './pages/komi/channeltype/channeltypedetail/channeltypedetail.component';
import { ProxytypeComponent } from './pages/komi/proxytype/proxytype.component';
import { ProxytypedetailComponent } from './pages/komi/proxytype/proxytypedetail/proxytypedetail.component';
import { TransactioncostComponent } from './pages/komi/transactioncost/transactioncost.component';
import { TransactioncostdetailComponent } from './pages/komi/transactioncost/transactioncostdetail/transactioncostdetail.component';
import { BranchComponent } from './pages/komi/branch/branch.component';
import { BranchdetailComponent } from './pages/komi/branch/branchdetail/branchdetail.component';
import { CityComponent } from './pages/komi/city/city.component';
import { LimitComponent } from './pages/komi/limit/limit.component';
import { LimitdetailComponent } from './pages/komi/limit/limitdetail/limitdetail.component';
import { MappingAccountTypeComponent } from './pages/komi/mapping-account-type/mapping-account-type.component';
import { MappingAccountTypeDetailComponent } from './pages/komi/mapping-account-type/mapping-account-type-detail/mapping-account-type-detail.component';
import { MappingIdTypeComponent } from './pages/komi/mapping-id-type/mapping-id-type.component';
import { MappingIdTypeDetailComponent } from './pages/komi/mapping-id-type/mapping-id-type-detail/mapping-id-type-detail.component';
import { MappingresidentComponent } from './pages/komi/mappingresident/mappingresident.component';
import { MappingresidentdetailComponent } from './pages/komi/mappingresident/mappingresidentdetail/mappingresidentdetail.component';
import { MappingCustomerTypeComponent } from './pages/komi/mapping-customer-type/mapping-customer-type.component';
import { MappingCustomerTypeDetailComponent } from './pages/komi/mapping-customer-type/mapping-customer-type-detail/mapping-customer-type-detail.component';
import { PrefundmanagerComponent } from './pages/komi/prefundmanager/prefundmanager.component';
import { PrefundmanagerdetailComponent } from './pages/komi/prefundmanager/prefundmanagerdetail/prefundmanagerdetail.component';
import { PrefundManageDashboardDetailComponent } from './pages/komi/prefund-manager-dashboard/prefund-manage-dashboard-detail/prefund-manage-dashboard-detail.component';
import { PrefundManagerDashboardComponent } from './pages/komi/prefund-manager-dashboard/prefund-manager-dashboard.component';
import { LogmonitorComponent } from './pages/komi/logmonitor/logmonitor.component';
import { ActionlogComponent } from './pages/komi/actionlog/actionlog.component';
import { SystemlogComponent } from './pages/komi/systemlog/systemlog.component';
import { EventLogComponent } from './pages/komi/event-log/event-log.component';
import { SysParamComponent } from './pages/komi/sys-param/sys-param.component';
import { SysParamDetailComponent } from './pages/komi/sys-param/sys-param-detail/sys-param-detail.component';
import { ProfileComponent } from './pages/root/profile/profile.component';
import { ResetpasswordComponent } from './pages/forgotpassword/resetpassword/resetpassword.component';
import { SmtpconfigComponent } from './pages/komi/smtpconfig/smtpconfig.component';
import { AdminnotificationComponent } from './pages/komi/adminnotification/adminnotification.component';
import { AdminnotifdetailComponent } from './pages/komi/adminnotification/adminnotifdetail/adminnotifdetail.component';
import { VerifikasiemailComponent } from './pages/forgotpassword/verifikasiemail/verifikasiemail.component';
import { SystemLogInboundComponent } from './pages/komi/system-log-inbound/system-log-inbound.component';
import { SystemLogOutboundComponent } from './pages/komi/system-log-outbound/system-log-outbound.component';
import { ConnectionChannelComponent } from './pages/komi/connection-channel/connection-channel.component';
import { ConnectionChannelDetailComponent } from './pages/komi/connection-channel/connection-channel-detail/connection-channel-detail.component';
import { TransactionmonparamComponent } from './pages/komi/transactionmonparam/transactionmonparam.component';
import { ProxyhistoryComponent } from './pages/komi/proxyhistory/proxyhistory.component';
import { UimloginComponent } from './pages/uimlogin/uimlogin.component';
import { TransactionhkComponent } from './pages/komi/transactionhk/transactionhk.component';
import { ProxysummaryComponent } from './pages/komi/proxysummary/proxysummary.component';

import { RekonsiliasiComponent } from './pages/komi/rekonsiliasi/rekonsiliasi.component';
import { ProxylistComponent } from './pages/komi/proxylist/proxylist.component';
import { ReconJobComponent } from './pages/komi/recon-job/recon-job.component';
import { ReconResultsComponent } from './pages/komi/recon-results/recon-results.component';
import { ReconJobDetailsComponent } from './pages/komi/recon-job/recon-job-details/recon-job-details.component';
import { RekonsiliasiDetailComponent } from './pages/komi/rekonsiliasi/rekonsiliasi-detail/rekonsiliasi-detail.component';
import { ReconResultDetailComponent } from './pages/komi/recon-results/recon-result-detail/recon-result-detail.component';
import { ReconformatregComponent } from './pages/komi/reconformatreg/reconformatreg.component';
import { ReconformatregdetailComponent } from './pages/komi/reconformatreg/reconformatregdetail/reconformatregdetail.component';
import { ReconScheduleComponent } from './pages/komi/recon-schedule/recon-schedule.component';
import { ReconScheduledetailComponent } from './pages/komi/recon-schedule/recon-scheduledetail/recon-scheduledetail.component';
const routes: Routes = [
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  {
    path: ':config',
    canActivate: [GuardGuard],
    component: KomiinterceptComponent,
  },
  {
    path: ':config',
    canActivate: [GuardGuard],
    component: KomiinterceptComponent,
  },
  {
    path: 'mgm',
    canActivate: [GuardGuard],
    component: MainmenulayoutComponent,
    children: [
      { path: 'home', component: KomihomeComponent },
      { path: 'profile', component: ProfileComponent },
      {
        path: 'user',
        canActivate: [GuardGuard],
        children: [
          { path: 'userslist', component: UsersComponent },
          {
            path: 'userslist',
            children: [
              { path: 'detail', component: UserdetailComponent },
              { path: 'detail#/:id', component: UserdetailComponent },
            ],
          },
        ],
      },
      {
        path: 'acl',
        canActivate: [GuardGuard],
        children: [
          { path: 'grouplist', component: ApplicationgroupComponent },
          {
            path: 'grouplist',
            children: [
              { path: 'detail', component: ApplicationdetailComponent },
              { path: 'detail#/:id', component: ApplicationdetailComponent },
            ],
          },
        ],
      },
      {
        path: 'system',
        canActivate: [GuardGuard],
        children: [
          { path: 'sysparam', component: SysParamComponent },
          {
            path: 'sysparam',
            children: [
              { path: 'detail', component: SysParamDetailComponent },
              { path: 'detail#/:id', component: SysParamDetailComponent },
            ],
          },
        ],
      },
      {
        path: 'settings',
        canActivate: [GuardGuard],
        children: [
          { path: 'bicadminlist', component: BicadministrationComponent },
          {
            path: 'bicadminlist',
            children: [
              { path: 'detail', component: BicadmindetailComponent },
              { path: 'detail#/:id', component: BicadmindetailComponent },
            ],
          },
          { path: 'sysparamslist', component: SystemparamComponent },
          {
            path: 'sysparamslist',
            children: [
              { path: 'detail', component: SystemparamdetailComponent },
              { path: 'detail#/:id', component: SystemparamdetailComponent },
            ],
          },
          { path: 'netmgm', component: NetworkmanageComponent },
          {
            path: 'proxymaintenancelist',
            component: ProxymaintenanceComponent,
          },
          {
            path: 'proxymaintenancelist',
            children: [
              { path: 'detail', component: ProxymaintenancedetailComponent },
              {
                path: 'detail#/:id',
                component: ProxymaintenancedetailComponent,
              },
            ],
          },
          { path: 'trxcostlist', component: TransactioncostComponent },
          {
            path: 'trxcostlist',
            children: [
              { path: 'detail', component: TransactioncostdetailComponent },
              {
                path: 'detail#/:id',
                component: TransactioncostdetailComponent,
              },
            ],
          },
          {
            path: 'prefunddashboard',
            component: PrefundManagerDashboardComponent,
          },
          {
            path: 'prefunddashboard',
            children: [
              {
                path: 'detail',
                component: PrefundManageDashboardDetailComponent,
              },
              {
                path: 'detail#/:id',
                component: PrefundManageDashboardDetailComponent,
              },
            ],
          },
          { path: 'prefundmgmlist', component: PrefundmanagerComponent },
          {
            path: 'prefundmgmlist',
            children: [
              { path: 'detail', component: PrefundmanagerdetailComponent },
              { path: 'detail#/:id', component: PrefundmanagerdetailComponent },
            ],
          },
          { path: 'proxyaliaslist', component: AliasproxyComponent },
          {
            path: 'proxyaliaslist',
            children: [
              { path: 'detail', component: AliasproxydetailComponent },
              { path: 'detail#/:id', component: AliasproxydetailComponent },
            ],
          },
          { path: 'branchlist', component: BranchComponent },
          {
            path: 'branchlist',
            children: [
              { path: 'detail', component: BranchdetailComponent },
              { path: 'detail#/:id', component: BranchdetailComponent },
            ],
          },
          { path: 'smtpconfig', component: SmtpconfigComponent },
        ],
      },
      {
        path: 'master',
        canActivate: [GuardGuard],
        children: [
          { path: 'channeltypelist', component: ChanneltypeComponent },
          {
            path: 'channeltypelist',
            children: [
              { path: 'detail', component: ChanneltypedetailComponent },
              { path: 'detail#/:id', component: ChanneltypedetailComponent },
            ],
          },
          { path: 'proxytypelist', component: ProxytypeComponent },
          {
            path: 'proxytypelist',
            children: [
              { path: 'detail', component: ProxytypedetailComponent },
              { path: 'detail#/:id', component: ProxytypedetailComponent },
            ],
          },
          { path: 'limitlist', component: LimitComponent },
          {
            path: 'limitlist',
            children: [
              { path: 'detail', component: LimitdetailComponent },
              { path: 'detail#/:id', component: LimitdetailComponent },
            ],
          },
          { path: 'accounttype', component: MappingAccountTypeComponent },
          {
            path: 'accounttype',
            children: [
              { path: 'detail', component: MappingAccountTypeDetailComponent },
              {
                path: 'detail#/:id',
                component: MappingAccountTypeDetailComponent,
              },
            ],
          },
          { path: 'customertype', component: MappingCustomerTypeComponent },
          {
            path: 'customertype',
            children: [
              { path: 'detail', component: MappingCustomerTypeDetailComponent },
              {
                path: 'detail#/:id',
                component: MappingCustomerTypeDetailComponent,
              },
            ],
          },
          { path: 'idtype', component: MappingIdTypeComponent },
          {
            path: 'idtype',
            children: [
              { path: 'detail', component: MappingIdTypeDetailComponent },
              { path: 'detail#/:id', component: MappingIdTypeDetailComponent },
            ],
          },
          { path: 'resident', component: MappingresidentComponent },
          {
            path: 'resident',
            children: [
              { path: 'detail', component: MappingresidentdetailComponent },
              {
                path: 'detail#/:id',
                component: MappingresidentdetailComponent,
              },
            ],
          },
          {
            path: 'adminnotificationlist',
            component: AdminnotificationComponent,
          },
          {
            path: 'adminnotificationlist',
            children: [
              { path: 'detail', component: AdminnotifdetailComponent },
              {
                path: 'detail#/:id',
                component: AdminnotifdetailComponent,
              },
            ],
          },
          {
            path: 'connectionchannellist',
            component: ConnectionChannelComponent,
          },
          {
            path: 'connectionchannellist',
            children: [
              { path: 'detail', component: ConnectionChannelDetailComponent },
              {
                path: 'detail#/:id',
                component: ConnectionChannelDetailComponent,
              },
            ],
          },

          { path: 'citylist', component: CityComponent },
        ],
      },
      {
        path: 'monitor',
        canActivate: [GuardGuard],
        children: [
          {
            path: 'transmonitoringlist',
            component: TransactionmonitorComponent,
          },
          {
            path: 'transmonitoringlist',
            children: [
              { path: 'detail', component: TransactionmonitorComponent },
              { path: 'detail#/:id', component: TransactionmonitorComponent },
            ],
          },
          {
            path: 'transmonitoringparam',
            component: TransactionmonparamComponent,
          },
          {
            path: 'transmonitoringhk',
            component: TransactionhkComponent,
          },
          {
            path: 'logmonitor',
            component: LogmonitorComponent,
          },
          {
            path: 'actionevent',
            component: ActionlogComponent,
          },
          {
            path: 'systemevent',
            component: SystemlogComponent,
          },
          {
            path: 'inboundlogsystem',
            component: SystemLogInboundComponent,
          },
          {
            path: 'outboundlogsystem',
            component: SystemLogOutboundComponent,
          },
          {
            path: 'eventlog',
            component: EventLogComponent,
          },
          {
            path: 'proxylist',
            component: ProxylistComponent,
          },
          { path: 'proxyhistory', component: ProxyhistoryComponent },
        ],
      },
      {
        path: 'report',
        canActivate: [GuardGuard],
        children: [
          { path: 'transaction', component: TransactionreportComponent },
          {
            path: 'transaction',
            children: [
              { path: 'detail', component: TransactionreportComponent },
              { path: 'detail#/:id', component: TransactionreportComponent },
            ],
          },
          { path: 'proxysummary', component: ProxysummaryComponent },
        ],
      },
      {
        path: 'rekonsiliasi',
        canActivate: [GuardGuard],
        children: [
          { path: 'formatregistration', component: ReconformatregComponent },
          {
            path: 'formatregistration',
            children: [
              { path: 'detail', component: ReconformatregdetailComponent },
              { path: 'detail#/:id', component: ReconformatregdetailComponent },
            ],
          },
          { path: 'schedulerlist', component: ReconScheduleComponent },
          {
            path: 'schedulerlist',
            children: [
              { path: 'detail', component: ReconScheduledetailComponent },
              { path: 'detail#/:id', component: ReconScheduledetailComponent },
            ],
          },
          { path: 'rekonsiliasi', component: RekonsiliasiComponent },
          {
            path: 'rekonsiliasi',
            children: [
              { path: 'upload', component: RekonsiliasiDetailComponent },
              { path: 'upload#/:id', component: RekonsiliasiDetailComponent },
            ],
          },
          { path: 'reconjob', component: ReconJobComponent },
          {
            path: 'reconjob',
            children: [
              { path: 'detail', component: ReconJobDetailsComponent },
              { path: 'detail#/:id', component: ReconJobDetailsComponent },
            ],
          },
          { path: 'reconresults', component: ReconResultsComponent },
          {
            path: 'reconresults',
            children: [
              { path: 'detail', component: ReconResultDetailComponent },
              { path: 'detail#/:id', component: ReconResultDetailComponent },
            ],
          },
          {
            path: 'rekonsiliasi',
            children: [
              { path: 'templatefile', component: TransactionmonitorComponent },
            ],
          },
        ],
      },
    ],
  },
  {
    path: 'nopage',
    component: BackmenulayoutComponent,
    children: [{ path: '404', component: ErrorpageComponent }],
  },
  {
    path: 'auth',
    data: { title: 'Login' },
    component: NomenulayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'uimlogin', component: UimloginComponent },
      { path: 'forgotpassword', component: ForgotpasswordComponent },
      { path: 'resetpassword/:id', component: ResetpasswordComponent },
      { path: 'verifikasiemail/:id', component: VerifikasiemailComponent },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
