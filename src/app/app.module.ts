import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {
  HashLocationStrategy,
  LocationStrategy,
  PathLocationStrategy,
} from '@angular/common';
import { AppComponent } from './app.component';
import { MessageService } from 'primeng/api';
import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { MenubarModule } from 'primeng/menubar';
import {MenuModule} from 'primeng/menu';
import { PanelMenuModule } from 'primeng/panelmenu';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { CheckboxModule } from 'primeng/checkbox';
import {
  DynamicDialogModule,
  DialogService,
  DynamicDialogRef,
  DynamicDialogConfig,
} from 'primeng/dynamicdialog';
import { DividerModule } from 'primeng/divider';
import { MainmenulayoutComponent } from './layout/mainmenulayout/mainmenulayout.component';
import { NomenulayoutComponent } from './layout/nomenulayout/nomenulayout.component';
import { HomeadminComponent } from './pages/homeadmin/homeadmin.component';
import { HomeComponent } from './pages/home/home.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import { TabViewModule } from 'primeng/tabview';
import { BlockUIModule } from 'primeng/blockui';
import { SelectButtonModule } from 'primeng/selectbutton';
import {RadioButtonModule} from 'primeng/radiobutton';
import { StepsModule } from 'primeng/steps';
import { InputNumberModule } from 'primeng/inputnumber';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { PickListModule } from 'primeng/picklist';
import { TooltipModule } from 'primeng/tooltip';
import { ListboxModule } from 'primeng/listbox';
import { OrderListModule } from 'primeng/orderlist';
import { CalendarModule } from 'primeng/calendar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { NgxGaugeModule } from 'ngx-gauge';
import { ChipModule } from 'primeng/chip';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { AccordionModule } from 'primeng/accordion';
import { ToastModule } from 'primeng/toast';
import { ProgressBarModule } from 'primeng/progressbar';
import { KeyFilterModule } from 'primeng/keyfilter';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ApplicationgroupComponent } from './pages/komi/applicationgroup/applicationgroup.component';
import { UsersComponent } from './pages/komi/users/users.component';
import { ApplicationsComponent } from './pages/root/applications/applications.component';
import { SmtpaccountsComponent } from './pages/root/smtpaccounts/smtpaccounts.component';
import { OauthsettingsComponent } from './pages/root/oauthsettings/oauthsettings.component';
import { EventlogsComponent } from './pages/root/eventlogs/eventlogs.component';
import { ResourceusageComponent } from './pages/root/resourceusage/resourceusage.component';
import { ErrorpageComponent } from './pages/errorpage/errorpage.component';
import { BackmenulayoutComponent } from './layout/backmenulayout/backmenulayout.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotpasswordComponent } from './pages/forgotpassword/forgotpassword.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorHttpService } from './interceptors/interceptor-http.service';
import { FullmenulayoutComponent } from './layout/fullmenulayout/fullmenulayout.component';
import { TablehelperComponent } from './generic/tablehelper/tablehelper.component';
import { ApplicationdetailComponent } from './pages/komi/applicationgroup/applicationdetail/applicationdetail.component';
import { UserdetailComponent } from './pages/komi/users/userdetail/userdetail.component';
import { BicadministrationComponent } from './pages/komi/bicadministration/bicadministration.component';
import { ProxymaintenanceComponent } from './pages/komi/proxymaintenance/proxymaintenance.component';
import { TransactionmonitorComponent } from './pages/komi/transactionmonitor/transactionmonitor.component';
import { TransactionreportComponent } from './pages/komi/transactionreport/transactionreport.component';
import { EnvServiceProvider } from './env/env.service.provider';
import { KomihomeComponent } from './pages/komi/komihome/komihome.component';
import { KomiinterceptComponent } from './pages/komi/komihome/komiintercept.component';
import { BicadmindetailComponent } from './pages/komi/bicadministration/bicadmindetail/bicadmindetail.component';
import { ProxymaintenancedetailComponent } from './pages/komi/proxymaintenance/proxymaintenancedetail/proxymaintenancedetail.component';
import { SystemparamComponent } from './pages/komi/systemparam/systemparam.component';
import { SystemparamdetailComponent } from './pages/komi/systemparam/systemparamdetail/systemparamdetail.component';
import { AliasproxyComponent } from './pages/komi/aliasproxy/aliasproxy.component';
import { AliasproxydetailComponent } from './pages/komi/aliasproxy/aliasproxydetail/aliasproxydetail.component';
import { NetworkmanageComponent } from './pages/komi/networkmanage/networkmanage.component';
import { ChanneltypeComponent } from './pages/komi/channeltype/channeltype.component';
import { ChanneltypedetailComponent } from './pages/komi/channeltype/channeltypedetail/channeltypedetail.component';
import { ProxytypeComponent } from './pages/komi/proxytype/proxytype.component';
import { ProxytypedetailComponent } from './pages/komi/proxytype/proxytypedetail/proxytypedetail.component';
import { PrefundmanagerComponent } from './pages/komi/prefundmanager/prefundmanager.component';
import { PrefundmanagerdetailComponent } from './pages/komi/prefundmanager/prefundmanagerdetail/prefundmanagerdetail.component';
import { TransactioncostComponent } from './pages/komi/transactioncost/transactioncost.component';
import { TransactioncostdetailComponent } from './pages/komi/transactioncost/transactioncostdetail/transactioncostdetail.component';
import { BranchComponent } from './pages/komi/branch/branch.component';
import { BranchdetailComponent } from './pages/komi/branch/branchdetail/branchdetail.component';
import { LimitComponent } from './pages/komi/limit/limit.component';
import { LimitdetailComponent } from './pages/komi/limit/limitdetail/limitdetail.component';
import { MappingresidentComponent } from './pages/komi/mappingresident/mappingresident.component';
import { MappingresidentdetailComponent } from './pages/komi/mappingresident/mappingresidentdetail/mappingresidentdetail.component';
import { MappingIdTypeComponent } from './pages/komi/mapping-id-type/mapping-id-type.component';
import { MappingIdTypeDetailComponent } from './pages/komi/mapping-id-type/mapping-id-type-detail/mapping-id-type-detail.component';
import { MappingAccountTypeComponent } from './pages/komi/mapping-account-type/mapping-account-type.component';
import { MappingAccountTypeDetailComponent } from './pages/komi/mapping-account-type/mapping-account-type-detail/mapping-account-type-detail.component';
import { MappingCustomerTypeComponent } from './pages/komi/mapping-customer-type/mapping-customer-type.component';
import { MappingCustomerTypeDetailComponent } from './pages/komi/mapping-customer-type/mapping-customer-type-detail/mapping-customer-type-detail.component';
import { PrefundManagerDashboardComponent } from './pages/komi/prefund-manager-dashboard/prefund-manager-dashboard.component';
import { PrefundManageDashboardDetailComponent } from './pages/komi/prefund-manager-dashboard/prefund-manage-dashboard-detail/prefund-manage-dashboard-detail.component';
import { DatePipe } from '@angular/common';
import { CronEditorModule } from 'cron-editor';
import { LogmonitorComponent } from './pages/komi/logmonitor/logmonitor.component';
import { ActionlogComponent } from './pages/komi/actionlog/actionlog.component';
import { SystemlogComponent } from './pages/komi/systemlog/systemlog.component';
import { EventLogComponent } from './pages/komi/event-log/event-log.component';
import { SysParamComponent } from './pages/komi/sys-param/sys-param.component';
import { SysParamDetailComponent } from './pages/komi/sys-param/sys-param-detail/sys-param-detail.component';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { ProfileComponent } from './pages/root/profile/profile.component';
import { ChangePasswordComponent } from './pages/root/change-password/change-password.component';
import { ResetpasswordComponent } from './pages/forgotpassword/resetpassword/resetpassword.component';
import { SmtpconfigComponent } from './pages/komi/smtpconfig/smtpconfig.component';
import { LdapconfigComponent } from './pages/komi/ldapconfig/ldapconfig.component';
import { AdminnotificationComponent } from './pages/komi/adminnotification/adminnotification.component';
import { AdminnotifdetailComponent } from './pages/komi/adminnotification/adminnotifdetail/adminnotifdetail.component';

import { DailyChartComponent } from './pages/komi/komihome/component/daily-chart/daily-chart.component';

import { VerifikasiemailComponent } from './pages/forgotpassword/verifikasiemail/verifikasiemail.component';
import { SystemLogInboundComponent } from './pages/komi/system-log-inbound/system-log-inbound.component';
import { SystemLogOutboundComponent } from './pages/komi/system-log-outbound/system-log-outbound.component';
import { ConnectionChannelComponent } from './pages/komi/connection-channel/connection-channel.component';
import { ConnectionChannelDetailComponent } from './pages/komi/connection-channel/connection-channel-detail/connection-channel-detail.component';
import { BarChartComponent } from './pages/komi/komihome/component/bar-chart/bar-chart.component';

import { MultiSelectModule } from 'primeng/multiselect';
import { TransactionmonparamComponent } from './pages/komi/transactionmonparam/transactionmonparam.component';
import { ProxyhistoryComponent } from './pages/komi/proxyhistory/proxyhistory.component';
import { UimloginComponent } from './pages/uimlogin/uimlogin.component';
import { CityComponent } from './pages/komi/city/city.component';
import { TransactionhkComponent } from './pages/komi/transactionhk/transactionhk.component';
import { ProxysummaryComponent } from './pages/komi/proxysummary/proxysummary.component';
import { RekonsiliasiComponent } from './pages/komi/rekonsiliasi/rekonsiliasi.component';
import { FileUploadModule } from 'primeng/fileupload';
import { WeeklyChartComponent } from './pages/komi/komihome/component/weekly-chart/weekly-chart.component';
import { MonthlyChartComponent } from './pages/komi/komihome/component/monthly-chart/monthly-chart.component';
import { BussinessChartComponent } from './pages/komi/komihome/component/bussiness-chart/bussiness-chart.component';
import { ApporvalConfirmationComponent } from './generic/apporval-confirmation/apporval-confirmation.component';
import { WidgetsCardsComponent } from './generic/widgets-cards/widgets-cards.component';
import { ProxylistComponent } from './pages/komi/proxylist/proxylist.component';
import { ReconJobComponent } from './pages/komi/recon-job/recon-job.component';
import { ReconResultsComponent } from './pages/komi/recon-results/recon-results.component';
import { ReconJobDetailsComponent } from './pages/komi/recon-job/recon-job-details/recon-job-details.component';
import { RekonsiliasiDetailComponent } from './pages/komi/rekonsiliasi/rekonsiliasi-detail/rekonsiliasi-detail.component';
import { ReconResultDetailComponent } from './pages/komi/recon-results/recon-result-detail/recon-result-detail.component';
import { ReconformatregComponent } from './pages/komi/reconformatreg/reconformatreg.component';
import { ReconformatregdetailComponent } from './pages/komi/reconformatreg/reconformatregdetail/reconformatregdetail.component';
import { ReconScheduleComponent } from './pages/komi/recon-schedule/recon-schedule.component';
import { ReconScheduledetailComponent } from './pages/komi/recon-schedule/recon-scheduledetail/recon-scheduledetail.component';

@NgModule({
  declarations: [
    AppComponent,
    MainmenulayoutComponent,
    NomenulayoutComponent,
    HomeadminComponent,
    HomeComponent,
    ApplicationsComponent,
    UsersComponent,
    ApplicationsComponent,
    SmtpaccountsComponent,
    OauthsettingsComponent,
    EventlogsComponent,
    ResourceusageComponent,
    ErrorpageComponent,
    BackmenulayoutComponent,
    LoginComponent,
    ForgotpasswordComponent,
    FullmenulayoutComponent,
    TablehelperComponent,
    ApplicationdetailComponent,
    UserdetailComponent,
    BicadministrationComponent,
    ProxymaintenanceComponent,
    TransactionmonitorComponent,
    TransactionreportComponent,
    KomihomeComponent,
    KomiinterceptComponent,
    BicadmindetailComponent,
    ProxymaintenancedetailComponent,
    SystemparamComponent,
    SystemparamdetailComponent,
    AliasproxyComponent,
    AliasproxydetailComponent,
    NetworkmanageComponent,
    ChanneltypeComponent,
    ChanneltypedetailComponent,
    ProxytypeComponent,
    ProxytypedetailComponent,
    PrefundmanagerComponent,
    PrefundmanagerdetailComponent,
    TransactioncostComponent,
    TransactioncostdetailComponent,
    BranchComponent,
    BranchdetailComponent,
    LimitComponent,
    LimitdetailComponent,
    MappingresidentComponent,
    MappingresidentdetailComponent,
    MappingIdTypeComponent,
    MappingIdTypeDetailComponent,
    MappingAccountTypeComponent,
    MappingAccountTypeDetailComponent,
    MappingCustomerTypeComponent,
    MappingCustomerTypeDetailComponent,
    PrefundManagerDashboardComponent,
    PrefundManageDashboardDetailComponent,
    LogmonitorComponent,
    ActionlogComponent,
    SystemlogComponent,
    EventLogComponent,
    SysParamComponent,
    SysParamDetailComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ResetpasswordComponent,
    SmtpconfigComponent,
    LdapconfigComponent,
    AdminnotificationComponent,
    AdminnotifdetailComponent,
    ConnectionChannelComponent,

    ConnectionChannelDetailComponent,

    DailyChartComponent,
    BarChartComponent,

    VerifikasiemailComponent,
    SystemLogInboundComponent,
    SystemLogOutboundComponent,

    TransactionmonparamComponent,
    ProxyhistoryComponent,
    UimloginComponent,
    CityComponent,
    TransactionhkComponent,
    ProxysummaryComponent,
    ApplicationgroupComponent,
    RekonsiliasiComponent,
    WeeklyChartComponent,
    MonthlyChartComponent,
    BussinessChartComponent,
    ApporvalConfirmationComponent,
    WidgetsCardsComponent,
    ProxylistComponent,
    ReconJobComponent,
    ReconResultsComponent,
    ReconJobDetailsComponent,
    RekonsiliasiDetailComponent,
    ReconResultDetailComponent,
    ReconformatregComponent,
    ReconformatregdetailComponent,
    ReconScheduleComponent,
    ReconScheduledetailComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FileUploadModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    ToolbarModule,
    ButtonModule,
    InputTextModule,
    SplitButtonModule,
    MenubarModule,
    PanelMenuModule,
    MenuModule,
    BreadcrumbModule,
    DropdownModule,
    ChartModule,
    BlockUIModule,
    DividerModule,
    ProgressSpinnerModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    DynamicDialogModule,
    PanelModule,
    TableModule,
    SelectButtonModule,
    RadioButtonModule,
    StepsModule,
    InputNumberModule,
    AutoCompleteModule,
    PickListModule,
    ListboxModule,
    OrderListModule,
    CheckboxModule,
    CalendarModule,
    ChipModule,
    NgxGaugeModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    TooltipModule,
    MultiSelectModule,
    KeyFilterModule,
    AccordionModule,
    ProgressBarModule,
    TabViewModule,
    InputSwitchModule,
    CronEditorModule
  ],

  // import { SelectButtonModule } from 'primeng/selectbutton';
  // import { StepsModule } from 'primeng/steps';
  // import { InputNumberModule } from 'primeng/inputnumber';
  // import { AutoCompleteModule } from 'primeng/autocomplete';

  providers: [
    EnvServiceProvider,
    DynamicDialogRef,
    DynamicDialogConfig,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorHttpService,
      multi: true,
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    DialogService,
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
