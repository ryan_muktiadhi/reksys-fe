import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  constructor() { }
  public apiUrl = "http://localhost:3000/";
  public baseUrl = "http://localhost:3013/";
}
