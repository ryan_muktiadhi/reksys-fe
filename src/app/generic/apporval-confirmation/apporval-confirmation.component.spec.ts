import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApporvalConfirmationComponent } from './apporval-confirmation.component';

describe('ApporvalConfirmationComponent', () => {
  let component: ApporvalConfirmationComponent;
  let fixture: ComponentFixture<ApporvalConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApporvalConfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApporvalConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
