import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-apporval-confirmation',
  templateUrl: './apporval-confirmation.component.html',
  styleUrls: ['./apporval-confirmation.component.scss'],
})
export class ApporvalConfirmationComponent implements OnChanges, OnInit {
  constructor() {}
  ngOnInit(): void {}
  @Input() oldData: any;
  @Input() newData: any;
  filterOldData: any = [];
  actionLabel: any;
  action: any;
  loading: boolean = false;

  async ngOnChanges(changes: SimpleChanges) {
    console.log(this.oldData);
    console.log(this.newData);

    this.filterOldData = [];
    this.loading = true;
    if (this.oldData) {
      if (this.newData && !this.newData.module) {
        console.log('sorting');
        await this.newData.sort(function (a, b) {
          return a.label.localeCompare(b.label);
        });
        await this.oldData.map((oldItem) => {
          let idx = this.newData.findIndex((newitem) => {
            return oldItem.label == newitem.label;
          });
          if (idx > -1) {
            this.filterOldData.push(oldItem);
          }
        });
        await this.filterOldData.sort(function (a, b) {
          return a.label.localeCompare(b.label);
        });
      } else {
        this.filterOldData = this.oldData;
        console.log(this.filterOldData);
      }
      await this.getAction(this.oldData);
      this.loading = false;
    }
  }

  async loadData() {}

  async getAction(data: any) {
    //console.log(data);
    data.map((dt) => {
      if (dt.label == 'status') {
        this.action = dt.statusCode;
        switch (dt.statusCode) {
          case 3:
            this.actionLabel = 'Add Action';
            break;
          case 5:
          case 7:
            this.actionLabel = 'Edit Action';
            break;
          case 9:
            this.actionLabel = 'Delete Action';
            break;
          default:
            this.actionLabel = '';
            break;
        }
        //console.log(this.action);
      }
    });
  }
}
