import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-tablehelper',
  templateUrl: './tablehelper.component.html',
  styleUrls: ['./tablehelper.component.scss'],
})
export class TablehelperComponent implements OnInit {
  isActionBtn = true;
  // records:any[] = [];
  @ViewChild('dt')
  dt!: Table;
  first = 0;
  rows = 5;
  selectedColumns: any[];
  showFilter = false;

  @Input() records: any;
  @Input() wsearch: any;
  @Input() header: any;
  @Input() colnames: any;
  @Input() colwidth: any;
  @Input() colclasshalign: any;
  @Input() colmark: any;
  @Input() collink: any;
  @Input() collinkaction: any;
  @Input() actionbtn: any;
  @Input() addbtnlink: any;
  @Input() nopaging: any = true;
  @Input() reloadButton: any = false;
  @Input() defaultColumn: any = false;
  @Input() scrollheight: any = '300px';
  @Input() exportCSVButton: any = false;
  @Output() datadeleted = new EventEmitter<any>();
  @Output() datapreview = new EventEmitter<any>();
  @Output() dataPopUp = new EventEmitter<any>();
  @Output() editPopUp = new EventEmitter<any>();
  @Output() dataapprover = new EventEmitter<any>();
  @Output() changePage1 = new EventEmitter<any>();
  @Output() changePage2 = new EventEmitter<any>();
  @Output() download = new EventEmitter<any>();
  @Output() reload = new EventEmitter<any>();
  @Output() exportCSV = new EventEmitter<any>();
  cols: any[] = [];
  haveActionBtn: any;
  constructor() {}

  ngOnInit(): void {
    this.header.map((head) => {
      let data: any = {};
      data.field = head.sort;
      data.header = head.label;
      data.isOrder = true;
      this.cols.push(data);
    });

    /* Set init selectedColumns */
    console.log("DEFAULT COLOMNYA =>>>>>>>>>>",this.defaultColumn);
    let column = this.defaultColumn || 4;
    this.selectedColumns = this.cols.filter((c, index) => index < column);

    let i = 0;
    this.actionbtn.map((dt: any) => {
      if (dt > 0) i++;
    });

    if (i < 1) {
      this.isActionBtn = false;
    }

    if (
      this.records[0]?.created_date === 'No records' ||
      this.records[0]?.created_date === 'N/A'
    )
      this.isActionBtn = false;

    this.haveActionBtn =
      this.actionbtn[2] == 1 ||
      this.actionbtn[3] == 1 ||
      this.actionbtn[4] == 1 ||
      this.actionbtn[5] == 1 ||
      this.actionbtn[6] == 1 ||
      this.actionbtn[7] == 1 ||
      this.actionbtn[8] == 1 ||
      this.actionbtn[9] == 1;
  }

  deleteConfirmation(payload: any) {
    // console.log("Di Emit nih "+JSON.stringify(payload));
    this.datadeleted.emit(payload);
  }
  previewConfirmation(payload: any) {
    this.datapreview.emit(payload);
  }
  popUpModal(payload: any) {
    this.dataPopUp.emit(payload);
  }

  approveConfirmation(payload: any) {
    this.dataapprover.emit(payload);
  }

  PopUpEdit(payload: any) {
    this.editPopUp.emit(payload);
  }

  changePageEmit1(payload: any) {
    this.changePage1.emit(payload);
  }

  changePageEmit2(payload: any) {
    this.changePage2.emit(payload);
  }

  downloadEmit(payload: any) {
    this.download.emit(payload);
  }
  reloadEmit() {
    this.reload.emit();
  }
  exportCSVEmit() {
    this.exportCSV.emit();
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.records ? this.first === this.records.length - this.rows : true;
  }

  isFirstPage(): boolean {
    return this.records ? this.first === 0 : true;
  }
}
