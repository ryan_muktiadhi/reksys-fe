import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetsCardsComponent } from './widgets-cards.component';

describe('WidgetsCardsComponent', () => {
  let component: WidgetsCardsComponent;
  let fixture: ComponentFixture<WidgetsCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WidgetsCardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetsCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
