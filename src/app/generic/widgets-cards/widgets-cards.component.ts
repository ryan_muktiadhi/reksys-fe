import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-widgets-cards',
  templateUrl: './widgets-cards.component.html',
  styleUrls: ['./widgets-cards.component.scss'],
})
export class WidgetsCardsComponent implements OnChanges {
  @Input() widget: any;
  @Output() onClick = new EventEmitter<any>();
  constructor() {}
  ngOnChanges(): void {}
  selectWidget(payload: any) {
    console.log(this.widget);
    this.onClick.emit(payload);
    payload.value = !payload.value;
  }
}
