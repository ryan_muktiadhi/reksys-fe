import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { SessionStorageService } from 'angular-web-storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../loader/loader.service';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class InterceptorHttpService implements HttpInterceptor {
  constructor(
    public loaderService: LoaderService,
    private session: SessionStorageService,
    private authservice: AuthService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // if(req.method != 'GET' && !req.headers.has('Content-Type')){
    //   req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    //   req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
    // }

    if (!req.headers.has('Content-Type')) {
      req = req.clone({
        headers: req.headers.set('Content-Type', 'application/json'),
      });
      req = req.clone({
        headers: req.headers.set('Accept', 'application/json'),
      });
    } else {
      req = req.clone({ headers: req.headers.delete('Content-Type') });
    }

    //  req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    //  req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
    // console.log(">>>>>>>>>>>>>>>>>>>>>>> MASUK INTERCEPT NIH  "+ req.method)
    this.loaderService.isLoading.next(true);
    req = this.updateAccessToken(req);
    // req = this.updateAT(req);
    return next.handle(req).pipe(
      finalize(() => {
        this.loaderService.isLoading.next(false);
      })
    );
  }
  private updateAccessToken(req: HttpRequest<any>) {
    const authToken = this.session.get('accesstoken');
    if (authToken) {
      req = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${authToken}`),
      });
    }

    return req;
  }
  // private updateAT(req:HttpRequest<any>) : HttpRequest<any> {
  //     let authToken = "";
  //     this.authservice.sharedMessage.subscribe(message => authToken = message);

  //     if(authToken == ""){
  //       console.log(">>>> Token Kosong ");
  //     } else {
  //       req = req.clone({
  //         headers: req.headers.set("Authorization", `Bearer ${authToken}`)
  //       });
  //     }

  //     return req;

  // }
}
