import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import appadmin from 'src/app/_files/appadmin.json';
import { MessageService } from 'primeng/api';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
// import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
// import { BackendService } from 'src/app/services/backend.service';
import { SessionStorageService } from 'angular-web-storage';
import { BackendService } from 'src/app/services/backend.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-fullmenulayout',
  templateUrl: './fullmenulayout.component.html',
  styleUrls: ['./fullmenulayout.component.scss'],
})
export class FullmenulayoutComponent implements OnInit {
  items!: MenuItem[];
  sidemenus!: MenuItem[];
  username: string = 'Unknown';
  companytitle: string = 'Unknown';
  display: boolean = false;
  isProcess: boolean = false;
  // authToken = "";
  jsonUserInfo: any = {};
  ngOnInit(): void {
    console.log('###########APP COMPONENT Admin #############');
    this.isProcess = true;
    this.authservice.whoAmi().subscribe((value) => {
      this.jsonUserInfo = value;
      this.username = this.jsonUserInfo.data.fullname;
      this.companytitle = this.jsonUserInfo.data.tnname;
      this.items = [
        {
          label: this.username,
          icon: 'pi pi-fw pi-user',
          items: [
            {
              label: 'My Account',
              icon: 'pi pi-user-edit',
            },
            {
              label: '',
              separator: true,
            },
            {
              label: 'Sign out',
              icon: 'pi pi-fw pi-sign-out',
              command: () => {
                // this.delete();
                this.showDialog();
              },
            },
          ],
        },
        {
          label: 'Help',
          icon: 'pi pi-fw pi-info-circle',
        },
      ];
      let lvlTn = parseInt(this.jsonUserInfo.data.leveltenant);
      console.log('>> Level tenant ' + this.jsonUserInfo.data.leveltenant);
      console.log(this.jsonUserInfo);
      if (lvlTn == 0) {
        this.sidemenus = appadmin;
      }
      this.isProcess = false;
    });
  }
  title = 'krakatoa';
  constructor(
    private messageService: MessageService,
    private sessionStorage: SessionStorageService,
    private route: Router,
    private formBuilder: FormBuilder,
    private backend: BackendService,
    private authservice: AuthService,
    private router: Router
  ) {}
  showDialog() {
    this.display = true;
  }
  signOut() {
    this.display = false;
    this.isProcess = true;
    console.log('Ini harusnya keluar');

    this.backend
      .post('adm/auth/signout', { appid: 7, appname: 'recs' }, false)
      .subscribe(
        (data: BackendResponse) => {
          // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
          this.isProcess = false;
          if (data.status === 200) {
            this.authservice.loggedOut();
            this.router.navigate(['/auth/login']);
          } else {
          }
        },
        (error) => {}
      );

    // this.isProcess= true
  }
}
