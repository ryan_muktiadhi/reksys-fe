import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainmenulayoutComponent } from './mainmenulayout.component';

describe('MainmenulayoutComponent', () => {
  let component: MainmenulayoutComponent;
  let fixture: ComponentFixture<MainmenulayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainmenulayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainmenulayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
