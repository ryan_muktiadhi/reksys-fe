import { Component, OnInit } from '@angular/core';
import { MegaMenuItem, MenuItem } from 'primeng/api';
import appadmin from 'src/app/_files/appadmin.json';
import { MessageService } from 'primeng/api';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
// import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
// import { BackendService } from 'src/app/services/backend.service';
import { SessionStorageService } from 'angular-web-storage';
import { BackendService } from 'src/app/services/backend.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { TooltipModule } from 'primeng/tooltip';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';
@Component({
  selector: 'app-mainmenulayout',
  templateUrl: './mainmenulayout.component.html',
  styleUrls: ['./mainmenulayout.component.scss'],
})
export class MainmenulayoutComponent implements OnInit {
  title: string = 'Unknown';
  items!: MenuItem[];
  testing!:any[];
  sidemenus!: MenuItem[];
 
  username: string = 'Unknown';
  companytitle: string = 'Unknown';
  display: boolean = false;
  isProcess: boolean = false;
  changePasswordDisplay: boolean = false;
  passwordMessage: string = '';
  sidebar: boolean = true;
  isSmallmenu:boolean = true;
  smallMenus:any[] = [];
  // itemsSmallMenu:any[] =[];
  itemsSmallMenu:{ [id: string]: any; } = {}
  // authToken = "";
  jsonUserInfo: any = {};

  // testmenu: MenuItem[];



  ngOnInit(): void {
    console.log('###########APP COMPONENT#############');
    this.title = this.routeA.snapshot.data['title'];
    this.isProcess = true;
    this.authservice.whoAmi().subscribe(async (value) => {
      this.jsonUserInfo = value;
      // console.log(">> INSIDE Main Menu "+JSON.stringify(this.jsonUserInfo));
      // INSIDE Main Menu {"status":200,"data":{"id":"1","fullname":"KKTCIMB","userid":"kktcimb@cimb.co.id","idtenant":"1","idsubtenant":"0","leveltenant":"0","tnname":"PT Bank CIMB","tnstatus":1,"tnflag":1,"tnparentid":0,"cdtenant":"9624","bioemailactive":"kktcimb@cimb.co.id","biophoneactive":"5554444","bioaddress":"Kantor Pusat","bionik":"3400342324342","bionpwp":null,"bioidtipenik":1,"bioidcorel":3,"orgid":0,"appscount":3,"apps":[{"id":"1","expiredate":"2022-05-23","id_application":"1","paidstatus":"1","active":1,"defaultactive":null,"appname":"default","applabel":"Default","description":"Krakatoa Administer"},{"id":"2","expiredate":"2022-05-24","id_application":"2","paidstatus":"1","active":1,"defaultactive":null,"appname":"vam","applabel":"Virtual account manager","description":"This application for maintained and monitoring Virtual Account Management purposed. And this is general operation form Banking Needed "},{"id":"3","expiredate":"2021-07-13","id_application":"3","paidstatus":"1","active":1,"defaultactive":null,"appname":"crm","applabel":"Bank CRM","description":"This contain all modules for Customer Relationship Management purpose on front end banking, and it can be customed as the users needed"}],"sidemenus":[],"iat":1629743103}}
      this.username = this.jsonUserInfo.data.fullname;
      this.companytitle = this.jsonUserInfo.data.tnname;
      let lastLogin = new Date(
        this.jsonUserInfo.data.last_login
      ).toLocaleDateString('en-US', {
        day: 'numeric',
        month: 'long',
        year: 'numeric',
      });

      this.items = [
        {
          label: this.username,
          icon: 'pi pi-fw pi-user',
          items: [
            {
              label: 'My Account',
              icon: 'pi pi-user-edit',
              command: () => {
                this.toProfile();
              },
            },
            {
              label: '',
              separator: true,
            },
            {
              label: 'Sign out',
              icon: 'pi pi-fw pi-sign-out',
              command: () => {
                // this.delete();
                this.showDialog();
              },
            },
          ],
        },
        {
          label: `Last Login : ${lastLogin}`,
        },
        // {
        //   label: 'Help',
        //   icon: 'pi pi-fw pi-info-circle',
        // },
      ];


     this.testing = [
        {label:"Formatq Registration",icon:"pi pi-file-o",routerLink:"/mgm/rekonsiliasi/formatregistration",acl:{create:1,read:1,update:1,delete:1,view:1,approval:0}},
        {label:"Registration",icon:"pi pi-file-o",routerLink:"/mgm/rekonsiliasi/reconjob",acl:{create:1,read:1,update:1,delete:1,view:1,approval:0}}
      ]








      let lvlTn = parseInt(this.jsonUserInfo.data.leveltenant);

      // console.log(this.jsonUserInfo);
      if (this.jsonUserInfo?.data.notif?.changePassword) {
        this.passwordMessage = this.jsonUserInfo?.data.notif?.changePassword;
        this.changePasswordDisplay = true;
      }

      if (lvlTn == 0) {
        var home = {
          label: 'Dashboard',
          icon: 'pi pi-fw pi-home',
          routerLink: '/mgm/home',
        };
        this.sidemenus.push(home);

        this.sidemenus.push(appadmin);
      } else {
        let home: MenuItem = {
          label: 'Dashboard',
          icon: 'pi pi-fw pi-home',
          routerLink: '/mgm/home',
        };
        
        this.sidemenus = this.jsonUserInfo.data.sidemenus;
        this.sidemenus.unshift(home);
      }
      console.log("Modules element ", this.sidemenus);

       this.smallMenus = [];
      await this.sidemenus.map((modulelabels) =>{
        // console.log("Modules element ", JSON.stringify(modulelabels));

        let objSmalMenus = {};
        
        if(modulelabels.items){
          let itms = [];
          // let itemMenuObj = {label:"Format Registration",routerLink:"/mgm/rekonsiliasi/formatregistration"}
          itms.push(modulelabels.items);
          objSmalMenus = {label:modulelabels.label,icon:modulelabels.icon, ispop:true};
          // console.log("ITEM MENUNYA ",modulelabels.items);
          // let itemsmall =[];itemsmall.push(modulelabels.items);
          this.itemsSmallMenu[modulelabels.label] = modulelabels.items;
          // ,items:[[{label:modulelabels.label, items:modulelabels.items}]]
          this.smallMenus.push(objSmalMenus);

        } else {
          objSmalMenus = {label:modulelabels.label,icon:modulelabels.icon,routerLink:modulelabels.routerLink, ispop:false};
          this.smallMenus.push(objSmalMenus);
        }
      });




      this.isProcess = false;
      console.log("MenusMega >> 2 " + JSON.stringify(this.smallMenus));
      // console.log("Childrennya >> 2 " + JSON.stringify(this.itemsSmallMenu));





    });
  }
  toProfile() {
    console.log('profile');
    this.router.navigate(['/mgm/profile']);
  }

  toogleSmallMenu(data:any){
    console.log("Diclieck eventnya ", data);
    console.log("Childrennya >> " + JSON.stringify(this.itemsSmallMenu[data]));
  }


  getUsername() {
    this.username = this.jsonUserInfo.data.fullname;
    return this.username;
  }

  constructor(
    private messageService: MessageService,
    private sessionStorage: SessionStorageService,
    private routeA: ActivatedRoute,
    private route: Router,
    private formBuilder: FormBuilder,
    private backend: BackendService,
    private authservice: AuthService,
    private router: Router
  ) {}
  showDialog() {
    this.display = true;
  }
  signOut() {
    this.display = false;
    this.isProcess = true;
    let payload = { appid: 7, appname: 'recs' };
    console.log('Ini harusnya keluar >> ' + JSON.stringify(payload));

    this.backend.post('adm/auth/signout', payload, false).subscribe(
      (data: BackendResponse) => {
        // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
        this.isProcess = false;
        if (data.status === 200) {
          this.authservice.loggedOut();
          this.router.navigate(['/auth/login']);
        } else {
        }
      },
      (error) => {}
    );

    // this.isProcess= true
  }
}
