import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NomenulayoutComponent } from './nomenulayout.component';

describe('NomenulayoutComponent', () => {
  let component: NomenulayoutComponent;
  let fixture: ComponentFixture<NomenulayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NomenulayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NomenulayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
