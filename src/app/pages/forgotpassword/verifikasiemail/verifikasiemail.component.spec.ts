import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifikasiemailComponent } from './verifikasiemail.component';

describe('VerifikasiemailComponent', () => {
  let component: VerifikasiemailComponent;
  let fixture: ComponentFixture<VerifikasiemailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerifikasiemailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifikasiemailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
