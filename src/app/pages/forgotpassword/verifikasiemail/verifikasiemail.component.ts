import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { BackendService } from 'src/app/services/backend.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { Location } from '@angular/common';
import { ForgotpasswordService } from 'src/app/services/forgotpassword/forgotpassword.service';

@Component({
  selector: 'app-verifikasiemail',
  templateUrl: './verifikasiemail.component.html',
  styleUrls: ['./verifikasiemail.component.scss'],
})
export class VerifikasiemailComponent implements OnInit {
  blockedDocument: boolean = false;
  password = '';
  confirmPassword = '';
  errorMsg = '';
  isProcess: boolean = false;
  userForm!: FormGroup;
  submitted = false;
  sama = false;
  isPasswordSame = true;
  isValidatePass = true;
  id = '';
  cek = '';
  constructor(
    private messageService: MessageService,
    private sessionStorage: SessionStorageService,
    private route: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private backend: BackendService,
    private authservice: AuthService,
    private forgotpasswordService: ForgotpasswordService,
    private location: Location
  ) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group(
      {
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required],
      },
      { validator: this.checkPassword('password', 'confirmPassword') }
    );
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.userForm.controls;
  }
  get userFormControl() {
    return this.userForm.controls;
  }

  checkPassword(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }
      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
        this.isPasswordSame = matchingControl.status == 'VALID' ? true : false;
      }
    };
  }

  onSubmit(data: any) {
    this.submitted = true;

    this.cek = this.userForm.controls.confirmPassword.status;
    // console.log('this.userForm', this.userForm.controls.confirmPassword.status);
    console.log('this.userForm', this.userForm);
    if (this.cek == 'INVALID') {
      this.submitted = true;
    } else {
      this.activatedRoute.params.subscribe((paramsId) => {
        this.id = paramsId.id;
      });

      let payload;
      payload = {
        password: this.userForm.get('password')?.value,
        confirmPassword: this.userForm.get('confirmPassword')?.value,
        id: this.id,
      };
      // console.log('payload ::', payload);

      this.forgotpasswordService
        .verifikasiemail(payload)
        .subscribe((resp: BackendResponse) => {
          console.log(resp);
          if (resp.status === 200) {
            this.showTopCenterInfo('Password has beed changed');
            setTimeout(() => {
              this.route.navigate(['/auth/login']);
            }, 4000);
          } else if (resp.status === 201) {
            this.showTopCenterErr('Link Expried');
            setTimeout(() => {
              this.route.navigate(['/auth/login']);
            }, 4000);
          } else if (resp.status === 422) {
            console.log('cek');
            const validateControl = this.userForm.controls.password;
            validateControl.setErrors({ validatePass: true });
            this.isValidatePass =
              validateControl.status == 'VALID' ? true : false;
          }
        });
    }

    //  this.blockDocument();
  }
  showTopCenterInfo(message: string) {
    this.messageService.add({
      severity: 'info',
      summary: 'Confirmed',
      detail: message,
    });
  }

  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }

  blockDocument() {
    this.blockedDocument = true;
    //  setTimeout(() => {
    //      this.blockedDocument = false;
    //      this.showTopCenterErr("Invalid user and password!")
    //  }, 3000);
  }
}
