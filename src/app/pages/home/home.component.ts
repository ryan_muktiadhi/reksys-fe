import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  appsByLiscense:any[] = [];
  display: boolean = false;
  userInfo:any = {};
  appIdSelected:string = "0";
  appLabelSelected:string = "unknown";
  appLabelRouteLink:string = "unknown";
  tokenID:string = "";
  constructor(private authservice:AuthService) { }

  ngOnInit(): void {
      this.authservice.whoAmi().subscribe((value) =>{
        console.log(">>> User Info : "+JSON.stringify(value));
          this.userInfo = value.data;
          this.tokenID = value.tokenId
          if(this.userInfo.apps.length > 0){
            this.appsByLiscense = this.userInfo.apps;
          }

          


      });
  }

  showDialog(payload:any) {
    // this.appIdSelected = this.userInfo.leveltenant == "0"?  payload.id_application:payload.idapp;
    console.log(">>>>>>> "+JSON.stringify(payload));
    this.appIdSelected = payload.id_application;
    this.appLabelSelected = payload.applabel;
    this.appLabelRouteLink=payload.routelink;
    // console.log(">>>>>>> Payload "+this.appIdSelected);
    // this.comparentchildservice.publish('call-parent', payload);
     this.display = true;
  }
  switchApp(){
    this.display = false;
    var payloadNumber: number = +this.appIdSelected;
    window.open(this.appLabelRouteLink+"/"+this.tokenID);
  }
}
