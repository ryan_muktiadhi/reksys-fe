import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { FormsModule } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-homeadmin',
  templateUrl: './homeadmin.component.html',
  styleUrls: ['./homeadmin.component.scss'],
})
export class HomeadminComponent implements OnInit {
  basicData: any;
  basicOptions: any;
  breadcrumbs!: MenuItem[];
  userInfo: any = {};
  app: any = {};
  applicationAvailable: number = 0;
  tokenID: string = '';
  home!: MenuItem;
  typeStrings!: TypeString[];
  selectedType!: TypeString;
  constructor(private authservice: AuthService) {}
  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/' };
    this.typeStrings = [
      { id: '1', label: 'In a Month' },
      { id: '2', label: 'In a Year' },
    ];
    this.selectedType = { id: '1', label: 'In a Month' };
    this.authservice.whoAmi().subscribe((value) => {
      console.log('>>> User Info : ' + JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.applicationAvailable = this.userInfo.appscount - 1;
      this.app = this.userInfo.apps[0];
    });
    //   this.basicData = {
    //     labels: [5, 6, 7, 8, 9, 10, 11,12,13,14,15,16,17,18,19,20,21,22,23],
    //     datasets: [
    //         {
    //             label: 'Logged In Users',
    //             data: [5, 2, 1, 0, 9, 10, 1,2,10,0,1,7,7,8,1,0,2,2,3],
    //             fill: false,
    //             borderColor: '#42A5F5',
    //             tension: .4
    //         },
    //     ]
    // };
  }
}

export interface TypeString {
  id?: string;
  label?: string;
}
