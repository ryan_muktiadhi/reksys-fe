import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { AdminnotificationService } from 'src/app/services/komi/adminnotification.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';

@Component({
  selector: 'app-adminnotifdetail',
  templateUrl: './adminnotifdetail.component.html',
  styleUrls: ['./adminnotifdetail.component.scss'],
})
export class AdminnotifdetailComponent implements OnInit {
  extraInfo: any = {};
  inputform: any = {};
  userlist: any[] = [];
  isEdit: boolean = false;
  recordid: any = 0;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userInfo: any = {};
  selectedModules: any = [];
  tokenID: string = '';
  groupname: any = '';
  submitted = false;
  user: any;
  group: any;
  emailname: any;
  fullname: any;
  apps: any = [];
  aclData: any = {
    read: 0,
    create: 0,
    update: 0,
    delete: 0,
    view: 0,
  };
  listnotifysource: any = [];
  listnotifysourcetmp: any = [];
  listnotifydest: any = [];
  // listmenubymodules:any = [];
  appNotSelected = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private router: Router,
    private location: Location,
    private formBuilder: FormBuilder,
    private adminnotificationService: AdminnotificationService,
    private activatedRoute: ActivatedRoute,
    private bussinesparamService: BussinesparamService,
    private usermanagerService: UsermanagerService,
    private admNotService: AdminnotificationService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/' };
    this.breadcrumbs = [
      {
        label: 'Admin Notification',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.initData();
  }

  getSelectedUser() {
    console.log('>>>>>>>>>> ON Edit DATA  getSelectedUser');
    this.apps = this.userInfo.apps;
    // console.log(">>>>>>>>>> Route active "+this.activatedRoute.snapshot.paramMap.get('id'));
    if (this.activatedRoute.snapshot.paramMap.get('id')) {
      let grpid = this.activatedRoute.snapshot.paramMap.get('id');
      this.recordid = grpid;
      this.admNotService.getById(grpid).subscribe((res) => {
        console.log(res);
        let data = res.data;
        this.group = data[0].devisi;
        this.groupname = this.group;
        this.emailname = data[0].email_user;
        this.fullname = data[0].fullname;

        var selectedUser = this.userlist.filter((obj) => {
          return obj.id == data[0].iduser;
        });
        // var selectedUser = data[0].iduser;

        console.log(selectedUser);
        if (selectedUser.length > 0) {
          console.log(JSON.stringify(selectedUser));
          this.user = selectedUser[0];
        }
        let filterType = [];
        this.listnotifydest = this.listnotifysource.filter((ar) =>
          res.data.find((rm) => rm.cd_busparam == ar.label)
        );
        this.listnotifysource = this.listnotifysource.filter(
          (ar) => !res.data.find((rm) => rm.cd_busparam == ar.label)
        );
      });
    }
  }

  getSelectedType() {
    console.log('>>>>>>>>>> ON Edit DATA  getSelectedType');
    this.apps = this.userInfo.apps;
    // console.log(">>>>>>>>>> Route active "+this.activatedRoute.snapshot.paramMap.get('id'));
    if (this.activatedRoute.snapshot.paramMap.get('id')) {
      let grpid = this.activatedRoute.snapshot.paramMap.get('id');

      this.admNotService.getById(grpid).subscribe((res) => {
        console.log(res);
        console.log(this.userlist);
        var selectedUser = this.userlist.filter((obj) => {
          return obj.id == grpid;
        });
        if (selectedUser.length > 0) {
          this.user = selectedUser[0];
          this.group = selectedUser[0].groupname;
        }
      });
    }
  }

  async initData() {
    this.authservice.whoAmi().subscribe(async (value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      // console.log(">>>> Userinfo : "+JSON.stringify(this.userInfo));

      this.apps = this.userInfo.apps;

      await this.bussinesparamService
        .getAllByCategory('NOTIFY_EMAIL_TYPE')
        .subscribe((result: BackendResponse) => {
          // console.log(result.data);
          this.listnotifysource = result.data;
          this.listnotifysourcetmp = [];
          // console.log(this.listnotifysource);
        });
      // console.log(this.listnotifydest);
      await this.usermanagerService.retriveUsers().subscribe((userresult) => {
        this.userlist = userresult.data;
        if (this.isEdit) {
          this.getSelectedUser();
        }
        // {"status":200,"data":[{"iduser":"3","fullname":"Admin Komi","userid":"adminkomi@bankmantap.co.id","active":1,"idgroupuseracl":null,"groupname":"Administrator","cd_busparam":"ERROR, INFO, SUCCESS","created_at":"2021-10-18 03:10:09"},{"iduser":"9","fullname":"User Komi Bank Mantap","userid":"userkomi@bankmantap.co.id","active":1,"idgroupuseracl":"2","groupname":"Admin Bank Mantap","cd_busparam":"ERROR","created_at":"2021-10-18 04:10:46"}]}
      });
    });
  }

  async onSubmit() {
    this.submitted = true;
    if (!this.isEdit) {
      // console.log(this.listnotifydest);

      let payload = {};

      if (this.user) {
        payload = {
          iduser: this.user.id,
          type: this.listnotifydest,
          fullname: this.fullname,
          emailname: this.emailname,
          group: this.group,
        };
      } else {
        payload = {
          iduser: 0,
          type: this.listnotifydest,
          fullname: this.fullname,
          emailname: this.emailname,
          group: this.group,
        };
      }
      console.log(JSON.stringify(payload));
      this.admNotService.addData(payload).subscribe((resp: BackendResponse) => {
        if (resp.status == 200) {
          this.location.back();
        }
      });
    } else {
      console.log(this.listnotifydest);

      let payload = {};

      if (this.user) {
        payload = {
          id: this.recordid,
          iduser: this.user.id,
          type: this.listnotifydest,
          fullname: this.fullname,
          emailname: this.emailname,
          group: this.group,
        };
      } else {
        payload = {
          id: this.recordid,
          iduser: 0,
          type: this.listnotifydest,
          fullname: this.fullname,
          emailname: this.emailname,
          group: this.group,
        };
      }

      console.log(JSON.stringify(payload));

      this.admNotService
        .editData(payload)
        .subscribe((resp: BackendResponse) => {
          if (resp.status == 200) {
            this.location.back();
          }
        });
    }
  }

  onSelectUserData(data: any) {
    console.log('onChange');
    console.log(JSON.stringify(data));
    if (data.value) {
      this.group = data.value.groupname;
      this.emailname = data.value.bioemailactive;
      this.fullname = data.value.fullname;
    } else {
      this.group = null;
      this.emailname = null;
      this.fullname = null;
    }
  }

  onCancel() {
    this.location.back();
  }
}
