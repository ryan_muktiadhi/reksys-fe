import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ApprovalService } from 'src/app/services/approval.service';
import { AuthService } from 'src/app/services/auth.service';
import { AdminnotificationService } from 'src/app/services/komi/adminnotification.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-adminnotification',
  templateUrl: './adminnotification.component.html',
  styleUrls: ['./adminnotification.component.scss'],
})
export class AdminnotificationComponent implements OnInit {
  display = false;
  viewApprove = false;
  displayPrv = false;
  selectedbic: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  admnotiflist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  newData: any;
  oldData: any;
  bicheader: any = [
    { label: 'Full Name', sort: 'fullname' },
    { label: 'Email', sort: 'email_user' },
    { label: 'Devisi', sort: 'devisi' },
    { label: 'Notifies For', sort: 'notif' },
    { label: 'Created Date', sort: 'created_at' },
    { label: 'Status', sort: 'active' },
  ];
  biccolname: any = [
    'fullname',
    'email_user',
    'devisi',
    'notif',
    'created_at',
    'active',
  ];
  biccolhalign: any = ['', '', 'p-text-center', '', '', 'p-text-center'];
  biccolwidth: any = ['', '', '', { width: '150px' }, '', { width: '150px' }];
  biccollinghref: any = { url: '#', label: 'Application' };
  bicactionbtn: any = [1, 1, 1, 1, 1, 1];
  bicaddbtn = { route: 'detail', label: 'Add Data' };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private admNotService: AdminnotificationService,
    private router: Router,
    private aclMenuService: AclmenucheckerService,
    private approvalService: ApprovalService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Admin Notification' }];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingUser();
  }
  refreshingUser() {
    console.log(this.router.url);
    this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
      var count = Object.keys(dataacl.acl).length;
      console.log(dataacl);
      if (count > 0) {
        this.bicactionbtn[0] = dataacl.acl.create;
        this.bicactionbtn[1] = dataacl.acl.read;
        this.bicactionbtn[2] = dataacl.acl.update;
        this.bicactionbtn[3] = dataacl.acl.delete;
        this.bicactionbtn[4] = dataacl.acl.view;
        this.bicactionbtn[5] = dataacl.acl.approval;
      }
    });

    this.isFetching = true;

    this.admNotService
      .getAllAdminNotification()
      .subscribe(async (admnotifs: BackendResponse) => {
        this.admnotiflist = admnotifs.data;

        if (this.admnotiflist.length < 1) {
          let objtmp = {
            iduser: 'No records',
            fullname: 'No records',
            userid: 'No records',
            active: 'No records',
            groupname: 'No records',
            cd_busparam: 'No records',
            created_at: 'No records',
          };
          this.admnotiflist = [];
          this.admnotiflist.push(objtmp);
        }
        this.isFetching = false;
      });
    // }
    // });
  }

  async approvalData(data: any) {
    console.log(data);
    this.viewApprove = true;
    this.selectedbic = data;
    this.newData = null;
    if (data.idapproval && data.active != 9) {
      console.log('new data');
      this.getApprovalData(data.idapproval);
    } else {
      let oldData = await this.setApprovalForm(this.selectedbic);
      oldData = await this.filterForm(oldData);
      this.oldData = oldData;
      this.newData = null;
    }
  }
  getApprovalData(id) {
    this.approvalService
      .getByid(id)
      .subscribe(async (resp: BackendResponse) => {
        let oldFormData = await this.setApprovalForm(this.selectedbic);
        let newData = await this.setApprovalForm(
          JSON.parse(resp.data[0].jsondata)
        );
        this.oldData = oldFormData;
        console.log(this.oldData);
        newData = await this.filterForm(newData);
        this.newData = newData;
        console.log(this.newData);
      });
  }
  async filterForm(formData) {
    let filter = ['fullname', 'notif', 'email', 'status'];
    let filterFormData = formData.filter((formItem) =>
      filter.includes(formItem.label)
    );

    return filterFormData;
  }
  async setApprovalForm(data) {
    const keys = Object.keys(data);
    const input = data;
    let arrData = [];

    if (keys.length > 0) {
      await keys.map(async (key: any) => {
        let data: any = {};
        key === 'active' || key === 'isactive'
          ? (data.label = 'status')
          : key === 'emailuser' || key === 'emailname' || key === 'email_user'
          ? (data.label = 'email')
          : key === 'type'
          ? (data.label = 'notif')
          : (data.label = key);
        if (data.label === 'status') {
          data.value =
            input[key] == 0
              ? 'Not Active'
              : input[key] == 1
              ? 'Active'
              : 'Waiting for Approval';
          data.statusCode = input[key];
        } else if (data.label === 'notif') {
          let value = [];
          await input[key].map((dt) => {
            value.push(dt.label || dt);
          });
          data.value = value;
        } else {
          data.value = input[key];
        }
        if (data.label != 'id' && data.label != 'userid') {
          arrData.push(data);
        }
      });
      return arrData;
    }
  }

  reject(status) {
    let payload = {
      id: this.selectedbic.id,
      oldactive: this.selectedbic.active,
      isactive: status,
      idapproval: this.selectedbic.idapproval,
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.admNotService.reject(payload).subscribe((result: BackendResponse) => {
      // console.log(">>>>>>>> return "+JSON.stringify(result));
      if (result.status === 200) {
        this.refreshingUser();
        this.viewApprove = false;
      }
    });
  }

  approvalSubmit(status) {
    console.log(this.selectedbic);
    // console.log(status);
    let payload = {
      id: this.selectedbic.iduser,
      oldactive: this.selectedbic.active,
      isactive: status,
      idapproval: this.selectedbic.idapproval,
    };
    console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.admNotService
      .updateApprovalStatus(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingUser();
          this.viewApprove = false;
        }
      });
  }

  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedbic = data;
  }

  async previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedbic = data;
    let oldData = await this.setApprovalForm(this.selectedbic);
    oldData = await this.filterForm(oldData);
    this.oldData = oldData;
    this.newData = null;
  }

  deleteBic() {
    console.log(this.selectedbic);
    let payload = {
      id: this.selectedbic.id,
      iduser: this.selectedbic.id,
    };
    this.admNotService
      .deleteData(payload)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingUser();
      });
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Request to Delete',
      detail: message,
    });
  }
}
