import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { Table } from 'primeng/table';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BranchService } from 'src/app/services/komi/branch.service';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { ProxytypeService } from 'src/app/services/komi/proxytype.service';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { element } from 'protractor';
@Component({
  selector: 'app-aliasproxy',
  templateUrl: './aliasproxy.component.html',
  styleUrls: ['./aliasproxy.component.scss'],
})
export class AliasproxyComponent implements OnInit {
  isdataexist = false;
  @ViewChild('dt')
  dt!: Table;
  first = 0;
  rows = 5;

  fileName = 'ExcelSheet.xlsx';

  rangeDates: Date[];

  branchOffices: any = [];
  branchSelected: any = {};
  channels: any = [];
  channelSelected: any = {};
  proxyaddrestype: any = [];
  proxyTypeSelected: any = [];

  display = false;
  displayPrv = false;
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';
  proxyData: any[] = [];
  // prxheader:any = [{'label':'Name', 'sort':'name'},{'label':'Acc Num', 'sort':'accnum'}, {'label':'Proxy Address', 'sort':'prxaddress'}, {'label':'Branch', 'sort':'branch'},{'label':'Channel', 'sort':'channel'},{'label':'Prx Type', 'sort':'prxtype'},{'label':'Sec ID', 'sort':'secid'},{'label':'Residence', 'sort':'custresidence'},{'label':'Status', 'sort':'stsprxadd'},{'label':'Action', 'sort':'stsprxadd'}];
  // prxcolname:any = ["name", "accnum", "prxaddress", "branch", "channel", "prxtype", "secid", "custresidence", "stsprxadd"]
  // prxcolhalign:any = ["p-text-center","p-text-center","","p-text-center","","","","","","p-text-center"]
  // prxcolwidth:any = ["","","","","","","","","",""];
  // prxcollinghref:any = {'url':'#','label':'Application'}
  // prxactionbtn:any = [0,0,0,0,0]
  // prxaddbtn ={'route':'detail', 'label':'Add Data'}
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private channeltypeService: ChanneltypeService,
    private branchService: BranchService,
    private proxytypeService: ProxytypeService,
    public datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Proxy/Alias Manager' }];
    this.channeltypeService
      .getAllChnByTenant()
      .subscribe((response: BackendResponse) => {
        if (response.status == 200) {
          this.channels = response.data;
        } else {
          this.channels = [];
        }
      });
    this.branchService
      .getAllBranchByTenant()
      .subscribe((response: BackendResponse) => {
        // console.log(JSON.stringify(response));
        if (response.status == 200) {
          console.log(response.data);
          this.branchOffices = response.data.Branchs;
        } else {
          this.branchOffices = [];
        }
      });
    this.proxytypeService
      .getAllPrxTByTenant()
      .subscribe((response: BackendResponse) => {
        console.log(JSON.stringify(response));
        if (response.status == 200) {
          console.log(response.data);
          this.proxyaddrestype = response.data;
        } else {
          this.proxyaddrestype = [];
        }
      });
    // this.proxyaddrestype=[{label:"Proxy I", value:1},{label:"Proxy II", value:2},{label:"Proxy III", value:3}];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh BIC ');
    this.proxyadminService
      .getAllProxyByTenant()
      .subscribe((result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.isdataexist = false;
          this.proxyData = [];
          // id, name, accnum, prxaddress, branch, channel, proxytype, secondid, custresident, proxystat, created_date, change_who, last_update_date, idtenant
          let objtmp = {
            name: 'N/A',
            accnum: 'N/A',
            prxaddress: 'N/A',
            branch: 'N/A',
            channel: 'N/A',
            proxytype: 'N/A',
            secondid: 'N/A',
            custresident: 'N/A',
            stsprxadd: 'N/A',
          };
          this.proxyData.push(objtmp);
        } else {
          this.isdataexist = true;
          this.proxyData = result.data.ProxyAdmins;
        }
        this.isFetching = false;
      });
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.proxyData
      ? this.first === this.proxyData.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.proxyData ? this.first === 0 : true;
  }

  refreshData() {
    this.isdataexist = false;
    this.proxyData = [];
    this.isFetching = true;
    console.log('Range date : ' + this.rangeDates);
    let start_date = null;
    let end_date = null;
    let payload: any = {};
    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    }
    // console.log("Range {"+start_date+"-"+end_date+"}");
    // console.log("Status : "+JSON.stringify(this.isactivesSelected));
    // console.log("Channel : "+JSON.stringify(this.channelSelected));
    // console.log("TransType : "+JSON.stringify(this.transactiontypeSelected));
    if (start_date) payload.start_date = start_date;
    if (end_date) payload.end_date = end_date;
    if (this.branchSelected) payload.branch = this.branchSelected.branchcode;
    if (this.channelSelected)
      payload.channel = this.channelSelected.channelcode;
    if (this.proxyTypeSelected)
      payload.prxtype = this.proxyTypeSelected.proxycode;

    console.log(JSON.stringify(payload));

    this.proxyadminService
      .getProxyByParam(payload)
      .subscribe((result: BackendResponse) => {
        console.log(JSON.stringify(result));
        if (result.status == 200) {
          this.isdataexist = true;
          this.proxyData = result.data;
        } else {
          this.isdataexist = false;
          this.proxyData = [];
          let objtmp = {
            name: 'N/A',
            accnum: 'N/A',
            prxaddress: 'N/A',
            branch: 'N/A',
            channel: 'N/A',
            proxytype: 'N/A',
            secondid: 'N/A',
            custresident: 'N/A',
            stsprxadd: 'N/A',
          };
          this.proxyData.push(objtmp);
        }
        this.isFetching = false;
      });
  }

  exportexcel(): void {
    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    this.fileName = 'xls' + datenow + '.xlsx';
    /* table id is passed over here */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }
  openPDF(): void {
    var doc = new jsPDF('l', 'mm', [297, 210]);
    var col = [
      [
        'Name',
        'Acc No',
        'Proxy Addr',
        'Branch',
        'Channel',
        'Proxy Type',
        'Second ID',
        'Cust Residence',
        'Proxy Status',
      ],
    ];
    var rows = [];
    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    this.fileName = 'pdf' + datenow + '.pdf';
    this.proxyData.forEach((element) => {
      var temp = [
        element.name,
        element.prxaddress,
        element.branch,
        element.channel,
        element.proxytype,
        element.secondid,
        element.custresident,
        element.proxystat,
      ];
      rows.push(temp);
    });
    // doc.autoTable(col, rows);
    autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (rows) => {},
    });

    doc.save(this.fileName);
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
