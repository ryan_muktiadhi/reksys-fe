import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { Table } from 'primeng/table';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BranchService } from 'src/app/services/komi/branch.service';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { ProxytypeService } from 'src/app/services/komi/proxytype.service';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { element } from 'protractor';
import { AliasproxyService } from '../../../services/komi/aliasproxy.service';
import { ConnectionchannelService } from '../../../services/komi/connectionchannel.service';

@Component({
  selector: 'app-aliasproxy',
  templateUrl: './aliasproxy.component.html',
  styleUrls: ['./aliasproxy.component.scss'],
})
export class AliasproxyComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  rangeDates: Date[];
  startDate: any = new Date();
  endDate: any = new Date();
  dateTime: any;
  dateNow: any;
  birthday: any;

  interEndDate: any;
  interStartDate: any;

  branchOffices: any = [];
  branchSelected: any = {};
  channels: any = [];
  channelSelected: any = {};
  proxyaddrestype: any = [];
  proxyTypeSelected: any = [];
  SecondIdSelected: any = [];
  secondIdType: any = [];

  display = false;
  displayPrv = false;

  isFetching = false;

  proxyData: any[] = [];
  range: number;
  isLoggedIn: boolean = false;
  isdataexist: boolean;
  first: any;
  rows: any;
  fileName: string;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private branchService: BranchService,
    private proxytypeService: ProxytypeService,
    private aliasproxyService: AliasproxyService,
    private connectionchannelService: ConnectionchannelService,
    private datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Proxy/Alias Manager' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();

    this.connectionchannelService
      .getConnectionChannelbyTenant()
      .subscribe((response: BackendResponse) => {
        if (response.status == 200) {
          this.channels = response.data;
        } else {
          this.channels = [];
        }
      });

    this.aliasproxyService
      .getBySecondId()
      .subscribe((response: BackendResponse) => {
        if (response.status == 200) {
          this.secondIdType = response.data;
        } else {
          this.secondIdType = [];
        }
      });

    // this.aliasproxyService
    //   .getByProxy()
    //   .subscribe((response: BackendResponse) => {
    //     if (response.status == 200) {
    //       this.proxyaddrestype = response.data;
    //     } else {
    //       this.proxyaddrestype = [];
    //     }
    //   });
    this.proxyaddrestype = [
      { label: 'Mobile Phone', proxy_type: '01' },
      { label: 'Email', proxy_type: '02' },
    ];

    // this.channeltypeService
    //   .getAllChnByTenant()
    //   .subscribe((response: BackendResponse) => {
    //     console.log(JSON.stringify(response));
    //     if (response.status == 200) {
    //       this.channels = response.data;
    //     } else {
    //       this.channels = [];
    //     }
    //   });
    //
    // this.branchService
    //   .getAllBranchByTenant()
    //   .subscribe((response: BackendResponse) => {
    //     // console.log(JSON.stringify(response));
    //     if (response.status == 200) {
    //       console.log(response.data);
    //       this.branchOffices = response.data.Branchs;
    //     } else {
    //       this.branchOffices = [];
    //     }
    //   });

    // this.proxytypeService
    //   .getAllPrxTByTenant()
    //   .subscribe((response: BackendResponse) => {
    //     console.log(JSON.stringify(response));
    //     if (response.status == 200) {
    //       console.log(response.data);
    //       this.proxyaddrestype = response.data;
    //     } else {
    //       this.proxyaddrestype = [];
    //     }
    //   });
  }

  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh BIC ');
    this.aliasproxyService
      .getAllProxyManagement()
      .subscribe((response: BackendResponse) => {
        //console.log('>>>>>>> ' + JSON.stringify(response));
        if (response.status === 202) {
          this.isdataexist = false;
          this.proxyData = [];
          const objtmp = {
            komi_unique_id: 'N/A',
            display_name: 'N/A',
            account_number: 'N/A',
            proxy_value: 'N/A',
            // branch: 'N/A',
            channel: 'N/A',
            channel_name: 'N/A',
            proxy_type: 'N/A',
            scnd_id_type: 'N/A',
            resident_status: 'N/A',
            resp_status: 'N/A',
            operation_type: 'N/A',
          };
          this.proxyData.push(objtmp);
        } else {
          this.isdataexist = true;
          this.proxyData = response.data.proxy;
        }
        this.isFetching = false;
      });
  }

  refreshData() {
    this.isdataexist = false;
    this.proxyData = [];
    this.isFetching = true;
    const payload: any = {};

    if (this.startDate) {
      payload.startDate = this.datepipe.transform(
        this.startDate,
        'yyyy-MM-dd 00:00:00'
      );
    }
    this.dateTime = new Date(
      this.startDate.getFullYear(),
      this.startDate.getMonth(),
      this.startDate.getDate() + Number(this.range)
    );

    if (this.endDate > this.dateTime) {
      this.isLoggedIn = true;
      this.endDate = this.dateTime;
      payload.endDate = this.datepipe.transform(
        this.dateTime,
        'yyyy-MM-dd 00:00:00'
      );
    } else if (this.endDate == null) {
      this.isLoggedIn = false;
      this.endDate = this.dateTime;
      payload.endDate = this.datepipe.transform(
        this.dateTime,
        'yyyy-MM-dd 00:00:00'
      );
    } else if (this.endDate < this.startDate) {
      this.isLoggedIn = false;
      this.endDate = this.startDate;
      payload.endDate = this.datepipe.transform(
        this.startDate,
        'yyyy-MM-dd 00:00:00'
      );
    } else {
      this.isLoggedIn = false;
      payload.endDate = this.datepipe.transform(
        this.endDate,
        'yyyy-MM-dd 00:00:00'
      );
    }

    this.isLoggedIn = false;

    if (this.SecondIdSelected) {
      payload.secondid = this.SecondIdSelected.secondId;
    }
    console.log(this.SecondIdSelected.secondId);

    if (this.channelSelected) {
      payload.channel = this.channelSelected.channel_type;
    }
    console.log(this.channelSelected.channel_type);

    if (this.proxyTypeSelected) {
      payload.prxtype = this.proxyTypeSelected.proxy_type;
    }

    // interval start date now
    this.interStartDate = new Date(
      this.startDate.getFullYear(),
      this.startDate.getMonth(),
      this.startDate.getDate() + Number(1)
    );
    payload.interStartDate = this.datepipe.transform(
      this.interStartDate,
      'yyyy-MM-dd 00:00:00'
    );

    // interval end date now
    this.interStartDate = new Date(
      this.startDate.getFullYear(),
      this.startDate.getMonth(),
      this.startDate.getDate() + Number(1)
    );
    payload.interStartDate = this.datepipe.transform(
      this.interStartDate,
      'yyyy-MM-dd 00:00:00'
    );

    // interval end date now
    this.interEndDate = new Date(
      this.endDate.getFullYear(),
      this.endDate.getMonth(),
      this.endDate.getDate() + Number(1)
    );
    payload.interEndDate = this.datepipe.transform(
      this.interEndDate,
      'yyyy-MM-dd 00:00:00'
    );

    console.log('JSON>>>', JSON.stringify(payload));

    this.aliasproxyService
      .getByParam(payload)
      .subscribe((result: BackendResponse) => {
        //console.log(JSON.stringify(result));
        if (result.status == 200) {
          this.isdataexist = true;
          this.proxyData = result.data;
        } else {
          this.isdataexist = false;
          this.proxyData = [];
          const objtmp = {
            komi_unique_id: 'N/A',
            display_name: 'N/A',
            account_number: 'N/A',
            proxy_value: 'N/A',
            // branch: 'N/A',
            channel: 'N/A',
            channel_name: 'N/A',
            proxy_type: 'N/A',
            scnd_id_type: 'N/A',
            resident_status: 'N/A',
            resp_status: 'N/A',
            operation_type: 'N/A',
          };
          this.proxyData.push(objtmp);
        }
        this.isFetching = false;
      });
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  maxDate() {
    const now = this.startDate;
    this.birthday = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() + Number(this.range)
    );

    var date = this.birthday,
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);

    this.birthday = [day, mnth, date.getFullYear()].join('-');
    this.isLoggedIn = true;
    return this.birthday;
  }

  isLastPage(): boolean {
    return this.proxyData
      ? this.first === this.proxyData.length - this.rows
      : true;
  }

  isFirstPage(): boolean {
    return this.proxyData ? this.first === 0 : true;
  }

  exportexcel(): void {
    const myDate = new Date();
    const datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    this.fileName = 'csv' + datenow + '.csv';
    /* table id is passed over here */
    const element = document.getElementById('excel-table');
    // const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.proxyData);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }

  openPDF(): void {
    const doc = new jsPDF('l', 'mm', [297, 210]);
    const col = [
      [
        'Komi Unique id',
        'Name',
        'Acc No',
        'Proxy Address',
        // 'Branch',
        'Channel',
        'Proxy Type',
        'Second ID',
        'Cust Residence',
        'Proxy Status',
        'Proxy Activity',
      ],
    ];
    const rows = [];
    const myDate = new Date();
    const datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    this.fileName = 'pdf' + datenow + '.pdf';
    this.proxyData.forEach((element) => {
      const temp = [
        element.komi_unique_id,
        element.account_name,
        element.account_number,
        element.proxy_value,
        // element.branch,
        element.channel_name,
        element.proxy_type,
        element.scnd_id_type,
        element.resident_status,
        element.resp_status,
        element.operation_type,
      ];
      rows.push(temp);
    });
    // doc.autoTable(col, rows);
    autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (rows) => {},
    });
  }
  // exportexcel(): void {
  //   let myDate = new Date();
  //   let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
  //   this.fileName = 'xls' + datenow + '.xlsx';
  //   /* table id is passed over here */
  //   let element = document.getElementById('excel-table');
  //   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

  removeFilter() {
    this.startDate = '';
    this.endDate = '';
    this.channels = '';
    this.secondIdType = '';
    this.proxyaddrestype = '';
    this.refreshingProxy();
  }

  // showTopSuccess(message: string) {
  //   this.messageService.add({
  //     severity: 'success',
  //     summary: 'Deleted',
  //     detail: message,
  //   });
  // }
}
