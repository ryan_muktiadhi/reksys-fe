import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/root/organization.service';
import { Location } from '@angular/common';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApplicationsService } from 'src/app/services/root/applications.service';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-applicationdetail',
  templateUrl: './applicationdetail.component.html',
  styleUrls: ['./applicationdetail.component.scss'],
})
export class ApplicationdetailComponent implements OnInit {
  extraInfo: any = {};
  inputform: any = {};
  activedetail: any = 1;
  titleActive: any = 'Please select module needed on this group ACL!';
  activebutton: any = 'Next';
  isEdit: boolean = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userInfo: any = {};
  selectedModules: any = [];
  tokenID: string = '';
  // orgForm!: FormGroup;
  groupName: string = '';
  groupDesc: string = '';
  submitted = false;
  groupId = '';
  apps: any = [];
  aclData: any = {
    read: 0,
    create: 0,
    update: 0,
    delete: 0,
    view: 0,
    approval: 0,
  };
  listmodulessource: any = [];
  listmodulessourcetmp: any = [];
  listmodulesdest: any = [];
  listmenubymodules: any = [];
  appNotSelected = false;
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private router: Router,
    private location: Location,
    private formBuilder: FormBuilder,
    private applicationsService: ApplicationsService,
    private activatedRoute: ActivatedRoute,
    private groupService: GroupServiceService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    // console.log(">>>EXTRA>>>>> "+this.extraInfo);
    // console.log(checkurl);
    if (checkurl) this.isEdit = true;

    // console.dir(this.router);
  }
  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Applications Group',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];

    // this.orgForm = this.formBuilder.group({
    //   organizationname: ['', Validators.required],
    //   description: [''],
    // });

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      // console.log(">>>> Userinfo : "+JSON.stringify(this.userInfo));
      if (!this.isEdit) {
        this.groupService.menus = [];
        this.apps = this.userInfo.apps;
        this.groupService
          .getAllModules(this.apps[0].idapp)
          .subscribe((modulesresult: BackendResponse) => {
            // console.log(">> MODULES >>"+JSON.stringify(modulesresult));
            this.listmodulessource = modulesresult.data;
            this.listmodulessourcetmp = [];
          });
      } else {
        console.log('>>>>>>>>>> ON ADD DATA  ');
        this.apps = this.userInfo.apps;
        // console.log(">>>>>>>>>> Route active "+this.activatedRoute.snapshot.paramMap.get('id'));
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          let grpid = this.activatedRoute.snapshot.paramMap.get('id');
          this.groupId = grpid;
          // console.log(">>>>>>>>>> Route active "+grpid);
          this.groupId = grpid || '';
          this.groupService
            .getGroupDetail(this.groupId)
            .subscribe((result: BackendResponse) => {
              console.log('>>>> Data JSON EDIT : ' + JSON.stringify(result));
              const groupObj = result.data.result;
              this.groupName = groupObj.group.groupname;
              let destTempList = groupObj.modules;
              // this.listmodulesdest = groupObj.modules;
              this.groupService
                .getAllModules(this.apps[0].idapp)
                .subscribe((modulesresult: BackendResponse) => {
                  // console.log(">> MODULES >>"+JSON.stringify(modulesresult));
                  this.listmodulessource = modulesresult.data;
                  // console.log(">> MODULES Dapat >>"+JSON.stringify(destTempList));

                  destTempList.forEach((elementdst, idx) => {
                    var ItemIndex = this.listmodulessource.findIndex(
                      (b) => b.id === elementdst.id
                    );
                    console.log('>>>> ' + ItemIndex);
                    if (ItemIndex > -1) {
                      this.listmodulesdest.push(
                        this.listmodulessource[ItemIndex]
                      );
                      this.listmodulessource.splice(ItemIndex, 1);
                    }
                  });

                  //  });
                  // this.listmodulessourcetmp.map((element, index) => {
                  //   destTempList.map((elementdst, idx) =>{
                  //       // console.log("id dest "+JSON.stringify(elementdst));
                  //       console.log(" TEST : "+" >>> "+element.id +"," + elementdst.id)
                  //       if(element.id === elementdst.id) {
                  //         this.listmodulesdest.push(element);
                  //       } else {
                  //         this.listmodulessource.push(element);
                  //       }
                  //   });
                  // })
                });
            });
        }
      }
    });
  }
  onRowSelect(event: any) {
    this.applicationsService.applications = [];
    this.appNotSelected = false;
    let index = this.applicationsService.allApplications.findIndex(
      (application) => {
        return application.id === event.data.id;
      }
    );
    this.applicationsService.allApplications[index].selected = true;
    this.applicationsService.applications.push(event.data);
  }
  async onSubmit() {
    this.submitted = true;
    // console.log('VALID ' + this.orgForm.valid + " "+ this.activedetail);
    // if (this.orgForm.valid) {
    if (this.groupName === '' || this.groupName === null) {
      this.inputform.errors = 'InputError';
      return true;
    }
    switch (this.activedetail) {
      case 1:
        this.titleActive = 'Please check Access Level for the menus!';
        this.activedetail = 2;
        this.activebutton = 'Save';
        this.selectedModules = this.listmodulesdest;
        this.listmenubymodules = await this.getAllmenuByModules(
          this.listmodulesdest,
          this.groupService,
          this.isEdit,
          this.groupId,
          this.userInfo
        );
        // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>> ABIS I KIRM "+JSON.stringify(this.listmenubymodules));
        break;
      case 2:
        this.titleActive = 'Please select module needed on this group ACL!';
        let reqData: any = {};
        await this.setupModuleReq();
        // reqData.
        // let group = {"groupName": this.orgForm.get('organizationname').value, "groupType":1};
        let group = { groupName: this.groupName, groupType: 1 };
        this.groupService.groupSetupData.group = group;
        reqData.group = this.groupService.groupSetupData.group;
        reqData.modules = this.selectedModules;
        reqData.menus = this.groupService.menus;
        this.groupService.reqGroup = reqData;
        this.groupService.haveModuleAndMenus = true;
        // console.log("Data to input "+JSON.stringify(reqData))
        if (!this.isEdit) {
          // console.log("Add to input "+JSON.stringify(this.groupService.reqGroup))
          this.groupService
            .regisGroup(this.groupService.reqGroup)
            .subscribe((result: BackendResponse) => {
              if (result.status === 200) {
                this.groupService.modules = [];
                this.groupService.reqGroup = [];
                this.location.back();
              }
            });
        } else {
          this.groupService.reqGroup.group.groupId = this.groupId;
          // console.log("Edit to input "+JSON.stringify(this.groupService.reqGroup))
          // this.groupService.reqGroup.group.groupId = this.groupId;
          this.groupService
            .editGroup(this.groupService.reqGroup)
            .subscribe((result: BackendResponse) => {
              if (result.status === 200) {
                this.groupService.modules = [];
                this.groupService.reqGroup = [];
                this.location.back();
              }
            });
        }

        break;
      default:
        break;
    }
  }
  // get f() {
  //   return this.orgForm.controls;
  // }

  onCancel() {
    this.activedetail = this.activedetail - 1;
    this.activebutton = 'Next';
  }
  async getAllmenuByModules(
    payload: any,
    gs: GroupServiceService,
    isEdit: boolean,
    groupid: any,
    userInfo: any
  ): Promise<any> {
    let jml: number = 1;
    const promise = new Promise(function (resolve, reject) {
      let menusTmp: any = [];
      let menus: any = [];
      payload.map(async (module): Promise<any> => {
        if (isEdit) {
          // let modulesTmp: any[] = [];
          let modules: any[] = [];
          gs.getAllMenuByModuleIdAndGroupId(module.id, groupid).subscribe(
            async (value) => {
              console.log('Menu Ambil dari Edit ' + JSON.stringify(value.data));
              menusTmp = [];
              // console.log("Menu Edit "+JSON.stringify(userInfo));
              // let menusobj = value.data.menus;
              //   menusobj.map((jsobj) => {
              //   menus.push(jsobj);
              // })
              // gs.menus = [];
              // let objmodule = {"id":value.data.id,"modulename":value.data.modulename,"created_byid":userInfo.id,"status":1,"idapplication":"4","modulecode":"BIFAST003","applabel":"KOMI","view":1}
              // modules.push(objmodule);
              // menus.push(value.data.menus);
              // modules.map(async (module: any) => {
              //   menus = module.menus;
              // });
              let menusobj = value.data.menus;

              await menusobj.map((jsobj) => {
                menusTmp.push(jsobj);
              });

              await menusTmp.map(async (menut: any) => {
                let data: any = {};
                let acl = {};
                acl = {
                  read: menut.fread,
                  create: menut.fcreate,
                  update: menut.fupdate,
                  delete: menut.fdelete,
                  approval: menut.fapproval,
                  view: menut.fview,
                };
                data.menuId = menut.id;
                data.menuName = menut.title;
                data.moduleId = value.data.id;
                // data.moduleName = menu.modulename;value.data
                data.moduleName = value.data.modulename;
                data.acl = acl;
                if (!gs.haveModuleAndMenus) {
                  menusTmp = [];
                  menus.push(data);
                  gs.menus.push(data);
                }
              });

              if (jml == payload.length) {
                // gs.menus = menus;
                console.log('>> MENU : ' + JSON.stringify(menus));
                resolve(menus);
              }
              jml++;
            }
          );
        } else {
          gs.getAllMenuByModuleId(module.id).subscribe(async (value) => {
            // console.log("Menu Ambil dari module "+JSON.stringify(value.data));
            // console.log("PAYLOAD "+payload.length+", jml "+jml);
            // let menusobj = value.data.menus;
            //   menusobj.map((jsobj) => {
            //   menus.push(jsobj);
            // })
            let menusobj = value.data.menus;

            await menusobj.map((jsobj) => {
              menusTmp.push(jsobj);
            });

            await menusTmp.map(async (menut: any) => {
              let data: any = {};
              let acl = {};
              acl = {
                read: menut.fread,
                create: menut.fcreate,
                update: menut.fupdate,
                delete: menut.fdelete,
                view: menut.fview,
              };
              data.menuId = menut.id;
              data.menuName = menut.title;
              // data.moduleId = menut.idmodule;value.data
              data.moduleId = value.data.id;
              // data.moduleName = menu.modulename;value.data
              data.moduleName = value.data.modulename;
              data.acl = acl;
              if (!gs.haveModuleAndMenus) {
                menusTmp = [];
                // console.log("Isi menu  "+JSON.stringify(data));
                menus.push(data);
                gs.menus.push(data);
              }
            });
            if (jml == payload.length) {
              menusTmp = [];
              // gs.menus = menus;
              console.log('>> MENU : ' + JSON.stringify(menus));
              resolve(menus);
            }
            jml++;
          });
        }

        // console.log(JSON.stringify(module))
      });
      // resolve('Promise returns after 1.5 second!');
    });
    return promise.then(function (value) {
      // console.log("Menu yang di masukin :"+JSON.stringify(value));
      return value;
      // Promise returns after 1.5 second!
    });
  }

  async check(event: any, datas: any, name: any, module: any) {
    // console.log("MASUK CECH")
    await this.setupAclData(name, event);
    // console.log("Modules on check "+JSON.stringify(this.aclData));
    // console.log("Modules on check "+JSON.stringify(datas));
    if (this.groupService.menus.length < 1) {
      // console.log("MENU NGGA ADA");
      let menuReq = {
        // moduleId: module.id,
        moduleId: module.idmodule,
        moduleName: datas.modulename,
        menuId: datas.id,
        menuName: datas.title,
        acl: {},
      };
      menuReq.acl = this.aclData;
      // console.log(JSON.stringify(menuReq));
      this.groupService.menus.push(menuReq);
    } else {
      // console.log("MENU BANYAK");
      let isExist = await this.groupService.menus.findIndex((menu) => {
        // console.log(">>> MENU SERVICE "+JSON.stringify(menu))
        // console.log(">>> MENU DATA "+JSON.stringify(datas))
        return parseInt(menu.menuId) === parseInt(datas.menuId);
        // return parseInt(menu.id) === parseInt(datas.id);
      });
      // console.log('isExist ' + isExist);
      if (isExist < 0) {
        let menuReq = {
          // moduleId: module.id,
          moduleId: module.moduleId,
          moduleName: datas.modulename,
          menuId: datas.id,
          menuName: datas.title,
          acl: {},
        };
        menuReq.acl = this.aclData;
        this.groupService.menus.push(menuReq);
        //console.log(this.groupService.menus);
      } else {
        // console.log("isExist > anem : ", name+" "+ isExist);
        // console.log("OBJECT "+JSON.stringify(this.groupService.menus));
        // console.log('Module ' + JSON.stringify(module));
        this.groupService.menus[isExist].idmodule = module.moduleId;
        this.groupService.menus[isExist].moduleId = module.moduleId;
        // this.groupService.menus[isExist].moduleId = module.id;

        if (name == 'approval') {
          this.groupService.menus[isExist].acl[name] = event.checked ? 1 : 0;
          this.groupService.menus[isExist].acl['create'] = 0;
          await this.setupAclData('create', { event: { checked: 0 } });
          // this.groupService.menus[isExist].acl['read'] = 0;
          this.groupService.menus[isExist].acl['update'] = 0;
          await this.setupAclData('update', { event: { checked: 0 } });
          this.groupService.menus[isExist].acl['delete'] = 0;
          await this.setupAclData('delete', { event: { checked: 0 } });
          this.groupService.menus[isExist].acl['view'] = 0;
          await this.setupAclData('view', { event: { checked: 0 } });
        } else {
          this.groupService.menus[isExist].acl[name] = event.checked ? 1 : 0;
          this.groupService.menus[isExist].acl['approval'] = 0;
        }

        // this.groupService.menus[isExist].acl['approval'] = event.checked ? 1 : 0;
        // this.groupService.menus[isExist]["f"+name] = event.checked ? 1 : 0;
        // console.log(JSON.stringify(this.groupService.menus));
      }
    }
  }

  async checkAll(event: any, datas: any, name: any) {
    // console.log(JSON.stringify(event));
    console.log(JSON.stringify(datas));
    let idx = 0;
    for (let i in datas) {
      // resultingArr.push(i + 1)
      if (name == 'approval') {
        await this.setupAclData(name, event);
        this.groupService.menus[idx].acl[name] = event.checked ? 1 : 0;
        this.groupService.menus[idx].acl['create'] = 0;
        await this.setupAclData('create', { event: { checked: 0 } });
        this.groupService.menus[idx].acl['read'] = 1;
        await this.setupAclData('read', { event: { checked: 1 } });
        this.groupService.menus[idx].acl['update'] = 0;
        await this.setupAclData('update', { event: { checked: 0 } });
        this.groupService.menus[idx].acl['delete'] = 0;
        await this.setupAclData('delete', { event: { checked: 0 } });
        this.groupService.menus[idx].acl['view'] = 1;
        await this.setupAclData('view', { event: { checked: 1 } });
      } else {
        await this.setupAclData(name, event);
        // console.log(JSON.stringify(event));
        // console.log(JSON.stringify(name));
        this.groupService.menus[idx].acl[name] = event.checked ? 1 : 0;
        this.groupService.menus[idx].acl['approval'] = 0;
      }
      // this.groupService.menus[idx].acl[name] = event.checked ? 1 : 0;
      idx++;
    }
  }

  setupAclData(name: string, event: any) {
    this.aclData = {
      read: 0,
      create: 0,
      update: 0,
      delete: 0,
      view: 0,
    };

    // console.log(JSON.stringify(event));

    switch (name) {
      case 'read': {
        this.aclData.read = event.checked ? 1 : 0;
        break;
      }
      case 'create': {
        this.aclData.create = event.checked ? 1 : 0;
        break;
      }
      case 'update': {
        this.aclData.update = event.checked ? 1 : 0;
        break;
      }
      case 'delete': {
        this.aclData.delete = event.checked ? 1 : 0;
        break;
      }
      case 'view': {
        this.aclData.view = event.checked ? 1 : 0;
        break;
      }
      case 'approval': {
        this.aclData.view = event.checked ? 1 : 0;
        break;
      }
      default: {
        //statements;
        break;
      }
    }
  }
  setupModuleReq() {
    this.groupService.modules.map((data) => {
      let moduleModel = {
        moduleId: '',
        acl: {
          view: 1,
        },
      };
      moduleModel.moduleId = data.id;
      this.selectedModules.push(moduleModel);
    });
  }
}
