import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ApprovalService } from 'src/app/services/approval.service';
import { AuthService } from 'src/app/services/auth.service';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-applicationgroup',
  templateUrl: './applicationgroup.component.html',
  styleUrls: ['./applicationgroup.component.scss'],
})
export class ApplicationgroupComponent implements OnInit {
  display = false;
  scrollheight: any = '400px';
  viewApprove = false;
  displayPrv = false;
  selectedgrp: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  warning: any = {};
  newData: any;
  oldData: any;
  grpheader: any = [
    { label: 'Group Name', sort: 'groupname' },
    { label: 'Status', sort: 'active' },
    { label: 'Created At', sort: 'created_at' },
  ];
  grpcolname: any = ['groupname', 'active', 'created_at'];
  grpcolhalign: any = ['', 'p-text-center', 'p-text-center'];
  grpcolwidth: any = ['', '', ''];
  // grpcolwidth: any = ['', { width: '170px' }];
  grpcollinghref: any = { url: '#', label: 'Application' };
  grpactionbtn: any = [1, 1, 1, 1, 1, 1];
  grpaddbtn = { route: 'detail', label: 'Add Data' };
  grpanizations: any[] = [];
  groupsData: any[] = [];
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private groupService: GroupServiceService,
    private aclMenuService: AclmenucheckerService,
    private router: Router,
    private approvalService: ApprovalService
  ) {}
  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Group & Acl' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
        console.log('MENU ALL ACL Set');
        this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
          if (JSON.stringify(dataacl.acl) === '{}') {
            console.log('No ACL Founded');
          } else {
            console.log('ACL Founded');
            console.log(dataacl.acl);
            this.grpactionbtn[0] = dataacl.acl.create;
            this.grpactionbtn[1] = dataacl.acl.read;
            this.grpactionbtn[2] = dataacl.acl.update;
            this.grpactionbtn[3] = dataacl.acl.delete;
            this.grpactionbtn[4] = dataacl.acl.view;
            this.grpactionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh group ');
    //   if ((data.status = 200)) {
    //     this.grpanizationService
    //       .retrivegrpByTenant()
    //       .subscribe((grpall: BackendResponse) => {
    //         // console.log('>>>>>>> ' + JSON.stringify(grpall));
    //         this.grpanizations = grpall.data;
    //         if(this.grpanizations.length < 1){
    //           let objtmp = {"grpcode":"No records", "grpname":"No records","grpdescription":"No records","application":"No records","created_by":"No records"}
    //           this.grpanizations = [];
    //           this.grpanizations.push(objtmp);
    //         }

    //        this.isFetching=false;
    //       });
    //   }
    // });
    this.groupService.getAllGroup().subscribe((result: BackendResponse) => {
      // console.log('>>>>>>> ' + JSON.stringify(result));
      if (result.data.userGroup?.length > 0) {
        this.groupsData = [];
        this.groupsData = result.data.userGroup;
      } else {
        this.groupsData = [];
        let objtmp = { groupname: 'No records' };
        this.groupsData.push(objtmp);
      }
      this.isFetching = false;
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedgrp = data;
  }

  async previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedgrp = data;
    this.newData = null;
    await this.getApprovalDetail(this.selectedgrp);
  }

  async approvalData(data: any) {
    this.viewApprove = true;
    this.selectedgrp = data;
    this.newData = null;
    this.oldData = null;
    if (data.idapproval && data.active != 9) {
      this.getApprovalData(data.idapproval);
    } else {
      await this.getApprovalDetail(this.selectedgrp);
    }
  }
  async filterForm(formData, filterData) {
    let filterFormData = formData.filter((formItem) => {
      return filterData.includes(formItem.label);
    });

    return filterFormData;
  }
  async getApprovalDetail(group) {
    let groupDetail;
    await this.groupService
      .getGroupDetail(group.id)
      .subscribe(async (resp: BackendResponse) => {
        groupDetail = resp.data.result;
        // console.log(groupDetail);
        let mergeData: any = {
          module: '',
          menu: '',
        };
        let groupData = await this.setApprovalForm(groupDetail.group);
        groupData = await this.filterForm(groupData, ['groupname', 'status']);
        let moduleData = groupDetail.modules;
        let menuData = groupDetail.menus;
        mergeData = groupData;
        mergeData.module = moduleData;
        mergeData.menu = menuData;
        this.oldData = groupData;
      });
  }

  getApprovalData(id) {
    this.approvalService
      .getByid(id)
      .subscribe(async (resp: BackendResponse) => {
        await this.getApprovalDetail(this.selectedgrp);

        let newData = JSON.parse(resp.data[0].jsondata);
        let mergeData: any = {
          module: '',
          menu: '',
        };
        let groupData: any = await this.setApprovalForm(newData.group);
        groupData = await this.filterForm(groupData, ['groupName']);
        let moduleData = newData.modules;
        let menuData = newData.menus;
        mergeData = groupData;
        mergeData.module = moduleData;
        mergeData.menu = menuData;
        this.newData = mergeData;
      });
  }
  async setApprovalForm(data) {
    const keys = Object.keys(data);
    const input = data;
    let arrData = [];

    if (keys.length > 0) {
      await keys.map(async (key: any) => {
        let data: any = {};
        key === 'active' || key === 'isactive'
          ? (data.label = 'status')
          : key === 'group'
          ? (data.label = 'groupname')
          : (data.label = key);

        if (Array.isArray(input[key])) {
          let value = [];
          await input[key].map((dt) => {
            value.push(dt.name);
          });

          data.value = value;
        } else if (data.label === 'status') {
          data.value =
            input[key] == 0
              ? 'Not Active'
              : input[key] == 1
              ? 'Active'
              : 'Waiting for Approval';
          data.statusCode = input[key];
        } else {
          data.value = input[key];
        }

        if (data.label != 'id' && data.label != 'userid') {
          arrData.push(data);
        }
      });
      return arrData;
    }
  }
  async setArrayApprovalForm(data) {
    let result = [];
    data.map(async (dt) => {
      let formated = await this.setApprovalForm(dt);
      result.push(formated);
    });
    return result;
  }

  approvalSubmit(status) {
    let payload = {
      id: this.selectedgrp.id,
      oldactive: this.selectedgrp.active,
      isactive: status,
      idapproval: this.selectedgrp.idapproval,
    };
    console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.groupService
      .editGroupActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingApp();
          this.viewApprove = false;
        }
      });
  }

  reject(status) {
    let payload = {
      id: this.selectedgrp.id,
      oldactive: this.selectedgrp.active,
      isactive: status,
      idapproval: this.selectedgrp.idapproval,
    };
    console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.groupService.reject(payload).subscribe((result: BackendResponse) => {
      // console.log(">>>>>>>> return "+JSON.stringify(result));
      if (result.status === 200) {
        this.refreshingApp();
        this.viewApprove = false;
      }
    });
  }

  deleteGroup() {
    console.log(this.selectedgrp);
    let group = this.selectedgrp;
    const payload = { group };
    console.log('>> Di delete ' + JSON.stringify(payload));
    this.groupService
      .deleteGroup(payload)
      .subscribe((resp: BackendResponse) => {
        console.log('>> hasil Delete ' + resp);
        if (resp.status === 200) {
          if (resp.data.message) {
            this.warning.show = true;
            this.warning.header = 'Cannot Delete Group';
            this.warning.message = resp.data.message;
          } else {
            this.showTopSuccess(resp.data);
            this.display = false;
            this.refreshingApp();
          }
        }
        this.display = false;
        this.refreshingApp();
      });
    // this.grpanizationService
    //   .deletegrp(payload)
    //   .subscribe((resp: BackendResponse) => {
    //     console.log(resp);
    //     if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //     this.display = false;
    //     this.refreshingApp();
    //   });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
