import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BicadmindetailComponent } from './bicadmindetail.component';

describe('BicadmindetailComponent', () => {
  let component: BicadmindetailComponent;
  let fixture: ComponentFixture<BicadmindetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BicadmindetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BicadmindetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
