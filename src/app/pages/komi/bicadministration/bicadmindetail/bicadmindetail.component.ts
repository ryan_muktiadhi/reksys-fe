import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { Location } from '@angular/common';
import { BackendResponse } from 'src/app/interfaces/backend-response';
@Component({
  selector: 'app-bicadmindetail',
  templateUrl: './bicadmindetail.component.html',
  styleUrls: ['./bicadmindetail.component.scss'],
})
export class BicadmindetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  old_code: any = '';
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private bicadminService: BicadminService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'BIC Administrator',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    // bank_code, bic_code, bank_name
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        bank_code: ['', Validators.required],
        bic_code: ['', Validators.required],
        bank_name: ['', Validators.required],
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.bicadminService
            .getBicById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit bic ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.groupForm.patchValue({
                bank_code: result.data.bicAdmins.bank_code,
                bic_code: result.data.bicAdmins.bic_code,
                bank_name: result.data.bicAdmins.bank_name,
              });
              this.old_code = result.data.bicAdmins.bic_code;
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name
      if (!this.isEdit) {
        payload = {
          bank_code: this.groupForm.get('bank_code')?.value,
          bic_code: this.groupForm.get('bic_code')?.value,
          bank_name: this.groupForm.get('bank_name')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.bicadminService
          .insertBicByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
            this.location.back();
          });
      } else {
        payload = {
          bank_code: this.groupForm.get('bank_code')?.value,
          bic_code: this.groupForm.get('bic_code')?.value,
          bank_name: this.groupForm.get('bank_name')?.value,
          old_code: this.old_code,
        };
        this.bicadminService
          .updateBicByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
            this.location.back();
          });
        // payload = {
        //   fullname: this.groupForm.get('fullname')?.value,
        //   userid: this.userId,
        //   group: this.groupForm.get('orgMlt')?.value,
        //   isactive: this.groupForm.get('isactive')?.value,
        // }
        // console.log(">>>>>>>> payload "+JSON.stringify(payload));
        // this.umService
        // .updatebyAdmin(payload)
        // .subscribe((result: BackendResponse) => {
        //   // console.log(">>>>>>>> return "+JSON.stringify(result));
        //   if (result.status === 200) {
        //     this.location.back();
        //   }
        // });
        // this.location.back();
      }
    }

    console.log(this.groupForm.valid);
  }
}
