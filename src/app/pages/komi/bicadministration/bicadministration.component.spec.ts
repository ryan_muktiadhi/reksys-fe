import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BicadministrationComponent } from './bicadministration.component';

describe('BicadministrationComponent', () => {
  let component: BicadministrationComponent;
  let fixture: ComponentFixture<BicadministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BicadministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BicadministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
