import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bicadministration',
  templateUrl: './bicadministration.component.html',
  styleUrls: ['./bicadministration.component.scss'],
})
export class BicadministrationComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  bicData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];

  bicDataheader: any = [
    { label: 'Bank Code', sort: 'bank_code' },
    { label: 'BIC Bank', sort: 'bic_bank' },
    { label: 'Bank Name', sort: 'bank_name' },
    { label: 'Status', sort: 'status' },
    { label: 'Active', sort: 'active' },
    { label: 'Created By', sort: 'created_by' },
    { label: 'Created Date', sort: 'created_date' },
    { label: 'Update By', sort: 'updated_by' },
    { label: 'Update Date', sort: 'updated_date' },
  ];
  bicDatacolname: any = [
    'bank_code',
    'bic_bank',
    'bank_name',
    'status',
    'active',
  ];

  bicDatacolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  bicDatacolwidth: any = ['', '', '', '', ''];
  bicDatacollinghref: any = { url: '#', label: 'Application' };
  bicDataactionbtn: any = [0, 1, 0, 1, 0, 1, 1];
  bicDataaddbtn = { label: 'Add Data' };

  groupForm!: FormGroup;

  bicDialog: boolean;
  delBicDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private bicadminService: BicadminService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      { label: 'Master Data' },
      { label: 'Bank Participant' },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 },
    ];

    /* Set datatable */
    this.cols = [
      { field: 'bank_code', header: 'Bank Code', isOrder: true },
      { field: 'bic_bank', header: 'BIC Bank', isOrder: true },
      { field: 'bank_name', header: 'Bank Name', isOrder: true },
      { field: 'status', header: 'Status', transform: true, isOrder: false },
      { field: 'created_date', header: 'Created Date', isOrder: true },
      { field: 'created_by', header: 'Created By', isOrder: false },
      { field: 'updated_date', header: 'Updated Date', isOrder: true },
      { field: 'updated_by', header: 'Updated By', isOrder: false },
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 4);

    /* Get data all */
    this.refreshingBic();
  }

  /* Get data all */
  refreshingBic() {
    this.isFetching = true;

    this.bicadminService.getAllBicByTenant().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          this.bicData = result.data;
        } else {
          this.bicData = [];
          let objtmp = {
            bank_code: 'No records',
            bic_bank: 'No records',
            bank_name: 'No records',
            status: 'No records',
            created_date: 'No records',
            created_by: 'No records',
            updated_date: 'No records',
            updated_by: 'No records',
          };
          this.bicData.push(objtmp);
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Get Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_bic: [''],
      bank_code: ['', Validators.required],
      bic_code: ['', Validators.required],
      bank_name: ['', Validators.required],
      status: [''],
    });
    this.bicDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;

    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          bankCode: this.groupForm.get('bank_code')?.value,
          bicBank: this.groupForm.get('bic_code')?.value,
          bankName: this.groupForm.get('bank_name')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.bicadminService.insertBicByTenant(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.bicDialog = false;
              this.isProcess = false;
              this.refreshingBic();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Inserted',
              });
            } else {
              this.bicDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.bicDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Insert Data Error -',
              detail: errorDetail,
            });
          }
        );
      } else {
        payload = {
          bankCode: this.groupForm.get('bank_code')?.value,
          bicBank: this.groupForm.get('bic_code')?.value,
          bankName: this.groupForm.get('bank_name')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_bic')?.value,
        };
        this.bicadminService.updateBicByTenant(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.bicDialog = false;
              this.isProcess = false;
              this.refreshingBic();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Updated',
              });
            } else {
              this.bicDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.bicDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Update Data Error -',
              detail: errorDetail,
            });
          }
        );
      }
    }
    console.log(this.groupForm.valid);
  }

  editBic(data: any) {
    this.groupForm = this.formBuilder.group({
      id_bic: [''],
      bank_code: ['', Validators.required],
      bic_code: ['', Validators.required],
      bank_name: ['', Validators.required],
      status: [''],
    });

    this.groupForm.patchValue({
      id_bic: data['id'],
      bank_code: data['bank_code'],
      bic_code: data['bic_bank'],
      bank_name: data['bank_name'],
      status: data['status'],
    });

    this.bicDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delBicDialog = true;
  }

  deleteBic() {
    this.isProcess = true;

    let bicData = this.delData;
    const payload = { bicData };
    this.bicadminService.deleteBicByTenant(payload.bicData.id).subscribe(
      (resp: BackendResponse) => {
        this.delBicDialog = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Success -',
          detail: 'Data Deleted',
        });
        this.refreshingBic();
      },
      (error) => {
        this.delBicDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Update Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }
}
