import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BranchService } from 'src/app/services/komi/branch.service';
import { CityService } from 'src/app/services/komi/city.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.scss'],
})
export class BranchComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  branchData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];
  optionCity: any[] = [];

  branchheader: any = [
    { label: 'Branch Code', sort: 'branch_code' },
    { label: 'Branch Name', sort: 'branch_name' },
    { label: 'Branch Acc. Fee', sort: 'branch_account_fee' },
    { label: 'Status', sort: 'status' },
    { label: 'Active', sort: 'active' },
    { label: 'Created By', sort: 'created_by' },
    { label: 'Created Date', sort: 'created_date' },
    { label: 'Update By', sort: 'updated_by' },
    { label: 'Update Date', sort: 'updated_date' },
  ];
  branchcolname: any = [
    'branch_code',
    'branch_name',
    'branch_account_fee',
    'status',
    'active',
  ];

  branchcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  branchcolwidth: any = ['', '', '', '', ''];
  branchcollinghref: any = { url: '#', label: 'Application' };
  branchactionbtn: any = [0, 1, 0, 1, 0, 1, 1];
  branchaddbtn = { label: 'Add Data' };

  groupForm!: FormGroup;

  branchDialog = false;
  delBranchDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private branchService: BranchService,
    private cityService: CityService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Master Data' }, { label: 'Branch' }];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    // this.cityService.getAllCity().subscribe((result: BackendResponse) => {
    //   if (result.status == 200) {
    //     for (let i = 0; i < result.data.length; i++) {
    //       this.optionCity.push(
    //         {
    //           label: result.data[i].CITY_NAME,
    //           value: result.data[i].CITY_CODE
    //         }
    //       );
    //     }
    //   }
    // });

    /* Set status options */
    this.status = [
      { label: 'Active', value: 1 },
      { label: 'In-Active', value: 0 },
    ];

    /* Set datatable */
    this.cols = [
      { field: 'branch_code', header: 'Branch Code', isOrder: true },
      { field: 'branch_name', header: 'Branch Name', isOrder: true },
      {
        field: 'branch_account_fee',
        header: 'Branch Acc. Fee',
        isOrder: false,
      },
      { field: 'status', header: 'Status', transform: true, isOrder: false },
      {
        field: 'created_date',
        header: 'Created Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'created_by',
        header: 'Created By',
        isOrder: false,
        width: '200px',
      },
      {
        field: 'updated_date',
        header: 'Updated Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'updated_by',
        header: 'Updated By',
        isOrder: false,
        width: '200px',
      },
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 4);

    /* Get data all */
    this.refreshingBranch();
  }

  /* Get data all */
  refreshingBranch() {
    this.isFetching = true;

    this.branchService.getAllBranchByTenant().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          this.branchData = result.data;
        } else {
          this.branchData = [];
          let objtmp = {
            branch_code: 'No records',
            branch_name: 'No records',
            branch_account_fee: 'No records',
            status: 'No records',
            created_date: 'No records',
            created_by: 'No records',
            updated_date: 'No records',
            updated_by: 'No records',
          };
          this.branchData.push(objtmp);
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Get Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_branch: [''],
      // optionCity: ['', Validators.required],
      kode_cabang: ['', Validators.required],
      nama_cabang: ['', Validators.required],
      acc_fee_cbg: ['', Validators.required],
      status: [''],
    });
    this.branchDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;
    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          // cityCode: this.groupForm.get('optionCity')?.value,
          branchCode: this.groupForm.get('kode_cabang')?.value,
          branchName: this.groupForm.get('nama_cabang')?.value,
          branchAccountFee: this.groupForm.get('acc_fee_cbg')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.branchService.insertBranchByTenant(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.branchDialog = false;
              this.isProcess = false;
              this.refreshingBranch();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Inserted',
              });
            } else {
              this.branchDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.branchDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Insert Data Error -',
              detail: errorDetail,
            });
          }
        );
      } else {
        payload = {
          // cityCode: this.groupForm.get('optionCity')?.value,
          branchCode: this.groupForm.get('kode_cabang')?.value,
          branchName: this.groupForm.get('nama_cabang')?.value,
          branchAccountFee: this.groupForm.get('acc_fee_cbg')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_branch')?.value,
        };
        this.branchService.updateBranchByTenant(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.branchDialog = false;
              this.isProcess = false;
              this.refreshingBranch();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Updated',
              });
            } else {
              this.branchDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.branchDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Update Data Error -',
              detail: errorDetail,
            });
          }
        );
      }
    }
  }

  editBranch(data: any) {
    this.groupForm = this.formBuilder.group({
      id_branch: [''],
      // optionCity: ['', Validators.required],
      kode_cabang: ['', Validators.required],
      nama_cabang: ['', Validators.required],
      acc_fee_cbg: ['', Validators.required],
      status: [''],
    });

    this.groupForm.patchValue({
      // optionCity: data["CITY_CODE"],
      kode_cabang: data['branch_code'],
      nama_cabang: data['branch_name'],
      acc_fee_cbg: data['branch_account_fee'],
      status: data['status'],
      id_branch: data['id'],
    });

    this.branchDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delBranchDialog = true;
  }

  deleteBranch() {
    this.isProcess = true;

    let dataBranch = this.delData;
    const payload = { dataBranch };
    this.branchService.deleteBranchByTenant(payload.dataBranch.id).subscribe(
      (resp: BackendResponse) => {
        this.delBranchDialog = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Success -',
          detail: 'Data Deleted',
        });
        this.refreshingBranch();
      },
      (error) => {
        this.delBranchDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Update Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }
}
