import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BranchService } from 'src/app/services/komi/branch.service';

@Component({
  selector: 'app-branchdetail',
  templateUrl: './branchdetail.component.html',
  styleUrls: ['./branchdetail.component.scss'],
})
export class BranchdetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  branchId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  old_code: any = '';
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private branchService: BranchService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Branch Management',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.stateOptions = [
      { label: 'Active', value: 1 },
      { label: 'Deactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // syscolname:any = ["paramname", "paramvalua","status", "created_date"]
      this.groupForm = this.formBuilder.group({
        citycode: ['', Validators.required],
        cityname: ['', Validators.required],
        branchcode: ['', Validators.required],
        branchname: ['', Validators.required],
        status: [0],
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.branchId = this.activatedRoute.snapshot.paramMap.get('id');
          this.branchService
            .getBranchById(this.branchId)
            .subscribe(async (result: BackendResponse) => {
              // console.log("Data edit bic "+JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.groupForm.patchValue({
                kode_cabang: result.data.BRANCH_CODE,
                nama_cabang: result.data.BRANCH_NAME,
                acc_fee_cbg: result.data.BRANCH_ACCOUNT_FEE,
                status: result.data.STATUS,
              });
              this.old_code = result.data.Branchs.id;
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name
      if (!this.isEdit) {
        payload = {
          branchCode: this.groupForm.get('kode_cabang')?.value,
          branchName: this.groupForm.get('nama_cabang')?.value,
          branchAccountFee: this.groupForm.get('acc_fee_cbg')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.branchService
          .insertBranchByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            // console.log(">>>>>>>> payload "+JSON.stringify(result));
            this.location.back();
          });
      } else {
        payload = {
          branchCode: this.groupForm.get('kode_cabang')?.value,
          branchName: this.groupForm.get('nama_cabang')?.value,
          branchAccountFee: this.groupForm.get('acc_fee_cbg')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_branch')?.value,
        };
        this.branchService
          .updateBranchByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            // console.log(">>>>>>>> payload "+JSON.stringify(result));
            this.location.back();
          });
      }
    }

    console.log(this.groupForm.valid);
  }
}
