import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChanneltypeComponent } from './channeltype.component';

describe('ChanneltypeComponent', () => {
  let component: ChanneltypeComponent;
  let fixture: ComponentFixture<ChanneltypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChanneltypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChanneltypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
