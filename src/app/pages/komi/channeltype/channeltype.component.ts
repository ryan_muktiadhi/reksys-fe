import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-channeltype',
  templateUrl: './channeltype.component.html',
  styleUrls: ['./channeltype.component.scss'],
})
export class ChanneltypeComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  chnData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];
  isAuthorized: any[];

  groupForm!: FormGroup;

  channelDialog = false;
  delChannelDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private channeltypeService: ChanneltypeService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Master Data' }, { label: 'Channel Type' }];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 },
    ];

    /* Set is_auth options */
    this.isAuthorized = [
      { label: 'TRUE', value: '1' },
      { label: 'FALSE', value: '0' },
    ];

    /* Set datatable */
    this.cols = [
      { field: 'cid', header: 'CID', isOrder: true, width: '200px' },
      {
        field: 'komi_channel_id',
        header: 'KOMI Channel ID',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'bank_channel_type',
        header: 'Bank Channel Type',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'iso_merchant_type',
        header: 'ISO Merchant Type',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'bi_channel_id',
        header: 'BI Channel ID',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'ip_address',
        header: 'IP Address',
        isOrder: true,
        width: '200px',
      },
      { field: 'key', header: 'Key', isOrder: true, width: '200px' },
      {
        field: 'description',
        header: 'Description',
        isOrder: false,
        width: '300px',
      },
      {
        field: 'is_authorized',
        header: 'Is Authorized',
        transformAuth: true,
        isOrder: false,
        width: '200px',
      },
      {
        field: 'status',
        header: 'Status',
        transform: true,
        isOrder: false,
        width: '200px',
      },
      {
        field: 'created_date',
        header: 'Created Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'created_by',
        header: 'Created By',
        isOrder: false,
        width: '200px',
      },
      {
        field: 'updated_date',
        header: 'Updated Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'updated_by',
        header: 'Updated By',
        isOrder: false,
        width: '200px',
      },
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 10);

    /* Get data all */
    this.refreshingChn();
  }

  /* Get data all */
  refreshingChn() {
    this.isFetching = true;

    this.channeltypeService.getAllChnByTenant().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          this.chnData = result.data;
        } else {
          this.chnData = [];
          let objtmp = {
            cid: 'No records',
            komi_channel_id: 'No records',
            bank_channel_type: 'No records',
            iso_merchant_type: 'No records',
            bi_channel_id: 'No records',
            ip_address: 'No records',
            key: 'No records',
            is_authorized: 'No records',
            description: 'No records',
            status: 'No records',
            created_date: 'No records',
            created_by: 'No records',
            updated_date: 'No records',
            updated_by: 'No records',
          };
          this.chnData.push(objtmp);
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Get Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_ch_type: [''],
      cid: ['', Validators.required],
      komi_ch_id: ['', Validators.required],
      bank_ch_type: ['', Validators.required],
      iso_merch_type: ['', Validators.required],
      bi_ch_id: ['', Validators.required],
      ip_address: ['', Validators.required],
      key: ['', Validators.required],
      description: [''],
      status_ch: [''],
      is_auth: [''],
    });
    this.channelDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;

    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          cid: this.groupForm.get('cid')?.value,
          komiChannelId: this.groupForm.get('komi_ch_id')?.value,
          bankChannelType: this.groupForm.get('bank_ch_type')?.value,
          isoMerchantType: this.groupForm.get('iso_merch_type')?.value,
          biChannelId: this.groupForm.get('bi_ch_id')?.value,
          ipAddress: this.groupForm.get('ip_address')?.value,
          key: this.groupForm.get('key')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status_ch')?.value,
          isAuthorized: this.groupForm.get('is_auth')?.value,
        };
        // console.log(">>>>>>>> payload caneltype" + JSON.stringify(payload));
        this.channeltypeService.insertChnByTenant(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.channelDialog = false;
              this.isProcess = false;
              this.refreshingChn();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Inserted',
              });
            } else {
              this.channelDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.channelDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Update Data Error -',
              detail: errorDetail,
            });
          }
        );
      } else {
        payload = {
          cid: this.groupForm.get('cid')?.value,
          komiChannelId: this.groupForm.get('komi_ch_id')?.value,
          bankChannelType: this.groupForm.get('bank_ch_type')?.value,
          isoMerchantType: this.groupForm.get('iso_merch_type')?.value,
          biChannelId: this.groupForm.get('bi_ch_id')?.value,
          ipAddress: this.groupForm.get('ip_address')?.value,
          key: this.groupForm.get('key')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status_ch')?.value,
          isAuthorized: this.groupForm.get('is_auth')?.value,
          id: this.groupForm.get('id_ch_type')?.value,
        };
        console.log(
          '>>>>>>>> payload edit caneltype' + JSON.stringify(payload)
        );
        this.channeltypeService.updateChnByTenant(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.channelDialog = false;
              this.isProcess = false;
              this.refreshingChn();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Updated',
              });
            } else {
              this.channelDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.channelDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Update Data Error -',
              detail: errorDetail,
            });
          }
        );
      }
    } else {
      this.isProcess = false;
    }
    console.log(this.groupForm);
  }

  /* Open modal edit data */
  editChannel(data: any) {
    console.log(data);
    this.groupForm = this.formBuilder.group({
      id_ch_type: [''],
      cid: ['', Validators.required],
      komi_ch_id: ['', Validators.required],
      bank_ch_type: ['', Validators.required],
      iso_merch_type: ['', Validators.required],
      bi_ch_id: ['', Validators.required],
      ip_address: ['', Validators.required],
      key: ['', Validators.required],
      description: [''],
      status_ch: [''],
      is_auth: [''],
    });

    this.groupForm.patchValue({
      id_ch_type: data['id'],
      cid: data['cid'],
      komi_ch_id: data['komi_channel_id'],
      bank_ch_type: data['bank_channel_type'],
      iso_merch_type: data['iso_merchant_type'],
      bi_ch_id: data['bi_channel_id'],
      ip_address: data['ip_address'],
      key: data['key'],
      description: data['description'],
      status_ch: data['status'],
      is_auth: data['is_authorized'],
    });

    this.channelDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delChannelDialog = true;
  }

  deleteChn() {
    this.isProcess = true;

    let chnData = this.delData;
    const payload = { chnData };
    this.channeltypeService.deleteChnByTenant(payload.chnData.id).subscribe(
      (resp: BackendResponse) => {
        this.delChannelDialog = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Success -',
          detail: 'Data Deleted',
        });
        this.refreshingChn();
        this.isProcess = false;
      },
      (error) => {
        this.delChannelDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Update Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }
}
