import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChanneltypedetailComponent } from './channeltypedetail.component';

describe('ChanneltypedetailComponent', () => {
  let component: ChanneltypedetailComponent;
  let fixture: ComponentFixture<ChanneltypedetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChanneltypedetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChanneltypedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
