import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { CityService } from 'src/app/services/komi/city.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})

export class CityComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = "";

  cityData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];

  groupForm!: FormGroup;

  cityDialog = false;
  delCityDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private cityService: CityService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      { label: 'Master Data' },
      { label: 'City' }
    ];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 }
    ];

    /* Set datatable */
    this.cols = [
      { field: 'CITY_CODE', header: 'City Code', isOrder: true },
      { field: 'CITY_NAME', header: 'City Name', isOrder: true },
      { field: 'STATUS', header: 'Status', transform: true, isOrder: false },
      { field: 'CREATED_DATE', header: 'Created Date', width: '200px', isOrder: true },
      { field: 'CREATED_BY', header: 'Created By', width: '200px', isOrder: false },
      { field: 'UPDATED_DATE', header: 'Updated Date', width: '200px', isOrder: true },
      { field: 'UPDATED_BY', header: 'Updated By', width: '200px', isOrder: false }
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 3);

    /* Get data all */
    this.refreshingCity();
  }

  refreshingCity() {
    this.isFetching = true;

    this.cityService.getAllCity().subscribe((result: BackendResponse) => {
      if (result.status == 200) {
        this.cityData = result.data;
      } else {
        this.cityData = [];
        let objtmp = {
          "CITY_CODE": "No records",
          "CITY_NAME": "No records",
          "STATUS": "No records",
          "CREATED_DATE": "No records",
          "CREATED_BY": "No records",
          "UPDATED_DATE": "No records",
          "UPDATED_BY": "No records"
        };
        this.cityData.push(objtmp);
      }
      this.isFetching = false;
    },
      (error) => {
        this.isFetching = false;
        this.messageService.add({ severity: 'error', summary: 'Get Data Error -', detail: 'Internal server error' });
      });
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_city: [''],
      kode_kota: ['', Validators.required],
      nama_kota: ['', Validators.required],
      status: [''],
    });
    this.cityDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;
    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          cityCode: this.groupForm.get('kode_kota')?.value,
          cityName: this.groupForm.get('nama_kota')?.value,
          status: this.groupForm.get('status')?.value
        };
        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.cityService.insertCity(payload).subscribe((result: BackendResponse) => {
          if (result.status == 200) {
            this.cityDialog = false;
            this.isProcess = false;
            this.refreshingCity();
            this.messageService.add({ severity: 'success', summary: 'Success -', detail: 'Data Inserted' });
          } else {
            this.cityDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Failed -', detail: result.data });
          }
        },
          (error) => {
            this.cityDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Insert Data Error -', detail: 'Internal server error' });
          });
      } else {
        payload = {
          cityCode: this.groupForm.get('kode_kota')?.value,
          cityName: this.groupForm.get('nama_kota')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_city')?.value
        };
        this.cityService.updateCity(payload).subscribe((result: BackendResponse) => {
          if (result.status == 200) {
            this.cityDialog = false;
            this.isProcess = false;
            this.refreshingCity();
            this.messageService.add({ severity: 'success', summary: 'Success -', detail: 'Data Updated' });
          } else {
            this.cityDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Failed -', detail: result.data });
          }
        },
          (error) => {
            this.cityDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Update Data Error -', detail: 'Internal server error' });
          });
      }
    }
  }

  editCity(data: any) {
    this.groupForm = this.formBuilder.group({
      id_city: [''],
      kode_kota: ['', Validators.required],
      nama_kota: ['', Validators.required],
      status: [''],
    });

    this.groupForm.patchValue({
      kode_kota: data["CITY_CODE"],
      nama_kota: data["CITY_NAME"],
      status: data["STATUS"],
      id_city: data["ID"]
    });

    this.cityDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delCityDialog = true;
  }

  deleteCity() {
    this.isProcess = true;

    let dataCity = this.delData;
    const payload = { dataCity };
    this.cityService.deleteCity(payload.dataCity.ID).subscribe((resp: BackendResponse) => {
      this.delCityDialog = false;
      this.messageService.add({ severity: 'success', summary: 'Success -', detail: 'Data Deleted' });
      this.refreshingCity();
    },
      (error) => {
        this.delCityDialog = false;
        this.isProcess = false;
        this.messageService.add({ severity: 'error', summary: 'Update Data Error -', detail: 'Internal server error' });
      });
  }
}
