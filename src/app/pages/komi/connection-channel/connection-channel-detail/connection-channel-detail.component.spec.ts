import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionChannelDetailComponent } from './connection-channel-detail.component';

describe('ConnectionChannelDetailComponent', () => {
  let component: ConnectionChannelDetailComponent;
  let fixture: ComponentFixture<ConnectionChannelDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectionChannelDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionChannelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
