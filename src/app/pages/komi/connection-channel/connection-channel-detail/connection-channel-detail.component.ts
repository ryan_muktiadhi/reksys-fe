import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ConnectionchannelService } from '../../../../services/komi/connectionchannel.service';

@Component({
  selector: 'app-connection-channel-detail',
  templateUrl: './connection-channel-detail.component.html',
  styleUrls: ['./connection-channel-detail.component.scss'],
})
export class ConnectionChannelDetailComponent implements OnInit {
  home!: MenuItem;
  extraInfo: any = {};
  isEdit: boolean = false;
  conChannelId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  groupForm!: FormGroup;
  submitted = false;
  userInfo: any = {};
  tokenID: string = '';
  old_code: any = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    public conChannelService: ConnectionchannelService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Connection Channel',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.groupForm = this.formBuilder.group({
        // channel_id: [{ value: '', disabled: true }, Validators.required],
        channel_id: ['', Validators.required],
        channel_name: ['', Validators.required],
        channel_type: ['', Validators.required],
        merchant_code: ['', Validators.required],
        secret_key: ['', Validators.required],
      });

      if (this.isEdit) {
        this.groupForm = this.formBuilder.group({
          channel_id: [{ value: '', disabled: true }, Validators.required],
          //channel_id : ['', Validators.required],
          channel_name: ['', Validators.required],
          channel_type: ['', Validators.required],
          merchant_code: ['', Validators.required],
          secret_key: ['', Validators.required],
        });
        //console.log(this.activatedRoute.snapshot.paramMap.get('id'));
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.conChannelId = this.activatedRoute.snapshot.paramMap.get('id');
          this.conChannelService
            .getConnectionChannelbyId(this.conChannelId)
            .subscribe(async (result: BackendResponse) => {
              //console.log('>>>>>>>??? ' + JSON.stringify(result));
              this.groupForm.patchValue({
                channel_id: result.data.channel.channel_id,
                channel_name: result.data.channel.channel_name,
                channel_type: result.data.channel.channel_type,
                merchant_code: result.data.channel.merchant_code,
                secret_key: result.data.channel.secret_key,
              });
              this.old_code = result.data.channel_id;
            });
        }
      }
    });
  }

  get f() {
    return this.groupForm.controls;
  }

  onSubmit(status) {
    this.submitted = true;
    if (this.groupForm.valid) {
      let payload = {};

      if (!this.isEdit) {
        payload = {
          channelId: this.groupForm.get('channel_id')?.value,
          channelName: this.groupForm.get('channel_name')?.value,
          channelType: this.groupForm.get('channel_type')?.value,
          merchantCode: this.groupForm.get('merchant_code')?.value,
          secretKey: this.groupForm.get('secret_key')?.value,
          active: 3,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.conChannelService
          .insertConnectionChannel(payload)
          .subscribe((result: BackendResponse) => {
            this.location.back();
          });
      } else {
        payload = {
          channelId: this.groupForm.get('channel_id')?.value,
          channelName: this.groupForm.get('channel_name')?.value,
          channelType: this.groupForm.get('channel_type')?.value,
          merchantCode: this.groupForm.get('merchant_code')?.value,
          secretKey: this.groupForm.get('secret_key')?.value,
          active: 7,
        };
        this.conChannelService
          .updateConnectionChannel(payload)
          .subscribe((result: BackendResponse) => {
            this.location.back();
          });
      }
    }
  }
}
