import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionChannelComponent } from './connection-channel.component';

describe('ConnectionChannelComponent', () => {
  let component: ConnectionChannelComponent;
  let fixture: ComponentFixture<ConnectionChannelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectionChannelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionChannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
