import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ConnectionchannelService } from '../../../services/komi/connectionchannel.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-connection-channel',
  templateUrl: './connection-channel.component.html',
  styleUrls: ['./connection-channel.component.scss'],
})
export class ConnectionChannelComponent implements OnInit {
  isdataexist = false;
  display = false;
  displayPrv = false;
  selectedconChannel: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  conChannelData: any[] = [];
  conChannelheader: any = [
    { label: 'Channel', sort: 'channel_id' },
    { label: 'Channel Name', sort: 'channel_name' },
    { label: 'Channel Type', sort: 'channel_type' },
    { label: 'Merchant Code', sort: 'merchant_code' },
    { label: 'Active', sort: 'Active' },
  ];
  conChannelcolname: any = [
    'channel_id',
    'channel_name',
    'channel_type',
    'merchant_code',
    'active',
  ];
  conChannelcolhalign: any = ['', '', '', '', 'p-text-center'];
  conChannelcolwidth: any = ['', '', '', '', ''];
  conChannellinghref: any = { url: '#', label: 'Application' };
  conChannelactionbtn: any = [1, 1, 1, 1, 0, 1];
  conChanneladdbtn = { route: 'detail', label: 'Add Data' };

  conChannelTypeSelected: any = [];
  conChannelType: any = [];
  conChannelIdSelected: any = [];
  conChannelId: any = [];
  viewApprove = false;

  constructor(
    private router: Router,
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public conChannelService: ConnectionchannelService,
    private aclMenuService: AclmenucheckerService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: ' Connection Channel' }];
    this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
      console.log(dataacl);
      var count = Object.keys(dataacl.acl).length;
      if (count > 0) {
        this.conChannelactionbtn[0] = dataacl.acl.create;
        this.conChannelactionbtn[1] = dataacl.acl.read;
        this.conChannelactionbtn[2] = dataacl.acl.update;
        this.conChannelactionbtn[3] = dataacl.acl.delete;
        this.conChannelactionbtn[4] = dataacl.acl.view;
        this.conChannelactionbtn[5] = dataacl.acl.approval;
      }
    });
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshtheConnection();

    this.conChannelService
      .getConnectionChannelbyTenant()
      .subscribe((response: BackendResponse) => {
        console.log('>>>>>>>qq ' + JSON.stringify(response));
        if (response.status == 200) {
          this.conChannelId = response.data;
          console.log(response.data);
        } else {
          this.conChannelId = [];
        }
      });

    this.conChannelService
      .getConnectionChannelbyTenant()
      .subscribe((response: BackendResponse) => {
        console.log('>>>>>>>qq ' + JSON.stringify(response));
        if (response.status == 200) {
          this.conChannelType = response.data;
          console.log(response.data);
        } else {
          this.conChannelType = [];
        }
      });
  }

  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedconChannel = data;
  }

  refreshtheConnection() {
    this.isFetching = true;
    console.log('>>>>>>> Refresh sysParam ');
    this.conChannelService
      .getAllConnectionChannel()
      .subscribe((result: BackendResponse) => {
        //console.log('>>>>>>> ' + JSON.stringify(result));

        if (result.status === 202) {
          this.conChannelData = [];
          let respo = {
            channel_id: 'No records',
            channel_name: 'No records',
            channel_type: 'No records',
            merchant_code: 'No records',
            active: 'No records',
          };
          this.conChannelData.push(respo);
        } else {
          console.log('>>>>>>Channel Response');
          this.conChannelData = result.data.channel;
        }
        this.isFetching = false;
      });
  }

  deleteConnectionChannel() {
    console.log(this.selectedconChannel);
    const conChannel = this.selectedconChannel;
    const payload = { conChannel };
    this.conChannelService
      .deleteConnectionChannel(payload.conChannel)
      .subscribe((resp: BackendResponse) => {
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshtheConnection();
      });
    console.log('?????????', payload.conChannel);
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedconChannel = data;
  }

  refreshData() {
    this.isdataexist = false;
    this.isFetching = true;

    const payload: any = {};

    if (this.conChannelIdSelected) {
      payload.channelId = this.conChannelIdSelected.channel_id;
    }
    console.log(this.conChannelIdSelected.channel_id);

    if (this.conChannelTypeSelected) {
      payload.channelType = this.conChannelTypeSelected.channel_type;
    }
    console.log(this.conChannelIdSelected.channel_type);

    this.conChannelService
      .getConnectionChannelbyParam(payload)
      .subscribe((response: BackendResponse) => {
        if (response.status == 200) {
          this.isdataexist = true;
          this.conChannelData = response.data;
        } else {
          this.isdataexist = false;
          this.conChannelData = [];
          const objtmp = {
            channel_id: 'No records',
            channel_name: 'No records',
            channel_type: 'No records',
            merchant_code: 'No records',
            active: 'No records',
          };
          this.conChannelData.push(objtmp);
        }
        this.isFetching = false;
      });
  }

  removeFilter() {
    this.conChannelId = '';
    this.conChannelType = '';
    this.refreshtheConnection();
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }

  approvalData(data: any) {
    console.log(data);
    this.viewApprove = true;
    this.selectedconChannel = data;
  }

  reject() {
    console.log('reject');
    let payload = {
      channelId: this.selectedconChannel.channel_id,
      oldactive: this.selectedconChannel.active,
      idapproval: this.selectedconChannel.idapproval,
    };
    console.log(payload);

    this.conChannelService
      .rejectChannelTemp(payload)
      .subscribe((result: BackendResponse) => {
        this.refreshtheConnection();
        this.viewApprove = false;
      });
  }

  approvalSubmit() {
    console.log('approval');

    let payload = {
      channelId: this.selectedconChannel.channel_id,
      oldactive: this.selectedconChannel.active,
      idapproval: this.selectedconChannel.idapproval,
    };

    this.conChannelService
      .approveChannel(payload)
      .subscribe((result: BackendResponse) => {
        if (result.status === 200) {
          this.refreshtheConnection();
          this.viewApprove = false;
        }
      });
  }
}
