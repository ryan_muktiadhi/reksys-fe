import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';
import { EventlogService } from 'src/app/services/komi/eventlog.service';
import { DatePipe } from '@angular/common';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
@Component({
  selector: 'app-event-log',
  templateUrl: './event-log.component.html',
  styleUrls: ['./event-log.component.scss'],
})
export class EventLogComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  startDate: any;
  endDate: any;
  userId: any;
  userInfo: any = {};
  tokenID: string = '';
  limitData: any[] = [];
  prxheader: any = [
    { label: 'User Id', sort: 'userid' },
    { label: 'Event', sort: 'value' },
    { label: 'Description', sort: 'description' },
    { label: 'Date Time', sort: 'formateddate' },
  ];
  prxcolname: any = ['userid', 'value', 'description', 'formateddate'];
  prxcolhalign: any = ['', '', '', ''];
  prxcolwidth: any = ['', '', '', ''];
  prxcollinghref: any = { url: '#', label: 'Application' };
  limitactionbtn: any = [0, 0, 0, 0, 0];
  prxaddbtn = { route: 'detail', label: 'Add Data' };
  options: any = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'Event Log Audit Trail',
    useBom: true,
    noDownload: false,
    headers: ['User Id', 'Event', 'Description', 'Date Time'],
  };
  fileName: string;
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private eventService: EventlogService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Event Log' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Limit ');
    this.eventService.getAll().subscribe(async (result: BackendResponse) => {
      if (result.status === 202) {
        this.limitData = [];
        let objtmp = {
          value: 'No records',
          description: 'No records',
          formateddate: 'No records',
          userid: 'No records',
        };
        this.limitactionbtn = [0, 0, 0, 0, 0];
        this.limitData.push(objtmp);
        this.isFetching = false;
      } else {
        this.limitData = result.data;
      }
      this.isFetching = false;
    });
  }

  filterData() {
    this.isFetching = true;
    this.limitData = [];
    console.log(this.startDate, this.endDate);
    let payload: any = {};
    if (this.startDate) {
      payload.startDate = this.datePipe.transform(this.startDate, 'yyyy/MM/dd');
    }
    if (this.endDate) {
      payload.endDate = this.datePipe.transform(this.endDate, 'yyyy/MM/dd');
    }
    if (this.userId) {
      payload.userId = this.userId;
    }
    this.eventService
      .getWithFilter(payload)
      .subscribe((result: BackendResponse) => {
        if (result.data.length > 0) {
          this.limitData = result.data;
        }
        this.isFetching = false;
      });
  }

  exportexcel() {
    if (this.limitData.length > 0) {
      console.log(this.limitData);
      new ngxCsv(this.limitData, 'Event Log', this.options);
    }
  }

  exportPDF() {
    var doc = new jsPDF('l', 'mm', [297, 210]);
    var col = [['User Id', 'Event', 'Description', 'Date Time']];
    var rows = [];

    let myDate = new Date();
    let datenow = this.datePipe.transform(myDate, 'yyyyMMddHHmmss');
    this.fileName = 'pdf' + datenow + '.pdf';
    this.limitData.forEach((element) => {
      var temp = [
        element.userid,
        element.value,
        element.description,
        element.formateddate,
      ];
      rows.push(temp);
    });
    // doc.autoTable(col, rows);
    autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (rows) => {},
    });

    doc.save(this.fileName);
  }

  removeFilter() {
    this.startDate = '';
    this.endDate = '';
    this.userId = '';
    this.refreshingProxy();
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
