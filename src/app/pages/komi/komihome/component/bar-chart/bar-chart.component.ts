import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
})
export class BarChartComponent implements OnChanges {
  basicData: any;
  basicOptions: any;
  @Input() labels: string[];
  @Input() dataSets = [];
  constructor() {}

  ngOnChanges(): void {
    console.log(this.dataSets);
    this.basicData = {
      labels: this.labels,
      datasets: this.dataSets,
    };
    this.basicOptions = {
      legend: {
        position: 'bottom',
      },

      scales: {
        xAxes: [
          {
            gridLines: {},
          },
        ],
        yAxes: [{}],
      },
    };
  }
}
