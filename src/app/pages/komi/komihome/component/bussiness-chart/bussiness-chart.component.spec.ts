import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BussinessChartComponent } from './bussiness-chart.component';

describe('BussinessChartComponent', () => {
  let component: BussinessChartComponent;
  let fixture: ComponentFixture<BussinessChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BussinessChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BussinessChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
