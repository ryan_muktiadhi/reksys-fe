import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-bussiness-chart',
  templateUrl: './bussiness-chart.component.html',
  styleUrls: ['./bussiness-chart.component.scss'],
})
export class BussinessChartComponent implements OnChanges {
  param: string;
  options: any;
  basicData: {
    labels: string[];
    datasets: {
      label: string;
      data: number[];
      fill: boolean;
      borderColor: string;
      tension: number;
    }[];
  };
  dataSuccess = {
    label: 'Sucessed Transaction SLA',
    data: [],
    fill: false,
    borderColor: '#537f2d',
    pointBackgroundColor: '#FFF',
    pointBorderWidth: '2',
    tension: 0.3,
  };

  dataSuccessNotSLA = {
    label: 'Sucessed Transaction Not SLA',
    data: [],
    fill: false,
    borderColor: '#78c92c',
    pointBackgroundColor: '#FFF',
    pointBorderWidth: '2',
    tension: 0.3,
  };
  dataFailed = {
    label: 'Failed Transaction SLA',
    data: [],
    fill: false,
    borderColor: '#D32F2F',
    pointBackgroundColor: '#FFF',
    pointBorderWidth: '2',
    tension: 0.3,
  };
  dataFailedNotSLA = {
    label: 'Failed Transaction Not SLA',
    data: [],
    fill: false,
    borderColor: '#f52749',
    pointBackgroundColor: '#FFF',
    pointBorderWidth: '2',
    tension: 0.3,
  };
  dataTimeOut = {
    label: 'Time Out Transaction',
    data: [],
    fill: false,
    borderColor: '#e8a704',
    pointBackgroundColor: '#FFF',
    pointBorderWidth: '2',
    tension: 0.3,
  };

  @Input() labels: string[];
  dataSets: any = [];
  @Input() data = [];
  @Input() width: any;
  @Input() height: any;
  @Input() dateRange: any;
  @Input() trxDetail: any;
  @Input() maxTran: any;
  totalTrx: any = 0;
  constructor() {}

  ngOnChanges(): void {
    this.initChart();
  }

  async initChart() {
    console.log(this.trxDetail);
    this.dataSets = [];
    this.totalTrx = 0;
    await this.data.map(async (dt) => {
      this.dataSuccess.data.push(dt.data.tranSuccess || 0);
      this.dataSuccessNotSLA.data.push(dt.data.tranSuccessNotSLA || 0);
      this.dataFailed.data.push(dt.data.tranFailed || 0);
      this.dataFailedNotSLA.data.push(dt.data.tranFailedNotSLA || 0);
      this.dataTimeOut.data.push(dt.data.tranTimeOut || 0);
    });

    if (this.trxDetail?.length > 0) {
      await this.trxDetail.map((dt) => {
        this.totalTrx = this.totalTrx + dt.tranTotal;
      });
    }
    this.dataSets.push(this.dataSuccess);
    this.dataSets.push(this.dataSuccessNotSLA);
    this.dataSets.push(this.dataFailed);
    this.dataSets.push(this.dataFailedNotSLA);
    this.dataSets.push(this.dataTimeOut);

    this.basicData = {
      labels: this.labels,
      datasets: this.dataSets,
    };
    const DISPLAY = false;
    const BORDER = true;
    const CHART_AREA = true;
    const TICKS = true;
    this.options = {
      responsive: true,
      legend: {
        position: 'bottom',
      },

      scales: {
        xAxes: [
          {
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
        yAxes: [
          {
            display: true,
            ticks: {
              max: this.maxTran ? Math.ceil(this.maxTran / 20) * 20 : 100,
              min: 0,
              stepSize: this.maxTran ? Math.floor((this.maxTran + 20) / 5) : 20,
              beginAtZero: true,
              padding: 20,
            },
          },
        ],
      },
    };
  }
}
