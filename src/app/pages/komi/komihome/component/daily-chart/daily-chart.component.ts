import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-daily-chart',
  templateUrl: './daily-chart.component.html',
  styleUrls: ['./daily-chart.component.scss'],
})
export class DailyChartComponent implements OnChanges {
  param: string;
  options: any;
  basicData: {
    labels: string[];
    datasets: {
      label: string;
      data: number[];
      fill: boolean;
      borderColor: string;
      tension: number;
    }[];
  };
  dataSuccess = {
    label: 'Accepted Transaction',
    data: [],
    amount: [],
    fill: false,
    pointRadius: 2,
    pointBackgroundColor: '#6ECB63',
    pointBorderWidth: '2',
    borderColor: '#6ECB63',
    tension: 0.3,
    borderWidth: 3.5,
  };
  dataSuccessNotSLA = {
    label: 'Accepted Trx Not SLA',
    data: [],
    amount: [],
    fill: false,
    pointRadius: 2,
    pointBackgroundColor: '#95CD41',
    pointBorderWidth: '2',
    borderColor: '#95CD41',
    tension: 0.3,
    borderWidth: 3.5,
  };
  dataFailed = {
    label: 'Rejected Trx SLA',
    data: [],
    amount: [],
    fill: false,
    pointRadius: 2,
    pointBackgroundColor: '#FF6363',
    pointBorderWidth: '2',
    borderColor: '#FF6363',
    tension: 0.3,
    borderWidth: 3.5,
  };
  dataFailedNotSLA = {
    label: 'Rejected Trx Not SLA',
    data: [],
    amount: [],
    fill: false,
    pointRadius: 2,
    pointBackgroundColor: '#FFAB76',
    pointBorderWidth: '2',
    borderColor: '#FFAB76',
    tension: 0.3,
    borderWidth: 3.5,
  };
  dataTimeOut = {
    label: 'Time Out Transaction',
    data: [],
    amount: [],
    fill: false,
    pointRadius: 2,
    pointBackgroundColor: '#FFE162',
    pointBorderWidth: '2',
    borderColor: '#FFE162',
    tension: 0.3,
    borderWidth: 3.5,
  };

  @Input() labels: string[];
  dataSets: any = [];
  @Input() data = [];
  @Input() width: any;
  @Input() height: any;
  @Input() dateRange: any;
  @Input() trxDetail: any;
  @Input() maxTran: any;
  totalTrx: any = 0;
  constructor() {}

  ngOnChanges(): void {
    this.initChart();
  }

  async initChart() {
    console.log(this.maxTran);
    this.dataSets = [];
    this.totalTrx = 0;
    this.basicData = null;
    if (this.data) {
      await this.data.map(async (dt) => {
        this.dataSuccess.data.push(dt.data.tranSuccess || 0);
        this.dataSuccess.amount.push(dt.data.tranSuccessAmount || 0);
        this.dataSuccessNotSLA.data.push(dt.data.tranSuccessNotSLA || 0);
        this.dataSuccessNotSLA.amount.push(
          dt.data.tranSuccessNotSLAAmount || 0
        );
        this.dataFailed.data.push(dt.data.tranFailed || 0);
        this.dataFailed.amount.push(dt.data.tranFailedAmount || 0);
        this.dataFailedNotSLA.data.push(dt.data.tranFailedNotSLA || 0);
        this.dataFailedNotSLA.amount.push(dt.data.dataFailedNotSLAAmount || 0);
        this.dataTimeOut.data.push(dt.data.tranTimeOut || 0);
        this.dataTimeOut.amount.push(dt.data.tranTimeOutAmount || 0);
      });
    }
    if (this.trxDetail?.length > 0) {
      await this.trxDetail.map((dt) => {
        this.totalTrx = this.totalTrx + dt.tranTotal;
      });
    }
    this.dataSets.push(this.dataSuccess);
    this.dataSets.push(this.dataSuccessNotSLA);
    this.dataSets.push(this.dataFailed);
    this.dataSets.push(this.dataFailedNotSLA);
    this.dataSets.push(this.dataTimeOut);

    this.basicData = {
      labels: this.labels,
      datasets: this.dataSets,
    };
    const DISPLAY = false;
    const BORDER = true;
    const CHART_AREA = true;
    const TICKS = true;
    this.options = {
      responsive: true,
      legend: {
        position: 'top',
        align: 'end',
        labels: {
          usePointStyle: true,
          boxWidth: 6,
        },
      },
      tooltips: {
        titleSpacing: 10,
        titleFontFamily: 'Poppins-Regular',
        titleFontColor: '#000',
        bodySpacing: 10,
        bodyFontFamily: 'Poppins-Regular',
        bodyFontColor: '#000',
        backgroundColor: 'rgba(255, 255, 255, .8)',
        borderColor: 'rgba(13, 99, 195, 1)',
        borderWidth: 1,
        displayColors: false,
        // border: ''
        callbacks: {
          afterBody: function (t, d) {
            let totalAmount: any = 0;
            if (t.length == 1) {
              totalAmount = d.datasets[t[0].datasetIndex].amount[t[0].index];
              totalAmount = new Intl.NumberFormat('id-ID', {
                style: 'currency',
                currency: 'IDR',
              }).format(totalAmount);
              return `Total Transaction Amount : ${totalAmount}`;
            } else if (t.length > 1) {
              t.map((dt) => {
                totalAmount =
                  parseInt(totalAmount) +
                  parseInt(d.datasets[dt.datasetIndex].amount[dt.index]);
              });
              totalAmount = new Intl.NumberFormat('id-ID', {
                style: 'currency',
                currency: 'IDR',
              }).format(totalAmount);
              return `Total Transaction Amount : ${totalAmount}`;
            }
          },
        },
      },
      scales: {
        xAxes: [
          {
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
        yAxes: [
          {
            display: true,
            ticks: {
              max: this.maxTran ? Math.ceil(this.maxTran / 20) * 20 : 100,
              min: 0,
              stepSize: this.maxTran ? Math.floor((this.maxTran + 10) / 5) : 20,

              beginAtZero: true,
              padding: 20,
            },
          },
        ],
      },
    };
  }
}
