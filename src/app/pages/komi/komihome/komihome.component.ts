import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { PrefundDashboardService } from 'src/app/services/komi/prefund-dashboard.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { any } from 'codelyzer/util/function';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import { IntegrateNotifService } from '../../../services/integrate-notif.service';
import { Observable, interval, Subscription } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-komihome',
  templateUrl: './komihome.component.html',
  styleUrls: ['./komihome.component.scss'],
})
export class KomihomeComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  userInfo: any = {};
  app: any = {};
  tokenID: string = '';
  home!: MenuItem;
  param: string;

  dailyData: any;
  labels: string[] = [];
  dataSets: string[] = [];

  weeklyData: any;
  weeklyLabels: string[] = [];
  weeklyDataSets: string[] = [];

  monthlyData: any;
  monthlyLabels: string[] = [];
  monthlylDataSets: string[] = [];

  tpmLabels: string[] = [];
  tpmDataSets: string[] = [];

  dailyRange: any;
  dailyDetail: any;
  weeklyRange: any;
  weeklyDetail: any;
  monthlyRange: any;
  monthlyDetail: any;
  businessDetail: any;
  businessData: any;
  businessLabels: string[] = [];
  businessDataSets: string[] = [];
  monitoringUsed: any[] = [];
  monitoringUsedPortal: any[] = [];
  isColorDisk: number = 0;
  isColorMemory: number = 0;
  isColorProcessor: number = 0;

  isColorDiskPortal: number = 0;
  isColorMemoryPortal: number = 0;
  isColorProcessorPortal: number = 0;

  diskMax: Number = 0;
  dataConfig = {};
  objTest = {};

  gaugeType = 'semi';
  gaugeValue = 0;
  gaugeMax = 0;
  gaugeMin = 0;
  upperLimit = 0;
  referenceLimit = 0;
  amberLevel = 0;
  redLevel = 0;
  gaugeStatus: string;
  stats = false;
  // gaugeAppendText = `/${this.gaugeMax}`;

  thresholdConfig = {
    //'100': { color: 'red' },
    //'900': { color: 'orange' },
    //'5000': { color: 'green' },
  };

  ciStatus: number = 0;
  coreStatus: number = 0;

  usrheader: any = [
    { label: 'No', sort: 'id' },
    { label: 'Surrounding System', sort: 'surrounding_system' },
    { label: 'Status', sort: 'status' },
  ];
  usrcolname: any = ['id', 'surrounding_system', 'status'];
  usrcolhalign: any = ['p-text-center', 'p-text-center', 'p-text-center'];
  userlist: any = [
    // {
    //   id: 1,
    //   surrounding_system: 'Core Banking',
    //   status: 'Active',
    // },
    // {
    //   id: 2,
    //   surrounding_system: 'CI-Connector',
    //   status: 'Active',
    // },
  ];

  payload: any = {
    transactionId: '000001',
    dateTime: '2021-12-21T11:52:32.923',
    merchantType: '6666',
    terminalId: 'KOMI000001',
    noRef: 'KOM21122111523210000',
    //accountNumber: '00102000752',
  };

  // reqEmail = {
  //   kodestatus: "INFO",
  //   message: "testing"
  // };

  selectWidgetModal: boolean = false;
  widgetList: any[] = [
    {
      name: 'Total Monthly Transaction',
      value: false,
      model: 'monthlyTransaction',
    },
    {
      name: 'Total Weekly Transaction',
      value: false,
      model: 'weeklyTransaction',
    },
    {
      name: 'Total Daily Transaction',
      value: false,
      model: 'dailyTransaction',
    },
    {
      name: 'TPM (Transaction Per Minutes)',
      value: false,
      model: 'TPM',
    },
    {
      name: 'Business Process',
      value: false,
      model: 'businessTransaction',
    },
    {
      name: 'Surrounding System ',
      value: false,
      model: 'sourSys',
    },
    {
      name: 'Prefund Management',
      value: false,
      model: 'prefMgm',
    },
    {
      name: 'Task Manage Core',
      value: false,
      model: 'TMC',
    },
    {
      name: 'Task Manager Portal',
      value: false,
      model: 'TMP',
    },
  ];
  selectedWidgets: any[] = [];

  usrcolwidth: any = ['', '', '', '', ''];
  usractionbtn: any = [0, 0, 0, 0, 0];

  usraddbtn = { route: 'detail', label: 'Add Data' };
  categoryChart: { name: string; code: string }[];
  selectedCategoryChart: any = { name: 'Daily Chart', code: 'daily' };

  nonTrxWidget: boolean = false;
  trxWidget: boolean = false;

  tpmSuccess = {
    label: 'Success Transaction',
    backgroundColor: '#537f2d',
    data: [],
  };

  tpmFailed = {
    label: 'Failed Transaction',
    backgroundColor: '#d40b0b',
    data: [],
  };

  tpmTimeOut = {
    label: 'Timeout Transaction',
    backgroundColor: '#FCD116',
    data: [],
  };

  tpsData: any = [];
  private updateSubscription: Subscription;
  private chartSubscription: Subscription;
  widgets: any = null;
  dailybusinessRange: { startDate: string; endDate: string };
  monthlyMaxTran: any;
  weeklyMaxTran: any;
  dailyMaxTran: any;

  constructor(
    private route: ActivatedRoute,
    private authservice: AuthService,
    private prefundDashboardService: PrefundDashboardService,
    private integrateNotif: IntegrateNotifService,
    private aclMenuService: AclmenucheckerService,
    private dashboardService: DashboardService
  ) {}

  ngOnInit(): void {
    this.param = this.route.snapshot.params.config;
    // console.log('>>>>'+this.param);
    this.home = { label: 'Dashboard', routerLink: '/' };
    console.log(this.updateSubscription);
    this.categoryChart = [
      { name: 'Daily Chart', code: 'daily' },
      { name: 'Weekly Chart', code: 'weekly' },
      { name: 'Monthly Chart', code: 'monthly' },
    ];
    this.initWidget();
    console.log(this.widgets);
  }

  async initWidget() {
    const savedWidget = localStorage.getItem('selectedWidget');
    if (savedWidget) {
      let savedwidget = JSON.parse(localStorage.getItem('selectedWidget'));
      let widgetShow = JSON.parse(localStorage.getItem('widgetsList'));
      this.trxWidget = widgetShow.trxWidget;
      this.nonTrxWidget = widgetShow.nonTrxWidget;
      const filterArray = await this.widgetList.filter((el) => {
        return savedwidget.some((f) => {
          if (f.model === el.model) {
            el.value = true;
          }
          return f.model === el.model;
        });
      });
      this.selectedWidgets = filterArray;
      if (this.selectedWidgets.length > 0) {
        const source = interval(60000);
        this.chartSubscription = source.subscribe((x) => {});
        this.widgets = {};
        await this.selectedWidgets.map(async (widget: any) => {
          this.widgets[widget.model] = true;
          switch (widget.model) {
            case 'monthlyTransaction':
              this.getMonthlyChart();
              const month = source.subscribe((x) => this.getMonthlyChart());
              this.chartSubscription.add(month);
              break;
            case 'weeklyTransaction':
              this.getWeeklyChart();
              const weekSubs = source.subscribe((x) => this.getWeeklyChart());
              this.chartSubscription.add(weekSubs);
              break;
            case 'dailyTransaction':
              this.getDailyChart();
              const dailySubs = source.subscribe((x) => this.getDailyChart());
              this.chartSubscription.add(dailySubs);
              break;
            case 'TPM':
              this.getTPSData();
              const TPSsubs = source.subscribe((x) => this.getTPSData());
              this.chartSubscription.add(TPSsubs);
              break;
            case 'businessTransaction':
              this.getBusinessChart();
              const bussinessSubs = source.subscribe((x) =>
                this.getBusinessChart()
              );
              this.chartSubscription.add(bussinessSubs);
              break;
            case 'sourSys':
              this.getCheckService();
              const sourSubs = source.subscribe((x) => this.getCheckService());
              this.chartSubscription.add(sourSubs);
              break;
            case 'prefMgm':
              this.getBalanceInquiry();
              this.getProp();
              const prefSubs = source.subscribe((x) => {
                this.getBalanceInquiry();
                this.getProp();
              });
              this.chartSubscription.add(prefSubs);
              break;
            case 'TMC':
              this.getMonitoringUsed();
              const tmcSubs = source.subscribe((x) => this.getMonitoringUsed());
              this.chartSubscription.add(tmcSubs);
              break;
            case 'TMP':
              this.getMonitoringUsedPortal();
              const tmpSubs = source.subscribe((x) =>
                this.getMonitoringUsedPortal()
              );
              this.chartSubscription.add(tmpSubs);
              break;
            default:
            // code block
          }
        });
      }
      console.log(this.widgets);
    }
  }

  async updateWidgets() {
    this.widgets = {};
    this.chartSubscription?.unsubscribe();
    this.selectedWidgets = [];
    this.trxWidget = false;
    this.nonTrxWidget = false;
    this.widgetList.map((wg: any) => {
      if (wg.value == true) {
        this.selectedWidgets.push(wg);
      }
    });
    if (this.selectedWidgets.length > 0) {
      const source = interval(60000);
      this.chartSubscription = source.subscribe((x) => {});
      await this.selectedWidgets.map(async (widget: any) => {
        this.widgets[widget.model] = true;
        switch (widget.model) {
          case 'monthlyTransaction':
            this.getMonthlyChart();
            const month = source.subscribe((x) => this.getMonthlyChart());
            this.chartSubscription.add(month);
            this.trxWidget = true;
            break;
          case 'weeklyTransaction':
            this.getWeeklyChart();
            const weekSubs = source.subscribe((x) => this.getWeeklyChart());
            this.chartSubscription.add(weekSubs);
            this.trxWidget = true;
            break;
          case 'dailyTransaction':
            this.getDailyChart();
            const dailySubs = source.subscribe((x) => this.getDailyChart());
            this.chartSubscription.add(dailySubs);
            this.trxWidget = true;
            break;
          case 'TPM':
            this.getTPSData();
            const TPSsubs = source.subscribe((x) => this.getTPSData());
            this.chartSubscription.add(TPSsubs);
            this.trxWidget = true;
            break;
          case 'businessTransaction':
            this.getBusinessChart();
            const bussinessSubs = source.subscribe((x) =>
              this.getBusinessChart()
            );
            this.chartSubscription.add(bussinessSubs);
            this.trxWidget = true;
            break;
          case 'sourSys':
            this.getCheckService();
            const sourSubs = source.subscribe((x) => this.getCheckService());
            this.chartSubscription.add(sourSubs);
            this.nonTrxWidget = true;
            break;
          case 'prefMgm':
            this.getBalanceInquiry();
            this.getProp();
            const prefSubs = source.subscribe((x) => {
              this.getBalanceInquiry();
              this.getProp();
            });
            this.chartSubscription.add(prefSubs);
            this.nonTrxWidget = true;
            break;
          case 'TMC':
            this.getMonitoringUsed();
            const tmcSubs = source.subscribe((x) => this.getMonitoringUsed());
            this.chartSubscription.add(tmcSubs);
            this.nonTrxWidget = true;
            break;
          case 'TMP':
            this.getMonitoringUsedPortal();
            const tmpSubs = source.subscribe((x) =>
              this.getMonitoringUsedPortal()
            );
            this.chartSubscription.add(tmpSubs);
            this.nonTrxWidget = true;
            break;
          default:
          // code block
        }
      });
      localStorage.removeItem('selectedWidget');
      let widgetShow = {
        trxWidget: this.trxWidget,
        nonTrxWidget: this.nonTrxWidget,
      };
      localStorage.setItem(
        'selectedWidget',
        JSON.stringify(this.selectedWidgets)
      );
      localStorage.setItem('widgetsList', JSON.stringify(widgetShow));
    } else {
      this.widgets = null;
      localStorage.removeItem('selectedWidget');
      localStorage.removeItem('widgetsList');
    }

    this.selectWidgetModal = !this.selectWidgetModal;
  }

  async selectWidget(data: any) {
    console.log();
    const index = await this.widgetList.findIndex((widget) => {
      return widget.model == data.model;
    });
    console.log(index);
  }

  getTPSData() {
    console.log('GetTps');
    let startDate = new Date();
    startDate = new Date(startDate.setHours(startDate.getHours() - 5));
    var startDateFormat = moment(startDate).format('YYYY-MM-DD HH:mm:ss');
    let endDate = new Date();
    var endDateFormat = moment(endDate).format('YYYY-MM-DD HH:mm:ss');
    let payload = {
      startDate: startDateFormat,
      endDate: endDateFormat,
    };
    this.dashboardService.getTPS(payload).subscribe(async (resp) => {
      console.log(resp);
      if (resp.data.length > 0) {
        let labels = [];
        let dataSets = [];
        await resp.data.map(async (dt) => {
          let label;
          let hour = dt.hour;
          label = hour;
          await dt.dataHour.map(async (dtH) => {
            let minutes = dtH.minutes;
            label = label + ':' + minutes;
            labels.push(label);
            label = hour;

            this.tpmSuccess.data.push(dtH.tranSuccess);
            this.tpmFailed.data.push(dtH.tranFailed);
            this.tpmTimeOut.data.push(dtH.tranTimeOut);
          });
        });
        dataSets.push(this.tpmSuccess);
        dataSets.push(this.tpmFailed);
        dataSets.push(this.tpmTimeOut);

        this.tpmLabels = labels;
        this.tpmDataSets = dataSets;
      }
    });
  }

  getDailyChart() {
    console.log('getDaily');
    let currenDate = new Date();
    let startDateDaily = new Date(currenDate.setHours(0, 0, 0));
    let endDateDaily = new Date(currenDate.setHours(23, 59, 59));
    let startDateDailyFormated = moment(startDateDaily).format(
      'YYYY-MM-DD HH:mm:ss'
    );
    let endDateDailyFormated = moment(endDateDaily).format(
      'YYYY-MM-DD HH:mm:ss'
    );
    let dailyPayload = {
      startDate: startDateDailyFormated,
      endDate: endDateDailyFormated,
    };
    this.dailyRange = dailyPayload;
    this.dashboardService.getDaily(dailyPayload).subscribe(async (resp) => {
      this.dailyDetail = resp.detail;
      if (resp.data.length > 0) {
        let labels = [];
        this.dailyData = resp.data;
        let maxTran = 0;
        if (resp.detail) {
          await resp.detail.map((detailData) => {
            maxTran =
              maxTran > detailData.maxTran ? maxTran : detailData.maxTran;
          });
          console.log(maxTran);
        }
        await resp.data.map(async (dt) => {
          let label;
          let hour = dt.hour;
          label = hour;
          labels.push(label);
        });
        this.labels = labels;
        this.dailyMaxTran = maxTran;
      }
    });
  }

  getBusinessChart() {
    let currenDate = new Date();
    let startDateDaily = new Date(currenDate.setHours(0, 0, 0));
    let endDateDaily = new Date(currenDate.setHours(23, 59, 59));
    let startDateDailyFormated = moment(startDateDaily).format(
      'YYYY-MM-DD HH:mm:ss'
    );
    let endDateDailyFormated = moment(endDateDaily).format(
      'YYYY-MM-DD HH:mm:ss'
    );
    let dailyPayload = {
      startDate: startDateDailyFormated,
      endDate: endDateDailyFormated,
    };
    this.dailybusinessRange = dailyPayload;
    this.dashboardService.getBusiness(dailyPayload).subscribe(async (resp) => {
      if (resp.data.length > 0) {
        let labels = [];
        this.businessDetail = resp.detail;

        this.businessData = resp.data;
        await resp.data.map(async (dt) => {
          let label;
          let hour = dt.hour;
          label = hour;
          labels.push(label);
        });
        this.businessLabels = labels;
      }
    });
  }

  getWeeklyChart() {
    let currenDate = new Date();
    let startDateWeekly = new Date(currenDate.setHours(0, 0, 0));
    let day = currenDate.getDay();
    startDateWeekly.setDate(startDateWeekly.getDate() - day);
    let endDateWeekly = new Date(currenDate.setHours(23, 59, 59));

    let startDateWeeklyFormated = moment(startDateWeekly).format(
      'YYYY-MM-DD HH:mm:ss'
    );
    let endDateWeeklyFormated = moment(endDateWeekly).format(
      'YYYY-MM-DD HH:mm:ss'
    );

    let weeklyPayload = {
      startDate: startDateWeeklyFormated,
      endDate: endDateWeeklyFormated,
    };
    this.weeklyRange = weeklyPayload;

    this.dashboardService.getWeekly(weeklyPayload).subscribe(async (resp) => {
      if (resp.data.length > 0) {
        let labels = [];
        this.weeklyDetail = resp.detail;
        this.weeklyData = resp.data;
        let maxTran = 0;
        if (resp.detail) {
          await resp.detail.map((detailData) => {
            maxTran =
              maxTran > detailData.maxTran ? maxTran : detailData.maxTran;
          });
          console.log(maxTran);
        }
        await resp.data.map(async (dt) => {
          let label;
          let day = dt.day;
          label = day;
          labels.push(label);
        });
        this.weeklyLabels = labels;
        this.weeklyMaxTran = maxTran;
      }
    });
  }

  getMonthlyChart() {
    let currenDate = new Date();
    let startDateMonthly = new Date(currenDate.setHours(0, 0, 0));
    startDateMonthly.setDate(1);
    let endDateMonthly = new Date(currenDate.setHours(23, 59, 59));

    let startDateMonthlyFormated = moment(startDateMonthly).format(
      'YYYY-MM-DD HH:mm:ss'
    );
    let endDateMonthlyFormated = moment(endDateMonthly).format(
      'YYYY-MM-DD HH:mm:ss'
    );
    let monthlyPayload = {
      startDate: startDateMonthlyFormated,
      endDate: endDateMonthlyFormated,
    };
    this.monthlyRange = monthlyPayload;
    this.dashboardService.getMonthly(monthlyPayload).subscribe(async (resp) => {
      if (resp.data.length > 0) {
        this.monthlyDetail = resp.detail;
        let labels = [];
        this.monthlyData = resp.data;
        let maxTran = 0;
        if (resp.detail) {
          await resp.detail.map((detailData) => {
            maxTran =
              maxTran > detailData.maxTran ? maxTran : detailData.maxTran;
          });
          console.log(maxTran);
        }
        await resp.data.map(async (dt) => {
          let label;
          let date = dt.date;
          label = date;
          labels.push(label);
        });

        this.monthlyLabels = labels;
        this.monthlyMaxTran = maxTran;
      }
    });
  }

  getMonitoringUsed() {
    this.prefundDashboardService
      .getDashboardMonitoring()
      .subscribe((result: any) => {
        // console.log('MONITORING USED >>>>>');
        // console.log(JSON.stringify(result.disk));
        this.monitoringUsed = [];

        //memory
        var nMemo = JSON.stringify(result.memory.usedMemory);
        var hMemo = nMemo.replace(/[%'"]/g, '');
        //processor
        var nProc = JSON.stringify(result.processor.used);
        var hProc = nProc.replace(/[%'"]/g, '');
        //disk
        var nDisk = JSON.stringify(result.disk.percent);
        var hDisk = nDisk.replace(/[%'"]/g, '');

        // logika color memo
        if (Number(hProc) >= 68) {
          this.isColorProcessor = 0;
        } else if (Number(hProc) >= 64) {
          this.isColorProcessor = 1;
        } else {
          this.isColorProcessor = 2;
        }

        // logika color proc / cpu
        if (Number(hMemo) >= 68) {
          this.isColorMemory = 0;
        } else if (Number(hMemo) >= 64) {
          this.isColorMemory = 1;
        } else {
          this.isColorMemory = 2;
        }

        // logika color disk
        if (Number(hDisk) >= 68) {
          this.isColorDisk = 0;
        } else if (Number(hDisk) >= 64) {
          this.isColorDisk = 1;
        } else {
          this.isColorDisk = 2;
        }

        let objtmp = {
          percentMemory: Number(hMemo),
          percentProcessor: Number(hProc),
          percentDisk: Number(hDisk),
        };

        this.monitoringUsed.push(objtmp);

        // console.log('MONITORING USED >>>>>>', this.monitoringUsed);
      });
  }

  getMonitoringUsedPortal() {
    this.monitoringUsedPortal = [];

    this.prefundDashboardService.getDashboardMonitoringPortal().subscribe(
      (result: any) => {
        // console.log('MONITORING USED PORTAL >>>>>');
        // console.log(JSON.stringify(result.data));
        result = {
          status: 200,
          data: {
            processor: { used: 0.5 },
            memory: {
              totalMemMb: 15883.14,
              usedMemMb: 901.83,
              freeMemMb: 14981.31,
              usedMemPercentage: 5.68,
              freeMemPercentage: 94.32,
            },
            disk: {
              totalGb: '47.8',
              usedGb: '6.8',
              freeGb: '41.0',
              usedPercentage: '14.3',
              freePercentage: '85.7',
            },
          },
        };
        this.monitoringUsedPortal = [];
        //memory
        var nMemo = JSON.stringify(result.data.memory.usedMemPercentage);
        var hMemo = nMemo.replace(/[%'"]/g, '');
        //processor
        var nProc = JSON.stringify(result.data.processor.used);
        var hProc = nProc.replace(/[%'"]/g, '');
        //disk
        var nDisk = JSON.stringify(result.data.disk.usedPercentage);
        var hDisk = nDisk.replace(/[%'"]/g, '');
        // logika color memo
        if (Number(hProc) >= 68) {
          this.isColorProcessorPortal = 0;
        } else if (Number(hProc) >= 64) {
          this.isColorProcessorPortal = 1;
        } else {
          this.isColorProcessorPortal = 2;
        }
        // logika color proc / cpu
        if (Number(hMemo) >= 68) {
          this.isColorMemoryPortal = 0;
        } else if (Number(hMemo) >= 64) {
          this.isColorMemoryPortal = 1;
        } else {
          this.isColorMemoryPortal = 2;
        }
        // logika color disk
        if (Number(hDisk) >= 68) {
          this.isColorDiskPortal = 0;
        } else if (Number(hDisk) >= 64) {
          this.isColorDiskPortal = 1;
        } else {
          this.isColorDiskPortal = 2;
        }
        let objtmp = {
          percentMemoryPortal: Number(hMemo),
          percentProcessorPortal: Number(hProc),
          percentDiskPortal: Number(hDisk),
        };
        this.monitoringUsedPortal.push(objtmp);
        // console.log('MONITORING USED PORTAL >>>>>>', this.monitoringUsedPortal);
      },
      (err) => {}
    );
  }

  getCheckService() {
    this.prefundDashboardService
      .getCheckService()
      .subscribe((result: BackendResponse) => {
        console.log('Check Service >>>>>');
        console.log(JSON.stringify(result));
        console.log(result.data.KomiStatus);

        console.log(result.data.KomiStatus.CiConnector);
        console.log(result.data.KomiStatus.CoreBankSystem);
        let ciStatus = result.data.KomiStatus.CiConnector;
        let coreStatus = result.data.KomiStatus.CoreBankSystem;
        if (ciStatus == 'Connected') {
          this.ciStatus = 3;
        } else if (ciStatus == 'NotConnected') {
          this.ciStatus = 4;
        } else {
          this.ciStatus = 4;
        }

        if (coreStatus == 'Connected') {
          this.coreStatus = 3;
        } else if (coreStatus == 'NotConnected') {
          this.coreStatus = 4;
        } else {
          this.coreStatus = 4;
        }

        this.userlist = [
          {
            id: 1,
            surrounding_system: 'Core Banking',
            status: this.coreStatus,
          },
          {
            id: 2,
            surrounding_system: 'CI-Connector',
            status: this.ciStatus,
          },
        ];
      });
  }

  emailnotif(reqNotif) {
    this.integrateNotif.emailnotification(reqNotif).subscribe((result: any) => {
      console.log('+++++++++++++++++++>>notif mail');
    });
  }

  getBalanceInquiry() {
    this.prefundDashboardService
      .balanceInquiry(this.payload)
      .subscribe((result: any) => {
        this.gaugeValue = result.balance;
        this.gaugeStatus = result.status;

        console.log('>>>>>>>>>>>>>>????' + result.balance);
        console.log('>>>>>>>>>>>>>>????' + result.status);
      });
  }

  getProp() {
    this.prefundDashboardService
      .getDashboardProp()
      .subscribe((result: BackendResponse) => {
        this.gaugeMax = result.data.dashboardProp.max;
        this.gaugeMin = result.data.dashboardProp.min;
        this.upperLimit = result.data.dashboardProp.upper_limit;
        this.referenceLimit = result.data.dashboardProp.reference_limit;
        this.amberLevel = result.data.dashboardProp.amber_level;
        this.redLevel = result.data.dashboardProp.red_level;
        //this.gaugeValue = result.data.dashboardProp.current;

        let dataMin10 = +((this.gaugeMin * 10) / 100) + +this.gaugeMin; // 100
        let dataMin20 = +((this.gaugeMin * 20) / 100) + +this.gaugeMin; // 20%
        let status = this.gaugeStatus;
        let value = parseInt(String(this.gaugeValue));

        let reqNotif = {
          kodestatus: '',
          message: '',
        };

        console.log(value);

        if (status === 'RJCT' || status === 'NTMD') {
          console.log('>>>>>>>>>>>>>>>>>>>>>>> reject');
          this.stats = true;
          value = 0;
          reqNotif = {
            kodestatus: '',
            message: '',
          };
        }
        if (status === undefined) {
          this.stats = true;
          this.gaugeStatus = 'No Connection';
          value = 0;
          reqNotif = {
            kodestatus: '',
            message: '',
          };
        } else {
          this.stats = false;
          if (value < this.redLevel) {
            console.log('red level');
            this.thresholdConfig[value] = { color: 'red' };
            reqNotif = {
              kodestatus: 'INFO',
              message:
                'Saldo available mendekati batas bawah saldo SUB-RSD. Mohon top up saldo anda. Terima kasih',
            };
            console.log(reqNotif.message);
          }
          if (value < this.amberLevel && value > this.redLevel) {
            console.log('diantara amber dan red level');
            console.log(value < this.amberLevel && value > this.redLevel);
            this.thresholdConfig[value] = { color: 'orange' };
            reqNotif = {
              kodestatus: 'INFO',
              message:
                'Saldo available mendekati RED Level. Mohon top up saldo anda. Terima kasih',
            };
            console.log(reqNotif.message);
          }
          if (value < this.referenceLimit && value > this.amberLevel) {
            console.log(value < this.referenceLimit && value > this.amberLevel);
            this.thresholdConfig[value] = { color: 'green' };
          }
          if (value > this.referenceLimit && value < this.upperLimit) {
            // send email
            this.thresholdConfig[value] = { color: 'blue' };
            console.log('diantara reference dan upper');
          }
          if (value > this.upperLimit) {
            // send email
            this.thresholdConfig[value] = { color: 'red' };
            console.log('lebih dari upper limit');
            reqNotif = {
              kodestatus: 'INFO',
              message:
                'Saldo available melebihi batas atas saldo SUB-RSD. Silahkan cek saldo anda saat ini',
            };
            console.log(reqNotif.message);
          }
          this.emailnotif(reqNotif);
        }
        console.log(result.data);
      });
  }

  ngOnDestroy() {
    this.chartSubscription?.unsubscribe();
    this.updateSubscription?.unsubscribe();
  }
}
