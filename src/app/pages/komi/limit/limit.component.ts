import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { LimitService } from 'src/app/services/komi/limit.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-limit',
  templateUrl: './limit.component.html',
  styleUrls: ['./limit.component.scss'],
})

export class LimitComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = "";

  limitData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];

  groupForm!: FormGroup;

  limitDialog = false;
  delLimitDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private limitService: LimitService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      { label: 'Master Data' },
      { label: 'Limit' }
    ];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 }
    ];

    //NEW ADD : State Columns
    this.cols = [
      { field: 'LIMIT_KEY', header: 'Limit Key', isOrder: false },
      { field: 'LIMIT_VALUE', header: 'Limit Value', isOrder: true },
      { field: 'DESCRIPTION', header: 'Description', isOrder: false },
      { field: 'STATUS', header: 'Status', transform: true, isOrder: false },
      { field: 'CREATED_DATE', header: 'Created Date', isOrder: true, width: '200px' },
      { field: 'CREATED_BY', header: 'Created By', isOrder: false, width: '200px' },
      { field: 'UPDATE_DATE', header: 'Updated Date', isOrder: true, width: '200px' },
      { field: 'UPDATE_BY', header: 'Updated By', isOrder: false, width: '200px' }
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 4);

    /* Get data all */
    this.refreshingLimit();
  }

  refreshingLimit() {
    this.isFetching = true;

    this.limitService.getAll().subscribe(async (result: BackendResponse) => {
      if (result.status == 200) {
        this.limitData = result.data;
      } else {
        this.limitData = [];

        let objtmp = {
          LIMIT_KEY: 'No records',
          LIMIT_VALUE: 'No records',
          DESCRIPTION: 'No records',
          STATUS: 'No records',
          CREATED_DATE: 'No records',
          CREATED_BY: 'No records',
          UPDATE_DATE: 'No records',
          UPDATE_BY: 'No records',
        };
        this.limitData.push(objtmp);
      }
      this.isFetching = false;
    },
      (error) => {
        this.isFetching = false;
        this.messageService.add({ severity: 'error', summary: 'Get Data Error -', detail: 'Internal server error' });
      });
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_limit: [''],
      limit_key: ['', Validators.required],
      limit_value: ['', Validators.required],
      description: [''],
      status: [''],
    });
    this.limitDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;

    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          limitKey: this.groupForm.get('limit_key')?.value,
          limitValue: this.groupForm.get('limit_value')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.limitService.insert(payload).subscribe((result: BackendResponse) => {
          if (result.status == 200) {
            this.limitDialog = false;
            this.isProcess = false;
            this.refreshingLimit();
            this.messageService.add({ severity: 'success', summary: 'Success -', detail: 'Data Inserted' });
          } else {
            this.limitDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Failed -', detail: result.data });
          }
        },
          (error) => {
            this.limitDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Insert Data Error -', detail: 'Internal server error' });
          });
      } else {
        payload = {
          limitKey: this.groupForm.get('limit_key')?.value,
          limitValue: this.groupForm.get('limit_value')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_limit')?.value
        };
        this.limitService.update(payload).subscribe((result: BackendResponse) => {
          if (result.status == 200) {
            this.limitDialog = false;
            this.isProcess = false;
            this.refreshingLimit();
            this.messageService.add({ severity: 'success', summary: 'Success -', detail: 'Data Updated' });
          } else {
            this.limitDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Failed -', detail: result.data });
          }
        },
          (error) => {
            this.limitDialog = false;
            this.isProcess = false;
            this.messageService.add({ severity: 'error', summary: 'Update Data Error -', detail: 'Internal server error' });
          });
      }
    }
  }

  editLimit(data: any) {
    this.groupForm = this.formBuilder.group({
      id_limit: [''],
      limit_key: ['', Validators.required],
      limit_value: ['', Validators.required],
      description: [''],
      status: [''],
    });

    console.log(data);

    this.groupForm.patchValue({
      limit_key: data["LIMIT_KEY"],
      limit_value: data["LIMIT_VALUE"],
      description: data["DESCRIPTION"],
      status: data["STATUS"],
      id_limit: data["ID"],
    });

    this.limitDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delLimitDialog = true;
  }

  deleteLimit() {
    this.isProcess = true;

    let dataLimit = this.delData;
    const payload = { dataLimit };
    this.limitService.delete(payload.dataLimit.ID).subscribe((resp: BackendResponse) => {
      this.delLimitDialog = false;
      this.isProcess = false;
      this.messageService.add({ severity: 'success', summary: 'Success -', detail: 'Data Deleted' });
      this.refreshingLimit();
    },
      (error) => {
        this.delLimitDialog = false;
        this.isProcess = false;
        this.messageService.add({ severity: 'error', summary: 'Update Data Error -', detail: 'Internal server error' });
      });
  }
}
