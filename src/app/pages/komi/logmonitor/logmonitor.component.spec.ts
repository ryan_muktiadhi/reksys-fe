import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogmonitorComponent } from './logmonitor.component';

describe('LogmonitorComponent', () => {
  let component: LogmonitorComponent;
  let fixture: ComponentFixture<LogmonitorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogmonitorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogmonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
