import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingAccountTypeDetailComponent } from './mapping-account-type-detail.component';

describe('MappingAccountTypeDetailComponent', () => {
  let component: MappingAccountTypeDetailComponent;
  let fixture: ComponentFixture<MappingAccountTypeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingAccountTypeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingAccountTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
