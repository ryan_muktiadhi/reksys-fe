import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, SelectItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { AccounttypeService } from 'src/app/services/komi/accounttype.service';

@Component({
  selector: 'app-mapping-account-type-detail',
  templateUrl: './mapping-account-type-detail.component.html',
  styleUrls: ['./mapping-account-type-detail.component.scss'],
})
export class MappingAccountTypeDetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  proxytypes: any = [];
  proxystates: any = [];
  registrarbics: any = [];
  idtypes: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  items: SelectItem[];
  item: string;
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private proxyadminService: ProxyadminService,
    private bussinesparamService: BussinesparamService,
    private bicadminService: BicadminService,
    private accTypeService: AccounttypeService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Account Type',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // registration_id, proxy_type, proxy_value, registrar_bic, account_number, account_type, account_name, identification_number_type, identification_number_value, proxy_status
      this.groupForm = this.formBuilder.group({
        code: ['', Validators.required],
        name_cb: ['', Validators.required],
        type_cb: ['', Validators.required],
        name_bf: ['', Validators.required],
        type_bf: ['', Validators.required],
        status: ['', Validators.required],
      });

      this.proxytypes = [];
      this.idtypes = [];
      this.registrarbics = [];

      this.bussinesparamService
        .getAllByCategory('KOMI_LMT_STS')
        .subscribe((result: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(result));
          if (result.status === 200) {
            this.proxytypes = result.data;
          } else {
            // this.proxyData = result.data.bicAdmins;
          }
        });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.accTypeService
            .getById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit bic ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.groupForm.patchValue({
                code: result.data.accType.code,
                name_cb: result.data.accType.name_cb,
                type_cb: result.data.accType.type_cb,
                name_bf: result.data.accType.name_bf,
                type_bf: result.data.accType.type_bf,
                status: result.data.accType.status,
              });
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name
      if (!this.isEdit) {
        payload = {
          code: this.groupForm.get('code')?.value,
          name_cb: this.groupForm.get('name_cb')?.value,
          type_cb: this.groupForm.get('type_cb')?.value,
          name_bf: this.groupForm.get('name_bf')?.value,
          type_bf: this.groupForm.get('type_bf')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.accTypeService
          .insert(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.location.back();
            }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
          });
      } else {
        payload = {
          id: this.userId,
          code: this.groupForm.get('code')?.value,
          name_cb: this.groupForm.get('name_cb')?.value,
          type_cb: this.groupForm.get('type_cb')?.value,
          name_bf: this.groupForm.get('name_bf')?.value,
          type_bf: this.groupForm.get('type_bf')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.accTypeService
          .update(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.location.back();
            }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
          });
      }
    }

    console.log(this.groupForm.valid);
  }
}
