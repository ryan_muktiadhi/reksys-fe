import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { AccounttypeService } from 'src/app/services/komi/accounttype.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-mapping-account-type',
  templateUrl: './mapping-account-type.component.html',
  styleUrls: ['./mapping-account-type.component.scss'],
})
export class MappingAccountTypeComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  accTypeData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];

  groupForm!: FormGroup;

  accTypeDialog = false;
  delAccTypeDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;

  accTypeDataheader: any = [
    { label: 'Bank Account Type', sort: 'bank_account_type' },
    { label: 'BI Account Type', sort: 'bi_account_type' },
    { label: 'Description', sort: 'description' },
    { label: 'Status', sort: 'status' },
    { label: 'Active', sort: 'active' },
    { label: 'Created By', sort: 'created_by' },
    { label: 'Created Date', sort: 'created_date' },
    { label: 'Update By', sort: 'updated_by' },
    { label: 'Update Date', sort: 'updated_date' },
  ];
  accTypeDatacolname: any = [
    'bank_account_type',
    'bi_account_type',
    'description',
    'status',
    'active',
  ];

  accTypeDatacolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  accTypeDatacolwidth: any = ['', '', '', '', ''];
  accTypeDatacollinghref: any = { url: '#', label: 'Application' };
  accTypeDataactionbtn: any = [0, 1, 0, 1, 0, 1, 1];
  branchaddbtn = { label: 'Add Data' };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private accTypeService: AccounttypeService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Master Data' }, { label: 'Account Type' }];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 },
    ];

    /* Set datatable */
    this.cols = [
      {
        field: 'bank_account_type',
        header: 'Bank Account Type',
        isOrder: true,
      },
      { field: 'bi_account_type', header: 'BI Account Type', isOrder: true },
      { field: 'description', header: 'Description', isOrder: true },
      { field: 'status', header: 'Status', transform: true, isOrder: false },
      {
        field: 'created_date',
        header: 'Created Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'created_by',
        header: 'Created By',
        isOrder: false,
        width: '200px',
      },
      {
        field: 'updated_date',
        header: 'Updated Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'updated_by',
        header: 'Updated By',
        isOrder: false,
        width: '200px',
      },
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 4);

    /* Get data all */
    this.refreshingAccType();
  }

  refreshingAccType() {
    this.isFetching = true;

    this.accTypeService.getAll().subscribe(
      async (result: BackendResponse) => {
        if (result.status == 200) {
          this.accTypeData = result.data;
          console.log(this.accTypeData);
        } else {
          this.accTypeData = [];

          let objtmp = {
            bank_account_type: 'No records',
            bi_account_type: 'No records',
            description: 'No records',
            status: 'No records',
            created_date: 'No records',
            created_by: 'No records',
            updated_date: 'No records',
            updated_by: 'No records',
          };
          this.accTypeData.push(objtmp);
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Get Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_acc_type: [''],
      bank_account_type: ['', Validators.required],
      bi_account_type: ['', Validators.required],
      description: [''],
      status: [''],
    });
    this.accTypeDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;
    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          bankAccountType: this.groupForm.get('bank_account_type')?.value,
          biAccountType: this.groupForm.get('bi_account_type')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
        };

        this.accTypeService.insert(payload).subscribe(
          async (result: BackendResponse) => {
            if (result.status == 200) {
              this.accTypeDialog = false;
              this.isProcess = false;
              this.refreshingAccType();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Inserted',
              });
            } else {
              this.accTypeDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.accTypeDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Insert Data Error -',
              detail: errorDetail,
            });
          }
        );
      } else {
        payload = {
          bankAccountType: this.groupForm.get('bank_account_type')?.value,
          biAccountType: this.groupForm.get('bi_account_type')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_acc_type')?.value,
        };
        this.accTypeService.update(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.accTypeDialog = false;
              this.isProcess = false;
              this.refreshingAccType();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Updated',
              });
            } else {
              this.accTypeDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.accTypeDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Update Data Error -',
              detail: errorDetail,
            });
          }
        );
      }
    }
  }

  editAccType(data: any) {
    this.groupForm = this.formBuilder.group({
      id_acc_type: [''],
      bank_account_type: ['', Validators.required],
      bi_account_type: ['', Validators.required],
      description: [''],
      status: [''],
    });

    this.groupForm.patchValue({
      bank_account_type: data['bank_account_type'],
      bi_account_type: data['bi_account_type'],
      description: data['description'],
      status: data['status'],
      id_acc_type: data['id'],
    });

    this.accTypeDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delAccTypeDialog = true;
  }

  deleteAccType() {
    this.isProcess = true;

    let dataAccType = this.delData;
    const payload = { dataAccType };
    this.accTypeService.delete(payload.dataAccType.id).subscribe(
      (resp: BackendResponse) => {
        this.delAccTypeDialog = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Success -',
          detail: 'Data Deleted',
        });
        this.refreshingAccType();
      },
      (error) => {
        this.delAccTypeDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Update Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }
}
