import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingCustomerTypeDetailComponent } from './mapping-customer-type-detail.component';

describe('MappingCustomerTypeDetailComponent', () => {
  let component: MappingCustomerTypeDetailComponent;
  let fixture: ComponentFixture<MappingCustomerTypeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingCustomerTypeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingCustomerTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
