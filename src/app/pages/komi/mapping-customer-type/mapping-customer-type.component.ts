import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { CusttypeService } from 'src/app/services/komi/custtype.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-mapping-customer-type',
  templateUrl: './mapping-customer-type.component.html',
  styleUrls: ['./mapping-customer-type.component.scss'],
})
export class MappingCustomerTypeComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  custTypeData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];

  custTypeDataheader: any = [
    { label: 'Bank Customer Type', sort: 'bank_cust_type' },
    { label: 'BI Customer Type', sort: 'bi_cust_type' },
    { label: 'Description', sort: 'description' },
    { label: 'Status', sort: 'status' },
    { label: 'Active', sort: 'active' },
    { label: 'Created By', sort: 'created_by' },
    { label: 'Created Date', sort: 'created_date' },
    { label: 'Update By', sort: 'updated_by' },
    { label: 'Update Date', sort: 'updated_date' },
  ];
  custTypeDatacolname: any = [
    'bank_cust_type',
    'bi_cust_type',
    'description',
    'status',
    'active',
  ];

  custTypeDatacolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  custTypeDataolwidth: any = ['', '', '', '', ''];
  custTypeDatacollinghref: any = { url: '#', label: 'Application' };
  custTypeDataactionbtn: any = [0, 1, 0, 1, 0, 1, 1];
  custTypeDataaddbtn = { label: 'Add Data' };

  groupForm!: FormGroup;

  custTypeDialog = false;
  delCustTypeDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private custTypeService: CusttypeService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Master Data' }, { label: 'Customer Type' }];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 },
    ];

    /* Set datatable */
    this.cols = [
      { field: 'bank_cust_type', header: 'Bank Customer Type', isOrder: true },
      { field: 'bi_cust_type', header: 'BI Customer Type', isOrder: true },
      { field: 'description', header: 'Description', isOrder: false },
      { field: 'status', header: 'Status', transform: true, isOrder: false },
      {
        field: 'created_date',
        header: 'Created Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'created_by',
        header: 'Created By',
        isOrder: false,
        width: '200px',
      },
      {
        field: 'updated_date',
        header: 'Updated Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'updated_by',
        header: 'Updated By',
        isOrder: false,
        width: '200px',
      },
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 4);

    /* Get data all */
    this.refreshingCustType();
  }

  refreshingCustType() {
    this.isFetching = true;

    this.custTypeService.getAll().subscribe(
      async (result: BackendResponse) => {
        if (result.status == 200) {
          this.custTypeData = result.data;
        } else {
          this.custTypeData = [];
          let objtmp = {
            bank_cust_type: 'No records',
            bi_cust_type: 'No records',
            description: 'No records',
            status: 'No records',
            created_date: 'No records',
            created_by: 'No records',
            updated_date: 'No records',
            updated_by: 'No records',
          };
          this.custTypeData.push(objtmp);
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Get Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_cust_type: [''],
      bank_cust_type: ['', Validators.required],
      bi_cust_type: ['', Validators.required],
      description: [''],
      status: [''],
    });
    this.custTypeDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;
    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          bankCustType: this.groupForm.get('bank_cust_type')?.value,
          biCustType: this.groupForm.get('bi_cust_type')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
        };
        // console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.custTypeService.insert(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.custTypeDialog = false;
              this.isProcess = false;
              this.refreshingCustType();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Inserted',
              });
            } else {
              this.custTypeDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.custTypeDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Insert Data Error -',
              detail: errorDetail,
            });
          }
        );
      } else {
        payload = {
          bankCustType: this.groupForm.get('bank_cust_type')?.value,
          biCustType: this.groupForm.get('bi_cust_type')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_cust_type')?.value,
        };
        this.custTypeService.update(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.custTypeDialog = false;
              this.isProcess = false;
              this.refreshingCustType();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Updated',
              });
            } else {
              this.custTypeDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.custTypeDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Update Data Error -',
              detail: errorDetail,
            });
          }
        );
      }
    }
  }

  editCustType(data: any) {
    this.groupForm = this.formBuilder.group({
      id_cust_type: [''],
      bank_cust_type: ['', Validators.required],
      bi_cust_type: ['', Validators.required],
      description: [''],
      status: [''],
    });

    this.groupForm.patchValue({
      bank_cust_type: data['bank_cust_type'],
      bi_cust_type: data['bi_cust_type'],
      description: data['description'],
      status: data['status'],
      id_cust_type: data['id'],
    });

    this.custTypeDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delCustTypeDialog = true;
  }

  deleteCustType() {
    this.isProcess = true;

    let dataCustType = this.delData;
    const payload = { dataCustType };
    this.custTypeService.delete(payload.dataCustType.id).subscribe(
      (resp: BackendResponse) => {
        this.delCustTypeDialog = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Success -',
          detail: 'Data Deleted',
        });
        this.isProcess = false;
        this.refreshingCustType();
      },
      (error) => {
        this.delCustTypeDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Update Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }
}
