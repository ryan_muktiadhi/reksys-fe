import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingIdTypeDetailComponent } from './mapping-id-type-detail.component';

describe('MappingIdTypeDetailComponent', () => {
  let component: MappingIdTypeDetailComponent;
  let fixture: ComponentFixture<MappingIdTypeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingIdTypeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingIdTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
