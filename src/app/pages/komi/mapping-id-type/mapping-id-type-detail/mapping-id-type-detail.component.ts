import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, SelectItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { IdentityTypeService } from 'src/app/services/komi/identity-type.service';

@Component({
  selector: 'app-mapping-id-type-detail',
  templateUrl: './mapping-id-type-detail.component.html',
  styleUrls: ['./mapping-id-type-detail.component.scss'],
})
export class MappingIdTypeDetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  proxytypes: any = [];
  proxystates: any = [];
  registrarbics: any = [];
  idtypes: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  items: SelectItem[];
  item: string;
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private proxyadminService: ProxyadminService,
    private bussinesparamService: BussinesparamService,
    private bicadminService: BicadminService,
    private idTypeService: IdentityTypeService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Identity Type',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // registration_id, proxy_type, proxy_value, registrar_bic, account_number, account_type, account_name, identification_number_type, identification_number_value, proxy_status
      this.groupForm = this.formBuilder.group({
        type_cb: ['', Validators.required],
        type_bf: ['', Validators.required],
        status: ['', Validators.required],
      });

      this.proxytypes = [];
      this.idtypes = [];
      this.registrarbics = [];

      this.bussinesparamService
        .getAllByCategory('KOMI_LMT_STS')
        .subscribe((result: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(result));
          if (result.status === 200) {
            this.proxytypes = result.data;
          } else {
            // this.proxyData = result.data.bicAdmins;
          }
        });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.idTypeService
            .getById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit bic ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.groupForm.patchValue({
                //  code: result.data.idType.code,
                //  name: result.data.idType.name,
                type_cb: result.data.bank_id_type,
                type_bf: result.data.bi_id_type,
                status: result.data.status,
              });
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name
      if (!this.isEdit) {
        payload = {
          code: this.groupForm.get('code')?.value,
          name: this.groupForm.get('name')?.value,
          bankIdType: this.groupForm.get('type_cb')?.value,
          biIdType: this.groupForm.get('type_bf')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.idTypeService
          .insert(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status === 200) {
              // this.location.back();
              this.router.navigate(['/mgm/master/idtype']);
            }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
            //this.location.back();
          });
      } else {
        payload = {
          id: this.userId,
          code: this.groupForm.get('code')?.value,
          name: this.groupForm.get('name')?.value,
          bankIdType: this.groupForm.get('type_cb')?.value,
          biIdType: this.groupForm.get('type_bf')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.idTypeService
          .update(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status === 200) {
              this.router.navigate(['/mgm/master/idtype']);
            }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
            //this.location.back();
          });
      }
    }

    console.log(this.groupForm.valid);
  }
}
