import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingIdTypeComponent } from './mapping-id-type.component';

describe('MappingIdTypeComponent', () => {
  let component: MappingIdTypeComponent;
  let fixture: ComponentFixture<MappingIdTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingIdTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingIdTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
