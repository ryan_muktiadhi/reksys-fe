import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { ResidentstatusService } from 'src/app/services/komi/residentstatus.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-mappingresident',
  templateUrl: './mappingresident.component.html',
  styleUrls: ['./mappingresident.component.scss'],
})
export class MappingresidentComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  residData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];

  residDataheader: any = [
    { label: 'Bank Resident Status', sort: 'bank_resident_status' },
    { label: 'BI Resident Status', sort: 'bi_resident_status' },
    { label: 'Description. Fee', sort: 'description' },
    { label: 'Status', sort: 'status' },
    { label: 'Active', sort: 'active' },
    { label: 'Created By', sort: 'created_by' },
    { label: 'Created Date', sort: 'created_date' },
    { label: 'Update By', sort: 'updated_by' },
    { label: 'Update Date', sort: 'updated_date' },
  ];
  residDatacolname: any = [
    'bank_resident_status',
    'bi_resident_status',
    'description',
    'status',
    'active',
  ];

  residDatacolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  residDatacolwidth: any = ['', '', '', '', ''];
  residDatacollinghref: any = { url: '#', label: 'Application' };
  residDataactionbtn: any = [0, 1, 0, 1, 0, 1, 1];
  residDataaddbtn = { label: 'Add Data' };

  groupForm!: FormGroup;

  residDialog = false;
  delResidDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private residentService: ResidentstatusService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Master Data' }, { label: 'Resident Status' }];

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 },
    ];

    /* Set datatable */
    this.cols = [
      {
        field: 'bank_resident_status',
        header: 'Bank Resident Status',
        isOrder: true,
      },
      {
        field: 'bi_resident_status',
        header: 'BI Resident Status',
        isOrder: true,
      },
      { field: 'description', header: 'Description', isOrder: false },
      { field: 'status', header: 'Status', transform: true, isOrder: false },
      {
        field: 'created_date',
        header: 'Created Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'created_by',
        header: 'Created By',
        isOrder: false,
        width: '200px',
      },
      {
        field: 'updated_date',
        header: 'Updated Date',
        isOrder: true,
        width: '200px',
      },
      {
        field: 'updated_by',
        header: 'Updated By',
        isOrder: false,
        width: '200px',
      },
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 4);

    /* Get data all */
    this.refreshingResid();
  }

  refreshingResid() {
    this.isFetching = true;

    this.residentService.getAll().subscribe(
      async (result: BackendResponse) => {
        if (result.status == 200) {
          this.residData = result.data;
        } else {
          this.residData = [];

          let objtmp = {
            bank_resident_status: 'No records',
            bi_resident_status: 'No records',
            description: 'No records',
            status: 'No records',
            created_date: 'No records',
            created_by: 'No records',
            updated_date: 'No records',
            updated_by: 'No records',
          };
          this.residData.push(objtmp);
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Get Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }

  /* Open modal add data */
  openNew() {
    this.isEdit = false;

    this.groupForm = this.formBuilder.group({
      id_resid: [''],
      bank_resid_status: ['', Validators.required],
      bi_resid_status: ['', Validators.required],
      description: [''],
      status: [''],
    });
    this.residDialog = true;
  }

  get f() {
    return this.groupForm.controls;
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;

    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          bankResidentStatus: this.groupForm.get('bank_resid_status')?.value,
          biResidentStatus: this.groupForm.get('bi_resid_status')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
        };
        this.residentService.insert(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.residDialog = false;
              this.isProcess = false;
              this.refreshingResid();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Inserted',
              });
            } else {
              this.residDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.residDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Insert Data Error -',
              detail: errorDetail,
            });
          }
        );
      } else {
        payload = {
          bankResidentStatus: this.groupForm.get('bank_resid_status')?.value,
          biResidentStatus: this.groupForm.get('bi_resid_status')?.value,
          description: this.groupForm.get('description')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.groupForm.get('id_resid')?.value,
        };
        // console.log(">>>>>>>> payload " + JSON.stringify(payload));
        this.residentService.update(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.residDialog = false;
              this.isProcess = false;
              this.refreshingResid();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Updated',
              });
            } else {
              this.residDialog = false;
              this.isProcess = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.residDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Update Data Error -',
              detail: errorDetail,
            });
          }
        );
      }
    }
  }

  editResid(data: any) {
    this.groupForm = this.formBuilder.group({
      id_resid: [''],
      bank_resid_status: ['', Validators.required],
      bi_resid_status: ['', Validators.required],
      description: [''],
      status: [''],
    });

    this.groupForm.patchValue({
      bank_resid_status: data['bank_resident_status'],
      bi_resid_status: data['bi_resident_status'],
      description: data['description'],
      status: data['status'],
      id_resid: data['id'],
    });

    this.residDialog = true;
    this.isEdit = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delResidDialog = true;
  }

  deleteResid() {
    this.isProcess = true;

    let dataResid = this.delData;
    const payload = { dataResid };
    this.residentService.delete(payload.dataResid.id).subscribe(
      (resp: BackendResponse) => {
        this.delResidDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'success',
          summary: 'Success -',
          detail: 'Data Deleted',
        });
        this.refreshingResid();
      },
      (error) => {
        this.delResidDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Update Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }
}
