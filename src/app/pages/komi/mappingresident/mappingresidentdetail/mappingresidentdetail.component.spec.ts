import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingresidentdetailComponent } from './mappingresidentdetail.component';

describe('MappingresidentdetailComponent', () => {
  let component: MappingresidentdetailComponent;
  let fixture: ComponentFixture<MappingresidentdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingresidentdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingresidentdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
