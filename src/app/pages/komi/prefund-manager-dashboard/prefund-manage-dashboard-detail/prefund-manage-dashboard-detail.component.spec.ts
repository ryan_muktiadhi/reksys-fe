import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrefundManageDashboardDetailComponent } from './prefund-manage-dashboard-detail.component';

describe('PrefundManageDashboardDetailComponent', () => {
  let component: PrefundManageDashboardDetailComponent;
  let fixture: ComponentFixture<PrefundManageDashboardDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrefundManageDashboardDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrefundManageDashboardDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
