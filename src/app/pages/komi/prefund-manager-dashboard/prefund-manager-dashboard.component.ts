import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';
import { PrefundDashboardService } from 'src/app/services/komi/prefund-dashboard.service';
import { max } from 'moment';
import { AclmenucheckerService } from '../../../services/utils/aclmenuchecker.service';
import { Router } from '@angular/router';
import { PrefundmgmService } from '../../../services/komi/prefundmgm.service';
import { ApprovalService } from '../../../services/approval.service';

@Component({
  selector: 'app-prefund-manager-dashboard',
  templateUrl: './prefund-manager-dashboard.component.html',
  styleUrls: ['./prefund-manager-dashboard.component.scss'],
})
export class PrefundManagerDashboardComponent implements OnInit {
  display = false;
  displayPrv = false;
  viewDisplay = false;
  selectedLimitData: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  limitData: any[] = [];
  prefundData: any[] = [];
  limitDataHeader: any = [
    { label: 'Upper Limit', sort: 'upper_limit' },
    { label: 'Reference Limit', sort: 'reference_limit' },
    { label: 'Amber level', sort: 'amber_level' },
    { label: 'Red Level', sort: 'red_level' },
    { label: 'Active', sort: 'Active' },
  ];
  limitDatacolname: any = [
    'upperLimit',
    'referenceLimit',
    'amberLevel',
    'redLevel',
    'active',
  ];
  limitDatacolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  limitDatacolwidth: any = ['', '', '', '', ''];
  limitDatacollinghref: any = { url: '#', label: 'Application' };
  limitDataactionbtn: any = [0, 1, 1, 0, 0, 1];
  limitDataaddbtn = { route: 'detail', label: 'Add Data' };

  gaugeType = 'semi';
  gaugeValue = 0;
  gaugeMax = 0;
  gaugeMin = 0;
  upperLimit = 0;
  referenceLimit = 0;
  amberLevel = 0;
  redLevel = 0;
  active;
  userid;
  idapproval;
  newData: any;
  oldData: any;
  //gaugeAppendText = `/${this.gaugeMax}`;

  // minRadar: 0;
  // maxRadar: 1500;
  // valueRadar: 1000;

  thresholdConfig = {};

  viewApprove = false;

  constructor(
    private router: Router,
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private limitService: LimitService,
    private prefundDashboardService: PrefundDashboardService,
    private prefundService: PrefundmgmService,
    private aclMenuService: AclmenucheckerService,
    private approvalService: ApprovalService
  ) {}

  payload: any = {
    transactionId: '000001',
    dateTime: '2021-12-21T11:52:32.923',
    merchantType: '6666',
    terminalId: 'KOMI000001',
    noRef: 'KOM21122111523210000',
    accountNumber: '00102000752',
  };

  ngOnInit() {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Prefund Managament' }];
    this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
      console.log(dataacl);
      var count = Object.keys(dataacl.acl).length;
      if (count > 0) {
        // this.limitDataactionbtn[0] = dataacl.acl.create;
        this.limitDataactionbtn[1] = dataacl.acl.read;
        this.limitDataactionbtn[2] = dataacl.acl.update;
        //this.limitDataactionbtn[3] = dataacl.acl.delete;
        this.limitDataactionbtn[4] = 0;
        this.limitDataactionbtn[5] = dataacl.acl.approval;
      }
    });
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    this.getBalanceInquiry();
    this.getProps();
  }

  getProps() {
    this.isFetching = true;
    this.prefundDashboardService
      .getDashboardProp()
      .subscribe((result: BackendResponse) => {
        if (result.status === 200) {
          this.userid = result.data.dashboardProp.id;
          // this.gaugeMax = result.data.dashboardProp.max;
          // this.gaugeMin = result.data.dashboardProp.min;
          this.upperLimit = result.data.dashboardProp.upper_limit;
          this.referenceLimit = result.data.dashboardProp.reference_limit;
          this.amberLevel = result.data.dashboardProp.amber_level;
          this.redLevel = result.data.dashboardProp.red_level;
          this.gaugeValue = result.data.dashboardProp.current;
          this.active = result.data.dashboardProp.active;
          this.idapproval = result.data.dashboardProp.idapproval;

          console.log(result.data.dashboardProp.amber_level);

          this.limitData = [];
          let respo = {
            upper_limit: this.upperLimit,
            reference_limit: this.referenceLimit,
            amber_level: this.amberLevel,
            red_level: this.redLevel,
            active: this.active,
          };
          this.limitData.push(respo);
          console.log(this.limitData);
          // const dataMin10 = +((this.gaugeMin * 10) / 100) + +this.gaugeMin; // 100
          // const dataMin20 = +((this.gaugeMin * 20) / 100) + +this.gaugeMin; // 20%
          let value = parseInt(String(this.gaugeValue));

          if (value < this.redLevel) {
            console.log('kurang dari this.redLevel');
            this.thresholdConfig[value] = { color: 'red' };
            console.log(this.thresholdConfig);
          }
          if (value < this.amberLevel && value > this.redLevel) {
            console.log('kurang dari 2 ');
            console.log(this.amberLevel, '+', this.redLevel);
            this.thresholdConfig[value] = { color: 'orange' };
          }
          if (value < this.referenceLimit && value > this.amberLevel) {
            console.log('kurang dari 3 ');
            console.log(this.referenceLimit, '+', this.amberLevel);
            this.thresholdConfig[value] = { color: 'green' };
          }
          if (value > this.referenceLimit) {
            // send email
            console.log('lebih dari nilai upperLimit');
            this.thresholdConfig[value] = { color: 'blue' };
            console.log(this.gaugeMax);
          }
        } else {
          this.limitData = [];
          let respo = {
            min: 'No Records',
            max: 'No Records',
          };
          this.limitData.push(respo);
        }
        this.isFetching = false;
      });
  }

  refreshingProxy(): void {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Limit ');
    this.prefundDashboardService
      .getHistory()
      .subscribe(async (result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.limitData = [];

          let objtmp = {
            refNum: 'No records',
            transactionType: 'No records',
            prefundAmount: 'No records',
            formated_date: 'No records',
          };
          this.limitData.push(objtmp);
        } else {
          this.limitData = result.data.historyData;
        }
        // if(result.data.userGroup.length > 0) {
        //   this.proxyData = [];
        //   this.proxyData = result.data.userGroup;
        // } else {
        //   this.proxyData = [];
        //   let objtmp = {"groupname":"No records"};
        //   this.proxyData.push(objtmp);
        // }
        this.isFetching = false;
      });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedLimitData = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedLimitData = data;
  }

  deleteLimit() {
    console.log(this.selectedLimitData);
    this.limitService
      .delete(this.selectedLimitData.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingProxy();
      });
    // this.proxyadminService.deleteGroup(payload).subscribe((resp: BackendResponse)=>{
    //   console.log(">> hasil Delete "+ resp);
    //       if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //   this.display = false;
    //   this.refreshingBic();
    // });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }

  getBalanceInquiry() {
    this.prefundDashboardService
      .balanceInquiry(this.payload)
      .subscribe((result: any) => {
        this.gaugeValue = result.balance || this.gaugeValue;

        console.log('>>>>>>>>>>>>>>????' + result.balance);
      });
  }

  reject() {
    console.log('reject');
    let payload = {
      id: this.userid,
      oldactive: this.selectedLimitData.active,
      idapproval: this.idapproval,
    };
    console.log(payload);

    this.prefundService
      .rejectPrefund(payload)
      .subscribe((result: BackendResponse) => {
        this.refreshingProxy();
        this.viewApprove = false;
      });
  }

  viewData(data: any) {
    // console.log(data);
    this.viewDisplay = true;
    this.limitData = data;
  }

  async setApprovalForm(data) {
    const keys = Object.keys(data);
    const input = data;
    let arrData = [];

    if (keys.length > 0) {
      await keys.map(async (key: any) => {
        let data: any = {};
        key === 'active' || key === 'isactive'
          ? (data.label = 'status')
          : key === 'amber_level' || key === 'amberLevel'
          ? (data.label = 'Amber Level')
          : key === 'red_level' || key === 'redLevel'
          ? (data.label = 'Red Level')
          : key === 'reference_limit' || key === 'referenceLimit'
          ? (data.label = 'Reference Limit')
          : key === 'upper_limit' || key === 'upperLimit'
          ? (data.label = 'Upper Limit')
          : // : key === 'secret_key' || key === 'secretKey'
            //     ? (data.label = 'Secret Key')
            (data.label = key);

        // key === 'active' || key === 'isactive'
        //     ? (data.label = 'active')
        //     : key === 'active'
        //         ? (data.label = 'value')
        //         : (data.label = key);

        if (data.label === 'status') {
          data.value =
            input[key] == 0
              ? 'Not Active'
              : input[key] == 1
              ? 'Active'
              : 'Waiting for Approval';
          data.statusCode = input[key];
        } else if (data.label === 'active') {
          let value = [];
          await input[key].map((dt) => {
            value.push(dt.label || dt);
          });
          data.value = value;
        } else {
          data.value = input[key];
        }
        if (data.label != 'id' && data.label != 'userid') {
          arrData.push(data);
        }
      });
      return arrData;
    }
  }

  getApprovalData(id) {
    this.approvalService
      .getByid(id)
      .subscribe(async (resp: BackendResponse) => {
        let oldData: any = [];
        oldData.push(this.selectedLimitData);
        let oldFormData = await this.setApprovalForm(this.selectedLimitData);
        let newData = await this.setApprovalForm(
          JSON.parse(resp.data[0].jsondata)
        );
        this.oldData = oldFormData;
        console.log('old' + this.oldData);
        this.newData = newData;
        console.log(this.newData);
      });
  }

  async approvalData(data: any) {
    console.log(data);
    this.viewApprove = true;
    this.selectedLimitData = data;
    if (data.idapproval && data.active != 9) {
      console.log('new data');
      this.getApprovalData(data.idapproval);
    } else {
      let oldData = await this.setApprovalForm(this.selectedLimitData);
      // oldData = await this.filterForm(oldData);
      this.oldData = oldData;
      this.newData = null;
    }
  }

  approvalSubmit() {
    console.log('approval');
    let payload = {
      id: this.selectedLimitData.id,
      oldactive: this.selectedLimitData.active,
      idapproval: this.selectedLimitData.idapproval,
    };

    console.log('idaproval', payload.idapproval);
    console.log(payload.id);

    this.prefundService
      .approvePrefund(payload)
      .subscribe((result: BackendResponse) => {
        if (result.status === 200) {
          this.refreshingProxy();
          this.viewApprove = false;
        }
        location.reload();
      });
  }
}
