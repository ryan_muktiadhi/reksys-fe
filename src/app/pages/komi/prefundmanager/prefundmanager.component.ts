import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { PrefundmgmService } from 'src/app/services/komi/prefundmgm.service';

@Component({
  selector: 'app-prefundmanager',
  templateUrl: './prefundmanager.component.html',
  styleUrls: ['./prefundmanager.component.scss'],
})
export class PrefundmanagerComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  display = false;
  displayPrv = false;
  userInfo: any = {};
  tokenID: string = '';
  prefundData: any[] = [];
  selectedprefund: any = [];
  minmax: string = ' ';

  constructor(
    private router: Router,
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private prefundService: PrefundmgmService
  ) {}
  // private sysparamService:sysparamService

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Prefund Management' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingPrefund();
  }
  refreshingPrefund() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh sysParam ');
    this.prefundService
      .getAllPrefundByTenant()
      .subscribe((result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.prefundData = [];
          let objtmp = {
            citycode: 'No records',
            cityname: 'No records',
            branchcode: 'No records',
            branchname: 'No records',
            status: 'No records',
          };
          this.prefundData.push(objtmp);
        } else {
          this.prefundData = result.data.prefunds;
          console.log(JSON.stringify(this.prefundData));
        }

        this.isFetching = false;
      });
  }

  detailPrefund(data: any) {
    this.router.navigate([
      '/mgm/settings/prefundmgmlist/detail#/' + data.idparticipant,
    ]);
  }

  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedprefund = data;
  }

  previewConfirmation(data: any) {
    console.log(data);
  }

  deletePrefund() {
    console.log(this.selectedprefund);
    let Prefund = this.selectedprefund;
    const payload = { Prefund };
    console.log('>> Di delete ' + JSON.stringify(payload.Prefund));
    this.prefundService
      .deletePrefundByTenant(payload.Prefund.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingPrefund();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
