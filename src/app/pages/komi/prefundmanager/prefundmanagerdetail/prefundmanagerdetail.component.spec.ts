import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrefundmanagerdetailComponent } from './prefundmanagerdetail.component';

describe('PrefundmanagerdetailComponent', () => {
  let component: PrefundmanagerdetailComponent;
  let fixture: ComponentFixture<PrefundmanagerdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrefundmanagerdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrefundmanagerdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
