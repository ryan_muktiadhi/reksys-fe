import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { PrefundmgmService } from 'src/app/services/komi/prefundmgm.service';

@Component({
  selector: 'app-prefundmanagerdetail',
  templateUrl: './prefundmanagerdetail.component.html',
  styleUrls: ['./prefundmanagerdetail.component.scss'],
})
export class PrefundmanagerdetailComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedprefund: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  participationid: any = null;
  prefundData: any[] = [];
  prefundheader: any = [
    { label: 'Prefund Amount', sort: 'amount' },
    { label: 'Created Date', sort: 'created_date' },
  ];
  prefundcolname: any = ['amount', 'created_date'];
  prefundcolhalign: any = ['p-text-center', 'p-text-center'];
  prefundcolwidth: any = ['', ''];
  prefundcollinghref: any = { url: '#', label: 'Application' };
  prefundactionbtn: any = [0, 0, 0, 0, 0];
  prefundaddbtn = { route: 'detail', label: 'Add Data' };

  constructor(
    private authservice: AuthService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    public dialogService: DialogService,
    public messageService: MessageService,
    private prefundService: PrefundmgmService
  ) {}
  // private sysparamService:sysparamService

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Prefund Management',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingPrefund();
  }
  refreshingPrefund() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh sysParam ');
    if (this.activatedRoute.snapshot.paramMap.get('id')) {
      this.participationid = this.activatedRoute.snapshot.paramMap.get('id');
      this.prefundService.getPrefundById(this.participationid).subscribe(
        (result: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(result));
          if (result.status === 202) {
            this.prefundData = [];
            let objtmp = { amount: 'No records', created_date: 'No records' };
            this.prefundData.push(objtmp);
          } else {
            this.prefundData = result.data.prefunds;
          }

          this.isFetching = false;
        },
        (error) => {
          console.log;
        }
      );
    }
  }

  onBack() {
    this.location.back();
  }
}
