import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxyhistoryComponent } from './proxyhistory.component';

describe('ProxyhistoryComponent', () => {
  let component: ProxyhistoryComponent;
  let fixture: ComponentFixture<ProxyhistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxyhistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxyhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
