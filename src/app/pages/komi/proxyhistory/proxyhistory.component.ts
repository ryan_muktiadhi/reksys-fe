import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { Table } from 'primeng/table';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BranchService } from 'src/app/services/komi/branch.service';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { ProxytypeService } from 'src/app/services/komi/proxytype.service';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { element } from 'protractor';

@Component({
  selector: 'app-proxyhistory',
  templateUrl: './proxyhistory.component.html',
  styleUrls: ['./proxyhistory.component.scss']
})
export class ProxyhistoryComponent implements OnInit {

  home!: MenuItem;
  breadcrumbs!: MenuItem[];

  userInfo: any = {};
  tokenID: string = '';
  proxyHistData: any[] = [];

  rangeDates: Date[];
  cols: any[];
  selectedColumns: any[];

  branchName: any = [];
  branchSelected: any = {};
  proxyAlias: string = '';

  isFetching: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private branchService: BranchService,
    private proxyadminService: ProxyadminService,
    public datepipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      { label: 'Monitoring' },
      { label: 'Proxy History' }
    ];

    this.branchService.getAllBranchByTenant().subscribe((result: BackendResponse) => {
      if (result.status == 200) {
        for (let i = 0; i < result.data.length; i++) {
          this.branchName.push(
            {
              label: result.data[i].BRANCH_NAME,
              value: result.data[i].BRANCH_CODE
            }
          );
        }
      }
    });

    //NEW ADD : State Columns
    this.cols = [
      { field: 'branchId', header: 'Branch ID', isOrder: true, width: "200px" },
      { field: 'proxyAlias', header: 'Proxy Alias', isOrder: false, width: "200px" },
      { field: 'proxyType', header: 'Proxy Type', isOrder: true, width: "200px" },
      { field: 'proxyOperation', header: 'Proxy Operation', isOrder: true, width: "200px" },
      { field: 'issuedDate', header: 'Issued Date', isOrder: true, width: "200px" },
      { field: 'issuedBy', header: 'Issued By', isOrder: false, width: "200px" },
      { field: 'statusCode', header: 'Status Code', isOrder: true, width: "200px" },
      { field: 'statusMessage', header: 'Status Message', isOrder: false, width: "200px" },
    ];


    this.selectedColumns = this.cols;

    let objtmp = {
      branchId: 'No record',
      proxyAlias: 'No record',
      proxyType: 'No record',
      proxyOperation: 'No record',
      issuedDate: 'No record',
      issuedBy: 'No record',
      statusCode: 'No record',
      statusMessage: 'No record',
    };
    this.proxyHistData.push(objtmp);

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    // this.initialData();
  }

  refreshingProxyHist() {
    this.isFetching = true;

    let start_date = null;
    let end_date = null;

    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    }

    let diffDay = this.calculateDiff(start_date, end_date);

    if (start_date == null && end_date == null) {
      this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Please choose Date Range of Proxy History Data' });
    } else {
      if (diffDay >= 7) {
        this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Maximal Date Range only 7 Days' });
      } else {
        if (this.branchSelected.length == undefined) {
          this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Please choose Branch...' });
        } else {
          let searchPrxAlias: string;
          if (this.proxyAlias.charAt(0) == '%' || this.proxyAlias.charAt(this.proxyAlias.length - 1) == '%') {
            searchPrxAlias = this.proxyAlias;
          } else {
            searchPrxAlias = '%' + this.proxyAlias + '%';
          }

          let payload: any = {};
          payload = {
            branchId: this.branchSelected,
            proxyAlias: searchPrxAlias,
            trxDate1: start_date,
            trxDate2: end_date,
          }

          console.log(payload);
          this.proxyadminService
            .getProxyByParam(payload)
            .subscribe((result: BackendResponse) => {
              if (result.status == 200) {
                this.proxyHistData = result.data.results;
              } else {
                if (result.data.status == 'F') {
                  this.messageService.add({ severity: 'warn', summary: 'Failed', detail: 'Data not found' });
                } else if (result.data.status == 'E') {
                  this.messageService.add({ severity: 'error', summary: 'Error', detail: result.data.statusMessage });
                }
              }
              this.isFetching = false;
            },
              (error) => {
                this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Internal server error' });
                this.isFetching = false;
              });
        }
      }
      this.isFetching = false;
    }
    this.isFetching = false;
  }

  calculateDiff(date1, date2) {
    date1 = new Date(date1);
    date2 = new Date(date2);

    return Math.floor(
      (Date.UTC(
        date2.getFullYear(),
        date2.getMonth(),
        date2.getDate()
      ) -
        Date.UTC(
          date1.getFullYear(),
          date1.getMonth(),
          date1.getDate()
        )) /
      (1000 * 60 * 60 * 24)
    );
  }
}
