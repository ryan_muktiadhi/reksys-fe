import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxylistComponent } from './proxylist.component';

describe('ProxylistComponent', () => {
  let component: ProxylistComponent;
  let fixture: ComponentFixture<ProxylistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxylistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
