import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { ProxylistService } from 'src/app/services/komi/proxylist.service';
import { ProxymanagementService } from 'src/app/services/komi/proxymanagement.service';
import { ProxytypeService } from 'src/app/services/komi/proxytype.service';
import { IdentityTypeService } from 'src/app/services/komi/identity-type.service';
import { ResidentstatusService } from 'src/app/services/komi/residentstatus.service';
import { CusttypeService } from 'src/app/services/komi/custtype.service';
import { BranchService } from 'src/app/services/komi/branch.service';
import { jsPDF } from 'jspdf';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-proxylist',
  templateUrl: './proxylist.component.html',
  styleUrls: ['./proxylist.component.scss'],
})
export class ProxylistComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  proxyData: any[] = [];
  proxyOperationType: any[] = [];
  proxyOprOptions: any[] = [];
  dataList: any[] = [];
  proxyType: any[] = [];
  customerIdType: any[] = [];
  customerType: any[] = [];
  residentStatus: any[] = [];
  branch: any[] = [];

  proxyTabel: boolean = false;
  cols: any[];
  selectedColumns: any[];
  searchOption: any[];
  proxyOperation: any = '';
  formName: any = '';

  showSearch: boolean = true;
  showForm: boolean = false;
  showData: boolean = false;
  showDataSuccess: boolean = false;

  groupForm!: FormGroup;
  proxyForm!: FormGroup;

  channelDialog = false;
  delChannelDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  submitted: boolean = false;
  submittedForm: boolean = false;

  private selectedOption: string;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxylistService: ProxylistService,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer,
    private datePipe: DatePipe,
    private proxymanagementService: ProxymanagementService,
    private proxytypeService: ProxytypeService,
    private identityTypeService: IdentityTypeService,
    private residentstatusService: ResidentstatusService,
    private custtypeService: CusttypeService,
    private branchService: BranchService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Channel' }, { label: 'Proxy List' }];

    this.clearMessage();

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;

      this.groupForm = this.formBuilder.group({
        searchValue: ['', Validators.required],
        searchOption: ['cif'],
      });

      /* Set status options */
      this.searchOption = [
        { label: 'CIF', value: 'cif' },
        { label: 'Account Number', value: 'acc' },
        { label: 'Proxy', value: 'proxy' },
      ];

      /* Set datatable */
      this.cols = [
        { field: 'cifNumber', header: 'CIF No', width: '200px' },
        { field: 'accountNumber', header: 'Acc No', width: '200px' },
        { field: 'customerName', header: 'Acc Name', width: '150px' },
        { field: 'proxyType', header: 'Type ID', width: '200px' },
        { field: 'customerId', header: 'ID', width: '200px' },
        { field: 'proxyAlias', header: 'Proxy Alias', width: '200px' },
        {
          field: 'proxyStatus',
          header: 'Status',
          transform: true,
          width: '200px',
        },
        { field: 'registrationId', header: 'Registration ID', width: '200px' },
      ];

      this.proxyOperationType = [
        { label: 'New Registration', value: 'NEWR' },
        { label: 'Update / Modify', value: 'AMND' },
        { label: 'Deregistration', value: 'ICTV' },
        { label: 'Deregistration', value: 'DEAC' },
        { label: 'Suspend', value: 'SUSP' },
        { label: 'Suspend', value: 'SUSB' },
        { label: 'Activate', value: 'ACTV' },
        { label: 'Activate', value: 'ACTB' },
        { label: 'Porting', value: 'PORT' },
      ];

      /* Set init selectedColumns */
      this.selectedColumns = this.cols.filter((c, index) => index < 7);

      this.getProxyType();
      this.getResidentStatus();
      this.geCustomerType();
      this.geCustomerIdType();
      this.getBranch();
    });
  }

  get f() {
    return this.groupForm.controls;
  }

  get pf() {
    return this.proxyForm.controls;
  }

  getOptionLabel(value, dataList) {
    let label;
    dataList.forEach((element) => {
      if (element.value == value) {
        label = element.label;
        return label;
      }
    });
    return label;
  }

  getOptionValue(label, options) {
    let value;
    options.forEach((element) => {
      if (element.label == label) {
        value = element.value;
        return value;
      }
    });
    return value;
  }

  disableButton(value, label) {
    let proxyValue = this.getOptionValue(label, this.proxyOperationType);
    if (value == proxyValue) {
      return true;
    } else {
      return false;
    }
  }

  searchProxy() {
    this.submitted = true;
    this.isProcess = true;
    this.proxyData = [];
    this.proxyTabel = false;

    if (this.groupForm.valid) {
      let payload = {};
      let searchOption = this.groupForm.get('searchOption')?.value;
      switch (searchOption) {
        case 'cif':
          payload = {
            cif: this.groupForm.get('searchValue')?.value.trim(),
          };
          break;
        case 'acc':
          payload = {
            accountNumber: this.groupForm.get('searchValue')?.value.trim(),
          };
          break;
        case 'proxy':
          payload = {
            proxyAlias: this.groupForm.get('searchValue')?.value.trim(),
          };
          break;
        default:
          payload = {};
          break;
      }

      this.proxylistService.get(payload).subscribe(
        (result: BackendResponse) => {
          if (result.status == 200 && result.data.responseCode == 'S') {
            this.channelDialog = false;
            this.isProcess = false;
            this.proxyData = result.data.data.list;

            // console.log(this.proxyData);
            // this.proxyData.forEach( (element) => {
            //   element["proxyStatus"] = this.getOptionLabel(element["proxyStatus"],this.proxyOperationType);
            // });

            this.proxyTabel = true;
          } else {
            this.proxyData = [];
            let objtmp = {
              cifNumber: 'No records',
              accountNumber: 'No records',
              customerName: 'No records',
              proxyType: 'No records',
              customerId: 'No records',
              proxyAlias: 'No records',
              proxyStatus: 'No records',
              registrationId: 'No records',
            };
            this.proxyData.push(objtmp);
            this.channelDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Failed -',
              detail: result.data.responseMessage,
            });
          }
        },
        (error) => {
          this.isFetching = false;
          this.isProcess = false;
          this.messageService.add({
            severity: 'error',
            summary: 'Proxy List -',
            detail: 'Internal server error',
          });
        }
      );
    } else {
      this.isProcess = false;
      this.isFetching = false;
    }
  }

  getProxyType() {
    this.isFetching = true;
    this.proxyType = [];

    this.proxytypeService.getAllTByStatusActive().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          let resultList = result.data;
          resultList.forEach((element) => {
            let resultData = {
              label: element.DESCRIPTION,
              value: element.PROXY_TYPE,
            };
            this.proxyType.push(resultData);
          });
        } else {
          this.proxyType = [{ label: 'No Data', value: '-' }];
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Failed get Proxy Type -',
          detail: 'Internal server error',
        });
      }
    );
  }

  geCustomerIdType() {
    this.isFetching = true;
    this.customerIdType = [];

    this.identityTypeService.getAllTByStatusActive().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          let tempData = result.data;
          tempData.forEach((element) => {
            let tempDataOption = {
              label: element.DESCRIPTION,
              value: element.BI_ID_TYPE,
            };
            this.customerIdType.push(tempDataOption);
          });
        } else {
          this.customerIdType = [{ label: 'No Data', value: '-' }];
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Failed get Customer Id Type -',
          detail: 'Internal server error',
        });
      }
    );
  }

  geCustomerType() {
    this.isFetching = true;
    this.customerType = [];

    this.custtypeService.getAllTByStatusActive().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          let tempData = result.data;
          tempData.forEach((element) => {
            let tempDataOption = {
              label: element.DESCRIPTION,
              value: element.BI_CUST_TYPE,
            };
            this.customerType.push(tempDataOption);
          });
        } else {
          this.customerType = [{ label: 'No Data', value: '-' }];
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Failed get Customer Type -',
          detail: 'Internal server error',
        });
      }
    );
  }

  getResidentStatus() {
    this.isFetching = true;
    this.residentStatus = [];

    this.residentstatusService.getAll().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          let tempData = result.data;
          tempData.forEach((element) => {
            let tempDataOption = {
              label: element.DESCRIPTION,
              value: element.BI_RESIDENT_STATUS,
            };
            this.residentStatus.push(tempDataOption);
          });
        } else {
          this.residentStatus = [{ label: 'No Data', value: '-' }];
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Failed get Resident Status -',
          detail: 'Internal server error',
        });
      }
    );
  }

  getBranch() {
    this.isFetching = true;
    this.branch = [];

    this.branchService.getAllTByStatusActive().subscribe(
      (result: BackendResponse) => {
        if (result.status == 200) {
          let tempData = result.data;
          tempData.forEach((element) => {
            let tempDataOption = {
              label: element.BRANCH_NAME,
              value: element.BRANCH_CODE,
            };
            this.branch.push(tempDataOption);
          });
        } else {
          this.branch = [{ label: 'No Data', value: '-' }];
        }
        this.isFetching = false;
      },
      (error) => {
        this.isFetching = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Failed get Branch -',
          detail: 'Internal server error',
        });
      }
    );
  }

  clearMessage() {
    this.messageService.clear();
  }

  cancel() {
    this.clearMessage();
    this.showData = false;
    this.showForm = false;
    this.isFetching = false;
    this.isProcess = false;
    this.proxyTabel = true;
    this.showSearch = true;
  }

  clear() {
    this.clearMessage();
    this.ngOnInit();
    this.submitted = false;
    this.proxyData = [];
    this.dataList = [];
    this.showDataSuccess = false;
    this.showData = false;
    this.proxyTabel = false;
    this.showSearch = true;
  }

  suspend(data: any) {
    this.clearMessage();
    this.dataList = data;
    this.proxyOperation = 'SUSP';
    this.formName = 'Suspend';

    this.proxyOprOptions = [
      { label: 'Request by Bank (SUSB)', value: 'SUSB' },
      { label: 'Request by Customer (SUSP)', value: 'SUSP' },
    ];

    this.proxyForm = this.formBuilder.group({
      refNumber: [''],
      customerAccountNumber: [
        this.dataList['accountNumber'],
        Validators.required,
      ],
      proxyAlias: [this.dataList['proxyAlias'], Validators.required],
      proxyOperationType: ['', Validators.required],
      proxyInformation: ['', Validators.required],
      branchInput: [null, Validators.required],
    });

    this.proxyTabel = false;
    this.showSearch = false;
    this.showForm = true;
  }

  activate(data: any) {
    this.clearMessage();
    this.dataList = data;
    this.proxyOperation = 'ACTV';
    this.formName = 'Activate';

    this.proxyOprOptions = [
      { label: 'Request by Bank (ACTB)', value: 'ACTB' },
      { label: 'Request by Customer (ACTV)', value: 'ACTV' },
    ];

    this.proxyForm = this.formBuilder.group({
      refNumber: [''],
      customerAccountNumber: [
        this.dataList['accountNumber'],
        Validators.required,
      ],
      proxyAlias: [this.dataList['proxyAlias'], Validators.required],
      proxyOperationType: ['', Validators.required],
      proxyInformation: ['', Validators.required],
      branchInput: [null, Validators.required],
    });

    this.proxyTabel = false;
    this.showSearch = false;
    this.showForm = true;
  }

  deactivate(data: any) {
    this.clearMessage();
    this.dataList = data;
    this.proxyOperation = 'DEAC';
    this.formName = 'Deregistrasi';

    this.proxyForm = this.formBuilder.group({
      refNumber: [''],
      customerAccountNumber: [
        this.dataList['accountNumber'],
        Validators.required,
      ],
      proxyAlias: [this.dataList['proxyAlias'], Validators.required],
      proxyInformation: ['', Validators.required],
      branchInput: [null, Validators.required],
    });

    this.proxyTabel = false;
    this.showSearch = false;
    this.showForm = true;
  }

  update(data: any) {
    this.clearMessage();
    this.dataList = data;
    this.proxyOperation = 'AMND';

    this.formName = 'Update';

    this.proxyForm = this.formBuilder.group({
      refNumber: [null],
      registrationId: [this.dataList['registrationId'], Validators.required],
      customerAccountNumber: [
        this.dataList['accountNumber'],
        Validators.required,
      ],
      proxyAlias: [this.dataList['proxyAlias'], Validators.required],
      proxyInformation: ['', Validators.required],
      customerIdType: ['', Validators.required],
      customerId: ['', Validators.required],
      branchInput: [null, Validators.required],
    });

    this.proxyTabel = false;
    this.showSearch = false;
    this.showForm = true;
  }

  inquiry() {
    this.clearMessage();
    this.showForm = false;
    this.isProcess = true;
    this.isFetching = true;

    if (this.groupForm.valid) {
      if (this.proxyOperation == 'ACTV' || this.proxyOperation == 'SUSP') {
        this.dataList['proxyOperationType'] =
          this.proxyForm.get('proxyOperationType')?.value;
      } else {
        this.dataList['proxyOperationType'] = this.proxyOperation;
      }

      // convert value to label
      this.dataList['proxyInformation'] =
        this.proxyForm.get('proxyInformation')?.value;
      this.dataList['branchInput'] = this.proxyForm.get('branchInput')?.value;

      this.isFetching = false;
      this.isProcess = false;
      this.showData = true;
    } else {
      this.isFetching = false;
      this.isProcess = false;
    }
  }

  inquiryUpdate() {
    this.clearMessage();
    this.submitted = true;
    this.isProcess = true;

    // // get check is sec value same
    if (this.dataList['proxyType'] == '02') {
      this.proxyForm.controls['proxyAlias'].setValidators([
        Validators.required,
        Validators.email,
      ]);
      this.proxyForm.controls['proxyAlias'].updateValueAndValidity();
    }

    if (this.groupForm.valid) {
      let payload = {};

      payload = {
        customerAccountNumber: this.proxyForm
          .get('customerAccountNumber')
          ?.value.trim(),
        proxyType: this.dataList['proxyType'],
        proxyAlias: this.proxyForm.get('proxyAlias')?.value,
        proxyOperationType: this.proxyOperation,
        customerSecondaryType: this.dataList['customerSecondaryType'],
        customerSecondaryValue: this.dataList['customerSecondaryValue'],
      };

      this.proxymanagementService.inquiry(payload).subscribe(
        (result: BackendResponse) => {
          if (result.status == 200 && result.data.responseCode == 'S') {
            console.log(this.dataList);

            this.dataList['branchInput'] =
              this.proxyForm.get('branchInput')?.value;
            this.dataList['proxyOperationType'] = this.proxyOperation;
            this.dataList['accountNumber'] = this.proxyForm
              .get('customerAccountNumber')
              ?.value.trim();

            this.isProcess = false;
            this.showForm = false;
            this.showData = true;
          } else {
            this.isFetching = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: null,
              detail: result.data.responseMessage,
            });
          }
        },
        (error) => {
          this.isFetching = false;
          this.isProcess = false;
          this.messageService.add({
            severity: 'error',
            summary: 'Get Data Error -',
            detail: 'Internal server error',
          });
        }
      );
    } else {
      this.isFetching = false;
      this.isProcess = false;
    }
  }

  submit() {
    this.clearMessage();
    this.isProcess = true;
    this.isFetching = true;
    this.submittedForm = true;

    if (this.groupForm.valid) {
      let payload = {};
      payload = {
        proxyOperationType: this.dataList['proxyOperationType'],
        proxyType: this.dataList['proxyType'],
        proxyAlias: this.dataList['proxyAlias'],
        customerAccountNumber: this.dataList['accountNumber'],
        branchInput: this.dataList['branchInput'],
        branchCode: this.dataList['branchCode'],
        registrationId: this.dataList['registrationId'],
        registrationStatus: this.dataList['registrationStatus'],
        customerType: this.dataList['customerType'],
        customerResidentStatus: this.dataList['customerResidentStatus'],
        customerCityCode: this.dataList['customerCityCode'],
        customerAccountType: this.dataList['customerAccType'],
        customerAccountName: this.dataList['customerName'],
        customerSecondaryType: this.dataList['customerSecondaryType'],
        customerSecondaryValue: this.dataList['customerSecondaryValue'],
        customerCurrency: this.dataList['customerCurrency'],
        cifNumber: this.dataList['cifNumber'],
        displayName: this.dataList['displayName'],
        userInput: this.userInfo.userid,
        proxyInformation: this.dataList['proxyInformation'],
      };

      this.proxymanagementService.submit(payload).subscribe(
        (result: BackendResponse) => {
          if (result.status == 200 && result.data.responseCode == 'S') {
            // Assign data and convert field
            this.dataList['customerRegistrationId'] =
              result.data.data.customerRegistrationId;
            this.dataList['refNumber'] = result.data.transactionId;
            this.dataList['responseDateTime'] = result.data.responseDateTime;

            this.isProcess = false;
            this.showData = false;
            this.showDataSuccess = true;
            this.messageService.add({
              severity: 'success',
              summary: null,
              detail: result.data.responseMessage,
            });
          } else {
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: null,
              detail: result.data.responseMessage,
            });
          }
        },
        (error) => {
          this.isFetching = false;
          this.isProcess = false;
          this.messageService.add({
            severity: 'error',
            summary: 'Error -',
            detail: 'Internal server error',
          });
        }
      );
    } else {
      this.isProcess = false;
    }
  }

  resi() {
    let data = this.dataList;

    let pdf = new jsPDF('p', 'mm', [57, 40]);
    pdf.setFontSize(4);
    // line count
    let lineHeight = pdf.getLineHeight() / pdf.internal.scaleFactor;
    let imgUrl = 'assets/logos/logo-bjb.png';
    pdf.addImage(imgUrl, 'PNG', 3, 4, 5, 3);
    pdf.text('BUKTI TRANSAKSI BI FAST', 10, 7);
    pdf.text(
      this.datePipe.transform(data['responseDateTime'], 'dd/MM/yyyy HH:mm'),
      13,
      9
    );

    var yPos = 13;
    var xPosLabel = 6;
    var xPosValue = xPosLabel + 15;

    yPos += lineHeight;
    pdf.setFont(undefined, 'normal');
    pdf.text('Reference No.', xPosLabel, yPos);
    pdf.text(': ' + data['refNumber'], xPosValue, yPos);

    yPos += 2;
    yPos += lineHeight;
    pdf.text('Data Nasabah', xPosLabel, yPos);
    pdf.line(xPosLabel, yPos + 0.2, xPosLabel + 9, yPos + 0.2);

    yPos += 1;
    yPos += lineHeight;
    pdf.text('No. Rekening', xPosLabel, yPos);
    pdf.text(': ' + data['accountNumber'], xPosValue, yPos);

    yPos += lineHeight;
    pdf.text('Nama Nasabah', xPosLabel, yPos);
    var debitedAccountNameSplited = pdf.splitTextToSize(
      data['customerName'],
      20
    );
    var debitedAccountNameSplitedLines = debitedAccountNameSplited.length; // splitted text is a string array
    var blockHeight = debitedAccountNameSplitedLines * yPos;
    pdf.text(': ' + debitedAccountNameSplited, xPosValue, yPos);

    yPos += lineHeight;
    pdf.text('No. Identitas', xPosLabel, yPos);
    pdf.text(': ' + data['customerId'], xPosValue, yPos);

    yPos += 5;
    yPos += lineHeight;
    pdf.text('Data Proxy', xPosLabel, yPos);
    pdf.line(xPosLabel, yPos + 0.2, xPosLabel + 7, yPos + 0.2);

    yPos += 1;
    yPos += lineHeight;
    pdf.text('Jenis Proxy', xPosLabel, yPos);
    pdf.text(
      ': ' + this.getOptionLabel(data['proxyType'], this.proxyType),
      xPosValue,
      yPos
    );

    yPos += lineHeight;
    pdf.text('Proxy', xPosLabel, yPos);
    pdf.text(': ' + data['proxyAlias'], xPosValue, yPos);

    yPos += lineHeight;
    pdf.text('ID Registrasi', xPosLabel, yPos);
    pdf.text(': ' + data['customerRegistrationId'], xPosValue, yPos);

    yPos += 12;
    yPos += lineHeight;
    xPosLabel = 30 - this.userInfo.fullname.length;
    xPosValue = xPosLabel + 9;
    pdf.text('KOMI Portal', xPosLabel, yPos);
    pdf
      .text(': ' + this.userInfo.fullname, xPosValue, yPos)
      .setFont(undefined, 'bold');

    // pdf.output('datauri')
    pdf.save(
      'proxy-management-' +
        data['proxyOperationType'] +
        '-ref-' +
        data['refNumber'] +
        '.pdf'
    );
  }
}
