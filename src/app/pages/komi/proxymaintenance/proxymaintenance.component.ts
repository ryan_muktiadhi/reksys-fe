import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';

@Component({
  selector: 'app-proxymaintenance',
  templateUrl: './proxymaintenance.component.html',
  styleUrls: ['./proxymaintenance.component.scss'],
})
export class ProxymaintenanceComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  proxyData: any[] = [];
  prxheader: any = [
    { label: 'Reg ID', sort: 'registration_id' },
    { label: 'Type', sort: 'proxy_type' },
    { label: 'Value', sort: 'proxy_value' },
    { label: 'Reg BIC', sort: 'registrar_bic' },
    { label: 'Acc Num', sort: 'account_number' },
    { label: 'Acc Type', sort: 'account_type' },
    { label: 'Acc Name', sort: 'account_name' },
    { label: 'Status', sort: 'proxy_status' },
    { label: 'Created', sort: 'created_date' },
  ];
  prxcolname: any = [
    'registration_id',
    'proxy_type',
    'proxy_value',
    'registrar_bic',
    'account_number',
    'account_type',
    'account_name',
    'proxy_status',
    'created_date',
  ];
  prxcolhalign: any = [
    'p-text-center',
    'p-text-center',
    '',
    'p-text-center',
    '',
    '',
    '',
    '',
    '',
  ];
  prxcolwidth: any = ['', '', '', '', '', '', '', '', ''];
  prxcollinghref: any = { url: '#', label: 'Application' };
  prxactionbtn: any = [1, 1, 1, 1, 0];
  prxaddbtn = { route: 'detail', label: 'Add Data' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Proxy Maintenance' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh BIC ');
    // this.proxyadminService.getAllProxyByTenant().subscribe((result: BackendResponse) => {
    //   console.log('>>>>>>> ' + JSON.stringify(result));
    //   if(result.status === 202) {
    //       this.proxyData = [];

    //       let objtmp = {"registration_id":"No records", "proxy_type":"No records", "proxy_value":"No records", "registrar_bic":"No records", "account_number":"No records", "account_type":"No records", "account_name":"No records", "identification_number_type":"No records","identification_number_value":"No records", "proxy_status":"No records", "created_date":"No records"};
    //       this.proxyData.push(objtmp);
    //   } else {
    //     this.proxyData = result.data.bicAdmins;
    //   }
    //   // if(result.data.userGroup.length > 0) {
    //   //   this.proxyData = [];
    //   //   this.proxyData = result.data.userGroup;
    //   // } else {
    //   //   this.proxyData = [];
    //   //   let objtmp = {"groupname":"No records"};
    //   //   this.proxyData.push(objtmp);
    //   // }
    //   this.isFetching=false;
    // });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedProxy = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedProxy = data;
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
