import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxymaintenancedetailComponent } from './proxymaintenancedetail.component';

describe('ProxymaintenancedetailComponent', () => {
  let component: ProxymaintenancedetailComponent;
  let fixture: ComponentFixture<ProxymaintenancedetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxymaintenancedetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxymaintenancedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
