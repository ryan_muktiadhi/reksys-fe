import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxysummaryComponent } from './proxysummary.component';

describe('ProxysummaryComponent', () => {
  let component: ProxysummaryComponent;
  let fixture: ComponentFixture<ProxysummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxysummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxysummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
