import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';

@Component({
  selector: 'app-proxysummary',
  templateUrl: './proxysummary.component.html',
  styleUrls: ['./proxysummary.component.scss'],
})
export class ProxysummaryComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';
  proxSumData: any[] = [];
  tempProxSumData: any[] = [];

  //NEW ADD : Columns
  cols: any[];
  selectedColumns: any[];

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private proxyadminService: ProxyadminService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reporting' }, { label: 'Proxy Summary' }];

    //NEW ADD : State Columns
    this.cols = [
      {
        field: 'branchCode',
        header: 'Branch Name',
        totalHead: true,
        wscolW: 20,
      },
      {
        field: 'totalProxy',
        header: 'Total Proxy',
        totalHead: false,
        wscolW: 15,
      },
      { field: 'countActv', header: 'ACTV', totalHead: false, wscolW: 10 },
      { field: 'countIctv', header: 'ICTV', totalHead: false, wscolW: 10 },
      { field: 'countSusp', header: 'SUSP', totalHead: false, wscolW: 10 },
      { field: 'countSusb', header: 'SUSB', totalHead: false, wscolW: 10 },
      { field: 'countPort', header: 'PORT', totalHead: false, wscolW: 10 },
    ];

    this.selectedColumns = this.cols;

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    this.proxSumData = [];
    let objtmp = {
      branchCode: 'No record',
      totalProxy: 'No record',
      countActv: 'No record',
      countIctv: 'No record',
      countSusp: 'No record',
      countSusb: 'No record',
      countPort: 'No record',
    };
    this.proxSumData.push(objtmp);

    this.initialData();
  }

  initialData() {
    this.isFetching = true;

    let payload = [];
    this.proxyadminService
      .getProxySummary(payload)
      .subscribe((result: BackendResponse) => {
        if (result.status == 200) {
          for (let i = 0; i < result.data.results.length; i++) {
            this.tempProxSumData.push({
              branchCode: result.data.results[i].branchCode,
              totalProxy: parseInt(result.data.results[i].count),
              countActv:
                result.data.results[i].proxyStatus == 'ACTV'
                  ? parseInt(result.data.results[i].count)
                  : 0,
              countIctv:
                result.data.results[i].proxyStatus == 'ICTV'
                  ? parseInt(result.data.results[i].count)
                  : 0,
              countSusp:
                result.data.results[i].proxyStatus == 'SUSP'
                  ? parseInt(result.data.results[i].count)
                  : 0,
              countSusb:
                result.data.results[i].proxyStatus == 'SUSB'
                  ? parseInt(result.data.results[i].count)
                  : 0,
              countPort:
                result.data.results[i].proxyStatus == 'PORT'
                  ? parseInt(result.data.results[i].count)
                  : 0,
            });
          }

          let map = this.tempProxSumData.reduce((prev, next) => {
            if (next.branchCode in prev) {
              prev[next.branchCode].totalProxy += next.totalProxy;
              prev[next.branchCode].countActv += next.countActv;
              prev[next.branchCode].countIctv += next.countIctv;
              prev[next.branchCode].countSusp += next.countSusp;
              prev[next.branchCode].countSusb += next.countSusb;
              prev[next.branchCode].countPort += next.countPort;
            } else {
              prev[next.branchCode] = next;
            }
            return prev;
          }, {});

          this.tempProxSumData = Object.keys(map).map(
            (branchCode) => map[branchCode]
          );

          // console.log(this.tempProxSumData);

          this.proxSumData = this.tempProxSumData;
        } else {
          if (result.data.status == 'F') {
            this.messageService.add({
              severity: 'warn',
              summary: 'Failed',
              detail: 'Data not found',
            });
          } else if (result.data.status == 'E') {
            this.messageService.add({
              severity: 'error',
              summary: 'Error',
              detail: result.data.statusMessage,
            });
          }
        }
        this.isFetching = false;
      });
  }

  getSumTotalProxy(data: any[]) {
    return data.reduce((acc, { totalProxy }) => (acc += +(totalProxy || 0)), 0);
  }
  getSumActv(data: any[]) {
    return data.reduce((acc, { countActv }) => (acc += +(countActv || 0)), 0);
  }
  getSumIctv(data: any[]) {
    return data.reduce((acc, { countIctv }) => (acc += +(countIctv || 0)), 0);
  }
  getSumSusp(data: any[]) {
    return data.reduce((acc, { countSusp }) => (acc += +(countSusp || 0)), 0);
  }
  getSumSusb(data: any[]) {
    return data.reduce((acc, { countSusb }) => (acc += +(countSusb || 0)), 0);
  }
  getSumPort(data: any[]) {
    return data.reduce((acc, { countPort }) => (acc += +(countPort || 0)), 0);
  }

  exportExcel() {
    let myDate = new Date();
    let currentDate = this.datepipe.transform(myDate, 'd MMM y');
    let wscols = [];
    let heading = [['PROXY SUMMARY REPORT']];
    let subHeading = [['PERIOD: ' + currentDate]];

    wscols = this.selectedColumns.map((col) => ({ width: col.wscolW }));

    let element = document.getElementById('tb-prxsum');
    const worksheet = XLSX.utils.table_to_sheet(element, null); //{ origin: 'A3' });

    XLSX.utils.sheet_add_aoa(worksheet, heading);
    XLSX.utils.sheet_add_aoa(worksheet, subHeading, { origin: 'A2' });

    // this.create_gap_rows(worksheet, 1);

    // XLSX.utils.sheet_add_aoa(worksheet, subHeading, { origin: -1 });

    worksheet['!merges'] = [
      { s: { c: 0, r: 0 }, e: { c: wscols.length - 1, r: 0 } },
      { s: { c: 0, r: 1 }, e: { c: wscols.length - 1, r: 1 } },
    ];
    worksheet['!cols'] = wscols;

    const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'array',
    });
    this.saveAsExcelFile(
      excelBuffer,
      'PROXY SUMMARY REPORTING - ' + currentDate
    );
    /* save to file */
    // XLSX.writeFile(excelBuffer, "products");
  }

  create_gap_rows(ws, nrows) {
    var ref = XLSX.utils.decode_range(ws['!ref']); // get original range
    ref.e.r += nrows; // add to ending row
    ws['!ref'] = XLSX.utils.encode_range(ref); // reassign row
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
  }
}
