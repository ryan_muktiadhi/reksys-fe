import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxytypeComponent } from './proxytype.component';

describe('ProxytypeComponent', () => {
  let component: ProxytypeComponent;
  let fixture: ComponentFixture<ProxytypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxytypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxytypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
