import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { ProxytypeService } from 'src/app/services/komi/proxytype.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-proxytype',
  templateUrl: './proxytype.component.html',
  styleUrls: ['./proxytype.component.scss'],
})
export class ProxytypeComponent implements OnInit {
  display = false;
  displayPrv = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';

  prxData: any[] = [];
  cols: any[];
  selectedColumns: any[];
  status: any[];

  prxDataheader: any = [
    { label: 'Proxy Code', sort: 'proxy_code' },
    { label: 'Proxy Name', sort: 'proxy_name' },
    { label: 'Status', sort: 'status' },
  ];
  prxDatacolname: any = ['proxy_code', 'proxy_name', 'status', 'active'];

  prxDatacolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  prxDatacolwidth: any = ['', '', '', '', ''];
  prxDatacollinghref: any = { url: '#', label: 'Application' };
  prxDataactionbtn: any = [0, 1, 0, 1, 0, 1, 1];
  prxDataaddbtn = { label: 'Add Data' };

  groupForm!: FormGroup;

  formDialog = false;
  delFormDialog = false;
  delData: any = [];

  isFetching: boolean = false;
  isProcess: boolean = false;
  isEdit: boolean = false;
  submitted: boolean = false;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxytypeService: ProxytypeService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Proxy Type' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    this.groupForm = this.formBuilder.group({
      id: [''],
      proxyCode: ['', Validators.required],
      proxyName: ['', Validators.required],
      status: ['', Validators.required],
    });

    /* Set status options */
    this.status = [
      { label: 'ACTIVE', value: 1 },
      { label: 'INACTIVE', value: 0 },
    ];

    /* Set datatable */
    this.cols = [
      { field: 'proxy_code', header: 'Proxy Code' },
      { field: 'proxy_name', header: 'Proxy Name' },
      { field: 'status', header: 'Status', transform: true },
      { field: 'created_date', header: 'Created Date', width: '200px' },
      { field: 'created_by', header: 'Created By', width: '200px' },
      { field: 'updated_date', header: 'Updated Date', width: '200px' },
      { field: 'updated_by', header: 'Updated By', width: '200px' },
    ];

    /* Set init selectedColumns */
    this.selectedColumns = this.cols.filter((c, index) => index < 4);

    this.refreshingPrx();
  }

  get f() {
    return this.groupForm.controls;
  }

  refreshingPrx() {
    this.isFetching = true;
    console.log('>>>>>>> Refresh Prx ');
    this.proxytypeService.getAll().subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));

      if (result.status === 200) {
        this.prxData = result.data;
      } else {
        this.prxData = [];
        let objtmp = {
          proxy_code: 'No records',
          proxy_name: 'No records',
          status: 'No records',
          created_date: 'No records',
          created_by: 'No records',
          updated_date: 'No records',
          updated_by: 'No records',
        };
        this.prxData.push(objtmp);
      }
      this.isFetching = false;
    });
  }

  /* Open modal add data */
  add() {
    this.groupForm;
    this.isEdit = false;
    this.formDialog = true;
    this.groupForm.reset();
  }

  /* Submit add data or edit data */
  onSubmit() {
    this.isProcess = true;
    this.submitted = true;

    if (this.groupForm.valid) {
      let payload = {};
      if (!this.isEdit) {
        payload = {
          proxyCode: this.groupForm.get('proxyCode')?.value,
          proxyName: this.groupForm.get('proxyName')?.value,
          status: this.groupForm.get('status')?.value,
        };

        this.proxytypeService.insert(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.formDialog = false;
              this.isProcess = false;
              this.groupForm.reset();
              this.submitted = false;
              this.refreshingPrx();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Inserted',
              });
            } else {
              this.formDialog = false;
              this.isProcess = false;
              this.groupForm.reset();
              this.submitted = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.groupForm.reset();
            this.formDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Insert Data Error -',
              detail: errorDetail,
            });
          }
        );
      } else {
        payload = {
          id: this.groupForm.get('id')?.value,
          proxyCode: this.groupForm.get('proxyCode')?.value,
          proxyName: this.groupForm.get('proxyName')?.value,
          status: this.groupForm.get('status')?.value,
        };
        this.proxytypeService.update(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status == 200) {
              this.formDialog = false;
              this.isProcess = false;
              this.groupForm.reset();
              this.submitted = false;
              this.refreshingPrx();
              this.messageService.add({
                severity: 'success',
                summary: 'Success -',
                detail: 'Data Updated',
              });
            } else {
              this.formDialog = false;
              this.isProcess = false;
              this.groupForm.reset();
              this.submitted = false;
              this.messageService.add({
                severity: 'error',
                summary: 'Failed -',
                detail: result.data,
              });
            }
          },
          async (error) => {
            let errorInput: any = [];
            let errorDetail = 'Internal server error';
            if (error.error.errors) {
              const errors = error.error.errors;
              await errors.map((err) => {
                errorInput.push(`${err.msg}`);
              });
              errorDetail = errorInput.join();
            }
            this.groupForm.reset();
            this.formDialog = false;
            this.isProcess = false;
            this.messageService.add({
              severity: 'error',
              summary: 'Insert Data Error -',
              detail: errorDetail,
            });
          }
        );
      }
    } else {
      this.isProcess = false;
    }
  }

  edit(data: any) {
    this.groupForm;
    this.isEdit = true;

    this.groupForm.patchValue({
      proxyCode: data['proxy_code'],
      proxyName: data['proxy_name'],
      status: data['status'],
      id: data['id'],
    });
    this.formDialog = true;
  }

  deleteConfirmation(data: any) {
    this.delData = data;
    this.delFormDialog = true;
  }

  delete() {
    this.isProcess = true;
    let dataPayload = this.delData;

    const payload = { dataPayload };
    this.proxytypeService.delete(payload.dataPayload.id).subscribe(
      (resp: BackendResponse) => {
        this.delFormDialog = false;
        this.isProcess = false;
        this.refreshingPrx();
        this.messageService.add({
          severity: 'success',
          summary: 'Success -',
          detail: 'Data Deleted',
        });
      },
      (error) => {
        this.delFormDialog = false;
        this.isProcess = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Update Data Error -',
          detail: 'Internal server error',
        });
      }
    );
  }
}
