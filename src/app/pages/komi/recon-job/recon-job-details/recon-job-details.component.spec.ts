import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconJobDetailsComponent } from './recon-job-details.component';

describe('ReconJobDetailsComponent', () => {
  let component: ReconJobDetailsComponent;
  let fixture: ComponentFixture<ReconJobDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconJobDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconJobDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
