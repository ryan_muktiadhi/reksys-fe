import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';
import { Location } from '@angular/common';
import { CronOptions } from 'cron-editor/lib/CronOptions';

@Component({
  selector: 'app-recon-job-details',
  templateUrl: './recon-job-details.component.html',
  styleUrls: ['./recon-job-details.component.scss'],
})
export class ReconJobDetailsComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  jobId: any;
  jobName: any;
  jobDesc: any;

  source1: any;
  source1Options: any[];

  source2: any;
  source2Options: any[];

  sourceMatchingField: any[] = [];
  targetMatchingField: any[] = [];

  sourceResultField: any[] = [];
  targetResultField: any[] = [];
  submit: boolean = false;
  sucessDialog: any = false;
  dialogMessage: string;
  availProp2: any = [];
  availProp1: any = [];

  availProp2Options: any = [];
  availProp1Options: any = [];

  matchingField1: any = [];
  matchingField2: any = [];

  reportField1: any = [];
  reportField2: any = [];
  selectAllReport1: any;
  selectAllReport2: any;
  disableMatch: boolean = true;
  isEdit: boolean;
  autoGenerateReport: any = false;
  autoGenerateReportOptions: [
    { label: 'Off'; value: 'false' },
    { label: 'On'; value: 'true' }
  ];

  //////////////////////////////////GUBAHAN RYAN//////////////////////
  dataPopulatManualSc1 = true;
  dataPopulatManualSc2 = true;
  execManualTypeSc = true
  dataPopulateSc1:any = false;
  // dataPopulatesSc1 = [{name: 'Manual', code: 0},{name: 'Scheduled', code: 1}]
  dataPopulateSc2:any = false;
  // dataPopulatesSc2 = [{name: 'Manual', code: 0},{name: 'Scheduled', code: 1}]
  execScType:any = false;
  cronsc1:any;
  filePathSc1:any;
  cronsc2:any;
  filePathSc2:any;
  execSc:any;
  cronDialog1: boolean = false;
  cronDialog2: boolean = false;
  cronDialog3: boolean = false;
////////////////////////////////////THIS IS FOR CRON/////////////////////////////////
  public cronExpression1 = '0 0 10 1/1 * ? *';
  public cronExpression2 = '0 0 10 1/1 * ? *';
  public cronExpression3 = '0 0 10 1/1 * ? *';
  public isCronDisabled = false;
  public cronOptions: CronOptions = {
    formInputClass: 'form-control cron-editor-input',
    formSelectClass: 'form-control cron-editor-select',
    formRadioClass: 'cron-editor-radio',
    formCheckboxClass: 'cron-editor-checkbox',
    defaultTime: '10:00:00',
    use24HourTime: true,
    hideMinutesTab: true,
    hideHourlyTab: false,
    hideDailyTab: false,
    hideWeeklyTab: false,
    hideMonthlyTab: false,
    hideYearlyTab: true,
    hideAdvancedTab: false,
    hideSeconds: true,
    removeSeconds: false,
    removeYears: false
  };




  ///////////////////////////////////////

  constructor(
    public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Registration', command: (event) => {
      this.location.back();
    }  },  { label: 'Detail'}];

    this.rekonsiliasiService.getEntity().subscribe((resp: any) => {
      if (resp.status == 200) {
        this.source1Options = resp.data;
        this.source2Options = resp.data;
      }
    });
    try {
      this.jobId = this.activatedRoute.snapshot.paramMap.get('id');
      console.log(this.jobId);
      this.isEdit = this.jobId ? true : false;
      if (this.isEdit) {
        this.initDetailData();
      }
    } catch (err) {
      console.log(err);
    }
  }

  initDetailData() {
    this.rekonsiliasiService.getJobsById(this.jobId).subscribe(async (resp) => {
      if (resp.status == 200) {
        this.jobName = resp.data.jobName;
        this.jobDesc = resp.data.jobDesc;
        this.source1 = resp.data.billedEntity;
        this.source2 = resp.data.referenceEntity;
        await this.getAvailProp1();
        await this.getAvailProp2();
        this.matchingField1 = resp.data.matchField1;
        this.matchingField2 = resp.data.matchingField2;
        this.reportField1 = resp.data.reportField1;
        this.reportField2 = resp.data.reportField2;
        console.log(this.reportField1);
      }
    });
  }

  checkCanMatch() {
    if (this.availProp1 && this.availProp2) {
      this.disableMatch = false;
    }
    console.log(this.disableMatch);
  }

  getMatchingField() {
    if (this.disableMatch == false) {
      this.matchingField1.push(`${this.availProp1}`);
      this.matchingField2.push(`${this.availProp2}`);
    }
  }
  removeMatch(selectedFiled1, selectedFiled2) {
    console.log(this.matchingField1);
    console.log(this.matchingField2);
    this.matchingField1 = this.matchingField1.filter(
      (dt) => dt !== selectedFiled1
    );
    this.matchingField2 = this.matchingField2.filter(
      (dt) => dt !== selectedFiled2
    );
    console.log(this.matchingField1);
    console.log(this.matchingField2);
  }

  getAvailProp1() {
    this.rekonsiliasiService
      .getReportProp(this.source1)
      .subscribe((resp: any) => {
        console.log(resp);
        if (resp.status == 200) {
          this.availProp1Options = resp.data;
        }
      });
  }

  onSelectAllReport1() {
    if (this.selectAllReport1 == false) {
      this.reportField1 = [];
    }
    if (this.selectAllReport1 == true) {
      this.reportField1 = this.availProp1Options;
    }
  }

  onSelectAllReport2() {
    if (this.selectAllReport2 == false) {
      this.reportField2 = [];
    }
    if (this.selectAllReport2 == true) {
      this.reportField2 = this.availProp2Options;
    }
  }

  getAvailProp2() {
    this.rekonsiliasiService
      .getReportProp(this.source2)
      .subscribe((resp: any) => {
        console.log(resp);
        if (resp.status == 200) {
          this.availProp2Options = resp.data;
        }
      });
  }

  onChangeSc1(){
    console.log(this.dataPopulateSc1);
  }
  onChangeSc2(){
    console.log(this.dataPopulateSc2);
  }
  onChangeEtype(){
    console.log(this.execScType);
  }

  cronDialog1Click(){
    this.cronDialog1=true;
  }
  cronSave1(){
    this.cronDialog1= false;
    this.cronsc1 = this.cronExpression1;
  }



  cronDialog2Click(){
    this.cronDialog2=true;
  }
  cronSave2(){
    this.cronDialog2= false;
    this.cronsc2 = this.cronExpression2;
  }
  cronDialog3Click(){
    this.cronDialog3=true;
  }
  cronSave3(){
    this.cronDialog3= false;
    this.execSc = this.cronExpression3;
  }

  onSubmit() {
    let payload: any = {};
    payload.idJob = this.jobId || null;
    payload.jobName = this.jobName;
    payload.jobDesc = this.jobDesc;
    payload.source1 = this.source1;
    payload.source2 = this.source2;
    payload.matchingField1 = this.matchingField1;
    payload.matchingField2 = this.matchingField2;
    payload.reportField1 = this.reportField1;
    payload.reportField2 = this.reportField2;
    payload.autoGenerateReport = this.autoGenerateReport;
   
    payload.cronsc1 = this.cronsc1;
    payload.cronsc2 = this.cronsc2;
    payload.execSc = this.execSc;
    payload.filePathSc1 = this.filePathSc1;
    payload.filePathSc2 = this.filePathSc2;
    payload.dataPopulateSc1 = this.dataPopulateSc1;
    payload.dataPopulateSc2 = this.dataPopulateSc2;
    payload.execScType = this.execScType;
    this.submit = true;
    if (!this.jobId) {
      this.rekonsiliasiService.postJob(payload).subscribe(
        (resp: any) => {
          if ((resp.status = 200)) {
            this.sucessDialog = true;
            this.dialogMessage = 'Data Successfully added';
          }

          this.submit = false;
        },
        (err: any) => {
          this.sucessDialog = true;
          this.dialogMessage = 'Internal Error';
        }
      );
    } else {
      this.rekonsiliasiService.editJob(payload).subscribe(
        (resp: any) => {
          if ((resp.status = 200)) {
            this.sucessDialog = true;
            this.dialogMessage = 'Data Successfully Edited';
          }

          this.submit = false;
        },
        (err: any) => {
          this.sucessDialog = true;
          this.dialogMessage = 'Internal Error';
        }
      );
    }
  }



  confirmSuccess() {
    this.sucessDialog = false;
    this.router.navigate(['/mgm/rekonsiliasi/reconjob']);
  }
}
