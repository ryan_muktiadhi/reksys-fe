import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconJobComponent } from './recon-job.component';

describe('ReconJobComponent', () => {
  let component: ReconJobComponent;
  let fixture: ComponentFixture<ReconJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconJobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
