import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';

@Component({
  selector: 'app-recon-job',
  templateUrl: './recon-job.component.html',
  styleUrls: ['./recon-job.component.scss'],
})
export class ReconJobComponent implements OnInit {
  display = false;
  scrollheight: any = '400px';
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;

  userInfo: any = {};
  tokenID: string = '';
  newData: any;
  oldData: any;
  usrheader: any = [
    { label: 'Id', sort: 'id' },
    { label: 'Name', sort: 'jobName' },
    { label: 'Primary Data', sort: 'billedEntity' },
    { label: 'Secondary Data', sort: 'referenceEntity' },
    { label: 'Description', sort: 'jobDesc' },
  ];
  usrcolname: any = [
    'id',
    'jobName',
    'billedEntity',
    'referenceEntity',
    'jobDesc',
  ];
  usrcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];

  usrcolwidth: any = [{ width: '130px' },
  { width: '230px' },
  { width: '200px' }, { width: '200px' }, { width: '230px' }];

  usractionbtn: any = [1, 1, 1, 1, 0, 0];
  usraddbtn = { route: 'detail', label: 'Add' };

  constructor(
    public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Registration' }];
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.rekonsiliasiService.getJobs().subscribe((orgall: any) => {
      console.log('>>>>>>> ' + JSON.stringify(orgall));
      this.userlist = orgall.data;
      if (this.userlist.length < 1) {
        let objtmp = {
          fullname: 'No records',
          userid: 'No records',
          active: 'No records',
          groupname: 'No records',
        };
        this.userlist = [];
        this.userlist.push(objtmp);
      }
      this.isFetching = false;
    });
  }

  deleteConfirmation(data: any) {
    this.display = true;
    this.selectedUser = data;
  }

  deleteRecon() {
    // console.log(this.selectedUser);
    let reconId = this.selectedUser.id;
    this.rekonsiliasiService.deleteRecon(reconId).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.showTopSuccess(resp.data);
      }
      this.display = false;
      this.refreshingUser();
    });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
