import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconResultDetailComponent } from './recon-result-detail.component';

describe('ReconResultDetailComponent', () => {
  let component: ReconResultDetailComponent;
  let fixture: ComponentFixture<ReconResultDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconResultDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconResultDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
