import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';
import * as XLSX from 'xlsx';
import { Location } from '@angular/common';
@Component({
  selector: 'app-recon-result-detail',
  templateUrl: './recon-result-detail.component.html',
  styleUrls: ['./recon-result-detail.component.scss'],
})
export class ReconResultDetailComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;

  jobName: any;
  jobDesc: any;

  source: any;
  sourceOptions: any[];

  status: any;
  statusOptions: any[];

  sourceMatchingField: any[] = [];
  targetMatchingField: any[] = [];

  sourceResultField: any[] = [];
  targetResultField: any[] = [];
  submit: boolean = false;
  sucessDialog: any = false;
  dialogMessage: string;
  sessionId: string;

  msgs1:any[]=[];


  detailHeader: any = [
    { label: 'Reference Number', sort: 'retrievalReferenceNumber' },
    { label: 'From Account', sort: 'fromAccount' },
    { label: 'To Account', sort: 'toAccount' },
    { label: 'Amount', sort: 'amountTransaction' },
  ];
  detailcolname: any = [
    'retrievalReferenceNumber',
    'fromAccount',
    'toAccount',
    'amountTransaction',
  ];
  detailcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  detailOnFetch: boolean = false;

  detailwidth: any = ['', '', '', '', '', ''];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  detailactionbtn: any = [0, 1, 0, 0, 0, 0, 0, 0];
  detailData: any[] = [];
  detailNoData: boolean;
  constructor(
    public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Result', command: (event) => {
      this.location.back();
    } },{ label: 'Detail' }];

    this.sessionId = this.activatedRoute.snapshot.paramMap.get('id');
    if (!this.sessionId) {
      this.router.navigate([`mgm/rekonsiliasi/rekonsiliasi`]);
    }
    this.initialData();
  }

  initialData() {
    // this.sourceOptions = [
    //   // { name: 'CBS', code: 'cbs' },
    //   // { name: 'BIFast', code: 'bifast' },
    //   'CBS',
    //   'BIFAST',
    // ];
    this.statusOptions = [
      { name: 'Match', code: 'MATCH' },
      { name: 'Not Match', code: 'NOT MATCH' },
      // { name: 'Pending', code: 'PENDING' },
      // { name: 'All', code: 'ALL' },
    ];
    this.rekonsiliasiService
      .getSessionsById(this.sessionId)
      .subscribe((resp: any) => {
        console.log(resp);
        this.sourceOptions = resp.data;
        // this.statusOptions = [
        //   { name: 'Match', code: 'MATCH' },
        //   { name: 'Not Match', code: 'NOT_MATCH' },
        //   { name: 'Pending', code: 'PENDING' },
        //   // { name: 'All', code: 'ALL' },
        // ];
      });
  }

  onSubmit() {
    let params = {
      sesId: this.sessionId,
      sourceName: this.source,
      stat: this.status,
    };
    this.fetchDetailData(params);
  }

  async fetchDetailData(params: any) {
    this.detailData = [];
    this.detailNoData = false;
    this.detailOnFetch = true;
    this.rekonsiliasiService
      .getSessionsDetail(params)
      .subscribe(async (resp: any) => {
        if (resp.data) {
          let data = resp.data;
          if (data) {
            if (data.length > 0) {
              let parsedData = JSON.parse(data);
              this.detailData = parsedData;
              console.log(">>>>>>>> PANJANG DATA ", parsedData.length);
              if(parsedData.length > 0) {
                await this.setDetailHeader(parsedData[0]);
              } else {
                this.msgs1.push({severity:'error', summary:'Error', detail:'No data found'});
                this.detailOnFetch = false;
              }

              // await this.setDetailHeader(parsedData[0]);
            } else {
              console.log(13);
              this.detailNoData = true;
            }
          }
        } else {
          console.log(12);
          this.detailNoData = true;
        }
        this.detailOnFetch = false;
      });
  }
  async setDetailHeader(data: any) {
    console.log(data);
    let detailHeaders = [];
    let detailCol = [];
    for (const key of Object.keys(data)) {
      console.log(key);
      let detailHeaderData: any = {};
      detailHeaderData.label = key;
      detailHeaderData.sort = key;
      detailHeaders.push(detailHeaderData);
      detailCol.push(key);
    }
    this.detailcolname = detailCol;
    this.detailHeader = detailHeaders;
  }

  confirmSuccess() {
    this.sucessDialog = false;
    this.router.navigate(['/mgm/rekonsiliasi/reconjob']);
  }

  exportexcel(): void {
    let today = new Date();

    var number = 1;
    var col = ['No'];
    this.detailHeader.map((dt) => {
      console.log(dt.label);
      col.push(dt.label);
    });

    var rows = [];
    rows.push(col);
    let tempRow;
    this.detailData.map(async (el) => {
      tempRow = [];
      tempRow.push(number++);
      for (const key of Object.keys(el)) {
        tempRow.push(el[key]);
      }
      rows.push(tempRow);
    });

    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(rows);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, `${this.sessionId}_${this.source}_${this.status}.csv`);
  }
}
