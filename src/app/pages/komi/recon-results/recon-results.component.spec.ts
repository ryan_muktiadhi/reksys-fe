import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconResultsComponent } from './recon-results.component';

describe('ReconResultsComponent', () => {
  let component: ReconResultsComponent;
  let fixture: ComponentFixture<ReconResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
