import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Console } from 'console';
import { MenuItem, MessageService } from 'primeng/api';
import { TabViewModule } from 'primeng/tabview';
import { Location } from '@angular/common';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';

@Component({
  selector: 'app-recon-results',
  templateUrl: './recon-results.component.html',
  styleUrls: ['./recon-results.component.scss'],
})
export class ReconResultsComponent implements OnInit {
  display = false;
  scrollheight: any = '400px';
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  newData: any;
/////////////////////////////////////GUBAHAN RYAN//////////////////////////////////
 // isCreateReport = false;
  indexRow:any[] = [];
  showFilter = false;
  cols: any[] = [];
  defaultColumn:any;
  selectedColumns: any[];
  haveActionBtn: any;
  isActionBtn = true;
//////////////////////////////////////////////////////////////////////////////////////////
  oldData: any;
  usrheader: any = [
    { label: 'Session Id', sort: 'sessionId' },
    { label: 'Recon Name', sort: 'jobName' },
    { label: 'Status', sort: 'status' },
    { label: 'Start Date', sort: 'datetimeCreation' },
    { label: 'End Date', sort: 'datetimeFinish' },
    { label: 'Primary Data', sort: 'billedEntity' },
    { label: 'Secondary Data', sort: 'referenceEntity' },
    { label: 'Total Match', sort: 'matchCount' },
    { label: 'Primary Data Total Not Match', sort: 'billedNotMatchCount' },
    // { label: 'Primary Data Total Pending', sort: 'billedPendingTotal' },

    // ,
    { label: 'Secondary Data Total Not Match', sort: 'referenceNotMatchCount' },
    // { label: 'Secondary Data Total Pending', sort: 'refPendingTotal' },
  ];
  usrcolname: any = [
    'sessionId',
    'jobName',
    'status',
    'datetimeCreation',
    'datetimeFinish',
    'billedEntity',
    'referenceEntity',
    'matchCount',
    'billedNotMatchCount  ',
    'referenceNotMatchCount  ',
    // 'refPendingTotal',
  ];
  usrcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  // usrcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  usrcolwidth: any = ['', '', '', '', '', ''];

  usractionbtn: any = [0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1];
  usraddbtn = { route: 'detail', label: 'Add Data' };

  detailHeader: any = [
    { label: 'Reference Number', sort: 'retrievalReferenceNumber' },
    { label: 'From Account', sort: 'fromAccount' },
    { label: 'To Account', sort: 'toAccount' },
    { label: 'Amount', sort: 'amountTransaction' },
  ];
  detailcolname: any = [
    'retrievalReferenceNumber',
    'fromAccount',
    'toAccount',
    'amountTransaction',
  ];
  detailcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  detailOnFetch: boolean = false;
  // usrcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  detailwidth: any = ['', '', '', '', '', ''];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  detailactionbtn: any = [0, 1, 0, 0, 0, 0, 0, 0];
  detailData: any[] = [];
  tabList: any[] = [];
  detailNoData: boolean = false;
  constructor(
    public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService,
    private router: Router,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Result' }];
    ////////////////////////////////////////// GUBAHAN RYAN ///////////////////////////////////////////////
    this.defaultColumn = 5;
    this.usrheader.map((head) => {
      let data: any = {};
      data.field = head.sort;
      data.header = head.label;
      data.isOrder = true;
      this.cols.push(data);
    });


    ///////////////////////////////////////////////////////////////////////////
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.detailNoData = false;
    this.rekonsiliasiService.getSessions().subscribe(async (orgall: any) => {
      console.log('>>>>>>> ' + JSON.stringify(orgall));
      await orgall.data.map((dt) => {
        // dt.status = dt.completed == true ?  dt.reportFilePath == null? 'Ready': 'Finished' : 'OnProgress';
        dt.status = dt.completed == true ?  'Finished' : 'OnProgress';
      });
      this.userlist = orgall.data;


    //   'sessionId',
    // 'jobName',
    // 'status',
    // 'datetimeCreation',
    // 'datetimeFinish',
    // 'billedEntity',
    // 'referenceEntity',
    // 'billedMatchTotal',
    // 'billedNotMatchCount  ',
    // 'referenceNotMatchCount  ',
    // 'refPendingTotal',
      if (this.userlist.length < 1) {
        let objtmp = {
          sessionId: 'No records',
          jobName: 'No records',
          status: 'No records',
          datetimeCreation: 'No records',
          datetimeFinish: 'No records',
          billedEntity: 'No records',
          referenceEntity: 'No records',
          matchCount: 'No records',
          billedNotMatchCount: 'No records',
          referenceNotMatchCount: 'No records',
          // refPendingTotal: 'No records'
        };
        this.userlist = [];
        this.userlist.push(objtmp);
      }
      ////////////////////////////////////////// GUBAHAN RYAN ///////////////////////////////////////////////
      let column = this.defaultColumn || 4;
      this.selectedColumns = this.cols.filter((c, index) => index < column);

      if (
        this.userlist[0]?.created_date === 'No records' ||
        this.userlist[0]?.created_date === 'N/A'
      )
        this.isActionBtn = false;
  
      this.haveActionBtn =
        this.usractionbtn[2] == 1 ||
        this.usractionbtn[3] == 1 ||
        this.usractionbtn[4] == 1 ||
        this.usractionbtn[5] == 1 ||
        this.usractionbtn[6] == 1 ||
        this.usractionbtn[7] == 1 ||
        this.usractionbtn[8] == 1 ||
        this.usractionbtn[9] == 1;
    



      //////////////////////////////////////////////////////////////////////////////////////////////////////
      this.isFetching = false;
    });
  }

  viewDetail(data: any) {
    this.router.navigate([
      `mgm/rekonsiliasi/reconresults/detail#/${data.sessionId}`,
    ]);
    // this.viewDisplay = true;
    // this.detailOnFetch = true;
    // this.selectedUser = data;
    // this.detailNoData = false;
    // this.getDetailSession(data);
    // this.tabList = [
    //   {
    //     title: `${data.billedEntity.toUpperCase()} Match`,
    //   },
    //   {
    //     title: `${data.billedEntity.toUpperCase()} UnMatch`,
    //   },
    //   {
    //     title: `${data.billedEntity.toUpperCase()} Pending`,
    //   },
    //   {
    //     title: `${data.referenceEntity.toUpperCase()} Match`,
    //   },
    //   {
    //     title: `${data.referenceEntity.toUpperCase()} UnMatch`,
    //   },
    //   {
    //     title: `${data.referenceEntity.toUpperCase()} Pending`,
    //   },
    // ];
    // console.log(this.tabList[0].title);
  }

  async generateReportDetail(data: any){
    // this.isCreateReport = true;
    console.log("Console Index ", data);
    this.indexRow.push(data.sessionId);
    this.removearrayGReport(data.sessionId);
    console.log("Keluar");
  }

  



  downloadReport(data: any) {
    console.log(data);
    if (data.reportFilePath) {
      this.rekonsiliasiService.downloadReport(data.sessionId);
    } else if (!data.reportFilePath) {
      this.showTopWarning('Report file not found');
    }
  }

  showTopInfo(message: string) {
    this.messageService.add({
      severity: 'info',
      summary: 'Info',
      detail: message,
    });
  }
  showTopWarning(message: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Warning',
      detail: message,
    });
  }
  removearrayGReport(sessionId: any) {
    console.log("First ", this.indexRow);
    let datapayload = {sessionId: sessionId}
    // setTimeout(() => {
    //   console.log('It will be printed 3-rd with delay ', sessionId);
    //   const index: number =  this.indexRow.indexOf(sessionId);
    //   if (index !== -1) {
    //     this.indexRow.splice(index, 1);
    //     this.refreshingUser();
    //   } 
    // }, 2000);
    this.rekonsiliasiService.getGenerateReport(datapayload).subscribe((genall: any)=>{
      console.log('>>>>>>> ' + JSON.stringify(genall));
      // this.refreshingUser();
        // console.log('It will be printed 3-rd with delay ', sessionId);
        const index: number =  this.indexRow.indexOf(sessionId);
        if (index !== -1) {
          this.indexRow.splice(index, 1);
          this.refreshingUser();
        } 
    });



  }
}


