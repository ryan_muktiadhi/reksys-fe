import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconScheduleComponent } from './recon-schedule.component';

describe('ReconScheduleComponent', () => {
  let component: ReconScheduleComponent;
  let fixture: ComponentFixture<ReconScheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconScheduleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
