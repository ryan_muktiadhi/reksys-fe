import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';

@Component({
  selector: 'app-recon-schedule',
  templateUrl: './recon-schedule.component.html',
  styleUrls: ['./recon-schedule.component.scss']
})
export class ReconScheduleComponent implements OnInit {
  display = false;
  scrollheight: any = '400px';
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;

  userInfo: any = {};
  tokenID: string = '';
  newData: any;
  oldData: any;

  // let objtmp = {
    // sourceId:'No Records',
  //   sourceName: 'No records',
  //   sourceDescription: 'No records',
  //   sourceId: 'No records',
  // };

// 1= Load Primary Data, 2=Load Secondary Data, 3= Rekon Matching


  usrheader: any = [
    { label: 'Id', sort: 'sourceId' },
    { label: 'Rekon Name', sort: 'sourceName' },
    { label: 'Type', sort: 'schedullerJobType' },
    { label: 'Last Running', sort: 'lastRunning' },
  ];
  usrcolname: any = [
    'sourceId',
    'sourceName',
    'schedullerJobType',
    'lastRunning',
  ];
  usrcolhalign: any = [
    '',
    '',
    ''
  ];

  usrcolwidth: any = [ { width: '100px' },
  { width: '330px' },
  { width: '310px' },{ width: '330px' },];

  usractionbtn: any = [0, 1, 0, 0, 1, 0];
  usraddbtn = { route: 'detail', label: 'Add Template' };
  constructor(public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService) { }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Scheduler Monitor' }];
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.rekonsiliasiService.getScheduleList().subscribe(async (orgall: any) => {
      let hasil = JSON.stringify(orgall.data);
      // let hasil = '[{"id":1,"Scheduler Job Name":"Populate Data from cbs", "reconDefinitionId":30,"Cron Expression":"0\/30 0\/1 * 1\/1 * ? *","Scheduler Creation Time":"2022-03-04 22:07:53.611","Scheduler Last Run":null},{"id":2,"Scheduler Job Name":"Populate Data from biFast", "reconDefinitionId":30,"Cron Expression":"0\/30 0\/1 * 1\/1 * ? *","Scheduler Creation Time":"2022-03-04 22:07:53.617","Scheduler Last Run":null},{"id":3,"Scheduler Job Name":"CBS vs BiFast Test 4Maret" , "reconDefinitionId":30,"Cron Expression":"","Scheduler Creation Time":"2022-03-04 22:07:53.621","Scheduler Last Run":null},{"id":4,"Scheduler Job Name":"Populate Data from cbs", "reconDefinitionId":248,"Cron Expression":"0\/30 0\/1 * 1\/1 * ? *","Scheduler Creation Time":"2022-03-04 22:46:03.142","Scheduler Last Run":null},{"id":5,"Scheduler Job Name":"Populate Data from biFast", "reconDefinitionId":248,"Cron Expression":"0\/30 0\/1 * 1\/1 * ? *","Scheduler Creation Time":"2022-03-04 22:46:03.148","Scheduler Last Run":null},{"id":6,"Scheduler Job Name":"CBS vs BiFast Test 4Maret v2", "reconDefinitionId":248,"Cron Expression":"","Scheduler Creation Time":"2022-03-04 22:46:03.153","Scheduler Last Run":null}]'
      // // console.log('>>>>>>> HASIL MONITOR ', hasil);
      let dataMonitor = JSON.parse(hasil);
      console.log('>>>>>>> HASIL MONITOR ', JSON.stringify(dataMonitor));
    //   'sourceId',
    // 'sourceName',
    // 'entityName',
    // 'lastRunning',
      this.userlist = dataMonitor;
      if (this.userlist.length < 1) {
        let objtmp = {
          sourceId:'No Records',
          sourceName: 'No records',
          schedullerJobType : 'No Records',
          lastRunning: 'No records',
        };
        this.userlist = [];
        this.userlist.push(objtmp);
      } else {
        this.userlist = [];
        await dataMonitor.map((element)=>{
          // console.log(">>>>>>>>> Map ", element);
          let objtmp = {
            sourceId:element.id,
            sourceName: element['jobName'],
            schedullerJobType: element['schedullerJobType'],
            lastRunning: element['lastRunning'],
          };
          this.userlist.push(objtmp);
          
        });
        // console.log("BARU MASUKIN");
      }
      this.isFetching = false;
    });
  }
  viewData(data: any){
    console.log("DATA TO VIEW ",data);
  }
}
