import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconScheduledetailComponent } from './recon-scheduledetail.component';

describe('ReconScheduledetailComponent', () => {
  let component: ReconScheduledetailComponent;
  let fixture: ComponentFixture<ReconScheduledetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconScheduledetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconScheduledetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
