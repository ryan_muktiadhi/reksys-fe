import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconformatregComponent } from './reconformatreg.component';

describe('ReconformatregComponent', () => {
  let component: ReconformatregComponent;
  let fixture: ComponentFixture<ReconformatregComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconformatregComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconformatregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
