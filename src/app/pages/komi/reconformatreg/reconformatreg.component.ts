import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';

@Component({
  selector: 'app-reconformatreg',
  templateUrl: './reconformatreg.component.html',
  styleUrls: ['./reconformatreg.component.scss']
})
export class ReconformatregComponent implements OnInit {
  display = false;
  scrollheight: any = '400px';
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;

  userInfo: any = {};
  tokenID: string = '';
  newData: any;
  oldData: any;

  // let objtmp = {
    // sourceId:'No Records',
  //   sourceName: 'No records',
  //   sourceDescription: 'No records',
  //   sourceId: 'No records',
  // };
  usrheader: any = [
    { label: 'Id', sort: 'sourceId' },
    { label: 'Name', sort: 'sourceName' },
    { label: 'Description', sort: 'sourceDescription' },
  ];
  usrcolname: any = [
    'sourceId',
    'sourceName',
    'sourceDescription',
  ];
  usrcolhalign: any = [
    '',
    '',
    ''
  ];

  usrcolwidth: any = [{ width: '130px' },
  { width: '360px' },
  { width: '440px' }];

  usractionbtn: any = [1, 1, 1, 1, 0, 0];
  usraddbtn = { route: 'detail', label: 'Add Template' };
  constructor(public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService) { }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Format Registration' }];
    this.refreshingUser();
  }

  refreshingUser() {
    this.isFetching = true;
    this.rekonsiliasiService.getEntityFormat().subscribe((orgall: any) => {
      console.log('>>>>>>> ' + JSON.stringify(orgall));
      this.userlist = orgall.data;
      if (this.userlist.length < 1) {
        let objtmp = {
          sourceId:'No Records',
          sourceName: 'No records',
          sourceDescription: 'No records',
        };
        this.userlist = [];
        this.userlist.push(objtmp);
      }
      this.isFetching = false;
    });
  }
  deleteConfirmation(data: any) {
    console.log(
      "Yang di delete ",data
    );
    this.display = true;
    this.selectedUser = data;
  }

  deleteRecon() {
    console.log(this.selectedUser);
    let reconId = this.selectedUser.sourceId;
    this.rekonsiliasiService.deleteFormat(reconId).subscribe((resp: any) => {
      if (resp.status === 200) {
        this.showTopSuccess(resp.data);
      }
      this.display = false;
      this.refreshingUser();
    });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }

}
