import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReconformatregdetailComponent } from './reconformatregdetail.component';

describe('ReconformatregdetailComponent', () => {
  let component: ReconformatregdetailComponent;
  let fixture: ComponentFixture<ReconformatregdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReconformatregdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReconformatregdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
