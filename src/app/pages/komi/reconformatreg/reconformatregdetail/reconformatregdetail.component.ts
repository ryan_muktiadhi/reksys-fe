import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';
import { Location } from '@angular/common';
import { BackendResponse } from 'src/app/interfaces/backend-response';
@Component({
  selector: 'app-reconformatregdetail',
  templateUrl: './reconformatregdetail.component.html',
  styleUrls: ['./reconformatregdetail.component.scss']
})
export class ReconformatregdetailComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  submit: boolean = false;
  sucessDialog: any = false;
  dialogMessage: string;
  isEdit: boolean;
  formatId: any;
  formatName: any;
  formatDesc: any;
  fieldSpt:any;
  fieldSpts =[{label:","},{label:"|"},{label:";"},{label:"#"}];
  headRow:any;
  startRow:any;
  templateFormat:any=1;
  listFields:any[]=[];
  availableMatchingKey:any;
  availableMatchingTemp:any[] = [];
  fieldName:any;
  autoGenerateReport: any = false;
  autoGenerateReportOptions: [
    { label: 'Off'; value: 'false' },
    { label: 'On'; value: 'true' }
  ];

  scrollheight: any = '200px';

  usrheader: any = [
    { label: 'Field Pos.', sort: 'fieldPosition' },
    { label: 'Field Name', sort: 'fieldDisplayName' },
    { label: 'Database Field', sort: 'fieldDbName' }
  ];
  usrcolname: any = [
    'fieldPosition',
    'fieldDbName',
    'fieldDisplayName',
  ];
  usrcolhalign: any = [
    '',
    '',
    'p-text-center'
  ];

  usrcolwidth: any = ['', '', '', '', '', ''];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [0, 1, 0, 1, 0, 0, 0, 0, 0, 0];
  usraddbtn = { route: 'detail', label: 'Add Entity' };
  inLoading: boolean = false;
  isFetching: boolean = false;

  constructor(public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,) { }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' },{ label: 'Format Registration', command: (event) => {
      this.location.back();
    } }, { label: 'Detail' }];
    this.refreshFields();
    try {
      this.formatId = this.activatedRoute.snapshot.paramMap.get('id');
      console.log(this.formatId);
      this.isEdit = this.formatId ? true : false;
      if (this.isEdit) {
        // this.initDetailData();
      }
    } catch (err) {
      console.log(err);
    }
  }

  refreshFields(){
    let objtmp = {
      fieldPosition: 'No records',
      fieldDbName: 'No records',
      fieldDisplayName: 'No records',
    };
    this.listFields = [];
    this.listFields.push(objtmp);
  }

  addListField(){
    if(this.listFields.length > 0) {
      let checkResul = this.listFields[0].fieldPosition;
      if(checkResul === "No records") this.listFields=[];
      if(this.listFields.length > 0) {
        let objtmp = {
          fieldPosition: this.listFields.length+1,
          fieldDbName: 'F'+""+(this.listFields.length+1)+"",
          fieldDisplayName: this.fieldName,
        };
        this.listFields.push(objtmp);
        this.availableMatchingTemp.push(this.fieldName);
      } else {
        let objtmp = {
          fieldPosition: 1,
          fieldDbName: 'F'+1+"",
          fieldDisplayName: this.fieldName,
        };
        this.listFields.push(objtmp);
        this.availableMatchingTemp.push(this.fieldName);
      } 
    }
  }

  deleteConfirmation(data: any) {
    console.log("Data di delete ", data);
    // this.display = true;
    // this.selectedUser = data;
    // this.listFields.forEach((elementdst, idx) => {
      var ItemIndex = this.listFields.findIndex(
        (b) => b.fieldPosition === data.fieldPosition
      );
      console.log('>>>> ' + ItemIndex);
      this.listFields.splice(ItemIndex, 1);
      this.availableMatchingTemp.splice(ItemIndex);



    //   if (ItemIndex > -1) {
    //     this.listmodulesdest.push(
    //       this.listmodulessource[ItemIndex]
    //     );
    //     this.listmodulessource.splice(ItemIndex, 1);
    //   }
    // });
        if(this.listFields.length < 1){
          let objtmp = {
            fieldPosition: 'No records',
            fieldDbName: 'No records',
            fieldDisplayName: 'No records',
          };
          this.listFields = [];
          this.listFields.push(objtmp);
        }


  }


  async billedUploader(event, fileupload: any) {
    this.inLoading = true;
    this.isFetching = true;
    // let billed = this.job.billedEntity;
    // let ref = this.job.referenceEntity;
    // if (this.selectedEntity == billed) {
    //   console.log('file1');
    //   this.file1Status = 'File Not Uploaded';
    //   this.file1Uploaded = false;
    // } else if (this.selectedEntity == ref) {
    //   console.log('file2');
    //   this.file2Status = 'File Not Uploaded';
    //   this.file2Uploaded = false;
    // }
    let fileList: FileList | null = event.files;
    let fileObject = fileList[0];
    let payload = { file: fileObject };
    let params = {
      headerRowNumber: this.headRow,
    };
    // this.uploadSource1 = true;
    this.rekonsiliasiService.uploadFileTemplate(payload, params).subscribe(
      (value: BackendResponse) => {
        if (value.status == 200) {
          console.log("RESPONSE UPLOAD ", value.data);
          this.messageService.add({
            severity: 'info',
            summary: 'File Uploaded',
            detail: '',
          });
          fileupload.clear();
          this.listFields = [];
          this.fieldSpt = value.data.separator
          this.listFields =value.data.fields;
          this.availableMatchingKey= value.data.availableMatchingKey
          console.log("Match ", value.data);
          // this.uploadedSource1 = true;
          // this.fileIdList[this.selectedEntity] = value.data.id;
          // // this.file1Id = value.data.id;

          // let billed = this.job.billedEntity;
          // let ref = this.job.referenceEntity;
          // console.log(this.selectedEntity);
          // if (this.selectedEntity == billed) {
          //   console.log('file1');
          //   this.file1Status = 'File Uploaded';
          //   this.file1Uploaded = true;
          // } else if (this.selectedEntity == ref) {
          //   console.log('file2');
          //   this.file2Status = 'File Uploaded';
          //   this.file2Uploaded = true;
          // }

          // this.uploadDialog = false;
          // this.isReconAvail();
        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'File Upload Failed',
            detail: '',
          });
        }
        // this.uploadSource1 = false;
        // this.getUploadHistory();
      },
      (err) => {
        // this.uploadSource1 = false;
        this.messageService.add({
          severity: 'error',
          summary: 'File Upload Failed',
          detail: '',
        });
        // this.getUploadHistory();
      }
    );
  }
  onSubmit() {
    let payload: any = {};
    payload.formatId = this.formatId || null;
    payload.formatName = this.formatName;
    payload.formatDesc = this.formatDesc;
    payload.isTemplate = this.templateFormat === 2?true:false;
    payload.startRow = this.startRow;
    payload.fieldSpt = this.fieldSpt;
    payload.templateData = this.listFields;
    payload.availableMatchingKey = this.templateFormat === 1?this.availableMatchingKey:this.availableMatchingTemp.toString();
    this.submit = true;
    if (!this.formatId) {

      console.log("Template format ", this.templateFormat);
      console.log("Masukin data format regist ", JSON.stringify(payload));



      this.rekonsiliasiService.postFormat(payload).subscribe(
        (resp: any) => {
          if ((resp.status = 200)) {
            this.location.back();  
          }
          this.submit = false;
          
        },
        (err: any) => {
          this.sucessDialog = true;
          this.dialogMessage = 'Internal Error';
        }
      );
    } else {

      console.log("Template format ", this.templateFormat);
      console.log("Masukin data format regist ", JSON.stringify(payload));



      // this.rekonsiliasiService.editJob(payload).subscribe(
      //   (resp: any) => {
      //     if ((resp.status = 200)) {
      //       this.sucessDialog = true;
      //       this.dialogMessage = 'Data Successfully Edited';
      //     }

      //     this.submit = false;
      //   },
      //   (err: any) => {
      //     this.sucessDialog = true;
      //     this.dialogMessage = 'Internal Error';
      //   }
      // );
    }
  }

  confirmSuccess() {
    this.sucessDialog = false;
    // this.router.navigate(['/mgm/rekonsiliasi/reconjob']);
  }
}
