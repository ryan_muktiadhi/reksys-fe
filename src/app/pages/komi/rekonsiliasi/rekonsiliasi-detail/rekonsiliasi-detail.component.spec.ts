import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RekonsiliasiDetailComponent } from './rekonsiliasi-detail.component';

describe('RekonsiliasiDetailComponent', () => {
  let component: RekonsiliasiDetailComponent;
  let fixture: ComponentFixture<RekonsiliasiDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RekonsiliasiDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RekonsiliasiDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
