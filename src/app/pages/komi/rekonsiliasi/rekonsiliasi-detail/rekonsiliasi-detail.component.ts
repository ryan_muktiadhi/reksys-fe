import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';
// import { Table } from 'primeng/table';
import { AuthService } from 'src/app/services/auth.service';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-rekonsiliasi-detail',
  templateUrl: './rekonsiliasi-detail.component.html',
  styleUrls: ['./rekonsiliasi-detail.component.scss'],
})
export class RekonsiliasiDetailComponent implements OnInit {
  fileName = '-';
  fileNameRekon = '-';
  fileNameCsv = '-';
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';
  logData: any[] = [];
  logDataTemp: any[] = [];
  div: string = '   ';
  uploadedFiles: any[] = [];
  myDate: Date;
  datenow: string;
  num: number = 1;
  numCsv: number = 1;
  inLoading: boolean = false;
  isdataexist: boolean = false;
  fetchHistory: boolean = false;

  usrheader: any = [
    { label: 'File Name', sort: 'fileName' },
    { label: 'Entity', sort: 'entity' },
    { label: 'Status', sort: 'status' },
    { label: 'Start Date', sort: 'datetimeStartProcess' },
    { label: 'End Date', sort: 'datetimeEndProcess' },
  ];
  usrcolname: any = [
    'fileName',
    'entityName',
    'status',
    'datetimeStartProcess',
    'datetimeEndProcess',
  ];
  usrcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];

  usrcolwidth: any = ['', '', '', '', '', ''];

  usractionbtn: any = [0, 1, 0, 0, 0, 0, 0, 0];
  userlist: any[] = [];

  // file upload 1
  public formData = new FormData();
  ReqJson: any = {};
  path: any = {};
  theFile: any = null;
  MAX_SIZE: number = 1048576;
  messages: string[] = [];

  // file upload 2
  display = false;
  msg: any = 'Idle';
  isFetching: boolean = false;

  job: any;
  jobOptions: any[];

  uploadSource1: boolean = false;
  uploadedSource1: boolean = false;
  uploadSource2: boolean = false;
  uploadedSource2: boolean = false;
  jobId: any;
  file2Id: any = null;
  file2Uploaded = false;

  file1Id: any = null;
  file1Uploaded = false;
  reconAvail: boolean = true;
  file1Status: any = 'File Not Uploaded';
  file2Status: any = 'File Not Uploaded';
  uploadDialog: boolean = false;
  selectedEntity: any;
  selectedEntityType: any;
  useLastUpload: boolean = false;
  uploadMethod: any;
  fileIdList: any = {};
  fileStatus: any = [];
  filteredData: any[];
  submit: boolean;
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,

    public rekonsiliasiService: RekonsiliasiService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Execute' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log('>>> User Info : ' + JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.myDate = new Date();

    this.jobId = this.activatedRoute.snapshot.paramMap.get('id');
    if (!this.jobId) {
      this.router.navigate([`mgm/rekonsiliasi/rekonsiliasi`]);
    }
    this.initialData();
    console.log(this.uploadedSource1);
    // this.getUploadHistory();
  }

  initialData() {
    this.rekonsiliasiService.getJobsById(this.jobId).subscribe((resp: any) => {
      console.log(resp);
      this.job = resp.data;
    });
    this.getUploadHistory();
    this.uploadMethod = this.useLastUpload
      ? 'Upload New File'
      : 'Use Last Uploaded File';
  }

  getUploadHistory() {
    this.fetchHistory = true;
    this.rekonsiliasiService
      .getUploadHistory(this.jobId)
      .subscribe((resp: any) => {
        this.userlist = resp.data;
        this.fetchHistory = false;
      });
  }

  onSubmit() {
    console.log(this.fileIdList);
    let billed = this.job.billedEntity;
    let rf = this.job.referenceEntity;
    let params = {
      jobId: this.job.id,
      file1Id: this.fileIdList[billed],
      file2Id: this.fileIdList[rf],
    };
    console.log(params);
    this.submit = true;
    this.rekonsiliasiService.triggerJob(params).subscribe(
      (resp: BackendResponse) => {
        console.log(resp);
        if (resp.status == 200) {
          this.showTopInfo('Reconsile Started');
          this.submit = false;
          this.file1Uploaded = false;
          this.file2Uploaded = false;
          this.selectedEntity = null;
        } else {
          this.showTopWarning('Starting Reconsile Failed');
          this.submit = false;
        }
      },
      (err) => {
        this.showTopWarning(err);
        this.submit = false;
      }
    );
  }

  onFileChange(event: any) {
    console.log(event);
    this.theFile = null;
    if (event.target.files && event.target.files.length > 0) {
      // Don't allow file sizes over 1MB
      if (event.target.files[0].size < this.MAX_SIZE) {
        // Set theFile property
        this.theFile = event.target.files[0];
        console.log('this.theFile', this.theFile.name);
      } else {
        // Display error message
        this.messages.push(
          'File: ' + event.target.files[0].name + ' is too large to upload.'
        );
      }
    }
  }

  async billedUploader(event, fileupload: any) {
    this.inLoading = true;
    this.isFetching = true;
    let billed = this.job.billedEntity;
    let ref = this.job.referenceEntity;

    if (this.selectedEntity == billed) {
      console.log('file1');
      this.file1Status = 'File Not Uploaded';
      this.file1Uploaded = false;
    } else if (this.selectedEntity == ref) {
      console.log('file2');
      this.file2Status = 'File Not Uploaded';
      this.file2Uploaded = false;
    }
    let fileList: FileList | null = event.files;
    let fileObject = fileList[0];
    let payload = { file: fileObject };

    let params = {
      entityName: this.selectedEntity,
      jobId: this.job.id,
    };

    this.uploadSource1 = true;

    this.rekonsiliasiService.uploadFile(payload, params).subscribe(
      (value: BackendResponse) => {
        if (value.status == 200) {
          this.messageService.add({
            severity: 'info',
            summary: 'File Uploaded',
            detail: '',
          });
          fileupload.clear();
          this.uploadedSource1 = true;
          this.fileIdList[this.selectedEntity] = value.data.id;
          // this.file1Id = value.data.id;

          let billed = this.job.billedEntity;
          let ref = this.job.referenceEntity;
          console.log(this.selectedEntity);
          if (this.selectedEntity == billed) {
            console.log('file1');
            this.file1Status = 'File Uploaded';
            this.file1Uploaded = true;
          } else if (this.selectedEntity == ref) {
            console.log('file2');
            this.file2Status = 'File Uploaded';
            this.file2Uploaded = true;
          }

          this.uploadDialog = false;
          this.isReconAvail();
        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'File Upload Failed',
            detail: '',
          });
        }
        this.uploadSource1 = false;
        // this.getUploadHistory();
      },
      (err) => {
        this.uploadSource1 = false;
        this.messageService.add({
          severity: 'error',
          summary: 'File Upload Failed',
          detail: '',
        });
        // this.getUploadHistory();
      }
    );
  }

  async refUploader(event, fileupload: any) {
    let datenow = this.datepipe.transform(this.myDate, 'yyyyMMdd');
    this.inLoading = true;
    this.isFetching = true;
    this.file2Uploaded = false;
    let fileList: FileList | null = event.files;
    let fileObject = fileList[0];
    let payload = { file: fileObject };

    let params = {
      entityName: this.job.referenceEntity,
      jobId: this.job.id,
    };

    this.uploadSource2 = true;

    this.rekonsiliasiService.uploadFile(payload, params).subscribe(
      (value: BackendResponse) => {
        if (value.status == 200) {
          this.messageService.add({
            severity: 'info',
            summary: 'File Uploaded',
            detail: '',
          });
          fileupload.clear();
          this.uploadedSource2 = true;
          this.file2Uploaded = true;
          this.file2Id = value.data.id;
          this.file2Status = 'File Uploaded';
          this.isReconAvail();
        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'File Upload Failed',
            detail: '',
          });
        }
        this.uploadSource2 = false;
        //this.getUploadHistory();
      },
      (err) => {
        this.uploadSource2 = false;
        this.messageService.add({
          severity: 'error',
          summary: 'File Upload Failed',
          detail: '',
        });
        // this.getUploadHistory();
      }
    );
  }

  isReconAvail() {
    if (this.fileIdList.length > 1) {
      this.reconAvail = false;
    }
    this.reconAvail = false;
    console.log(this.reconAvail);
  }

  openDialog(selectedEntity, type) {
    this.uploadDialog = true;
    this.selectedEntity = selectedEntity;
    this.selectedEntityType = type;
    let billed = this.job.billedEntity;
    let ref = this.job.referenceEntity;
  }

  changeUploadMethod() {
    this.filteredData = this.userlist;
    this.useLastUpload = !this.useLastUpload;

    this.uploadMethod = this.useLastUpload
      ? 'Upload New File'
      : 'Use Previous File';
    this.filteredData = this.filteredData.filter((dt) => {
      return dt.entity == this.selectedEntity && dt.reconJobId == this.jobId;
    });
    this.userlist.sort((a, b) => b.id - a.id);
  }

  dialogHide() {
    this.useLastUpload = false;
  }

  onSelectHistoryFile(data: any) {
    let billed = this.job.billedEntity;
    let ref = this.job.referenceEntity;
    this.fileIdList[this.selectedEntity] = data.id;
    if (this.selectedEntity == billed) {
      this.file1Status = 'File Uploaded';
      this.file1Uploaded = true;
    } else if (this.selectedEntity == ref) {
      this.file2Status = 'File Uploaded';
      this.file2Uploaded = true;
    }
    this.uploadDialog = false;
    this.isReconAvail();
  }

  showTopInfo(message: string) {
    this.messageService.add({
      severity: 'info',
      summary: 'Info',
      detail: message,
    });
  }
  showTopWarning(message: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Warning',
      detail: message,
    });
  }
}
