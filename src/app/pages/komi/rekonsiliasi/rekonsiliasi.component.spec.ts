import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RekonsiliasiComponent } from './rekonsiliasi.component';

describe('RekonsiliasiComponent', () => {
  let component: RekonsiliasiComponent;
  let fixture: ComponentFixture<RekonsiliasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RekonsiliasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RekonsiliasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
