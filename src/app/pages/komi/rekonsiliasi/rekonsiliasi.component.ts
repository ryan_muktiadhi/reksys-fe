import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { RekonsiliasiService } from 'src/app/services/komi/rekonsiliasi.service';
@Component({
  selector: 'app-rekonsiliasi',
  templateUrl: './rekonsiliasi.component.html',
  styleUrls: ['./rekonsiliasi.component.scss'],
})
export class RekonsiliasiComponent implements OnInit {
  display = false;
  scrollheight: any = '400px';
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;

  userInfo: any = {};
  tokenID: string = '';
  newData: any;
  oldData: any;
  usrheader: any = [
    { label: 'Id', sort: 'id' },
    { label: 'Recon Name', sort: 'jobName' },
    { label: 'Primary Data', sort: 'billedEntity' },
    { label: 'Secondary Data', sort: 'referenceEntity' },
    { label: 'Entity Description', sort: 'jobDesc' },
  ];
  usrcolname: any = [
    'sessionId',
    'jobName',
    'billedEntity',
    'referenceEntity',
    'jobDesc',
  ];
  usrcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  // usrcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  usrcolwidth: any = [{ width: '130px' },
  { width: '430px' },
  { width: '200px' }, { width: '200px' }, { width: '210px' }];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [0, 1, 0, 0, 0, 0, 0, 0, 1, 0];
  usraddbtn = { route: 'detail', label: 'Add Entity' };

  constructor(
    public messageService: MessageService,
    public rekonsiliasiService: RekonsiliasiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reconsiliation' }, { label: 'Execute' }];
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.rekonsiliasiService.getJobs().subscribe((orgall: any) => {
      this.userlist = orgall.data;
      if (this.userlist.length < 1) {
        let objtmp = {
          fullname: 'No records',
          userid: 'No records',
          active: 'No records',
          groupname: 'No records',
        };
        this.userlist = [];
        this.userlist.push(objtmp);
      }
      this.isFetching = false;
    });
  }

  uploadFile(data: any) {
    console.log(data);
    this.router.navigate([`mgm/rekonsiliasi/rekonsiliasi/upload#/${data.id}`]);
  }

  triggerJob(data: any) {
    console.log(data);
    this.rekonsiliasiService.triggerJob(data.id).subscribe(
      (resp: BackendResponse) => {
        console.log(resp);
        if (resp.status == 200) {
          this.showTopInfo('Reconsile Triggered');
        } else {
          this.showTopWarning('Reconsile Trigger Failed');
        }
      },
      (err) => {
        this.showTopWarning(err);
      }
    );
  }

  showTopInfo(message: string) {
    this.messageService.add({
      severity: 'info',
      summary: 'Info',
      detail: message,
    });
  }
  showTopWarning(message: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Warning',
      detail: message,
    });
  }
}
