import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmtpconfigComponent } from './smtpconfig.component';

describe('SmtpconfigComponent', () => {
  let component: SmtpconfigComponent;
  let fixture: ComponentFixture<SmtpconfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmtpconfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmtpconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
