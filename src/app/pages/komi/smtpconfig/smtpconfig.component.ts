import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ApprovalService } from 'src/app/services/approval.service';
import { AuthService } from 'src/app/services/auth.service';
import { SmtpconfigService } from 'src/app/services/komi/smtpconfig.service';

@Component({
  selector: 'app-smtpconfig',
  templateUrl: './smtpconfig.component.html',
  styleUrls: ['./smtpconfig.component.scss'],
})
export class SmtpconfigComponent implements OnInit {
  display = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  smtplist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  secureOption: any[] = [];
  //################################
  sendername: any = {};
  username: any = {};
  pwdemail: any = {};
  smtphost: any = {};
  port: any = {};
  secure: any = {};
  selectedData: any = {};
  viewApprove: boolean = false;
  newData: any;
  oldData: any;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    //private userService: UsermanagerService
    public smtpconfigService: SmtpconfigService,
    public approvalService: ApprovalService
  ) {}

  ngOnInit(): void {
    this.secureOption = [
      { label: 'SSL (Secure)', value: true },
      { label: 'TLS (Use for GMail)', value: false },
    ];
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Smtp Config' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingConfig();
  }
  refreshingConfig() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      if ((data.status = 200)) {
        this.smtpconfigService
          .retriveSmtpConfig()
          .subscribe((smtpll: BackendResponse) => {
            console.log('>>>>>>> ' + JSON.stringify(smtpll));
            this.smtplist = smtpll.data;
            // if (this.userlist.length < 1) {
            //   let objtmp = {
            //     fullname: 'No records',
            //     userid: 'No records',
            //     active: 'No records',
            //     groupname: 'No records',
            //   };
            //   this.userlist = [];
            //   this.userlist.push(objtmp);
            // }
            console.log(
              '>>>>>>> Retrive Config ' + JSON.stringify(this.smtplist)
            );

            this.smtplist.map((jsobj) => {
              // menusTmp.push(jsobj);
              if (jsobj.param_name === 'EMAIL_SENDERNAME') {
                this.sendername.param_name = jsobj.param_name;
                this.sendername.id = jsobj.id;
                this.sendername.value = jsobj.param_value;
                this.sendername.status = jsobj.active;
                this.sendername.approvalid = jsobj.idapproval;
              }
              if (jsobj.param_name === 'EMAIL_USER') {
                this.username.param_name = jsobj.param_name;
                this.username.id = jsobj.id;
                this.username.value = jsobj.param_value;
                this.username.status = jsobj.active;
                this.username.approvalid = jsobj.idapproval;
              }
              if (jsobj.param_name === 'EMAIL_PASS') {
                this.pwdemail.param_name = jsobj.param_name;
                this.pwdemail.id = jsobj.id;
                this.pwdemail.value = jsobj.param_value;
                this.pwdemail.status = jsobj.active;
                this.pwdemail.approvalid = jsobj.idapproval;
              }
              if (jsobj.param_name === 'EMAIL_HOST') {
                this.smtphost.param_name = jsobj.param_name;
                this.smtphost.id = jsobj.id;
                this.smtphost.value = jsobj.param_value;
                this.smtphost.status = jsobj.active;
                this.smtphost.approvalid = jsobj.idapproval;
              }
              if (jsobj.param_name === 'EMAIL_PORT') {
                this.port.id = jsobj.id;
                this.port.param_name = jsobj.param_name;
                this.port.value = jsobj.param_value;
                this.port.status = jsobj.active;
                this.port.approvalid = jsobj.idapproval;
              }
              if (jsobj.param_name === 'EMAIL_SECURE') {
                this.secure.param_name = jsobj.param_name;
                this.secure.id = jsobj.id;
                this.secure.value = jsobj.param_value === 'true' ? true : false;
                this.secure.status = jsobj.active;
                this.secure.approvalid = jsobj.idapproval;
                // this.secureOption = [
                //   { label: 'SSL (Secure)', value: true },
                //   { label: 'TLS (Use for GMail)', value: false }
                // ];
              }
            });
            // idsecure:any=0;
            // secure:any=false;

            this.isFetching = false;
          });
      }
    });
  }
  getApprovalData(selectedData) {
    console.log(selectedData);
    this.approvalService
      .getByid(selectedData.approvalid)
      .subscribe(async (resp: BackendResponse) => {
        let oldFormData = await this.setApprovalForm(selectedData);
        let param = selectedData.param_name;
        let newData = JSON.parse(resp.data[0].jsondata);
        newData.key = param;
        newData = await this.setApprovalForm(newData);
        this.oldData = oldFormData;
        this.newData = newData;
      });
  }

  async setApprovalForm(data) {
    const keys = Object.keys(data);
    const input = data;
    let arrData = [];

    if (keys.length > 0) {
      await keys.map(async (key: any) => {
        let data: any = {};
        key === 'key' || key === 'param_name'
          ? (data.label = 'Parameter')
          : (data.label = key);
        if (data.label === 'status') {
          data.value =
            input[key] == 0
              ? 'Not Active'
              : input[key] == 1
              ? 'Active'
              : 'Waiting for Approval';
          data.statusCode = input[key];
        } else if (data.label === 'value') {
          data.value = input[key].value || input[key];
        } else {
          data.value = input[key];
        }
        if (data.label != 'id' && data.label != 'userid') {
          arrData.push(data);
        }
      });
      return arrData;
    }
  }

  reject(status) {
    console.log(this.selectedData);
    let data = this.selectedData;
    let payload = {
      id: data.id,
      oldactive: data.status,
      isactive: status,
      idapproval: data.approvalid,
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.smtpconfigService
      .reject(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingConfig();
          this.viewApprove = false;
        }
      });
  }

  async filterForm(formData) {
    let filter = ['fullname', 'notif', 'email'];
    let filterFormData = formData.filter((formItem) =>
      filter.includes(formItem.label)
    );

    return filterFormData;
  }

  approvalSubmit(status) {
    console.log(this.selectedData);
    let data = this.selectedData;
    let payload = {
      id: data.id,
      oldactive: data.status,
      isactive: status,
      idapproval: data.approvalid,
    };
    console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.smtpconfigService
      .approveSmtpConfig(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingConfig();
          this.viewApprove = false;
        }
      });
  }

  updateSender() {
    if (this.sendername.status > 1) {
      this.selectedData = this.sendername;
      this.viewApprove = true;
      this.getApprovalData(this.sendername);
    } else {
      let payload = {
        id: this.sendername.id,
        value: this.sendername,
      };
      // console.log(JSON.stringify(payload));
      this.smtpconfigService
        .updateSmtpConfig(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.messageService.add({
              key: 'bc',
              severity: 'success',
              summary: 'Success',
              detail: 'Sender Name requested to change!',
            });
          } else {
            this.messageService.add({
              key: 'bc',
              severity: 'error',
              summary: 'Error',
              detail: 'Error requested to change Sender Name!',
            });
          }
        });
      this.refreshingConfig();
    }
  }
  updateUsername() {
    if (this.username.status > 1) {
      this.selectedData = this.username;
      this.viewApprove = true;
      this.getApprovalData(this.username);
    } else {
      let payload = { id: this.username.id, value: this.username };
      // console.log(JSON.stringify(payload));
      this.smtpconfigService
        .updateSmtpConfig(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.messageService.add({
              key: 'bc',
              severity: 'success',
              summary: 'Success',
              detail: 'User Name requested to change!',
            });
          } else {
            this.messageService.add({
              key: 'bc',
              severity: 'error',
              summary: 'Error',
              detail: 'Error requested to change User Name!',
            });
          }
        });
      this.refreshingConfig();
    }
  }

  updatePassword() {
    if (this.pwdemail.status > 1) {
      this.selectedData = this.pwdemail;
      this.viewApprove = true;
      this.getApprovalData(this.pwdemail);
    } else {
      let payload = { id: this.pwdemail.id, value: this.pwdemail };
      // console.log(JSON.stringify(payload));
      this.smtpconfigService
        .updateSmtpConfig(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.messageService.add({
              key: 'bc',
              severity: 'success',
              summary: 'Success',
              detail: 'Password requested to change!',
            });
          } else {
            this.messageService.add({
              key: 'bc',
              severity: 'error',
              summary: 'Error',
              detail: 'Error requested to change Password!',
            });
          }
        });
      this.refreshingConfig();
    }
  }

  updateSmtp() {
    if (this.smtphost.status > 1) {
      this.selectedData = this.smtphost;
      this.viewApprove = true;
      this.getApprovalData(this.smtphost);
    } else {
      let payload = { id: this.smtphost.id, value: this.smtphost };
      // console.log(JSON.stringify(payload));
      this.smtpconfigService
        .updateSmtpConfig(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.messageService.add({
              key: 'bc',
              severity: 'success',
              summary: 'Success',
              detail: 'SMTP Server requested to change!',
            });
          } else {
            this.messageService.add({
              key: 'bc',
              severity: 'error',
              summary: 'Error',
              detail: 'Error requested to change SMTP Server!',
            });
          }
        });
      this.refreshingConfig();
    }
  }
  updatePort() {
    if (this.port.status > 1) {
      this.selectedData = this.port;
      this.viewApprove = true;
      this.getApprovalData(this.port);
    } else {
      let payload = { id: this.port.id, value: this.port };
      // console.log(JSON.stringify(payload));
      this.smtpconfigService
        .updateSmtpConfig(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status === 200) {
            this.messageService.add({
              key: 'bc',
              severity: 'success',
              summary: 'Success',
              detail: 'Port Server requested to change!',
            });
          } else {
            this.messageService.add({
              key: 'bc',
              severity: 'error',
              summary: 'Error',
              detail: 'Error requested to change Port Server!',
            });
          }
        });
      this.refreshingConfig();
    }
  }
  onChange(event) {
    // console.log('event :' + event);
    // console.log(event.value);
    let payload = { id: this.secure.idsecure, value: event.value + '' };
    this.smtpconfigService
      .updateSmtpConfig(payload)
      .subscribe((result: BackendResponse) => {
        if (result.status === 200) {
          this.messageService.add({
            key: 'bc',
            severity: 'success',
            summary: 'Success',
            detail: 'Secure server requested to change!',
          });
        } else {
          this.messageService.add({
            key: 'bc',
            severity: 'error',
            summary: 'Error',
            detail: 'Error requested to change Secure Server!',
          });
        }
      });
  }

  showBottomCenter() {
    this.messageService.add({
      key: 'bc',
      severity: 'success',
      summary: 'Success',
      detail: 'Message Content',
    });
  }
}
