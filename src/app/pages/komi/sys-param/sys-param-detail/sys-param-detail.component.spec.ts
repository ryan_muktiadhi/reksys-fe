import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SysParamDetailComponent } from './sys-param-detail.component';

describe('SysParamDetailComponent', () => {
  let component: SysParamDetailComponent;
  let fixture: ComponentFixture<SysParamDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SysParamDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SysParamDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
