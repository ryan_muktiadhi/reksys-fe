import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { Location } from '@angular/common';
import { OrganizationService } from 'src/app/services/root/organization.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { FilterService } from 'primeng/api';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { SysParamService } from 'src/app/services/sys-param.service';

@Component({
  selector: 'app-sys-param-detail',
  templateUrl: './sys-param-detail.component.html',
  styleUrls: ['./sys-param-detail.component.scss'],
})
export class SysParamDetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private systemService: SysParamService,
    private authservice: AuthService,
    private groupService: GroupServiceService,
    private filterService: FilterService,
    private location: Location
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'System Param',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.stateOptions = [
      { label: 'Direct activated', value: 1 },
      { label: 'Activision link', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        parameter: ['', Validators.required],
        value: ['', Validators.required],
        description: [[], Validators.required],
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.systemService
            .getSysParamById(this.userId)

            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit user ' + JSON.stringify(result.data));

              this.user.parameter = result.data.param_name;
              this.user.value = result.data.param_value;
              this.user.description = result.data.description;

              this.groupForm.patchValue({
                parameter: this.user.parameter,
                value: this.user.value,
                description: this.user.description,
              });
              console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  formatOrgData(data: any) {
    data.map((dt: any) => {
      let formated: any = {};
      formated.name = dt.groupname;
      formated.id = dt.id;
      this.formatedOrg.push(formated);
    });
  }

  filterOrg(event: any) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.formatedOrg.length; i++) {
      let country = this.formatedOrg[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }

    this.orgSuggest = filtered;
  }

  onSubmit() {
    this.submitted = true;
    console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};

      payload = {
        id: this.userId,
        value: this.groupForm.get('value')?.value,
        description: this.groupForm.get('description')?.value,
      };
      console.log('>>>>>>>> payload ' + JSON.stringify(payload));
      this.systemService
        .updateSysParam(payload)
        .subscribe((result: BackendResponse) => {
          // console.log(">>>>>>>> return "+JSON.stringify(result));
          if (result.status === 200) {
            this.location.back();
          }
        });
    }

    console.log(this.groupForm.valid);
  }
}
