import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ApprovalService } from 'src/app/services/approval.service';
import { AuthService } from 'src/app/services/auth.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { SysParamService } from 'src/app/services/sys-param.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-sys-param',
  templateUrl: './sys-param.component.html',
  styleUrls: ['./sys-param.component.scss'],
})
export class SysParamComponent implements OnInit {
  display = false;
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  newData: any;
  oldData: any;
  usrheader: any = [
    { label: 'No', sort: 'id' },
    { label: 'Parameter', sort: 'param_name' },
    { label: 'Value', sort: 'param_value' },
    { label: 'Category', sort: 'category' },
    { label: 'Status', sort: 'active' },
    { label: 'Description', sort: 'description' },
  ];
  usrcolname: any = [
    'id',
    'param_name',
    'param_value',
    'category',
    'active',
    'description',
  ];
  usrcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  usrcolwidth: any = ['', '', '', '', '', ''];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [0, 1, 1, 0, 1, 1];
  usraddbtn = { route: 'detail', label: 'Add Data' };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    //private userService: UsermanagerService
    public sysService: SysParamService,
    private aclMenuService: AclmenucheckerService,
    private router: Router,
    private approvalService: ApprovalService
  ) {}

  viewData(data: any) {
    // console.log(data);
    this.viewDisplay = true;
    this.selectedUser = data;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Security parameters' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
        console.log('MENU ALL ACL Set');
        this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
          if (JSON.stringify(dataacl.acl) === '{}') {
            console.log('No ACL Founded');
          } else {
            console.log('ACL Founded');
            console.log(dataacl.acl);
            this.usractionbtn[0] = 0;
            this.usractionbtn[1] = dataacl.acl.read;
            this.usractionbtn[2] = dataacl.acl.update;
            // this.usractionbtn[3] = dataacl.acl.delete;
            this.usractionbtn[4] = dataacl.acl.view;
            this.usractionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      if ((data.status = 200)) {
        this.sysService
          .getAllSysParam()
          .subscribe((orgall: BackendResponse) => {
            // console.log('>>>>>>> ' + JSON.stringify(orgall));
            this.userlist = orgall.data;
            if (this.userlist.length < 1) {
              let objtmp = {
                id: 'No records',
                param_name: 'No records',
                param_value: 'No records',
                category: 'No records',
                description: 'No records',
              };
              this.userlist = [];
              this.userlist.push(objtmp);
            }
            this.isFetching = false;
          });
      }
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedUser = data;
  }
  reject() {
    let payload = {
      id: this.selectedUser.id,
      oldactive: this.selectedUser.active,
      idapproval: this.selectedUser.idapproval,
    };
    console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.sysService.reject(payload).subscribe((result: BackendResponse) => {
      // console.log(">>>>>>>> return "+JSON.stringify(result));
      if (result.status === 200) {
        this.refreshingUser();
        this.viewApprove = false;
      }
    });
  }
  async approvalData(data: any) {
    console.log(data);
    this.viewApprove = true;
    this.selectedUser = data;
    if (data.idapproval && data.active != 9) {
      console.log('new data');
      this.getApprovalData(data.idapproval);
    } else {
      let oldData = await this.setApprovalForm(this.selectedUser);
      // oldData = await this.filterForm(oldData);
      this.oldData = oldData;
      this.newData = null;
    }
  }

  async setApprovalForm(data) {
    const keys = Object.keys(data);
    const input = data;
    let arrData = [];

    if (keys.length > 0) {
      await keys.map(async (key: any) => {
        let data: any = {};
        key === 'active' || key === 'isactive'
          ? (data.label = 'status')
          : key === 'param_value'
          ? (data.label = 'value')
          : (data.label = key);

        if (Array.isArray(input[key])) {
          let value = [];
          await input[key].map((dt) => {
            value.push(dt.name);
          });
          console.log(value);
          data.value = value;
        }
        // else if (data.label === 'status') {
        //   data.value =
        //     input[key] == 0
        //       ? 'Not Active'
        //       : input[key] == 1
        //       ? 'Active'
        //       : 'Waiting for Approval';
        // }
        else {
          data.value = input[key];
        }

        if (data.label != 'id' && data.label != 'userid') {
          arrData.push(data);
        }
      });
      return arrData;
    }
  }
  getApprovalData(id) {
    this.approvalService
      .getByid(id)
      .subscribe(async (resp: BackendResponse) => {
        let oldData: any = [];
        oldData.push(this.selectedUser);
        let oldFormData = await this.setApprovalForm(this.selectedUser);
        let newData = await this.setApprovalForm(
          JSON.parse(resp.data[0].jsondata)
        );
        this.oldData = oldFormData;
        console.log(this.oldData);
        this.newData = newData;
        console.log(this.newData);
      });
  }

  approvalSubmit(status) {
    console.log(this.selectedUser);
    console.log(status);
    let payload = {
      id: this.selectedUser.id,
      oldactive: this.selectedUser.active,
      isactive: status,
      idapproval: this.selectedUser.idapproval,
    };
    console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.sysService
      .updateSysParamActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingUser();
          this.viewApprove = false;
        }
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
