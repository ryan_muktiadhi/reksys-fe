import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemLogOutboundComponent } from './system-log-outbound.component';

describe('SystemLogOutboundComponent', () => {
  let component: SystemLogOutboundComponent;
  let fixture: ComponentFixture<SystemLogOutboundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemLogOutboundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemLogOutboundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
