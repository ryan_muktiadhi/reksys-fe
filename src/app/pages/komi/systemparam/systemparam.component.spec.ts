import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemparamComponent } from './systemparam.component';

describe('SystemparamComponent', () => {
  let component: SystemparamComponent;
  let fixture: ComponentFixture<SystemparamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemparamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemparamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
