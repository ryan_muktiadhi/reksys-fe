import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { TrxcostService } from 'src/app/services/komi/trxcost.service';

@Component({
  selector: 'app-transactioncost',
  templateUrl: './transactioncost.component.html',
  styleUrls: ['./transactioncost.component.scss'],
})
export class TransactioncostComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedtrx: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  trxData: any[] = [];
  // bank_code, bic_code, bank_name, last_update_date, created_date, change_who, idtenant
  trxheader: any = [
    { label: 'Fee', sort: 'fee' },
    { label: 'Values', sort: 'values' },
    { label: 'Active', sort: 'status' },
  ];
  trxcolname: any = ['fee', 'values', 'status'];
  trxcolhalign: any = ['p-text-center', 'p-text-center', 'p-text-center'];
  trxcolwidth: any = [
    { width: '170px' },
    { width: '170px' },
    { width: '170px' },
  ];
  trxcollinghref: any = { url: '#', label: 'Application' };
  trxactionbtn: any = [1, 1, 1, 1, 0];
  trxaddbtn = { route: 'detail', label: 'Add Data' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private trxcostService: TrxcostService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Transaction Cost' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingTrx();
  }
  refreshingTrx() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Transaction Cost ');
    this.trxcostService
      .getAllTrxByTenant()
      .subscribe((result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.trxData = [];
          let objtmp = {
            fee: 'No records',
            values: 'No records',
            status: 'No records',
          };
          this.trxData.push(objtmp);
        } else {
          this.trxData = result.data.trxcosts;
        }
        this.isFetching = false;
      });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedtrx = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedtrx = data;
  }

  deleteTrx() {
    console.log(this.selectedtrx);
    let trxcost = this.selectedtrx;
    const payload = { trxcost };
    console.log('>> Di delete ' + JSON.stringify(payload.trxcost));
    this.trxcostService
      .deleteTrxByTenant(payload.trxcost.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingTrx();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
