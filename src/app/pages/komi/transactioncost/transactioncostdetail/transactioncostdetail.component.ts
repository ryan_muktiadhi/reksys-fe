import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { TrxcostService } from 'src/app/services/komi/trxcost.service';

@Component({
  selector: 'app-transactioncostdetail',
  templateUrl: './transactioncostdetail.component.html',
  styleUrls: ['./transactioncostdetail.component.scss'],
})
export class TransactioncostdetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  trxid: any = null;
  stateOptions: any[] = [];
  feeOption: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  old_code: any = '';
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private trxcostService: TrxcostService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Transaction Cost',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.feeOption = [
      { label: 'DEBET', value: 'DEBT' },
      { label: 'CREDIT', value: 'CRED' },
      { label: 'SYARIAH', value: 'SHAR' },
    ];
    this.stateOptions = [
      { label: 'Active', value: 1 },
      { label: 'Deactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // syscolname:any = ["paramname", "paramvalua","status", "created_date"]
      this.groupForm = this.formBuilder.group({
        fee: ['DEBT', Validators.required],
        values: ['', Validators.required],
        status: [0],
      });
      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.trxid = this.activatedRoute.snapshot.paramMap.get('id');
          this.trxcostService
            .getTrxById(this.trxid)
            .subscribe(async (result: BackendResponse) => {
              // console.log("Data edit bic "+JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.groupForm.patchValue({
                fee: result.data.trxcosts.fee,
                values: result.data.trxcosts.values,
                status: result.data.trxcosts.status,
              });
              this.old_code = result.data.trxcosts.id;
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name
      if (!this.isEdit) {
        payload = {
          fee: this.groupForm.get('fee')?.value,
          values: this.groupForm.get('values')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.trxcostService
          .insertTrxByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            // console.log(">>>>>>>> payload "+JSON.stringify(result));
            this.location.back();
          });
      } else {
        payload = {
          fee: this.groupForm.get('fee')?.value,
          values: this.groupForm.get('values')?.value,
          status: this.groupForm.get('status')?.value,
          id: this.old_code,
        };
        console.log(payload);
        this.trxcostService
          .updateTrxByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            // console.log(">>>>>>>> payload "+JSON.stringify(result));
            this.location.back();
          });
      }
    }

    console.log(this.groupForm.valid);
  }
}
