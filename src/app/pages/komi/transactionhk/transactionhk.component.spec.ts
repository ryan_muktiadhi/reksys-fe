import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionhkComponent } from './transactionhk.component';

describe('TransactionhkComponent', () => {
  let component: TransactionhkComponent;
  let fixture: ComponentFixture<TransactionhkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionhkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionhkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
