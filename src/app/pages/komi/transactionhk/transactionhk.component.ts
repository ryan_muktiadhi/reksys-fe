import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { TrxmonitorService } from 'src/app/services/komi/trxmonitor.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as FileSaver from 'file-saver';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'app-transactionhk',
  templateUrl: './transactionhk.component.html',
  styleUrls: ['./transactionhk.component.scss'],
})
export class TransactionhkComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';

  rangeDates: Date[];
  transactionType: any = [];
  transactionTypeSelected: any = {};

  isFetching: boolean = false;
  logData: any[] = [];
  cols: any[];
  selectedColumns: any[];

  totalRowCredit: number = 0;
  totalAmountC: number = 0;
  totalRowDebit: number = 0;
  totalAmountD: number = 0;

  first: number = 0;
  last: number = 0;
  totalRecords: number;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private trxmonitorService: TrxmonitorService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      { label: 'House Keeping Data' },
      { label: 'Transaction' },
    ];

    this.transactionType = [
      { label: 'Incoming Transaction', value: 'I' },
      { label: 'Outgoing Transaction', value: 'O' },
    ];

    //NEW ADD : State Columns
    this.cols = [
      {
        field: 'bifastId',
        header: 'Transaction ID',
        isOrder: false,
        width: '300px',
        wscolW: 40,
      },
      {
        field: 'retrievalRefNumber',
        header: 'Retrieval Ref Number',
        isOrder: false,
        width: '250px',
        wscolW: 40,
      },
      {
        field: 'trxType',
        header: 'Transaction Type',
        isOrder: true,
        width: '200px',
        wscolW: 15,
      },
      /*{ field: '', header: 'Sender', hasSub: true, width: "600px", wscolW: 20, optdisable: true },*/
      {
        field: 'senderBank',
        header: 'Sender Bank Code',
        isOrder: true,
        isSub: true,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'senderAccountName',
        header: 'Sender Account Name',
        isOrder: true,
        isSub: true,
        width: '250px',
        wscolW: 20,
      },
      {
        field: 'senderAccountNo',
        header: 'Sender Account Number',
        isOrder: false,
        isSub: true,
        width: '250px',
        wscolW: 20,
      },
      /*{ field: '', header: 'Receiver', hasSub: true, width: "600px", wscolW: 20 },*/
      {
        field: 'recipientBank',
        header: 'Receiver Bank Code',
        isOrder: true,
        isSub: true,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'recipientAccountName',
        header: 'Receiver Account Name',
        isOrder: true,
        isSub: true,
        width: '300px',
        wscolW: 30,
      },
      {
        field: 'recipientAccountNo',
        header: 'Receiver Account Number',
        isOrder: false,
        isSub: true,
        width: '300px',
        wscolW: 20,
      },
      {
        field: 'trxAmount',
        header: 'Amount',
        width: '200px',
        wscolW: 20,
        isOrder: true,
        data: true,
      },
      {
        field: 'categoryPurpose',
        header: 'Purpose Of Transaction',
        isOrder: false,
        width: '250px',
        wscolW: 20,
      },
      {
        field: 'requestDate',
        header: 'Initiation Date / Time',
        isOrder: false,
        width: '250px',
        wscolW: 25,
      },
      {
        field: 'responseDate',
        header: 'Complete Date / Time',
        isOrder: false,
        width: '250px',
        wscolW: 25,
      },
      {
        field: 'paymentInformation',
        header: 'Description',
        isOrder: false,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'trxStatusCode',
        header: 'Status',
        isOrder: true,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'trxStatusMessage',
        header: 'Status Message',
        isOrder: true,
        width: '200px',
        wscolW: 40,
      },
    ];

    this.selectedColumns = this.cols;

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    this.logData = [];
    let objtmp = {
      bifastId: 'No record',
      retrievalRefNumber: 'No record',
      trxType: 'No record',
      senderBank: 'No record',
      senderAccountName: 'No record',
      senderAccountNo: 'No record',
      recipientBank: 'No record',
      recipientAccountName: 'No record',
      recipientAccountNo: 'No record',
      trxAmount: 0,
      categoryPurpose: 'No record',
      requestDate: 'No record',
      responseDate: 'No record',
      paymentInformation: 'No record',
      trxStatusCode: 'No record',
      trxStatusMessage: 'No record',
      TEST1: 'No record',
      TEST2: 'No record',
    };
    this.logData.push(objtmp);
  }

  refreshData(event: LazyLoadEvent) {
    let start_date = null;
    let end_date = null;
    let firstRow = event.first == null ? 1 : event.first + 1;
    let maxRow = event.rows == null ? 10 : event.rows + event.first;

    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    }

    let diffDay = this.calculateDiff(start_date, end_date);
    // console.log(diffDay);

    if (start_date != null && end_date != null) {
      if (diffDay >= 7) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warning',
          detail: 'Maximal Date Range only 7 Days',
        });
      } else {
        this.isFetching = true;

        this.first = firstRow;
        let filters: any = {};
        if (
          this.transactionTypeSelected == 'I' ||
          this.transactionTypeSelected == 'O'
        ) {
          filters.trxType = '%' + this.transactionTypeSelected + '%';
        } else {
          filters.trxType = null;
        }
        if (event.filters)
          filters.bifastId =
            event.filters.bifastId.value == null
              ? null
              : '%' + event.filters.bifastId.value + '%';
        if (event.filters)
          filters.retrievalRefNumber =
            event.filters.retrievalRefNumber.value == null
              ? null
              : '%' + event.filters.retrievalRefNumber.value + '%';
        if (event.filters)
          filters.senderBank =
            event.filters.senderBank.value == null
              ? null
              : '%' + event.filters.senderBank.value + '%';
        if (event.filters)
          filters.senderAccountName =
            event.filters.senderAccountName.value == null
              ? null
              : '%' + event.filters.senderAccountName.value + '%';
        if (event.filters)
          filters.senderAccountNo =
            event.filters.senderAccountNo.value == null
              ? null
              : '%' + event.filters.senderAccountNo.value + '%';
        if (event.filters)
          filters.recipientBank =
            event.filters.recipientBank.value == null
              ? null
              : '%' + event.filters.recipientBank.value + '%';
        if (event.filters)
          filters.recipientAccountName =
            event.filters.recipientAccountName.value == null
              ? null
              : '%' + event.filters.recipientAccountName.value + '%';
        if (event.filters)
          filters.recipientAccountNo =
            event.filters.recipientAccountNo.value == null
              ? null
              : '%' + event.filters.recipientAccountNo.value + '%';
        if (event.filters)
          filters.trxAmount =
            event.filters.trxAmount.value == null
              ? null
              : '%' + event.filters.trxAmount.value + '%';
        if (event.filters)
          filters.categoryPurpose =
            event.filters.categoryPurpose.value == null
              ? null
              : '%' + event.filters.categoryPurpose.value + '%';
        if (event.filters)
          filters.paymentInformation =
            event.filters.paymentInformation.value == null
              ? null
              : '%' + event.filters.paymentInformation.value + '%';
        if (event.filters)
          filters.trxStatusCode =
            event.filters.trxStatusCode.value == null
              ? null
              : '%' + event.filters.trxStatusCode.value + '%';

        let fieldOrders: any = {};
        if (event.sortField && event.sortOrder) {
          let formatField = null;
          if (event.sortField == 'trxType') {
            formatField = 'TRX_TYPE';
          } else if (event.sortField == 'senderBank') {
            formatField = 'SENDER_BANK';
          } else if (event.sortField == 'senderAccountName') {
            formatField = 'SENDER_ACCOUNT_NAME';
          } else if (event.sortField == 'recipientAccountName') {
            formatField = 'RECIPIENT_ACCOUNT_NAME';
          } else if (event.sortField == 'trxAmount') {
            formatField = 'TRX_AMOUNT';
          } else if (event.sortField == 'trxStatusCode') {
            formatField = 'TRX_STATUS_CODE';
          } else if (event.sortField == 'trxStatusMessage') {
            formatField = 'TRX_STATUS_MESSAGE';
          }

          fieldOrders = {
            fieldName: formatField,
            orders: event.sortOrder == 1 ? 'ASC' : 'DESC',
          };
        }

        let payload: any = {};
        payload = {
          start_date: start_date,
          end_date: end_date,
          firstRow: firstRow,
          maxRow: maxRow,
          filters: filters,
          fieldOrders: fieldOrders,
        };

        console.log(JSON.stringify(payload));

        this.trxmonitorService
          .getTransHk(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.logData = result.data.results;
              this.last =
                result.data.results.length == null
                  ? 0
                  : result.data.results.length;
              this.totalRecords = result.data.results.length + firstRow;

              this.totalRowDebit = parseInt(result.data.total.countRowDebit);
              this.totalAmountD = parseInt(
                result.data.total.amountDebit == null
                  ? 0
                  : result.data.total.amountDebit
              );
              this.totalRowCredit = parseInt(result.data.total.countRowCredit);
              this.totalAmountC = parseInt(
                result.data.total.amountCredit == null
                  ? 0
                  : result.data.total.amountCredit
              );
              // if (event.filters) {
              //   this.logData = this.logData.filter(row => this.filterField(row, event.filters));
              // }
            } else {
              if (result.data.status == 'F') {
                this.messageService.add({
                  severity: 'warn',
                  summary: 'Failed',
                  detail: 'Data not found',
                });
              } else if (result.data.status == 'E') {
                this.messageService.add({
                  severity: 'error',
                  summary: 'Error',
                  detail: result.data.statusMessage,
                });
              }
            }
            this.isFetching = false;
          });
      }
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warning',
        detail: 'Please choose Date Range of Transaction Data',
      });
      this.isFetching = false;
    }
  }

  calculateDiff(date1, date2) {
    date1 = new Date(date1);
    date2 = new Date(date2);

    return Math.floor(
      (Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate()) -
        Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate())) /
        (1000 * 60 * 60 * 24)
    );
  }

  exportExcel() {
    let start_date = null;
    let end_date = null;
    let wscols = [];

    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'd MMM y');
      end_date = this.datepipe.transform(this.rangeDates[1], 'd MMM y');
    }

    if (start_date != null && end_date != null) {
      let heading = [['INDIVIDUAL CREDIT TRANSFER TRANSACTION DETAIL REPORT']];
      let subHeading = [['PERIOD: ' + start_date + '  s/d  ' + end_date]];

      wscols = this.selectedColumns.map((col) => ({ width: col.wscolW }));
      console.log(wscols);

      let element = document.getElementById('tb-transhk');
      const worksheet = XLSX.utils.table_to_sheet(element, null); //{ origin: 'A3' });

      XLSX.utils.sheet_add_aoa(worksheet, heading);
      XLSX.utils.sheet_add_aoa(worksheet, subHeading, { origin: 'A2' });

      this.create_gap_rows(worksheet, 1);

      XLSX.utils.sheet_add_aoa(worksheet, subHeading, { origin: -1 });

      worksheet['!merges'] = [
        { s: { c: 0, r: 0 }, e: { c: wscols.length - 1, r: 0 } },
        { s: { c: 0, r: 1 }, e: { c: wscols.length - 1, r: 1 } },
      ];
      worksheet['!cols'] = wscols;

      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = XLSX.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, 'reporting');
      /* save to file */
      // XLSX.writeFile(excelBuffer, "products");
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warning',
        detail: 'Please choose Date Range of Transaction Data',
      });
      this.isFetching = false;
    }
  }

  create_gap_rows(ws, nrows) {
    var ref = XLSX.utils.decode_range(ws['!ref']); // get original range
    ref.e.r += nrows; // add to ending row
    ws['!ref'] = XLSX.utils.encode_range(ref); // reassign row
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
    );
  }
}
