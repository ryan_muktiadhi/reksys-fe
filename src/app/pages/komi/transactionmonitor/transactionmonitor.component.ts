import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { CurrencyPipe, DatePipe } from '@angular/common';
// import { Table } from 'primeng/table';
import { AuthService } from 'src/app/services/auth.service';
import { TrxmonitorService } from 'src/app/services/komi/trxmonitor.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { element } from 'protractor';
import { SysparamService } from 'src/app/services/komi/sysparam.service';
import { MainmenulayoutComponent } from '../../../layout/mainmenulayout/mainmenulayout.component';
import { ConnectionchannelService } from '../../../services/komi/connectionchannel.service';

@Component({
  selector: 'app-transactionmonitor',
  templateUrl: './transactionmonitor.component.html',
  styleUrls: ['./transactionmonitor.component.scss'],
})
export class TransactionmonitorComponent implements OnInit {
  // customers: Customer[];

  rangeDates: Date[];
  isdataexist = false;
  isactives: any = [];
  isactivesSelected: any = {};
  channels: any = [];
  channelSelected: any = {};
  bifastTrxNo: any;
  transactiontype: any = [];
  transactiontypeSelected: any = {};
  fileName = 'ExcelSheet.xlsx';
  startDate: any = new Date();
  endDate: any;
  display = false;
  displayPrv = false;
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';
  logData: any[] = [];
  dateTime: any;
  interEndDate: any;
  interStartDate: any;
  dateNow: any;
  birthday: any;
  isMaxDate: boolean = false;
  sysParamData: any[] = [];
  range: number;
  jsonUserInfo: any = {};
  username: string = 'Unknown';
  div: string = '   ';
  // no: number = 1;

  totalRowCreditS: number = 0;
  totalAmountCS: number = 0;
  totalRowDebitS: number = 0;
  totalAmountDS: number = 0;
  totalRowCreditE: number = 0;
  totalAmountCE: number = 0;
  totalRowDebitE: number = 0;
  totalAmountDE: number = 0;
  totalRowCreditT: number = 0;
  totalAmountCT: number = 0;
  totalRowDebitT: number = 0;
  totalAmountDT: number = 0;

  totalRowCredit: number = 0;
  totalAmountCredit: number = 0;
  totalRowDebit: number = 0;
  totalAmountDebit: number = 0;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private trxmonitorService: TrxmonitorService,
    private channeltypeService: ChanneltypeService,
    private sysparamService: SysparamService,
    private ServUsername: MainmenulayoutComponent,
    private connectionchannelService: ConnectionchannelService
  ) {}

  objtmp = {
    id: 'No record',
    komi_unique_id: 'No record',
    bifast_trx_no: 'No record',
    komi_trx_no: 'No record',
    channel_type: 'No record',
    channel_name: 'No record',
    branch_code: 'No record',
    recipient_bank: 'No record',
    sender_bank: 'No record',
    recipient_account_no: 'No record',
    recipient_proxy_type: 'No record',
    recipient_proxy_alias: 'No record',
    recipient_account_name: 'No record',
    sender_account_no: 'No record',
    sender_account_name: 'No record',
    charge_type: 'No record',
    trx_type: 'No record',
    trx_amount: 0,
    trx_fee: 'No record',
    trx_initiation_date: 'No record',
    trx_status_code: 'No record',
    trx_status_message: 'No record',
    trx_reason_code: 'No record',
    trx_proxy_flag: 'No record',
    trx_SLA_flag: 'No record',
    trx_duration: 'No record',
    trx_complete_date: 'No record',
    trx_trans: 'No record',
    trx_reason_message: 'No record',
    trx_response_code: 'No record',
  };

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Transaction Monitoring' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.connectionchannelService
      .getConnectionChannelbyTenant()
      .subscribe((response: BackendResponse) => {
        if (response.status == 200) {
          this.channels = response.data;
        } else {
          this.channels = [];
        }
      });

    // this.channeltypeService
    //   .getAllChnByTenant()
    //   .subscribe((result: BackendResponse) => {
    //     if (result.status == 200) {
    //       // console.log(JSON.stringify(result.data));
    //       this.channels = result.data;
    //     } else {
    //       this.channels = [];
    //     }
    //   });
    // reff number
    // console.log('>>>>>>> Refresh sysParam ');
    this.range = 0;
    this.sysparamService
      .getAllSysParByTenant()
      .subscribe((result: BackendResponse) => {
        // console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.sysParamData = [];
          let objtmp = {
            paramname: 'No records',
            paramvalua: 'No records',
            status: 'No records',
            created_date: 'No records',
          };
          this.sysParamData.push(objtmp);
        } else {
          // KOMI_CORE_TRX_DETAIL_RANGE
          this.sysParamData = result.data;
          for (let i = 0; i < this.sysParamData.length; i++) {
            if (
              this.sysParamData[i].paramname == 'KOMI_PORTAL_TRX_DETAIL_RANGE'
            ) {
              this.range = this.sysParamData[i].paramvalua;
            }
          }
        }
      });

    // mantap
    this.isactives = [
      { label: 'Accepted', value: 'ACTC' },
      { label: 'Rejected', value: 'RJCT' },
      { label: 'Timeout', value: 'KSTS' },
      { label: 'Other', value: 'OTHR' },
    ];

    this.transactiontype = [
      { label: 'Incoming Transaction', value: 'I' },
      { label: 'Outgoing Transaction', value: 'O' },
    ];
    this.logData = [];

    this.initialData();
  }

  refreshData() {
    this.totalRowCreditS = 0;
    this.totalAmountCS = 0;
    this.totalRowDebitS = 0;
    this.totalAmountDS = 0;
    this.totalRowCreditE = 0;
    this.totalAmountCE = 0;
    this.totalRowDebitE = 0;
    this.totalAmountDE = 0;
    this.totalRowCreditT = 0;
    this.totalAmountCT = 0;
    this.totalRowDebitT = 0;
    this.totalAmountDT = 0;
    this.isdataexist = false;
    this.logData = [];
    this.isFetching = true;
    let payload: any = {};
    // if (this.rangeDates != undefined) {
    //   start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
    //   end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    // }
    // if (start_date) payload.start_date = start_date;
    // if (end_date) payload.end_date = end_date;

    if (this.startDate) {
      payload.startDate = this.datepipe.transform(
        this.startDate,
        'yyyy-MM-dd 00:00:00'
      );
    }
    this.dateTime = new Date(
      this.startDate.getFullYear(),
      this.startDate.getMonth(),
      this.startDate.getDate() + Number(this.range)
    );

    if (this.endDate > this.dateTime) {
      this.isMaxDate = true;
      this.endDate = this.dateTime;
      payload.endDate = this.datepipe.transform(
        this.dateTime,
        'yyyy-MM-dd 00:00:00'
      );
    } else if (this.endDate == null) {
      this.isMaxDate = false;
      this.endDate = this.dateTime;
      payload.endDate = this.datepipe.transform(
        this.dateTime,
        'yyyy-MM-dd 00:00:00'
      );
    } else if (this.endDate < this.startDate) {
      this.isMaxDate = false;
      this.endDate = this.startDate;
      payload.endDate = this.datepipe.transform(
        this.startDate,
        'yyyy-MM-dd 00:00:00'
      );
    } else {
      this.isMaxDate = false;
      payload.endDate = this.datepipe.transform(
        this.endDate,
        'yyyy-MM-dd 00:00:00'
      );
    }

    this.isMaxDate = false;

    if (this.isactivesSelected) payload.status = this.isactivesSelected.value;
    if (this.channelSelected) {
      payload.channel = this.channelSelected.channel_name;
      payload.channelType = this.channelSelected.channel_type;
    }
    if (this.bifastTrxNo) payload.bifasttrxno = this.bifastTrxNo;
    if (this.transactiontypeSelected)
      payload.trxtype = this.transactiontypeSelected.value;

    // interval start date now
    this.interStartDate = new Date(
      this.startDate.getFullYear(),
      this.startDate.getMonth(),
      this.startDate.getDate() + Number(1)
    );
    payload.interStartDate = this.datepipe.transform(
      this.interStartDate,
      'yyyy-MM-dd 00:00:00'
    );

    // interval end date now
    this.interEndDate = new Date(
      this.endDate.getFullYear(),
      this.endDate.getMonth(),
      this.endDate.getDate() + Number(1)
    );
    payload.interEndDate = this.datepipe.transform(
      this.interEndDate,
      'yyyy-MM-dd 00:00:00'
    );

    // console.log('JSON>>>', JSON.stringify(payload));

    this.trxmonitorService
      .postParam(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(JSON.stringify(result));
        if (result.status == 200) {
          this.isdataexist = true;
          // this.logData = result.data;
          this.logData = result.data.results;
          for (let i = 0; i < this.logData.length; i++) {
            if (this.logData[i].trx_proxy_flag == 'T')
              this.logData[i].trx_proxy_flag = 'Credit Transfer';
            if (this.logData[i].trx_proxy_flag == 'Y')
              this.logData[i].trx_proxy_flag = 'Credit Transfer with Proxy';
            this.logData[i].trx_trans = 'Transfer';
            this.logData[i].trx_initiation_date = this.datepipe.transform(
              this.logData[i].trx_initiation_date,
              'yyyy-MM-dd HH:mm:ss'
            );
            this.logData[i].trx_complete_date = this.datepipe.transform(
              this.logData[i].trx_complete_date,
              'yyyy-MM-dd HH:mm:ss'
            );
            if (this.logData[i].trx_response_code == 'ACTC')
              this.logData[i].trx_response_code = 'Accepted';
            if (this.logData[i].trx_response_code == 'RJCT')
              this.logData[i].trx_response_code = 'Rejected';
            if (this.logData[i].trx_response_code == 'KSTS')
              this.logData[i].trx_response_code = 'Timeout';
            if (this.logData[i].trx_response_code == 'OTHR')
              this.logData[i].trx_response_code = 'Other';

            if (this.logData[i].trx_SLA_flag == '1')
              this.logData[i].trx_SLA_flag = 'Sesuai SLA';
            if (this.logData[i].trx_SLA_flag == '2')
              this.logData[i].trx_SLA_flag = 'Tidak Sesuai SLA';
          }
          // console.log('this.logData>>>>>>>>>>>>', this.logData);

          this.totalRowDebitS = parseInt(
            result.data.total.respDebitS.countRowDebitS
          );
          this.totalAmountDS = parseInt(
            result.data.total.respDebitS.amountDebitS == null
              ? 0
              : result.data.total.respDebitS.amountDebitS
          );
          this.totalRowCreditS = parseInt(
            result.data.total.respCreditS.countRowCreditS
          );
          this.totalAmountCS = parseInt(
            result.data.total.respCreditS.amountCreditS == null
              ? 0
              : result.data.total.respCreditS.amountCreditS
          );
          this.totalRowDebitE = parseInt(
            result.data.total.respDebitE.countRowDebitE
          );
          this.totalAmountDE = parseInt(
            result.data.total.respDebitE.amountDebitE == null
              ? 0
              : result.data.total.respDebitE.amountDebitE
          );
          this.totalRowCreditE = parseInt(
            result.data.total.respCreditE.countRowCreditE
          );
          this.totalAmountCE = parseInt(
            result.data.total.respCreditE.amountCreditE == null
              ? 0
              : result.data.total.respCreditE.amountCreditE
          );
          this.totalRowDebitT = parseInt(
            result.data.total.respDebitT.countRowDebitT
          );
          this.totalAmountDT = parseInt(
            result.data.total.respDebitT.amountDebitT == null
              ? 0
              : result.data.total.respDebitT.amountDebitT
          );
          this.totalRowCreditT = parseInt(
            result.data.total.respCreditT.countRowCreditT
          );
          this.totalAmountCT = parseInt(
            result.data.total.respCreditT.amountCreditT == null
              ? 0
              : result.data.total.respCreditT.amountCreditT
          );

          this.totalRowDebit =
            this.totalRowDebitS + this.totalRowDebitE + this.totalRowDebitT;
          this.totalAmountDebit =
            this.totalAmountDS + this.totalAmountDE + this.totalAmountDT;
          this.totalRowCredit =
            this.totalRowCreditS + this.totalRowCreditE + this.totalRowCreditT;
          this.totalAmountCredit =
            this.totalAmountCS + this.totalAmountCE + this.totalAmountCT;
        } else {
          this.isdataexist = false;
          this.logData = [];

          this.logData.push(this.objtmp);
        }
        this.isFetching = false;
      });
  }

  initialData() {
    // this.isFetching = true;
    // this.trxmonitorService
    //   .getAllMonToday()
    //   .subscribe((result: BackendResponse) => {
    //     if (result.status === 200) {

    this.logData.push(this.objtmp);
    //   } else {
    //     this.isdataexist = true;
    //     this.logData = result.data.results;
    //   }
    this.isFetching = false;
    // });
  }

  maxDate() {
    const now = this.startDate;
    this.birthday = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() + Number(this.range)
    );

    var date = this.birthday,
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);

    this.birthday = [day, mnth, date.getFullYear()].join('-');
    this.isMaxDate = true;
    return this.birthday;
  }

  functionAmount() {
    var total = 0;
    for (var i = 0; i < this.logData.length; i++) {
      var product = this.logData[i].trx_amount;
      total += Number(product);
    }
    return total;
  }

  functionVolume() {
    return this.logData.length;
  }

  exportexcel(): void {
    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    // this.fileName = 'xls' + datenow + '.xlsx';
    this.fileName = 'csvTrxMon' + datenow + '.csv';
    /* table id is passed over here */
    // let element = document.getElementById('excel-table');
    var rows = [];
    var number = 1;
    var col = [
      'No',
      'Ref. Number',
      'Trx By Service',
      'Sender Bank Code',
      'Sender Acc Name',
      'Sender Acc Number',
      'Receiver Bank Code',
      'Receiver Acc Name',
      'Receiver Acc Number',
      'Amount',
      'Purpose of Trx',
      'Initiation Date',
      'CH',
      'Description',
      'Status',
      'Status Reason Code',
      'Sesuai SLA',
    ];
    rows.push(col);
    this.logData.forEach((element) => {
      var trx_proxy_flag = '';
      var pot = 'transfer';
      var trx_response_code = '';
      var trx_SLA_flag = '';
      let trx_initiation_date = '';

      let trx_amount = this.currencyRp(element.trx_amount);
      let channel_name = this.initialChannel(element.channel_name);

      if (element.trx_proxy_flag == 'Credit Transfer') {
        trx_proxy_flag = 'CT';
      } else {
        trx_proxy_flag = 'CT with Proxy';
      }

      // if (element.trx_response_code == 'ACTC') {
      //   trx_response_code = 'Accepted';
      // } else if (element.trx_response_code == 'RJCT') {
      //   trx_response_code = 'Rejected';
      // } else if (element.trx_response_code == 'KSTS') {
      //   trx_response_code = 'Timeout';
      // } else {
      //   trx_response_code = 'No Record';
      // }

      if (element.trx_SLA_flag == 'Sesuai SLA') {
        trx_SLA_flag = 'Yes';
      } else {
        trx_SLA_flag = 'No';
      }

      // trx_initiation_date
      trx_initiation_date = this.datepipe.transform(
        element.trx_initiation_date,
        'yyyy-MM-dd HH:mm:ss'
      );

      var temp = [
        number++,
        // element.komi_unique_id,
        element.bifast_trx_no,
        trx_proxy_flag,
        element.sender_bank,
        element.sender_account_name,
        element.sender_account_no,
        element.recipient_bank,
        element.recipient_account_name,
        element.recipient_account_no,
        trx_amount,
        pot,
        trx_initiation_date,
        channel_name,
        element.trx_status_message,
        element.trx_response_code,
        element.trx_reason_code,
        trx_SLA_flag,
      ];
      rows.push(temp);
    });
    // const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    // const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.logData);
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(rows);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
  openPDF(): void {
    var number = 1;
    var doc = new jsPDF('l', 'mm', [297, 210]);
    var col = [
      [
        'No',
        'Ref. Number',
        'Trx By Service',
        'Sender Bank Code',
        'Sender Acc Name',
        'Sender Acc Number',
        'Receiver Bank Code',
        'Receiver Acc Name',
        'Receiver Acc Number',
        'Amount',
        'Purpose of Trx',
        'Initiation Date',
        'CH',
        'Description',
        'Status',
        'Status Reason Code',
        'Sesuai SLA',
      ],
    ];
    var rows = [];

    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    this.fileName = 'pdf' + datenow + '.pdf';
    this.logData.forEach((element) => {
      var trx_proxy_flag = '';
      var pot = 'transfer';
      var trx_response_code = '';
      var trx_SLA_flag = '';
      let trx_initiation_date = '';

      let trx_amount = this.currencyRp(element.trx_amount);
      let channel_name = this.initialChannel(element.channel_name);

      if (element.trx_proxy_flag == 'Credit Transfer') {
        trx_proxy_flag = 'CT';
      } else {
        trx_proxy_flag = 'CT with Proxy';
      }

      // if (element.trx_response_code == 'ACTC') {
      //   trx_response_code = 'Accepted';
      // } else if (element.trx_response_code == 'RJCT') {
      //   trx_response_code = 'Rejected';
      // } else if (element.trx_response_code == 'KSTS') {
      //   trx_response_code = 'Timeout';
      // } else {
      //   trx_response_code = 'No Record';
      // }

      if (element.trx_SLA_flag == 'Sesuai SLA') {
        trx_SLA_flag = 'Yes';
      } else {
        trx_SLA_flag = 'No';
      }

      // trx_initiation_date
      trx_initiation_date = this.datepipe.transform(
        element.trx_initiation_date,
        'yyyy-MM-dd HH:mm:ss'
      );

      var temp = [
        number++,
        // element.komi_unique_id,
        element.bifast_trx_no,
        trx_proxy_flag,
        element.sender_bank,
        element.sender_account_name,
        element.sender_account_no,
        element.recipient_bank,
        element.recipient_account_name,
        element.recipient_account_no,
        trx_amount,
        pot,
        trx_initiation_date,
        channel_name,
        element.trx_status_message,
        element.trx_response_code,
        element.trx_reason_code,
        trx_SLA_flag,
      ];
      rows.push(temp);
    });
    // doc.autoTable(col, rows);
    // doc.text('Left aligned text', 15, 10);

    //header right
    doc.setFontSize(8);
    let dateGenerated = this.datepipe.transform(myDate, 'dd/MM/yyyy');
    let timeGenerated = this.datepipe.transform(myDate, 'HH:mm:ss');
    doc.text(
      'DATE GENERATED :\t' + dateGenerated,
      doc.internal.pageSize.getWidth() - 60,
      20,
      {
        align: 'left',
      }
    );
    doc.text(
      'TIME GENERATED : \t' + timeGenerated,
      doc.internal.pageSize.getWidth() - 60,
      25,
      {
        align: 'left',
      }
    );
    doc.text(
      'GENERATED BY     : \t' + this.ServUsername.getUsername(),
      doc.internal.pageSize.getWidth() - 60,
      30,
      {
        align: 'left',
      }
    );

    // header page
    doc.setFontSize(12);
    doc.text(
      'INDIVIDUAL CREDIT TRANSFER TRANSACTION DETAIL REPORT',
      doc.internal.pageSize.getWidth() / 2,
      40,
      {
        align: 'center',
      }
    );

    // range date
    doc.setFontSize(12);
    let stardate = this.datepipe.transform(this.startDate, 'yyyy/MM/dd');
    let endate = this.datepipe.transform(this.endDate, 'yyyy/MM/dd'); // note
    doc.text(
      'Date : ' + stardate + ' - ' + endate,
      doc.internal.pageSize.getWidth() / 2,
      45,
      {
        align: 'center',
      }
    );
    // note
    // doc.internal.pageSize.getHeight() - 25
    doc.setFontSize(8);
    doc.text(
      'Total Debit Volume : ' + this.totalRowDebit,
      doc.internal.pageSize.getWidth() / 20,
      60,
      {
        align: 'left',
      }
    );
    doc.text(
      'Total Debit Amount : Rp. ' + this.currencyRp(this.totalAmountDebit),
      doc.internal.pageSize.getWidth() / 20,
      65,
      {
        align: 'left',
      }
    );
    doc.text(
      'Total Credit Volume : ' + this.totalRowCredit,
      doc.internal.pageSize.getWidth() / 4,
      60,
      {
        align: 'left',
      }
    );
    doc.text(
      'Total Credit Amount : Rp. ' + this.currencyRp(this.totalAmountCredit),
      doc.internal.pageSize.getWidth() / 4,
      65,
      {
        align: 'left',
      }
    );

    autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (rows) => {},
      headStyles: { fillColor: [128, 128, 128] },
      styles: { fontSize: 8 },
      columnStyles: {
        0: { cellWidth: 9 },
        1: { cellWidth: 23 },
        2: { cellWidth: 15 },
        3: { cellWidth: 18 },
        4: { cellWidth: 15 },
        5: { cellWidth: 15 },
        6: { cellWidth: 18 },
        7: { cellWidth: 15 },
        8: { cellWidth: 21 },
        9: { cellWidth: 22 },
        10: { cellWidth: 15 },
        11: { cellWidth: 18 },
        12: { cellWidth: 10 },
        13: { cellWidth: 25 },
        14: { cellWidth: 15 },
        15: { cellWidth: 14 },
        16: { cellWidth: 13 },
      },
      margin: { top: 70, right: 8, bottom: 10, left: 8 },
      didDrawPage: function (data) {
        // Reseting top margin. The change will be reflected only after print the first page.
        data.settings.margin.top = 25;
      },
    });
    const pageCount = doc.internal.pages.length - 1;
    // console.log('pageCount', pageCount);
    for (var i = 1; i <= pageCount; i++) {
      doc.setPage(i);
      doc.text(
        'Page ' + String(i) + ' from ' + String(pageCount),
        doc.internal.pageSize.getWidth() - 60,
        10
      );
    }
    doc.save(this.fileName);
  }

  removeFilter() {
    this.startDate = new Date();
    this.endDate = null;
    this.isactivesSelected = [0];
    this.channelSelected = [0];
    this.transactiontypeSelected = [0];
    this.isMaxDate = false;
  }

  currencyRp(data: any) {
    // amount
    var number_string = data.toString(),
      sisa = number_string.length % 3,
      trx_amount = number_string.substr(0, sisa),
      ribuan = number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
      var separator = sisa ? '.' : '';
      trx_amount += separator + ribuan.join('.');
    }
    return trx_amount;
  }

  initialChannel(data: any) {
    // channel name
    var cek = data.split(/[\s,]+/);
    let first = [];
    let hasil = [];
    let channel_name = '';
    if (data == 'ATM') {
      channel_name = 'ATM';
    } else if (data == 'Teller' || data == 'Corebank') {
      channel_name = 'T';
    } else {
      if (cek.length > 1) {
        for (let i = 0; i < cek.length; i++) {
          first[i] = cek[i].charAt(0);
          hasil[i] = first[i];
          channel_name = channel_name + hasil[i];
        }
      } else {
        channel_name = cek[0].charAt(0);
      }
    }
    return channel_name;
  }
}

export interface Customer {
  id?: number;
  name?: string;
  country?: string;
  company?: string;
  date?: string | Date;
  status?: string;
  activity?: number;
  representative?: string;
  verified?: boolean;
  balance?: boolean;
}
