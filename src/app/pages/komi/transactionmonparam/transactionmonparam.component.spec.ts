import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionmonparamComponent } from './transactionmonparam.component';

describe('TransactionmonparamComponent', () => {
  let component: TransactionmonparamComponent;
  let fixture: ComponentFixture<TransactionmonparamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionmonparamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionmonparamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
