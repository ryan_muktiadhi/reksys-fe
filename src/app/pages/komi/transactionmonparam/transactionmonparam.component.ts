import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { TrxmonitorService } from 'src/app/services/komi/trxmonitor.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as FileSaver from 'file-saver';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'app-transactionmonparam',
  templateUrl: './transactionmonparam.component.html',
  styleUrls: ['./transactionmonparam.component.scss']
})
export class TransactionmonparamComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';

  rangeDates: Date[];
  transactionType: any = [];
  transactionTypeSelected: any = {};

  isFetching: boolean = false;
  logData: any[] = [];
  cols: any[];
  selectedColumns: any[];

  totalRowCreditS: number = 0;
  totalAmountCS: number = 0;
  totalRowDebitS: number = 0;
  totalAmountDS: number = 0;

  totalRowCreditF: number = 0;
  totalAmountCF: number = 0;
  totalRowDebitF: number = 0;
  totalAmountDF: number = 0;

  first: number = 0;
  last: number = 0;
  totalRecords: number;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private trxmonitorService: TrxmonitorService,
  ) { }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      { label: 'Monitoring' },
      { label: 'Transaction by Date Range' }
    ];

    this.transactionType = [
      { label: 'Incoming Transaction', value: "I" },
      { label: 'Outgoing Transaction', value: "O" },
    ];

    //NEW ADD : State Columns
    this.cols = [
      { field: 'bifastId', header: 'Transaction ID', isOrder: false, width: "300px" },
      { field: 'retrievalRefNumber', header: 'Retrieval Ref Number', isOrder: false, width: "250px" },
      { field: 'trxType', header: 'Transaction Type', isOrder: true, width: "200px" },
      /*{ field: '', header: 'Sender', hasSub: true, width: "600px", optdisable: true },*/
      { field: 'senderBank', header: 'Sender Bank Code', isOrder: true, isSub: true, width: "200px" },
      { field: 'senderAccountName', header: 'Sender Account Name', isOrder: true, isSub: true, width: "250px" },
      { field: 'senderAccountNo', header: 'Sender Account Number', isOrder: false, isSub: true, width: "250px" },
      /*{ field: '', header: 'Receiver', hasSub: true, width: "600px" },*/
      { field: 'recipientBank', header: 'Receiver Bank Code', isOrder: true, isSub: true, width: "200px" },
      { field: 'recipientAccountName', header: 'Receiver Account Name', isOrder: true, isSub: true, width: "300px" },
      { field: 'recipientAccountNo', header: 'Receiver Account Number', isOrder: false, isSub: true, width: "300px" },
      { field: 'trxAmount', header: 'Amount', width: "200px", isOrder: true, data: true },
      { field: 'categoryPurpose', header: 'Purpose Of Transaction', isOrder: false, width: "250px" },
      { field: 'requestDate', header: 'Initiation Date / Time', isOrder: false, width: "250px" },
      { field: 'responseDate', header: 'Complete Date / Time', isOrder: false, width: "250px" },
      { field: 'paymentInformation', header: 'Description', isOrder: false, width: "200px" },
      { field: 'trxStatusCode', header: 'Status', isOrder: true, width: "200px" },
      { field: 'trxStatusMessage', header: 'Status Message', isOrder: true, width: "200px" },
      { field: 'TEST1', header: 'Total Debit Volume', isOrder: true, width: "200px" },
      { field: 'TEST2', header: 'Total Credit Volume', isOrder: true, width: "200px" }
    ];

    this.selectedColumns = this.cols;

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    this.logData = [];
    let objtmp = {
      bifastId: 'No record',
      retrievalRefNumber: 'No record',
      trxType: 'No record',
      senderBank: 'No record',
      senderAccountName: 'No record',
      senderAccountNo: 'No record',
      recipientBank: 'No record',
      recipientAccountName: 'No record',
      recipientAccountNo: 'No record',
      trxAmount: 0,
      categoryPurpose: 'No record',
      requestDate: 'No record',
      responseDate: 'No record',
      paymentInformation: 'No record',
      trxStatusCode: 'No record',
      trxStatusMessage: 'No record',
      TEST1: 'No record',
      TEST2: 'No record',
    };
    this.logData.push(objtmp);
  }

  refreshData(event: LazyLoadEvent) {
    let start_date = null;
    let end_date = null;
    let firstRow = event.first == null ? 1 : event.first + 1;
    let maxRow = event.rows == null ? 10 : event.rows + event.first;

    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    }

    let diffDay = this.calculateDiff(start_date, end_date);
    // console.log(diffDay);

    if (start_date != null && end_date != null) {
      if (diffDay >= 7) {
        this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Maximal Date Range only 7 Days' });
      } else {
        this.isFetching = true;

        this.first = firstRow;
        let filters: any = {};
        if (this.transactionTypeSelected == "I" || this.transactionTypeSelected == "O") {
          filters.trxType = '%' + this.transactionTypeSelected + '%';
        } else {
          filters.trxType = null;
        }
        if (event.filters) filters.bifastId = event.filters.bifastId.value == null ? null : '%' + event.filters.bifastId.value + '%';
        if (event.filters) filters.retrievalRefNumber = event.filters.retrievalRefNumber.value == null ? null : '%' + event.filters.retrievalRefNumber.value + '%';
        if (event.filters) filters.senderBank = event.filters.senderBank.value == null ? null : '%' + event.filters.senderBank.value + '%';
        if (event.filters) filters.senderAccountName = event.filters.senderAccountName.value == null ? null : '%' + event.filters.senderAccountName.value + '%';
        if (event.filters) filters.senderAccountNo = event.filters.senderAccountNo.value == null ? null : '%' + event.filters.senderAccountNo.value + '%';
        if (event.filters) filters.recipientBank = event.filters.recipientBank.value == null ? null : '%' + event.filters.recipientBank.value + '%';
        if (event.filters) filters.recipientAccountName = event.filters.recipientAccountName.value == null ? null : '%' + event.filters.recipientAccountName.value + '%';
        if (event.filters) filters.recipientAccountNo = event.filters.recipientAccountNo.value == null ? null : '%' + event.filters.recipientAccountNo.value + '%';
        if (event.filters) filters.trxAmount = event.filters.trxAmount.value == null ? null : '%' + event.filters.trxAmount.value + '%';
        if (event.filters) filters.categoryPurpose = event.filters.categoryPurpose.value == null ? null : '%' + event.filters.categoryPurpose.value + '%';
        if (event.filters) filters.paymentInformation = event.filters.paymentInformation.value == null ? null : '%' + event.filters.paymentInformation.value + '%';
        if (event.filters) filters.trxStatusCode = event.filters.trxStatusCode.value == null ? null : '%' + event.filters.trxStatusCode.value + '%';

        let fieldOrders: any = {};
        if (event.sortField && event.sortOrder) {
          let formatField = null;
          if (event.sortField == "trxType") {
            formatField = 'TRX_TYPE'
          } else if (event.sortField == "senderBank") {
            formatField = 'SENDER_BANK'
          } else if (event.sortField == "senderAccountName") {
            formatField = 'SENDER_ACCOUNT_NAME'
          } else if (event.sortField == "recipientAccountName") {
            formatField = 'RECIPIENT_ACCOUNT_NAME'
          } else if (event.sortField == "trxAmount") {
            formatField = 'TRX_AMOUNT'
          } else if (event.sortField == "trxStatusCode") {
            formatField = 'TRX_STATUS_CODE'
          } else if (event.sortField == "trxStatusMessage") {
            formatField = 'TRX_STATUS_MESSAGE'
          }

          fieldOrders = {
            fieldName: formatField,
            orders: event.sortOrder == 1 ? 'ASC' : 'DESC'
          }
        }

        let payload: any = {};
        payload = {
          start_date: start_date,
          end_date: end_date,
          firstRow: firstRow,
          maxRow: maxRow,
          filters: filters,
          fieldOrders: fieldOrders
        }

        console.log(JSON.stringify(payload));

        this.trxmonitorService
          .postParam(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.logData = result.data.results;
              this.last = result.data.results.length == null ? 0 : result.data.results.length;
              this.totalRecords = result.data.results.length + firstRow;

              this.totalRowDebitS = parseInt(result.data.total.countRowDebitS);
              this.totalAmountDS = parseInt(result.data.total.amountDebitS == null ? 0 : result.data.total.amountDebitS);
              this.totalRowCreditS = parseInt(result.data.total.countRowCreditS);
              this.totalAmountCS = parseInt(result.data.total.amountCreditS == null ? 0 : result.data.total.amountCreditS);

              this.totalRowDebitF = parseInt(result.data.total.countRowDebitF);
              this.totalAmountDF = parseInt(result.data.total.amountDebitF == null ? 0 : result.data.total.amountDebitF);
              this.totalRowCreditF = parseInt(result.data.total.countRowCreditF);
              this.totalAmountCF = parseInt(result.data.total.amountCreditF == null ? 0 : result.data.total.amountCreditF);
              // if (event.filters) {
              //   this.logData = this.logData.filter(row => this.filterField(row, event.filters));
              // }
            } else {
              if (result.data.status == 'F') {
                this.messageService.add({ severity: 'warn', summary: 'Failed', detail: 'Data not found' });
              } else if (result.data.status == 'E') {
                this.messageService.add({ severity: 'error', summary: 'Error', detail: result.data.statusMessage });
              }
            }
            this.isFetching = false;
          });
      }
    } else {
      this.messageService.add({ severity: 'warn', summary: 'Warning', detail: 'Please choose Date Range of Transaction Data' });
      this.isFetching = false;
    }
  }

  calculateDiff(date1, date2) {
    date1 = new Date(date1);
    date2 = new Date(date2);

    return Math.floor(
      (Date.UTC(
        date2.getFullYear(),
        date2.getMonth(),
        date2.getDate()
      ) -
        Date.UTC(
          date1.getFullYear(),
          date1.getMonth(),
          date1.getDate()
        )) /
      (1000 * 60 * 60 * 24)
    );
  }

  // filterField(row, filter) {
  //   console.log(filter.length);
  //   for (var columnName in filter) {
  //     console.log(columnName.match);
  //     if (row[columnName] == null) {
  //       return false;
  //     }
  //     let rowValue: String = row[columnName].toString().toLowerCase();
  //     let filterMatchMode: String = filter[columnName].matchMode;
  //     if (filterMatchMode.includes("contains") && rowValue.includes(filter[columnName].value.toLowerCase())) {
  //       return true;
  //     } else if (filterMatchMode.includes("startsWith") && rowValue.startsWith(filter[columnName].value.toLowerCase())) {
  //       return true;
  //     } else if (filterMatchMode.includes("in") && filter[columnName].value.includes(rowValue)) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   }
  // }

  exportPdf() {
    var doc = new jsPDF('l', 'mm', [297, 210]);

    //CREATE TABLE
    var col = [this.selectedColumns.map(col => ({ title: col.header }))];
    var rows = [];
    this.logData.forEach((element) => {
      var temp = [
        element.trxDate,
        element.trxType,
        element.trxStatusMessage,
        element.komiTrxNo,
        element.bifastTrxNo,
        element.channelType,
        element.recipientBank,
        element.senderBank,
        element.recipientAccountNo,
        element.senderAccountNo,
        element.recipientAccountName,
        element.senderAccountName,
        element.trxAmount
      ];
      rows.push(temp);
    });

    autoTable(doc, {
      startY: 30,
      head: col,
      body: rows,
    });

    //SET TITLE PDF
    doc.setFontSize(24);
    doc.text("Reporting Transaction", 100, 20);

    //FILENAME
    doc.save('test.pdf');
  }

  exportExcel() {
    // const worksheet = XLSX.utils.json_to_sheet(this.selectedColumns.map(col => ({ title: col.header })));
    let element = document.getElementById('tb-transparam');
    const worksheet = XLSX.utils.table_to_sheet(element);
    const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, "products");
    /* save to file */
    // XLSX.writeFile(excelBuffer, "products");
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  // exportExcel(): void {
  //   let myDate = new Date();
  //   let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
  //   this.fileName = 'xls' + datenow + '.xlsx';

  //   /* table id is passed over here */
  //   let element = document.getElementById('excel-table');
  //   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

  //   /* generate workbook and add the worksheet */
  //   const wb: XLSX.WorkBook = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

  //   /* save to file */
  //   XLSX.writeFile(wb, this.fileName);
  // }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
  //   openPDF(): void {
  //     var doc = new jsPDF('l', 'mm', [297, 210]);
  //     var col = [
  //       [
  //         'Trx Date',
  //         'Status',
  //         'TrxNum',
  //         'Reff BiFAST',
  //         'Channel',
  //         'Dest Bank',
  //         'Src Bank',
  //         'Dest AccNum',
  //         'Src AccNum',
  //         'Dest Acc Name',
  //         'Src Acc Name',
  //         'Amount',
  //       ],
  //     ];
  //     var rows = [];

  //     let myDate = new Date();
  //     let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
  //     this.fileName = 'pdf' + datenow + '.pdf';
  //     this.logData.forEach((element) => {
  //       var temp = [
  //         element.created_date,
  //         element.status,
  //         element.trxnumber,
  //         element.refnumbifast,
  //         element.channel,
  //         element.destbank,
  //         element.srcbank,
  //         element.destaccnum,
  //         element.srcaccnum,
  //         element.destaccname,
  //         element.srcaccname,
  //         element.amount,
  //       ];
  //       rows.push(temp);
  //     });
  //     // doc.autoTable(col, rows);
  //     autoTable(doc, {
  //       head: col,
  //       body: rows,
  //       didDrawCell: (rows) => { },
  //     });

  //     doc.save(this.fileName);
  //   }

}

export interface Customer {
  id?: number;
  name?: string;
  country?: string;
  company?: string;
  date?: string | Date;
  status?: string;
  activity?: number;
  representative?: string;
  verified?: boolean;
  balance?: boolean;
}
