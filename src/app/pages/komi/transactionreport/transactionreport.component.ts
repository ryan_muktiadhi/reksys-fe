import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { DatePipe } from '@angular/common';
import { TrxreportService } from 'src/app/services/komi/trxreport.service';
import { BranchService } from 'src/app/services/komi/branch.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';

import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'app-transactionreport',
  templateUrl: './transactionreport.component.html',
  styleUrls: ['./transactionreport.component.scss'],
})
export class TransactionreportComponent implements OnInit {
  // customers: Customer[];
  rangeDates: Date[];
  isactives: any = [];
  description: string = '';
  isactivesSelected: any = {};
  channels: any = [];
  channelSelected: any = {};
  transactionType: any = [];
  transactionTypeSelected: any = 0;
  display = false;
  displayPrv = false;
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';
  rptData: any[] = [];

  //NEW ADD : Columns
  cols: any[];
  selectedColumns: any[];

  branchName: any = [];
  branchSelected: any = 0;

  totalRowCreditS: number = 0;
  totalAmountCS: number = 0;
  totalRowDebitS: number = 0;
  totalAmountDS: number = 0;

  totalRowCreditF: number = 0;
  totalAmountCF: number = 0;
  totalRowDebitF: number = 0;
  totalAmountDF: number = 0;

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private branchService: BranchService,
    private trxreportService: TrxreportService
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Reporting' }, { label: 'Transaction' }];

    // this.branchService.getAllBranchByTenant().subscribe((result: BackendResponse) => {
    //   if (result.status == 200) {
    //     for (let i = 0; i < result.data.length; i++) {
    //       this.branchName.push(
    //         {
    //           label: result.data[i].BRANCH_NAME,
    //           value: result.data[i].BRANCH_CODE
    //         }
    //       );
    //     }
    //   }
    // });

    this.transactionType = [
      { label: 'Incoming Transaction', value: 'I' },
      { label: 'Outgoing Transaction', value: 'O' },
    ];

    //NEW ADD : State Columns
    this.cols = [
      {
        field: 'bifastId',
        header: 'Transaction ID',
        isOrder: false,
        width: '300px',
        wscolW: 40,
      },
      {
        field: 'retrievalRefNumber',
        header: 'Retrieval Ref Number',
        isOrder: false,
        width: '250px',
        wscolW: 40,
      },
      {
        field: 'trxType',
        header: 'Transaction Type',
        isOrder: true,
        width: '200px',
        wscolW: 15,
      },
      /*{ field: '', header: 'Sender', hasSub: true, width: "600px", wscolW: 20, optdisable: true },*/
      {
        field: 'senderBank',
        header: 'Sender Bank Code',
        isOrder: true,
        isSub: true,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'senderAccountName',
        header: 'Sender Account Name',
        isOrder: true,
        isSub: true,
        width: '250px',
        wscolW: 20,
      },
      {
        field: 'senderAccountNo',
        header: 'Sender Account Number',
        isOrder: false,
        isSub: true,
        width: '250px',
        wscolW: 20,
      },
      /*{ field: '', header: 'Receiver', hasSub: true, width: "600px", wscolW: 20 },*/
      {
        field: 'recipientBank',
        header: 'Receiver Bank Code',
        isOrder: true,
        isSub: true,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'recipientAccountName',
        header: 'Receiver Account Name',
        isOrder: true,
        isSub: true,
        width: '300px',
        wscolW: 30,
      },
      {
        field: 'recipientAccountNo',
        header: 'Receiver Account Number',
        isOrder: false,
        isSub: true,
        width: '300px',
        wscolW: 20,
      },
      {
        field: 'trxAmount',
        header: 'Amount',
        width: '200px',
        wscolW: 20,
        isOrder: true,
        data: true,
      },
      {
        field: 'categoryPurpose',
        header: 'Purpose Of Transaction',
        isOrder: false,
        width: '250px',
        wscolW: 20,
      },
      {
        field: 'requestDate',
        header: 'Initiation Date / Time',
        isOrder: false,
        width: '250px',
        wscolW: 25,
      },
      {
        field: 'responseDate',
        header: 'Complete Date / Time',
        isOrder: false,
        width: '250px',
        wscolW: 25,
      },
      {
        field: 'paymentInformation',
        header: 'Description',
        isOrder: false,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'trxStatusCode',
        header: 'Status',
        isOrder: true,
        width: '200px',
        wscolW: 20,
      },
      {
        field: 'trxStatusMessage',
        header: 'Status Message',
        isOrder: true,
        width: '200px',
        wscolW: 40,
      },
    ];

    this.selectedColumns = this.cols;

    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });

    this.rptData = [];
    let objtmp = {
      bifastId: 'No record',
      retrievalRefNumber: 'No record',
      trxType: 'No record',
      senderBank: 'No record',
      senderAccountName: 'No record',
      senderAccountNo: 'No record',
      recipientBank: 'No record',
      recipientAccountName: 'No record',
      recipientAccountNo: 'No record',
      trxAmount: 0,
      categoryPurpose: 'No record',
      requestDate: 'No record',
      responseDate: 'No record',
      paymentInformation: 'No record',
      trxStatusCode: 'No record',
      trxStatusMessage: 'No record',
    };
    this.rptData.push(objtmp);
  }

  refreshReportData() {
    let start_date = null;
    let end_date = null;
    // let firstRow = event.first == null ? 1 : event.first + 1;
    // let maxRow = event.rows == null ? 10 : event.rows + event.first;

    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    }

    let diffDay = this.calculateDiff(start_date, end_date);
    // console.log(diffDay);

    if (start_date != null && end_date != null) {
      if (diffDay >= 7) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warning',
          detail: 'Maximal Date Range only 7 Days',
        });
      } else {
        this.isFetching = true;

        // this.first = firstRow;
        let filters: any = {};
        if (
          this.transactionTypeSelected == 'I' ||
          this.transactionTypeSelected == 'O'
        ) {
          filters.trxType = '%' + this.transactionTypeSelected + '%';
        } else {
          filters.trxType = '%%';
        }
        // if (event.filters) filters.bifastId = event.filters.bifastId.value == null ? null : '%' + event.filters.bifastId.value + '%';
        // if (event.filters) filters.retrievalRefNumber = event.filters.retrievalRefNumber.value == null ? null : '%' + event.filters.retrievalRefNumber.value + '%';
        // if (event.filters) filters.senderBank = event.filters.senderBank.value == null ? null : '%' + event.filters.senderBank.value + '%';
        // if (event.filters) filters.senderAccountName = event.filters.senderAccountName.value == null ? null : '%' + event.filters.senderAccountName.value + '%';
        // if (event.filters) filters.senderAccountNo = event.filters.senderAccountNo.value == null ? null : '%' + event.filters.senderAccountNo.value + '%';
        // if (event.filters) filters.recipientBank = event.filters.recipientBank.value == null ? null : '%' + event.filters.recipientBank.value + '%';
        // if (event.filters) filters.recipientAccountName = event.filters.recipientAccountName.value == null ? null : '%' + event.filters.recipientAccountName.value + '%';
        // if (event.filters) filters.recipientAccountNo = event.filters.recipientAccountNo.value == null ? null : '%' + event.filters.recipientAccountNo.value + '%';
        // if (event.filters) filters.trxAmount = event.filters.trxAmount.value == null ? null : '%' + event.filters.trxAmount.value + '%';
        // if (event.filters) filters.categoryPurpose = event.filters.categoryPurpose.value == null ? null : '%' + event.filters.categoryPurpose.value + '%';
        // if (event.filters) filters.paymentInformation = event.filters.paymentInformation.value == null ? null : '%' + event.filters.paymentInformation.value + '%';
        // if (event.filters) filters.trxStatusCode = event.filters.trxStatusCode.value == null ? null : '%' + event.filters.trxStatusCode.value + '%';

        // let fieldOrders: any = {};
        // if (event.sortField && event.sortOrder) {
        //   let formatField = null;
        //   if (event.sortField == "trxType") {
        //     formatField = 'TRX_TYPE'
        //   } else if (event.sortField == "senderBank") {
        //     formatField = 'SENDER_BANK'
        //   } else if (event.sortField == "senderAccountName") {
        //     formatField = 'SENDER_ACCOUNT_NAME'
        //   } else if (event.sortField == "recipientAccountName") {
        //     formatField = 'RECIPIENT_ACCOUNT_NAME'
        //   } else if (event.sortField == "trxAmount") {
        //     formatField = 'TRX_AMOUNT'
        //   } else if (event.sortField == "trxStatusCode") {
        //     formatField = 'TRX_STATUS_CODE'
        //   } else if (event.sortField == "trxStatusMessage") {
        //     formatField = 'TRX_STATUS_MESSAGE'
        //   }

        //   fieldOrders = {
        //     fieldName: formatField,
        //     orders: event.sortOrder == 1 ? 'ASC' : 'DESC'
        //   }
        // }

        let payload: any = {};
        payload = {
          start_date: start_date,
          end_date: end_date,
          // firstRow: firstRow,
          // maxRow: maxRow,
          filters: filters,
          // fieldOrders: fieldOrders
        };

        // console.log(JSON.stringify(payload));

        this.trxreportService
          .getAllReport(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.rptData = result.data.results;
              // this.last = result.data.results.length == null ? 0 : result.data.results.length;
              // this.totalRecords = result.data.results.length + firstRow;

              this.totalRowDebitS = parseInt(result.data.total.countRowDebitS);
              this.totalAmountDS = parseInt(
                result.data.total.amountDebitS == null
                  ? 0
                  : result.data.total.amountDebitS
              );
              this.totalRowCreditS = parseInt(
                result.data.total.countRowCreditS
              );
              this.totalAmountCS = parseInt(
                result.data.total.amountCreditS == null
                  ? 0
                  : result.data.total.amountCreditS
              );

              this.totalRowDebitF = parseInt(result.data.total.countRowDebitF);
              this.totalAmountDF = parseInt(
                result.data.total.amountDebitF == null
                  ? 0
                  : result.data.total.amountDebitF
              );
              this.totalRowCreditF = parseInt(
                result.data.total.countRowCreditF
              );
              this.totalAmountCF = parseInt(
                result.data.total.amountCreditF == null
                  ? 0
                  : result.data.total.amountCreditF
              );
            } else {
              if (result.data.status == 'F') {
                this.messageService.add({
                  severity: 'warn',
                  summary: 'Failed',
                  detail: 'Data not found',
                });
              } else if (result.data.status == 'E') {
                this.messageService.add({
                  severity: 'error',
                  summary: 'Error',
                  detail: result.data.statusMessage,
                });
              }
            }
            this.isFetching = false;
          });
      }
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warning',
        detail: 'Please choose Date Range of Transaction Data',
      });
      this.isFetching = false;
    }
  }

  calculateDiff(date1, date2) {
    date1 = new Date(date1);
    date2 = new Date(date2);

    return Math.floor(
      (Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate()) -
        Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate())) /
        (1000 * 60 * 60 * 24)
    );
  }

  exportExcel() {
    let start_date = null;
    let end_date = null;
    let start_date_exc = null;
    let end_date_exc = null;
    let wscols = [];

    let dataExport = [];

    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
      start_date_exc = this.datepipe.transform(this.rangeDates[0], 'd MMM y');
      end_date_exc = this.datepipe.transform(this.rangeDates[1], 'd MMM y');
    }

    let diffDay = this.calculateDiff(start_date, end_date);

    if (start_date != null && end_date != null) {
      if (diffDay >= 7) {
        this.messageService.add({
          severity: 'warn',
          summary: 'Warning',
          detail: 'Maximal Date Range only 7 Days',
        });
      } else {
        this.isFetching = true;

        let filters: any = {};
        if (
          this.transactionTypeSelected == 'I' ||
          this.transactionTypeSelected == 'O'
        ) {
          filters.trxType = '%' + this.transactionTypeSelected + '%';
        } else {
          filters.trxType = '%%';
        }

        let payload: any = {};
        payload = {
          start_date: start_date,
          end_date: end_date,
          filters: filters,
        };

        this.trxreportService
          .getAllReport(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              dataExport = result.data.results;

              let fieldSelected = this.selectedColumns.map((col) => col.field);
              const resDataExport = dataExport.map((data) =>
                fieldSelected.reduce((o, k) => ((o[k] = data[k]), o), {})
              );

              // this.totalRowDebit = parseInt(result.data.total.countRowDebit);
              // this.totalAmountD = parseInt(result.data.total.amountDebit == null ? 0 : result.data.total.amountDebit);
              // this.totalRowCredit = parseInt(result.data.total.countRowCredit);
              // this.totalAmountC = parseInt(result.data.total.amountCredit == null ? 0 : result.data.total.amountCredit);

              let heading = [
                ['INDIVIDUAL CREDIT TRANSFER TRANSACTION DETAIL REPORT'],
              ];
              let subHeading = [
                ['PERIOD: ' + start_date + '  s/d  ' + end_date],
              ];
              let headerField = [this.selectedColumns.map((col) => col.header)];

              // let element = document.getElementById('tb-transrpt');
              const worksheet = XLSX.utils.json_to_sheet([]);
              XLSX.utils.sheet_add_aoa(worksheet, heading);
              XLSX.utils.sheet_add_aoa(worksheet, subHeading, { origin: 'A2' });
              XLSX.utils.sheet_add_aoa(worksheet, headerField, {
                origin: 'A3',
              });
              XLSX.utils.sheet_add_json(worksheet, resDataExport, {
                origin: 'A4',
                skipHeader: true,
              });

              this.create_gap_rows(worksheet, 1);
              // XLSX.utils.sheet_add_aoa(worksheet, subHeading, { origin: -1 });

              wscols = this.selectedColumns.map((col) => ({
                width: col.wscolW,
              }));
              worksheet['!merges'] = [
                { s: { c: 0, r: 0 }, e: { c: wscols.length - 1, r: 0 } },
                { s: { c: 0, r: 1 }, e: { c: wscols.length - 1, r: 1 } },
              ];
              worksheet['!cols'] = wscols;

              const workbook = {
                Sheets: { data: worksheet },
                SheetNames: ['data'],
              };
              const excelBuffer: any = XLSX.write(workbook, {
                bookType: 'xlsx',
                type: 'array',
              });
              this.saveAsExcelFile(excelBuffer, 'reporting');
              /* save to file */
              // XLSX.writeFile(excelBuffer, "products");
            } else {
              if (result.data.status == 'F') {
                this.messageService.add({
                  severity: 'warn',
                  summary: 'Failed',
                  detail: 'Data not found',
                });
              } else if (result.data.status == 'E') {
                this.messageService.add({
                  severity: 'error',
                  summary: 'Error',
                  detail: result.data.statusMessage,
                });
              }
            }
            this.isFetching = false;
          });
      }
    } else {
      this.messageService.add({
        severity: 'warn',
        summary: 'Warning',
        detail: 'Please choose Date Range of Transaction Data',
      });
      this.isFetching = false;
    }
  }

  create_gap_rows(ws, nrows) {
    var ref = XLSX.utils.decode_range(ws['!ref']); // get original range
    ref.e.r += nrows; // add to ending row
    ws['!ref'] = XLSX.utils.encode_range(ref); // reassign row
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
    );
  }

  exportPdf() {
    var doc = new jsPDF('l', 'mm', [297, 210]);

    //CREATE TABLE
    var col = [
      this.selectedColumns.map((col) => ({
        title: col.header,
        styles: { cellWidth: 20 },
      })),
    ];
    var rows = [];
    this.rptData.forEach((element) => {
      var temp = [
        element.bifastId,
        element.retrievalRefNumber,
        element.trxType,
        element.senderBank,
        element.senderAccountName,
        element.senderAccountNo,
        element.recipientBank,
        element.recipientAccountName,
        element.recipientAccountNo,
        element.trxAmount,
        element.categoryPurpose,
        element.requestDate,
        element.responseDate,
        element.paymentInformation,
        element.trxStatusCode,
        element.trxStatusMessage,
      ];
      rows.push(temp);
    });

    autoTable(doc, {
      // startY: 30,
      head: col,
      body: rows,
    });

    //SET TITLE PDF
    // doc.setFontSize(24);
    // doc.text("Reporting Transaction", 100, 20);

    //FILENAME
    doc.save('test.pdf');
  }

  // ngOnInit(): void {
  //    this.home = {label: 'Dashboard', routerLink: '/mgm/home' };
  //   this.breadcrumbs = [{ label: 'Transaction Report' }];
  //   // this.channels=[{label:"ATM", value:1},{label:"Mobile Banking", value:2},{label:"Web Banking", value:3}];
  //   // this.transactiontype = [
  //   //   { label: 'Incoming Report', value: 1 },
  //   //   { label: 'Outgoing Report', value: 2 },
  //   // ];
  //   this.transactiontype = [
  //     { label: 'Summary Data Incoming', value: 1 },
  //     { label: 'Summary Data Outgoing', value: 2 },
  //     //{ label: 'Summary Data Branch', value: 3 },
  //     { label: 'Summary Data Fee', value: 3 },
  //   ];
  //   // this.transactiontype=[{label:"Incoming Transaction", value:1},{label:"Outgoing Transaction", value:2}];
  //   this.rptData = [];
  //   this.authservice.whoAmi().subscribe((value) => {
  //     // console.log(">>> User Info : "+JSON.stringify(value));
  //     this.userInfo = value.data;
  //     this.tokenID = value.tokenId;
  //   });
  //   this.initialData();
  // }
  // initialData() {
  //   this.rptData = [];
  //   this.isFetching = true;
  //   this.trxreportService
  //     .getAllReport()
  //     .subscribe((result: BackendResponse) => {
  //       console.log('>>>>>>> ' + JSON.stringify(result));

  //       if (result.status === 202) {
  //         // id, rpttype, change_who, status, description, pathfile, created_date, idtenant
  //         let objtmp = {
  //           created_date: 'No record',
  //           rpttype: 'No record',
  //           change_who: 'No record',
  //           status: 'No record',
  //         };
  //         this.rptData.push(objtmp);
  //       } else {
  //         this.rptData = result.data;
  //       }

  //       this.isFetching = false;
  //     });
  // }
  // refreshData() {
  //   this.rptData = [];
  //   this.isFetching = true;
  //   console.log('Range date : ' + this.rangeDates);
  //   let start_date = null;
  //   let end_date = null;
  //   let payload: any = {};
  //   if (this.rangeDates != undefined) {
  //     start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
  //     end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
  //     if (start_date) payload.start_date = start_date;
  //     if (end_date) payload.end_date = end_date;
  //     if (this.transactiontypeSelected) {
  //       payload.trxtype = this.transactiontypeSelected.value;
  //     } else {
  //       console.log('>>>>>>>>>>>>>>> KOSONGAN');
  //       payload.trxtype = 0;
  //     }
  //     if (this.description) payload.desc = this.description;
  //     console.log(JSON.stringify(payload));
  //     this.isFetching = true;
  //     this.trxreportService
  //       .generatReport(payload)
  //       .subscribe((response: BackendResponse) => {
  //         console.log(JSON.stringify(response));
  //         if (response.status == 200) {
  //           this.initialData();
  //           this.showTopSuccess(
  //             'Generating report, please wait or click refresh to check the report status'
  //           );
  //         }
  //         // this.isFetching=false;
  //       });
  //   } else {
  //     this.showTopError('Please fill the range date to generate data');
  //   }
  // }

  // async countAmount(dt) {
  //   let amountTot = 0;
  //   console.log(dt);
  //   let amountTotal = await dt[0].datacol.map((dt) => {
  //     console.log(dt);
  //     amountTot = amountTot + dt.amount;
  //   });
  //   console.log(amountTot);
  //   return amountTot;
  // }

  // async countFee(dt) {
  //   let amountTot = 0;
  //   console.log(dt);
  //   let amountTotal = await dt[0].datacol.map((dt) => {
  //     console.log(dt);
  //     amountTot = amountTot + dt.feeamount;
  //   });
  //   console.log(amountTot);
  //   return amountTot;
  // }

  // downloadXLS(payload, dt) {
  //   console.log(dt);
  //   console.log(payload);
  //   this.trxreportService
  //     .downloadReport(payload)
  //     .subscribe(async (response: BackendResponse) => {
  //       console.log(response);
  //       if (response.status == 200) {
  //         // let workbook = new Workbook();
  //         // let worksheet = workbook.addWorksheet('Report Transaction');
  //         // {"status":200,"data":[{"headercol":{"colhead":["Status","Trx Number","Reff BiFast","channel","destbank","srcbank","destaccnum","srcaccnum","destaccname","srcaccname","created_date","change_who","amount","transacttype","idtenant"]},"datacol":[{"id":"2","status":"ACTC","trxnumber":"2233443334","refnumbifast":"992233443334","channel":"ATM","destbank":"564","srcbank":"008","destaccnum":"3524534533454","srcaccnum":"799889998999","destaccname":"Peter Pan","srcaccname":"Rita Gunato","created_date":"2021-09-25 10:09:39","change_who":"SYS","last_updated_date":null,"amount":500000000,"transacttype":"Incoming","idtenant":10},{"id":"1","status":"RJCT","trxnumber":"2233443333","refnumbifast":"992233443333","channel":"Mobile Banking","destbank":"014","srcbank":"564","destaccnum":"899889998999","srcaccnum":"3524534533453","destaccname":"Udin Petor","srcaccname":"Peter Pan","created_date":"2021-09-25 10:09:14","change_who":"SYS","last_updated_date":null,"amount":100000000,"transacttype":"Outgoing","idtenant":10}],"created_date":"2021-09-28T05:38:17.662Z","srcpayload":{"start_date":"2021-09-23","end_date":"2021-09-30","trxtype":0,"desc":"ALL 1","iddata":"40","idtenant":"10"},"id":"2","filename":"20210928123817"}]}
  //         if (response.data.length > 0) {
  //           let allrecord = response.data[0];
  //           let workbook = new Workbook();
  //           let worksheet = workbook.addWorksheet('Report Transaction');
  //           worksheet.mergeCells('C1', 'F4');
  //           let titleRow = worksheet.getCell('C1');
  //           titleRow.value = 'Komi trafic Report';
  //           titleRow.font = {
  //             name: 'Calibri',
  //             size: 14,
  //             underline: 'single',
  //             bold: true,
  //             color: { argb: '0085A3' },
  //           };
  //           titleRow.alignment = { vertical: 'middle', horizontal: 'center' };
  //           worksheet.addRow([]);

  //           let totalTransaction = worksheet.getCell('I2');
  //           totalTransaction.value = 'Total Transaction';
  //           totalTransaction.font = {
  //             name: 'Calibri',
  //             size: 11,
  //             //underline: 'single',
  //             //bold: true,
  //             //color: { argb: '0085A3' },
  //           };
  //           totalTransaction.alignment = {
  //             vertical: 'middle',
  //             horizontal: 'center',
  //           };
  //           worksheet.addRow([]);

  //           let totalTransactionVal = worksheet.getCell('J2');
  //           totalTransactionVal.value = response.data.datacol.length + 1;
  //           totalTransactionVal.font = {
  //             name: 'Calibri',
  //             size: 11,
  //             //underline: 'single',
  //             //bold: true,
  //             //color: { argb: '0085A3' },
  //           };
  //           totalTransactionVal.alignment = {
  //             vertical: 'middle',
  //             horizontal: 'center',
  //           };
  //           worksheet.addRow([]);

  //           let totalAmount = worksheet.getCell('I3');
  //           totalAmount.value = 'Total Amount';
  //           totalAmount.font = {
  //             name: 'Calibri',
  //             size: 11,
  //             //underline: 'single',
  //             //bold: true,
  //             //color: { argb: '0085A3' },
  //           };
  //           totalAmount.alignment = {
  //             vertical: 'middle',
  //             horizontal: 'center',
  //           };
  //           worksheet.addRow([]);

  //           let totalAmountVal = worksheet.getCell('J3');
  //           totalAmountVal.value = await this.countAmount(response.data);
  //           totalAmountVal.font = {
  //             name: 'Calibri',
  //             size: 11,
  //             //underline: 'single',
  //             //bold: true,
  //             //color: { argb: '0085A3' },
  //           };
  //           totalAmountVal.alignment = {
  //             vertical: 'middle',
  //             horizontal: 'center',
  //           };
  //           worksheet.addRow([]);

  //           if (dt.rpttype == 3) {
  //             let totalFee = worksheet.getCell('I4');
  //             totalFee.value = 'Total Fee';
  //             totalFee.font = {
  //               name: 'Calibri',
  //               size: 11,
  //               //underline: 'single',
  //               //bold: true,
  //               //color: { argb: '0085A3' },
  //             };
  //             totalFee.alignment = {
  //               vertical: 'middle',
  //               horizontal: 'center',
  //             };
  //             worksheet.addRow([]);

  //             let totalFeeVal = worksheet.getCell('J4');
  //             totalFeeVal.value = await this.countFee(response.data);
  //             totalFeeVal.font = {
  //               name: 'Calibri',
  //               size: 11,
  //               //underline: 'single',
  //               //bold: true,
  //               //color: { argb: '0085A3' },
  //             };
  //             totalFeeVal.alignment = {
  //               vertical: 'middle',
  //               horizontal: 'center',
  //             };
  //             worksheet.addRow([]);
  //           }

  //           //Adding Header Row
  //           // console.log("Header "+JSON.stringify(allrecord.headercol.colhead));
  //           let headerRow = worksheet.addRow(allrecord.headercol.colhead);
  //           headerRow.eachCell((cell, number) => {
  //             cell.fill = {
  //               type: 'pattern',
  //               pattern: 'solid',
  //               fgColor: { argb: '4167B8' },
  //               bgColor: { argb: '' },
  //             };
  //             cell.font = {
  //               bold: true,
  //               color: { argb: 'FFFFFF' },
  //               size: 12,
  //             };
  //           });
  //           let datarows = allrecord.datacol;
  //           console.log('Data ' + JSON.stringify(allrecord.datacol));
  //           // ["Status","Trx Number","Reff BiFast","channel","destbank","srcbank","destaccnum","srcaccnum","destaccname","srcaccname","created_date","change_who","amount","transacttype","idtenant"]

  //           // Adding Data with Conditional Formatting
  //           // {"id":"2","status":"ACTC","trxnumber":"2233443334","refnumbifast":"992233443334","channel":"ATM","destbank":"564","srcbank":"008","destaccnum":"3524534533454","srcaccnum":"799889998999","destaccname":"Peter Pan","srcaccname":"Rita Gunato","created_date":"2021-09-25 10:09:39","change_who":"SYS","last_updated_date":null,"amount":500000000,"transacttype":"Incoming","idtenant":10}
  //           datarows.forEach((d) => {
  //             let datobj = [
  //               d.status,
  //               d.trxnumber,
  //               d.refnumbifast,
  //               d.channel,
  //               d.destbank,
  //               d.srcbank,
  //               d.destaccnum,
  //               d.srcaccnum,
  //               d.destaccname,
  //               d.srcaccname,
  //               d.created_date,
  //               d.change_who,
  //               d.amount,
  //               d.transacttype,
  //               d.idtenant,
  //             ];
  //             let row = worksheet.addRow(datobj);
  //           });
  //           //Generate & Save Excel File
  //           let myDate = new Date();
  //           let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
  //           let fileName = 'xl' + datenow + '.xlsx';

  //           workbook.xlsx.writeBuffer().then((data) => {
  //             let blob = new Blob([data], {
  //               type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  //             });
  //             fs.saveAs(blob, fileName);
  //           });
  //         } else {
  //           this.showTopError('No data on the report');
  //         }
  //       }
  //     });
  // }

  // async downloadPDF(payload) {
  //   console.log(payload);
  //   await this.trxreportService
  //     .downloadReport(payload)
  //     .subscribe(async (response: BackendResponse) => {
  //       // console.log(JSON.stringify(response));
  //       if (response.status == 200) {
  //         if (response.data.length > 0) {
  //           let totalAmount = await this.countAmount(response.data);
  //           let totalTransaction = response.data[0].datacol.length;
  //           let allrecord = response.data[0];
  //           var doc = new jsPDF('l', 'mm', [297, 210]);
  //           doc.text(`Total Transaction: ${totalTransaction}`, 10, 20);
  //           doc.text(`Total Amount: ${totalAmount}`, 10, 30);
  //           var col = [
  //             [
  //               'Trx Date',
  //               'Status',
  //               'TrxNum',
  //               'Reff BiFAST',
  //               'Channel',
  //               'Dest Bank',
  //               'Src Bank',
  //               'Dest AccNum',
  //               'Src AccNum',
  //               'Dest Acc Name',
  //               'Src Acc Name',
  //               'Amount',
  //             ],
  //           ];
  //           var rows = [];
  //           let myDate = new Date();
  //           let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
  //           let fileName = 'pdf' + datenow + '.pdf';
  //           let datarows = allrecord.datacol;
  //           datarows.forEach((element) => {
  //             var temp = [
  //               element.created_date,
  //               element.status,
  //               element.trxnumber,
  //               element.refnumbifast,
  //               element.channel,
  //               element.destbank,
  //               element.srcbank,
  //               element.destaccnum,
  //               element.srcaccnum,
  //               element.destaccname,
  //               element.srcaccname,
  //               element.amount,
  //             ];
  //             rows.push(temp);
  //           });
  //           autoTable(doc, {
  //             head: col,
  //             body: rows,
  //             didDrawCell: (rows) => {},
  //           });
  //           doc.save(fileName);
  //         } else {
  //           this.showTopError('No data on the report');
  //         }
  //       }
  //     });
  // }

  // showTopSuccess(message: string) {
  //   this.messageService.add({
  //     severity: 'success',
  //     summary: 'Generated Report',
  //     detail: message,
  //   });
  // }
  // showTopError(message: string) {
  //   this.messageService.add({
  //     severity: 'error',
  //     summary: 'Error',
  //     detail: message,
  //   });
  // }
}
