import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { Location } from '@angular/common';
import { OrganizationService } from 'src/app/services/root/organization.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { FilterService } from 'primeng/api';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { ForgotpasswordService } from 'src/app/services/forgotpassword/forgotpassword.service';

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.scss'],
})
export class UserdetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  stateOptionsEdit: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  submitted = false;
  orgsData: any[] = [];
  appInfoActive: any = {};
  orgSuggest: any = {};
  user: any = {};
  formatedOrg: any[] = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private umService: UsermanagerService,
    private authservice: AuthService,
    private groupService: GroupServiceService,
    private filterService: FilterService,
    private messageService: MessageService,
    private location: Location,
    private verifikasiService: ForgotpasswordService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Users Management',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.stateOptions = [
      { label: 'Direct activated', value: 1 },
      { label: 'Activision link', value: 0 },
    ];
    this.stateOptionsEdit = [
      { label: 'Active', value: 1 },
      { label: 'Deactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        username: ['', Validators.required],
        emailname: ['', Validators.required],
        fullname: ['', Validators.required],
        orgMlt: [[], Validators.required],
        temppass: ['', Validators.required],
        isactive: [0],
      });
      this.groupService
        .getAllGroupForData()
        .subscribe((grpall: BackendResponse) => {
          this.orgsData = grpall.data.userGroup;
          // console.log(JSON.stringify(this.orgsData));
          this.formatOrgData(this.orgsData);
        });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.umService
            .retriveUsersById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              // console.log('Data edit user ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.user.username = result.data.userid;
              this.user.fullname = result.data.fullname;
              this.user.emailname = result.data.bioemailactive;
              this.user.org = result.data.id;
              this.user.password = result.data.pwd;
              this.user.active = result.data.active;
              this.groupForm.patchValue({
                username: this.user.username,
                fullname: this.user.fullname,
                emailname: this.user.emailname,
                //groupobj: this.user.group,
                //orgobj: this.user.org,
                orgMlt: result.data.group,
                temppass: this.user.password,
                isactive: this.user.active,
              });
              this.groupForm.controls['username'].disable();
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  formatOrgData(data: any) {
    data.map((dt: any) => {
      let formated: any = {};
      formated.name = dt.groupname;
      formated.id = dt.id;
      this.formatedOrg.push(formated);
    });
  }

  filterOrg(event: any) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.formatedOrg.length; i++) {
      let country = this.formatedOrg[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
        filtered.push(country);
      }
    }

    this.orgSuggest = filtered;
  }

  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      let payload2 = {};

      if (!this.isEdit) {
        payload = {
          fullname: this.groupForm.get('fullname')?.value,
          userid: this.groupForm.get('username')?.value,
          password: this.groupForm.get('temppass')?.value,
          group: this.groupForm.get('orgMlt')?.value,
          isactive: this.groupForm.get('isactive')?.value,
          emailname: this.groupForm.get('emailname')?.value,
        };
        payload2 = {
          email: this.groupForm.value['emailname'],
          appname: 'recs',
        };
        // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.umService.insertByAdmin(payload).subscribe(
          (result: BackendResponse) => {
            if (result.status === 200) {
              // this.verifikasiService.verifikasi(payload2).subscribe(
              //   (result: BackendResponse) => {
              //     if (result.status === 200) {
              this.location.back();
              //     }
              //   },
              //   (err) => {
              //     console.log(err);
              //     this.showTopCenterErr(err.error.data);
              //   }
              // );
            }
          },
          (err) => {
            console.log(err);
            this.showTopCenterErr(err.error.data);
          }
        );
      } else {
        payload = {
          fullname: this.groupForm.get('fullname')?.value,
          userid: this.userId,
          group: this.groupForm.get('orgMlt')?.value,
          isactive: this.groupForm.get('isactive')?.value,
          emailname: this.groupForm.get('emailname')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.umService
          .updatebyAdmin(payload)
          .subscribe((result: BackendResponse) => {
            // console.log(">>>>>>>> return "+JSON.stringify(result));
            if (result.status === 200) {
              this.location.back();
            }
          });
      }
    }

    // console.log(this.groupForm.valid);
  }
  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
}
