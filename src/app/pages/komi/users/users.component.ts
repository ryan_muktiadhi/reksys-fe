import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';
import { ForgotpasswordService } from 'src/app/services/forgotpassword/forgotpassword.service';
import { ApprovalService } from 'src/app/services/approval.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  display = false;
  scrollheight: any = '400px';
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  newData: any;
  oldData: any;
  usrheader: any = [
    { label: 'Name', sort: 'fullname' },
    { label: 'User ID', sort: 'userid' },
    { label: 'Status', sort: 'active' },
    { label: 'Group Menu', sort: 'groupname' },
    { label: 'Attempt', sort: 'total_attempt' },
    { label: 'Created At', sort: 'created_at' },
  ];
  usrcolname: any = [
    'fullname',
    'userid',
    'active',
    'groupname',
    'total_attempt',
    'created_at',
  ];
  usrcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  // usrcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  usrcolwidth: any = ['', '', '', '', '', ''];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [1, 1, 1, 1, 1, 1];
  usraddbtn = { route: 'detail', label: 'Add Data' };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private userService: UsermanagerService,
    private aclMenuService: AclmenucheckerService,
    private verifikasiService: ForgotpasswordService,
    private approvalService: ApprovalService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Users Management' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then((data) => {
        console.log('MENU ALL ACL Set');
        this.aclMenuService.getAclMenu(this.router.url).then((dataacl) => {
          if (JSON.stringify(dataacl.acl) === '{}') {
            console.log('No ACL Founded');
          } else {
            console.log('ACL Founded');
            // console.log(dataacl.acl);
            this.usractionbtn[0] = dataacl.acl.create;
            this.usractionbtn[1] = dataacl.acl.read;
            this.usractionbtn[2] = dataacl.acl.update;
            this.usractionbtn[3] = dataacl.acl.delete;
            this.usractionbtn[4] = dataacl.acl.view;
            this.usractionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(data));
      if ((data.status = 200)) {
        this.userService.retriveUsers().subscribe((orgall: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(orgall));
          this.userlist = orgall.data;
          if (this.userlist.length < 1) {
            let objtmp = {
              fullname: 'No records',
              userid: 'No records',
              active: 'No records',
              groupname: 'No records',
            };
            this.userlist = [];
            this.userlist.push(objtmp);
          }
          this.isFetching = false;
        });
      }
    });
  }
  deleteConfirmation(data: any) {
    // console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedUser = data;
  }

  viewData(data: any) {
    // console.log(data);
    this.viewDisplay = true;
    this.selectedUser = data;
  }

  async approvalData(data: any) {
    // console.log(data);
    this.viewApprove = true;
    this.selectedUser = data;
    if (data.idapproval && data.active != 9) {
      console.log('new data');
      this.getApprovalData(data.idapproval);
    } else {
      let oldData = await this.setApprovalForm(this.selectedUser);
      oldData = await this.filterForm(oldData);
      this.oldData = oldData;
      this.newData = null;
    }
  }

  async filterForm(formData) {
    let filter = ['fullname', 'email', 'group', 'status'];
    let filterFormData = formData.filter((formItem) =>
      filter.includes(formItem.label)
    );

    return filterFormData;
  }

  async setApprovalForm(data) {
    const keys = Object.keys(data);
    const input = data;
    let arrData = [];

    if (keys.length > 0) {
      await keys.map(async (key: any) => {
        let data: any = {};
        key === 'active' || key === 'isactive'
          ? (data.label = 'status')
          : key === 'total_attempt'
          ? (data.label = 'total attempt')
          : key === 'bioemailactive' || key === 'emailname'
          ? (data.label = 'email')
          : key === 'groupname'
          ? (data.label = 'group')
          : (data.label = key);

        if (Array.isArray(input[key])) {
          let value = [];
          await input[key].map((dt) => {
            value.push(dt.name);
          });
          //          console.log(value);
          data.value = value;
        } else if (data.label === 'status') {
          data.value =
            input[key] == 0
              ? 'Not Active'
              : input[key] == 1
              ? 'Active'
              : 'Waiting for Approval';
          data.statusCode = input[key];
        } else {
          data.value = input[key];
        }

        if (data.label != 'id' && data.label != 'userid') {
          arrData.push(data);
        }
      });
      return arrData;
    }
  }
  getApprovalData(id) {
    this.approvalService
      .getByid(id)
      .subscribe(async (resp: BackendResponse) => {
        let oldData: any = [];
        oldData.push(this.selectedUser);
        let oldFormData = await this.setApprovalForm(this.selectedUser);
        let newData = await this.setApprovalForm(
          JSON.parse(resp.data[0].jsondata)
        );
        this.oldData = oldFormData;
        console.log(this.oldData);
        console.log('>>>>> old data nih');
        this.newData = newData;
        console.log(this.newData);
        console.log('>>>>> old data nih');
      });
  }

  reject() {
    let payload = {
      id: this.selectedUser.id,
      oldactive: this.selectedUser.active,
      isactive: 4,
      idapproval: this.selectedUser.idapproval,
    };
    let payload2 = {
      email: this.selectedUser.bioemailactive,
    };
    this.userService.reject(payload).subscribe((result: BackendResponse) => {
      // if (result.status === 200) {
      //   this.verifikasiService
      //     .verifikasi(payload2)
      //     .subscribe((result: BackendResponse) => {
      this.refreshingUser();
      this.viewApprove = false;
      //     });
      // }
    });
  }

  approvalSubmit(status) {
    // console.log('id user::', this.selectedUser.id);
    // console.log(status);
    let payload = {
      id: this.selectedUser.id,
      oldactive: this.selectedUser.active,
      isactive: status,
      idapproval: this.selectedUser.idapproval,
    };

    this.userService
      .retriveUsersById(this.selectedUser.id)
      .subscribe((orgall: BackendResponse) => {
        console.log('retriveusersbyid ' + JSON.stringify(orgall));
        let statUserActive = orgall.data.active;
        let payload2 = {
          email: orgall.data.bioemailactive,
          appname: 'recs',
        };

        if (statUserActive === 4) {
          this.userService
            .updatebyAdminActive(payload)
            .subscribe((result: BackendResponse) => {
              // console.log(">>>>>>>> return "+JSON.stringify(result));]
              if (result.status === 200) {
                // console.log('kirim verifikasi email >>>');
                this.verifikasiService
                  .verifikasi(payload2)
                  .subscribe((result: BackendResponse) => {
                    this.refreshingUser();
                    this.viewApprove = false;
                  });
              }
            });
        } else {
          this.userService
            .updatebyAdminActive(payload)
            .subscribe((result: BackendResponse) => {
              // console.log(">>>>>>>> return "+JSON.stringify(result));
              if (result.status === 200) {
                this.refreshingUser();
                this.viewApprove = false;
              }
            });
        }
      });

    // this.userService
    //   .updatebyAdminActive(payload)
    //   .subscribe((result: BackendResponse) => {
    //     // console.log(">>>>>>>> return "+JSON.stringify(result));
    //     if (result.status === 200) {
    //       this.refreshingUser();
    //       this.viewApprove = false;
    //     }
    //   });
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
  }

  deleteUser() {
    // console.log(this.selectedUser);
    let user = this.selectedUser;
    const payload = { user };
    this.userService
      .deleteUserByAdmin(payload)
      .subscribe((resp: BackendResponse) => {
        // console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingUser();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
