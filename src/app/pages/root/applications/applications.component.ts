import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss'],
})
export class ApplicationsComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  constructor() {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/' };
    this.breadcrumbs = [{ label: 'Applications Management' }];
  }
}
