import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OauthsettingsComponent } from './oauthsettings.component';

describe('OauthsettingsComponent', () => {
  let component: OauthsettingsComponent;
  let fixture: ComponentFixture<OauthsettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OauthsettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OauthsettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
