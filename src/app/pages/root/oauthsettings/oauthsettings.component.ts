import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-oauthsettings',
  templateUrl: './oauthsettings.component.html',
  styleUrls: ['./oauthsettings.component.scss'],
})
export class OauthsettingsComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  constructor() {}

  ngOnInit(): void {
    this.home = { label: 'Dashboard', routerLink: '/' };
    this.breadcrumbs = [{ label: 'LDAP Management' }];
  }
}
