import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';

import { AuthService } from 'src/app/services/auth.service';
import { BackendService } from 'src/app/services/backend.service';

//import { ProfileConfigComponent } from '../profile-config/profile-config.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';

interface Profile {
  fullname?: string;
  userid?: string;
  tenant?: string;
  level?: string;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  ref: DynamicDialogRef = new DynamicDialogRef();
  profile: Profile = {};
  bcitems: MenuItem[] = [];
  home: MenuItem = {};

  constructor(
    private authservice: AuthService,
    // private usermanager: UsermanagerService,
    private sessionStorage: SessionStorageService,
    private service: BackendService,
    public dialogService: DialogService,
    private route: Router,
    public messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.refreshData();
    this.bcitems = [{ label: 'Profile' }];
    this.home = { label: 'Dashboard', routerLink: '/mgm/home' };
  }

  refreshData(): void {
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      if ((data.status = 200)) {
        this.profile.fullname = data.data.fullname;
        this.profile.userid = data.data.userid;
        this.profile.tenant = data.data.tnname;
        this.profile.level = data.data.leveltenant;
      }
    });
  }

  changepassword() {
    this.ref = this.dialogService.open(ChangePasswordComponent, {
      header: 'Change Password',
      baseZIndex: 10000,
      dismissableMask: false,
    });
    this.ref.onClose.subscribe((value: any) => {
      if (value === 200) {
        this.refreshData();
        this.showTopSuccess('Password Changed Successfull');
      } else {
        this.showTopError('Failed Change Password');
      }
    });
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Success',
      detail: message,
    });
  }

  showTopInfo(message: string) {
    this.messageService.add({
      severity: 'info',
      summary: 'Info',
      detail: message,
    });
  }

  showTopWarning(message: string) {
    this.messageService.add({
      severity: 'warn',
      summary: 'Warning',
      detail: message,
    });
  }

  showTopError(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
}
