import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmtpaccountsComponent } from './smtpaccounts.component';

describe('SmtpaccountsComponent', () => {
  let component: SmtpaccountsComponent;
  let fixture: ComponentFixture<SmtpaccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmtpaccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmtpaccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
