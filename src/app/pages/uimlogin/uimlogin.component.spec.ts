import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UimloginComponent } from './uimlogin.component';

describe('UimloginComponent', () => {
  let component: UimloginComponent;
  let fixture: ComponentFixture<UimloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UimloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UimloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
