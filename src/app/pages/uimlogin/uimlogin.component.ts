import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { BackendService } from 'src/app/services/backend.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { RecaptchaErrorParameters } from 'ng-recaptcha';
import { EnvService } from '../../env/env.service';

@Component({
  selector: 'app-uimlogin',
  templateUrl: './uimlogin.component.html',
  styleUrls: ['./uimlogin.component.scss']
})
export class UimloginComponent implements OnInit {
  blockedDocument: boolean = false;
  //  userid = "";
  captchaSiteKey: string = '6LcvgrQcAAAAAB_tLF7BUtS1p2xY2sI8tvNxJW94';
  secret = '';
  errorMsg = '';
  isProcess: boolean = false;
  userForm!: FormGroup;
  submitted = false;
  constructor(
    private messageService: MessageService,
    private sessionStorage: SessionStorageService,
    private route: Router,
    private formBuilder: FormBuilder,
    private backend: BackendService,
    private authservice: AuthService,
    private environment: EnvService,
  ) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      userid: ['', Validators.required],
      secret: ['', Validators.required],
      // recaptcha: ['', Validators.required],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.userForm.controls;
  }
  get userFormControl() {
    return this.userForm.controls;
  }

  public resolved(captchaResponse: string): void {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  public onError(errorDetails: RecaptchaErrorParameters): void {
    console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  }
  onSubmit() {
    this.submitted = true;
    const tenantName = "BJB";//this.environment.tenantName;

    if (this.userForm.valid) {
      this.isProcess = true;
      this.backend
        .post(
          'adm/auth/signuimviaadmin',
          {
            credential: this.userForm.value['userid'],
            secret: this.userForm.value['secret'],
            appname: 'recs',
            tenantName: tenantName,
          },
          false
        )
        .subscribe(
          (data: BackendResponse) => {
            // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
            if (data.status === 200) {
              const authToken = data.data;
              this.sessionStorage.set('accesstoken', authToken);
              this.authservice.setAuthStatus(true);
              this.authservice.setToken(authToken);
              this.route.navigate(['mgm/home']);
              this.isProcess = false;
            } else {
              this.sessionStorage.clear();
              console.log(data);
              this.showTopCenterErr(
                `${data.data ? data.data : 'Invalid username or password'}`
              );
              this.authservice.setAuthStatus(false);
              this.isProcess = false;
            }
          },
          (error) => {
            this.showTopCenterErr(
              `${error.error.data ? error.error.data : 'Invalid username or password'}`
            );
            // this.showTopCenterErr('Invalid user and password');
            this.isProcess = false;
            this.authservice.setAuthStatus(false);
          }
        );
    }

    //  this.blockDocument();
  }
  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }

  blockDocument() {
    this.blockedDocument = true;
    //  setTimeout(() => {
    //      this.blockedDocument = false;
    //      this.showTopCenterErr("Invalid user and password!")
    //  }, 3000);
  }

}
