import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root',
})
export class ApprovalService {
  constructor(private service: BackendService) {}

  getByid(payload: any) {
    const url = 'adm/approval/getapprovalbyid/' + payload;
    return this.service.get(url);
  }
}
