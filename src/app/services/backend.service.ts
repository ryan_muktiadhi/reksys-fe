import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
// import { environment } from '@environtment';
import { catchError } from 'rxjs/operators';
import { EnvService } from '../env/env.service';
@Injectable({
  providedIn: 'root',
})
export class BackendService {
  constructor(private httpClient: HttpClient, private environment: EnvService) {}
  post(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  get(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    console.log("######################################### WALAH ", url);
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  put(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  patch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  delete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.apiUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }

  basePost(path: string, payload: any, authorized?: boolean): Observable<any> {
    const url = this.environment.baseUrl + path;
    return this.httpClient
      .post(url, payload)
      .pipe(catchError(this.handleError));
  }
  baseGet(path: string, authorized?: boolean): Observable<any> {
    const url = this.environment.baseUrl + path;
    return this.httpClient.get(url).pipe(catchError(this.handleError));
  }

  basePut(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.baseUrl + path;
    return this.httpClient.put(url, payload).pipe(catchError(this.handleError));
  }

  basePatch(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.baseUrl + path;
    return this.httpClient
      .patch(url, payload)
      .pipe(catchError(this.handleError));
  }

  baseDelete(path: string, payload?: any, authorized?: boolean): Observable<any> {
    const url = this.environment.baseUrl + path;
    return this.httpClient
      .delete(url, payload)
      .pipe(catchError(this.handleError));
  }


  // tslint:disable-next-line:typedef
  handleError(error: HttpErrorResponse) {
    console.log('error occured ', error);
    return throwError(error);
  }
}
