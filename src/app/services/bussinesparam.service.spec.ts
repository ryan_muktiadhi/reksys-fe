import { TestBed } from '@angular/core/testing';

import { BussinesparamService } from './bussinesparam.service';

describe('BussinesparamService', () => {
  let service: BussinesparamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BussinesparamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
