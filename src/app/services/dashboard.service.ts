import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private service: BackendService) {}
  getTPS(payload: any) {
    const url = `komi/dashboard/getTPS?startDate=${payload.startDate}&endDate=${payload.endDate}`;
    //const url =
    //  'komi/dashboard/getTPS?startDate=2021-11-19 00:00:00&endDate=2021-11-19 23:59:59';
    return this.service.baseGet(url);
  }
  getDaily(payload: any) {
    const url = `komi/dashboard/getDaily?startDate=${payload.startDate}&endDate=${payload.endDate}`;
    //const url = `komi/dashboard/getDaily?startDate=2021-12-06 11:01:45.015118&endDate=2021-12-06 15:01:45.015118`;
    //const url = `komi/dashboard/getDaily?startDate=2021-11-19 00:00:00&endDate=2021-11-19 23:59:59`;
    return this.service.baseGet(url);
  }
  getWeekly(payload: any) {
    const url = `komi/dashboard/getWeekly?startDate=${payload.startDate}&endDate=${payload.endDate}`;
    //const url = `komi/dashboard/getWeekly?startDate=2021-11-13 00:00:00&endDate=2021-11-19 23:59:59`;
    return this.service.baseGet(url);
  }
  getMonthly(payload: any) {
    const url = `komi/dashboard/getMonthly?startDate=${payload.startDate}&endDate=${payload.endDate}`;
    //const url = `komi/dashboard/getMonthly?startDate=2021-10-19 00:00:00&endDate=2021-11-19 00:00:00`;
    return this.service.baseGet(url);
  }

  getBusiness(payload: any) {
    //const url = `komi/dashboard/getDailyBusiness?startDate=${payload.startDate}&endDate=${payload.endDate}`;
    const url = `komi/dashboard/getDailyBusiness?startDate=2021-11-19 00:00:00&endDate=2021-11-19 23:59:59`;
    return this.service.baseGet(url);
  }
}
