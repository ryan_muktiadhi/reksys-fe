import { TestBed } from '@angular/core/testing';

import { IntegrateNotifService } from './integrate-notif.service';

describe('IntegrateNotifService', () => {
  let service: IntegrateNotifService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IntegrateNotifService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
