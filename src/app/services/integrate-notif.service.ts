import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root',
})
export class IntegrateNotifService {
  constructor(private service: BackendService) {}

  emailnotification(payload: any) {
    const url = 'komi/integrate/emailnotification';
    return this.service.basePost(url, payload);
  }
}
