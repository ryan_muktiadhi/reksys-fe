import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class AccounttypeService {
  constructor(private service: BackendService) {}
  getAll() {
    const url = 'komi/accounttype/getAll';
    return this.service.baseGet(url);
  }
  getById(payload: any) {
    const url = 'komi/accounttype/getById/' + payload;
    return this.service.baseGet(url);
  }
  insert(payload: any) {
    const url = 'komi/accounttype/insert';
    return this.service.basePost(url, payload);
  }
  update(payload: any) {
    const url = 'komi/accounttype/update';
    return this.service.basePost(url, payload);
  }
  delete(payload: any) {
    const url = 'komi/accounttype/delete/' + payload;
    return this.service.baseGet(url);
  }
}
