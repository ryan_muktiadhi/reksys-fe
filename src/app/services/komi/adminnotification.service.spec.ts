import { TestBed } from '@angular/core/testing';

import { AdminnotificationService } from './adminnotification.service';

describe('AdminnotificationService', () => {
  let service: AdminnotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminnotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
