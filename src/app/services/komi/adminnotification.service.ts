import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class AdminnotificationService {
  constructor(private service: BackendService) {}
  getAllAdminNotification() {
    const url = 'adm/adminnotif/get';
    return this.service.get(url);
  }
  updateApprovalStatus(payload: any) {
    const url = 'adm/adminnotif/updateApprovalStatus';
    return this.service.post(url, payload);
  }
  reject(payload: any) {
    const url = 'adm/adminnotif/reject';
    return this.service.post(url, payload);
  }
  getById(id: any) {
    const url = `adm/adminnotif/getbyId/${id}`;
    return this.service.get(url);
  }
  addData(payload: any) {
    const url = 'adm/adminnotif/addData';
    return this.service.post(url, payload);
  }

  editData(payload: any) {
    const url = 'adm/adminnotif/editData';
    return this.service.post(url, payload);
  }

  deleteData(payload: { iduser: any }) {
    const url = 'adm/adminnotif/deleteData';
    return this.service.post(url, payload);
  }
}
