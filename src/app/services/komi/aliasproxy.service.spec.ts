import { TestBed } from '@angular/core/testing';

import { AliasproxyService } from './aliasproxy.service';

describe('AliasproxyService', () => {
  let service: AliasproxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AliasproxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
