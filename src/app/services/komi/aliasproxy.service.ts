import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class AliasproxyService {

  constructor(private service: BackendService) { }
  getAllAliPrxByTenant() {
    const url = 'komi/AliPrxam/getAllAliPrxam';
    return this.service.baseGet(url);
  }
  getAliPrxById(payload: any) {
    const url = 'komi/AliPrxam/getAliPrxam/'+payload;
    return this.service.baseGet(url);
  }
  insertAliPrxByTenant(payload: any) {
    const url = 'komi/AliPrxam/insertAliPrxam';
    return this.service.basePost(url, payload);
  }
  updateAliPrxByTenant(payload: any) {
    const url = 'komi/AliPrxam/updateAliPrxam';
    return this.service.basePost(url, payload);
  }
  deleteAliPrxByTenant(payload: any) {
    const url = 'komi/AliPrxam/deleteAliPrxam/'+payload;
    return this.service.baseGet(url);
  }

  getAllProxyManagement() {
    const url = 'komi/proxymgmt/getAll';
    return this.service.baseGet(url);
  }

  getByParam(payload: any) {
    const url = 'komi/proxymgmt/getByParam';
    return this.service.basePost(url, payload);
  }

  getByChannel() {
    const url = 'komi/proxymgmt/getByChannel';
    return this.service.baseGet(url);
  }

  getBySecondId() {
    const url = 'komi/proxymgmt/getBySecondId';
    return this.service.baseGet(url);
  }

  getByProxy() {
    const url = 'komi/proxymgmt/getByProxy';
    return this.service.baseGet(url);
  }
}
