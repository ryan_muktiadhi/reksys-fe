import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class BranchService {
  constructor(private service: BackendService) {}
  getAllBranchByTenant() {
    const url = 'komi/branch/getAll';
    return this.service.baseGet(url);
  }
  getAllTByStatusActive() {
    const url = 'komi/branch/getByStatusActive';
    return this.service.baseGet(url);
  }
  getBranchById(payload: any) {
    const url = 'komi/branch/getBranch/' + payload;
    return this.service.baseGet(url);
  }
  insertBranchByTenant(payload: any) {
    const url = 'komi/branch/insert';
    return this.service.basePost(url, payload);
  }
  updateBranchByTenant(payload: any) {
    const url = 'komi/branch/update';
    return this.service.basePost(url, payload);
  }
  deleteBranchByTenant(payload: any) {
    const url = 'komi/branch/delete/' + payload;
    return this.service.baseGet(url);
  }
}
