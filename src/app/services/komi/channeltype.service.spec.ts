import { TestBed } from '@angular/core/testing';

import { ChanneltypeService } from './channeltype.service';

describe('ChanneltypeService', () => {
  let service: ChanneltypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChanneltypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
