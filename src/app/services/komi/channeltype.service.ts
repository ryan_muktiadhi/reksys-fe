import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class ChanneltypeService {
  constructor(private service: BackendService) {}
  getAllChnByTenant() {
    const url = 'komi/channeltype/getAll';
    return this.service.baseGet(url);
  }
  getChnById(payload: any) {
    const url = 'komi/channeltype/getChannelType/' + payload;
    return this.service.baseGet(url);
  }
  insertChnByTenant(payload: any) {
    const url = 'komi/channeltype/insert';
    return this.service.basePost(url, payload);
  }
  updateChnByTenant(payload: any) {
    const url = 'komi/channeltype/update';
    return this.service.basePost(url, payload);
  }
  deleteChnByTenant(payload: any) {
    const url = 'komi/channeltype/delete/' + payload;
    return this.service.baseGet(url);
  }
}
