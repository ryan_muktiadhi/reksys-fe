import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private service: BackendService) { }
  getAllCity() {
    const url = 'komi/city/getAll';
    return this.service.baseGet(url);
  }
  getCityById(payload: any) {
    const url = 'komi/city/getBranch/' + payload;
    return this.service.baseGet(url);
  }
  insertCity(payload: any) {
    const url = 'komi/city/insert';
    return this.service.basePost(url, payload);
  }
  updateCity(payload: any) {
    const url = 'komi/city/update';
    return this.service.basePost(url, payload);
  }
  deleteCity(payload: any) {
    const url = 'komi/city/delete/' + payload;
    return this.service.baseGet(url);
  }
}
