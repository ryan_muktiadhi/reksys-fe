import { TestBed } from '@angular/core/testing';

import { ConnectionchannelService } from './connectionchannel.service';

describe('ConnectionchannelService', () => {
  let service: ConnectionchannelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnectionchannelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
