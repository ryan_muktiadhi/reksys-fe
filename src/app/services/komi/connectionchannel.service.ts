import { Injectable } from '@angular/core';
import {BackendService} from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ConnectionchannelService {

  constructor(private service: BackendService) { }

  getAllConnectionChannel() {
    const url = 'komi/conchannel/getAll';
    return this.service.baseGet(url);
  }

  getConnectionChannelbyId(payload: any) {
    const url = 'komi/conchannel/getById/' + payload;
    return this.service.baseGet(url);
  }

  insertConnectionChannel(payload: any) {
    const url = 'komi/conchannel/insert';
    return this.service.basePost(url, payload);
  }

  updateConnectionChannel(payload: any) {
    const url = 'komi/conchannel/update';
    return this.service.basePost(url, payload);
  }

  deleteConnectionChannel(payload: any) {
    const url = 'komi/conchannel/delete';
    return this.service.basePost(url, payload);
  }

  getConnectionChannelbyParam(payload: any) {
    const url = 'komi/conchannel/getByParam';
    return this.service.basePost(url, payload);
  }

  getConnectionChannelbyTenant() {
    const url = 'komi/conchannel/getChannelId';
    return this.service.baseGet(url);
  }

  approveChannel(payload: any) {
    const url = 'komi/conchannel/approveChannel';
    return this.service.basePost(url, payload);
  }

  rejectChannelTemp(payload: any) {
    const url = 'komi/conchannel/rejectChannelTemp';
    return this.service.basePost(url, payload);
  }


}
