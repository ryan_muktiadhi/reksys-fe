import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class EventlogService {
  constructor(private service: BackendService) {}
  getAll() {
    const url = 'api/log/getAll';
    return this.service.get(url);
  }

  getSystemLog() {
    const url = 'komi/log/getSystemLog';
    return this.service.baseGet(url);
  }

  getInboundLog() {
    const url = 'komi/log/getInBoundlog';
    return this.service.baseGet(url);
  }

  getOutboundLog() {
    const url = 'komi/log/getOutBoundlog';
    return this.service.baseGet(url);
  }

  getWithFilter(payload: any) {
    const url = 'api/log/getWithFilter';
    return this.service.post(url, payload);
  }
}
