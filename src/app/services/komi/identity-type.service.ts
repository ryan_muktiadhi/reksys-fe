import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class IdentityTypeService {
  constructor(private service: BackendService) {}
  getAll() {
    const url = 'komi/idtype/getAll';
    return this.service.baseGet(url);
  }
  getAllTByStatusActive() {
    const url = 'komi/idtype/getByStatusActive';
    return this.service.baseGet(url);
  }
  getSecType() {
    const url = 'komi/idtype/getSecType';
    return this.service.baseGet(url);
  }
  getById(payload: any) {
    const url = 'komi/idtype/getById/' + payload;
    return this.service.baseGet(url);
  }
  insert(payload: any) {
    const url = 'komi/idtype/insert';
    return this.service.basePost(url, payload);
  }
  update(payload: any) {
    const url = 'komi/idtype/update';
    return this.service.basePost(url, payload);
  }
  delete(payload: any) {
    const url = 'komi/idtype/delete/' + payload;
    return this.service.baseGet(url);
  }
}
