import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class LimitService {
  constructor(private service: BackendService) {}
  getAll() {
    const url = 'komi/limit/getAll';
    return this.service.baseGet(url);
  }
  getById(payload: any) {
    const url = 'komi/limit/getById/' + payload;
    return this.service.baseGet(url);
  }
  insert(payload: any) {
    const url = 'komi/limit/insert';
    return this.service.basePost(url, payload);
  }
  update(payload: any) {
    const url = 'komi/limit/update';
    return this.service.basePost(url, payload);
  }
  delete(payload: any) {
    const url = 'komi/limit/delete/' + payload;
    return this.service.baseGet(url);
  }
}
