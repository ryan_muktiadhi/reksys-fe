import { TestBed } from '@angular/core/testing';

import { PrefundmgmService } from './prefundmgm.service';

describe('PrefundmgmService', () => {
  let service: PrefundmgmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrefundmgmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
