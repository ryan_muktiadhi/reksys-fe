import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class PrefundmgmService {

  constructor(private service: BackendService) { }
  getAllPrefundByTenant() {
    const url = 'komi/prefund/getAllPrefund';
    return this.service.baseGet(url);
  }
  getPrefundById(payload: any) {
    const url = 'komi/prefund/getPrefund/'+payload;
    return this.service.baseGet(url);
  }
  insertPrefundByTenant(payload: any) {
    const url = 'komi/prefund/insertPrefund';
    return this.service.basePost(url, payload);
  }
  updatePrefundByTenant(payload: any) {
    const url = 'komi/prefund/updatePrefund';
    return this.service.basePost(url, payload);
  }
  deletePrefundByTenant(payload: any) {
    const url = 'komi/prefund/deletePrefund/'+payload;
    return this.service.baseGet(url);
  }

  approvePrefund(payload: any) {
    const url = 'komi/prefund/approvePrefund';
    return this.service.basePost(url, payload);
  }

  rejectPrefund(payload: any) {
    const url = 'komi/prefund/rejectPrefund';
    return this.service.basePost(url, payload);
  }

}
