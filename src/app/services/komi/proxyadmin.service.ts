import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ProxyadminService {

  constructor(private service: BackendService) { }
  getAllProxyByTenant(payload: any) {
    const url = 'komi/Proxy/getAllProxy';
    return this.service.basePost(url, payload);
  }

  getProxyByParam(payload: any) {
    const url = 'komi/Proxy/getProxyByParam';
    return this.service.basePost(url, payload);
  }

  getProxySummary(payload: any) {
    const url = 'komi/Proxy/getProxySummary';
    return this.service.basePost(url, payload);
  }
}
