import { TestBed } from '@angular/core/testing';

import { ProxylistService } from './proxylist.service';

describe('ProxylistService', () => {
  let service: ProxylistService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProxylistService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
