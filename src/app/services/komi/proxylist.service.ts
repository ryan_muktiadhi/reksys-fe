import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class ProxylistService {
  constructor(private service: BackendService) {}

  get(payload: any) {
    const url = 'komi/channel/proxymanagement/list';
    return this.service.basePost(url, payload);
  }
}
