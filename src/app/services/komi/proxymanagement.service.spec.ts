import { TestBed } from '@angular/core/testing';

import { ProxymanagementService } from './proxymanagement.service';

describe('ProxymanagementService', () => {
  let service: ProxymanagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProxymanagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
