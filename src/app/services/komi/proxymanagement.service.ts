import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class ProxymanagementService {
  constructor(private service: BackendService) {}

  inquiry(payload: any) {
    const url = 'komi/channel/proxymanagement/inquiry';
    return this.service.basePost(url, payload);
  }

  inquiryBySecondId(payload: any) {
    const url = 'komi/channel/proxymanagement/inquiryBySecId';
    return this.service.basePost(url, payload);
  }

  otp(payload: any) {
    const url = 'komi/channel/proxymanagement/otp';
    return this.service.basePost(url, payload);
  }

  submit(payload: any) {
    const url = 'komi/channel/proxymanagement/';
    return this.service.basePost(url, payload);
  }
}
