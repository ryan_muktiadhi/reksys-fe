import { TestBed } from '@angular/core/testing';

import { ProxytypeService } from './proxytype.service';

describe('ProxytypeService', () => {
  let service: ProxytypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProxytypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
