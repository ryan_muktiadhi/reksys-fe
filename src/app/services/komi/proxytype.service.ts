import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class ProxytypeService {
  constructor(private service: BackendService) {}
  getAll() {
    const url = 'komi/proxytype/getAll';
    return this.service.baseGet(url);
  }
  getAllTByStatusActive() {
    const url = 'komi/proxytype/getByStatusActive';
    return this.service.baseGet(url);
  }
  getById(payload: any) {
    const url = 'komi/proxytype/get/' + payload;
    return this.service.baseGet(url);
  }
  insert(payload: any) {
    const url = 'komi/proxytype/insert';
    return this.service.basePost(url, payload);
  }
  update(payload: any) {
    const url = 'komi/proxytype/update';
    return this.service.basePost(url, payload);
  }
  delete(payload: any) {
    const url = 'komi/proxytype/delete/' + payload;
    return this.service.baseGet(url);
  }
}
