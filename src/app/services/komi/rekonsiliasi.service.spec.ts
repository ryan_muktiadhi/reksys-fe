import { TestBed } from '@angular/core/testing';

import { RekonsiliasiService } from './rekonsiliasi.service';

describe('RekonsiliasiService', () => {
  let service: RekonsiliasiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RekonsiliasiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
