import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EnvService } from 'src/app/env/env.service';
import { BackendService } from '../backend.service';
@Injectable({
  providedIn: 'root',
})
export class RekonsiliasiService {
  constructor(
    private service: BackendService,
    private httpClient: HttpClient,
    private environment: EnvService
  ) {}

  uploadTest(payload: any) {
    const url = 'komi/rekonsiliasi/testRecon2';
    return this.service.basePost(url, payload);
  }

  rekonsiliasi(payload: any) {
    const url = 'komi/rekonsiliasi/rekonsiliasi';
    return this.service.basePost(url, payload);
  }

  getRekonsiliasi() {
    // const url = 'komi/montrx/getAllMonTrx';
    const url = 'komi/rekonsiliasi/rekonsiliasi';
    return this.service.baseGet(url);
  }

  uploadByTenant(payload: any) {
    const url = 'komi/rekonsiliasi/uploadfile';
    // const url = 'komi/transaction/uploadfile';
    console.log('di service ', payload);

    const formData: FormData = new FormData();

    formData.append('doccsv', payload.file);
    return this.httpClient
      .post(this.environment.baseUrl + url, formData, {
        headers: {
          'Content-Type':
            'multipart/form-data; boundary=<calculated when request is sent>',
        },
      })
      .pipe(catchError(this.handleError));
    // basePost(path: string, payload: any, authorized?: boolean): Observable<any> {
    //   const url = this.environment.baseUrl + path;
    //   return this.httpClient
    //     .post(url, payload)
    //     .pipe(catchError(this.handleError));
    // }
    // return this.service.basePost(url, payload);
  }





  handleError(error: HttpErrorResponse) {
    console.log('error occured ', error);
    return throwError(error);
  }

  // getEntity() {
  //   const url = 'adm/rekon/getEntity';
  //   return this.service.get(url);
  // }

  // getEntityFormat() {
  //   const url = 'adm/rekon/getEntityFormat';
  //   return this.service.get(url);
  // }
  // getScheduleList() {
  //   const url = 'adm/rekon/getScheduleRekon';
  //   return this.service.get(url);
  // }

  // getAvailProp(entity1: any, entity2: any) {
  //   const url = `adm/rekon/getEntityProps/${entity1}/${entity2}`;
  //   return this.service.get(url);
  // }

  // getReportProp(entity1: any) {
  //   const url = `adm/rekon/getEntityProps/${entity1}`;
  //   return this.service.get(url);
  // }

  // postJob(payload: any) {
  //   const url = `adm/rekon/postJob`;
  //   return this.service.post(url, payload);
  // }

  // postFormat(payload: any) {
  //   const url = `adm/rekon/addEntityRekon`;
  //   return this.service.post(url, payload);
  // }



  // editJob(payload: any) {
  //   const url = `adm/rekon/editJob`;
  //   return this.service.post(url, payload);
  // }

  // getJobs() {
  //   const url = `adm/rekon/getJobs`;
  //   return this.service.get(url);
  // }

  // deleteRecon(id: any) {
  //   const url = `adm/rekon/deleteJob/${id}`;
  //   return this.service.post(url, null);
  // }

  // getJobsById(id: any) {
  //   const url = `adm/rekon/getJobs/${id}`;
  //   return this.service.get(url);
  // }

  // triggerJob(params: any) {
  //   const url = `adm/rekon/triggerJob/${params.jobId}/${params.file1Id}/${params.file2Id}`;
  //   return this.service.get(url);
  // }

  // getUploadHistory(id: any) {
  //   const url = `adm/rekon/getUploadHistory/${id}`;
  //   return this.service.get(url);
  // }

  // getSessions() {
  //   const url = `adm/rekon/getReconSessions`;
  //   return this.service.get(url);
  // }
  // // http://localhost:3101/sentinel-recon/report/generate/:sessionId
  // getGenerateReport(payload:any) {
  //   const url = `adm/rekon/getGenerateReportRekon`;
  //   return this.service.post(url, payload);
  // }



  // getSessionsById(id: any) {
  //   const url = `adm/rekon/getReconSessionsById/${id}`;
  //   return this.service.get(url);
  // }

  // getSessionsDetail(params: any) {
  //   const url = `adm/rekon/getSessionDetail/${params.sesId}/${params.sourceName}/${params.stat}`;
  //   return this.service.get(url);
  // }

  // downloadReport(id: any) {
  //   // http://localhost:3101/sentinel-recon/report/download-matching/:sessionId


  //   // const url = `adm/rekon/downloadReport/${id}`;
  //   // return this.service.get(url);
  //   const url = `adm/rekon/downloadReport/${id}`;
  //   //window.location.href = this.environment.apiUrl + url;
  //   window.open(this.environment.apiUrl + url, '_blank').focus();
  // }
  

  // uploadFile(payload: any, params: any) {
  //   const url = `adm/rekon/uploadfile/${params.entityName}/${params.jobId}`;

  //   console.log('di service ', payload);

  //   const formData: FormData = new FormData();

  //   formData.append('file', payload.file);
  //   return this.httpClient
  //     .post(this.environment.apiUrl + url, formData, {
  //       headers: {
  //         'Content-Type':
  //           'multipart/form-data; boundary=<calculated when request is sent>',
  //       },
  //     })
  //     .pipe(catchError(this.handleError));
  // }

  // uploadFileTemplate(payload: any, params: any) {
  //   const url = `adm/rekon/uploadfileTemplate/${params.headerRowNumber}`;

  //   console.log('di service Upload template', payload);

  //   const formData: FormData = new FormData();

  //   formData.append('file', payload.file);
  //   return this.httpClient
  //     .post(this.environment.apiUrl + url, formData, {
  //       headers: {
  //         'Content-Type':
  //           'multipart/form-data; boundary=<calculated when request is sent>',
  //       },
  //     })
  //     .pipe(catchError(this.handleError));
  // }
  getEntity() {
    const url = 'adm/rekon/getEntity';
    return this.service.baseGet(url);
  }

  getEntityFormat() {
    const url = 'adm/rekon/getEntityFormat';
    return this.service.baseGet(url);
  }
  getScheduleList() {
    const url = 'adm/rekon/getScheduleRekon';
    return this.service.baseGet(url);
  }

  getAvailProp(entity1: any, entity2: any) {
    const url = `adm/rekon/getEntityProps/${entity1}/${entity2}`;
    return this.service.baseGet(url);
  }

  getReportProp(entity1: any) {
    const url = `adm/rekon/getEntityProps/${entity1}`;
    return this.service.baseGet(url);
  }

  postJob(payload: any) {
    const url = `adm/rekon/postJob`;
    return this.service.basePost(url, payload);
  }

  postFormat(payload: any) {
    const url = `adm/rekon/addEntityRekon`;
    return this.service.basePost(url, payload);
  }



  editJob(payload: any) {
    const url = `adm/rekon/editJob`;
    return this.service.basePost(url, payload);
  }

  getJobs() {
    const url = `adm/rekon/getJobs`;
    return this.service.baseGet(url);
  }

  deleteRecon(id: any) {
    const url = `adm/rekon/deleteJob/${id}`;
    return this.service.basePost(url, null);
  }
  deleteFormat(id: any) {
    const url = `adm/rekon/deleteFormat/${id}`;
    return this.service.basePost(url, null);
  }

  getJobsById(id: any) {
    const url = `adm/rekon/getJobs/${id}`;
    return this.service.baseGet(url);
  }

  triggerJob(params: any) {
    const url = `adm/rekon/triggerJob/${params.jobId}/${params.file1Id}/${params.file2Id}`;
    return this.service.baseGet(url);
  }

  getUploadHistory(id: any) {
    const url = `adm/rekon/getUploadHistory/${id}`;
    return this.service.baseGet(url);
  }

  getSessions() {
    const url = `adm/rekon/getReconSessions`;
    return this.service.baseGet(url);
  }
  // http://localhost:3101/sentinel-recon/report/generate/:sessionId
  getGenerateReport(payload:any) {
    const url = `adm/rekon/getGenerateReportRekon`;
    return this.service.basePost(url, payload);
  }



  getSessionsById(id: any) {
    const url = `adm/rekon/getReconSessionsById/${id}`;
    return this.service.baseGet(url);
  }

  getSessionsDetail(params: any) {
    const url = `adm/rekon/getSessionDetail/${params.sesId}/${params.sourceName}/${params.stat}`;
    return this.service.baseGet(url);
  }

  downloadReport(id: any) {
    // http://localhost:3101/sentinel-recon/report/download-matching/:sessionId


    // const url = `adm/rekon/downloadReport/${id}`;
    // return this.service.get(url);
    const url = `adm/rekon/downloadReport/${id}`;
    //window.location.href = this.environment.apiUrl + url;
    window.open(this.environment.apiUrl + url, '_blank').focus();
  }
  

  uploadFile(payload: any, params: any) {
    const url = `adm/rekon/uploadfile/${params.entityName}/${params.jobId}`;

    console.log('di service ', payload);

    const formData: FormData = new FormData();

    formData.append('file', payload.file);
    return this.httpClient
      .post(this.environment.baseUrl + url, formData, {
        headers: {
          'Content-Type':
            'multipart/form-data; boundary=<calculated when request is sent>',
        },
      })
      .pipe(catchError(this.handleError));
  }

  uploadFileTemplate(payload: any, params: any) {
    const url = `adm/rekon/uploadfileTemplate/${params.headerRowNumber}`;

    console.log('di service Upload template', payload);

    const formData: FormData = new FormData();

    formData.append('file', payload.file);
    return this.httpClient
      .post(this.environment.baseUrl + url, formData, {
        headers: {
          'Content-Type':
            'multipart/form-data; boundary=<calculated when request is sent>',
        },
      })
      .pipe(catchError(this.handleError));
  }

}
