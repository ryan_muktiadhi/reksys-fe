import { TestBed } from '@angular/core/testing';

import { ResidentstatusService } from './residentstatus.service';

describe('ResidentstatusService', () => {
  let service: ResidentstatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResidentstatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
