import { TestBed } from '@angular/core/testing';

import { SmtpconfigService } from './smtpconfig.service';

describe('SmtpconfigService', () => {
  let service: SmtpconfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmtpconfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
