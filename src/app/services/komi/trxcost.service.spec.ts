import { TestBed } from '@angular/core/testing';

import { TrxcostService } from './trxcost.service';

describe('TrxcostService', () => {
  let service: TrxcostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrxcostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
