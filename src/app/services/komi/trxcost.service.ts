import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class TrxcostService {

  constructor(private service: BackendService) { }
  getAllTrxByTenant() {
    const url = 'komi/trxcost/getAlltrx';
    return this.service.baseGet(url);
  }
  getTrxById(payload: any) {
    const url = 'komi/trxcost/getTrx/'+payload;
    return this.service.baseGet(url);
  }
  insertTrxByTenant(payload: any) {
    const url = 'komi/trxcost/insertTrx';
    return this.service.basePost(url, payload);
  }
  updateTrxByTenant(payload: any) {
    const url = 'komi/trxcost/updateTrx';
    return this.service.basePost(url, payload);
  }
  deleteTrxByTenant(payload: any) {
    const url = 'komi/trxcost/deleteTrx/'+payload;
    return this.service.baseGet(url);
  }
}
