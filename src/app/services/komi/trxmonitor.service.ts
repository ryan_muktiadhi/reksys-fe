import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class TrxmonitorService {
  constructor(private service: BackendService) {}
  getAllMonToday() {
    // const url = 'komi/montrx/getAllMonTrx';
    const url = 'komi/transaction/getAllTransaction';
    return this.service.baseGet(url);
  }
  postParam(payload: any) {
    // const url = 'komi/montrx/getbyparam';
    const url = 'komi/transaction/getTransactionByParam';
    return this.service.basePost(url, payload);
  }
  getTransHk(payload: any) {
    const url = 'komi/montrx/gettranshkbyparam';
    return this.service.basePost(url, payload);
  }
}
