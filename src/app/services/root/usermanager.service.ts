import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class UsermanagerService {
  constructor(private service: BackendService) {}

  retriveProfile(id: string) {
    const url = 'adm/profile/' + id;
    return this.service.get(url);
  }

  putPassword(payload: any) {
    const url = 'adm/profile/changepassword';
    return this.service.put(url, payload);
  }

  insertByAdmin(payload: any) {
    const url = 'adm/umanager/insertbyadmin';
    return this.service.post(url, payload);
  }

  insertBySuper(payload: any) {
    const url = 'adm/umanager/insertbysuper';
    return this.service.post(url, payload);
  }

  updatebySuper(payload: any) {
    const url = 'adm/umanager/updatebysupperuser';
    return this.service.post(url, payload);
  }
  updatebyAdmin(payload: any) {
    const url = 'adm/umanager/updatebyadmin';
    return this.service.post(url, payload);
  }
  updatebyAdminActive(payload: any) {
    const url = 'adm/umanager/updatebyadminactive';
    return this.service.post(url, payload);
  }
  reject(payload: any) {
    const url = 'adm/umanager/reject';
    return this.service.post(url, payload);
  }
  // retriveUsers() {
  //   const url = 'adm/umanager/retriveusers';
  //   return this.service.get(url);
  // }
  retriveUsers() {
    const url = 'adm/umanager/retriveusersadmin';
    return this.service.get(url);
  }

  retriveUsersById(id: string) {
    const url = 'adm/umanager/retriveusersbyid/' + id;
    return this.service.get(url);
  }

  deleteUser(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/umanager/deletebysuper/${payload.user.id} `;
    return this.service.get(url);
  }
  deleteUserByAdmin(payload: any) {
    // console.log("HAPUS "+JSON.stringify(payload));
    const url = `adm/umanager/deletebyadmin/${payload.user.id} `;
    return this.service.get(url);
  }
}
