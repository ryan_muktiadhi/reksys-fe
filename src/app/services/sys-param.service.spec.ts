import { TestBed } from '@angular/core/testing';

import { SysParamService } from './sys-param.service';

describe('SysParamService', () => {
  let service: SysParamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SysParamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
