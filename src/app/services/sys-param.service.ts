import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root',
})
export class SysParamService {
  constructor(private service: BackendService) {}

  retriveProfile(id: string) {
    const url = 'adm/profile/' + id;
    return this.service.get(url);
  }

  getAllSysParam() {
    const url = 'adm/sysparam/getall';
    return this.service.get(url);
  }

  getSysParamById(id: string) {
    const url = 'adm/sysparam/getById/' + id;
    return this.service.get(url);
  }

  updateSysParam(payload: any) {
    const url = 'adm/sysparam/update';
    return this.service.post(url, payload);
  }
  updateSysParamActive(payload: any) {
    const url = 'adm/sysparam/updatebyadminactive';
    return this.service.post(url, payload);
  }
  reject(payload: any) {
    const url = 'adm/sysparam/reject';
    return this.service.post(url, payload);
  }
}
