import { TestBed } from '@angular/core/testing';

import { AclmenucheckerService } from './aclmenuchecker.service';

describe('AclmenucheckerService', () => {
  let service: AclmenucheckerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AclmenucheckerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
